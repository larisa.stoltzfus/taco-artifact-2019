############# PLOTTING
library(data.table)
library(plyr)
library(reshape2)
library(ggplot2)

dataFile <- "~/workspace/taco_kernels/data/taco_paper_data/denseleggy/mss_compare_denseleggy.csv"

frame <- read.csv(dataFile,header=FALSE,col.names=c("benchmark","type","size","zsize","platform","runtime"),sep=",",stringsAsFactors = FALSE)

# rename all platforms
frame$platform[like(frame$platform,"spa")] <- "Nvidia"
frame$platform[like(frame$platform,"fuji")] <- "AMD"
frame$platform[like(frame$platform,"mali")] <- "ARM"

frame[which(frame$benchmark=="j3d13pt"),]$benchmark <- "13-point (leggy[2])"
frame[which(frame$benchmark=="poisson"),]$benchmark <- "19-point (dense)"
frame[which(frame$benchmark=="j3d21pt"),]$benchmark <- "19-point (leggy[3])"
frame[which(frame$benchmark=="j3d27pt"),]$benchmark <- "27-point (dense)"

# collect all values in columns { platform, benchmark, size }
platforms <- sapply(frame, function(x) unique(x))$platform
benchmarks <- sapply(frame, function(x) unique(x))$benchmark
sizes <- sapply(frame, function(x) unique(x))$size
zsizes <- sapply(frame, function(x) unique(x))$zsize
types <- sapply(frame, function(x) unique(x))$type

# screw it, let's make a new data frame for this
columnsData <- c("platform", "benchmark", "size","zsize", "speedup") 
data <- data.frame(matrix(ncol = length(columnsData)))
colnames(data) <- columnsData

idx <- 1
    for( p in platforms)
    {
        for( b in benchmarks)
        {
            for( s in sizes)
            {
                for( z in zsizes)
                {
                
                  sub <- subset(frame, platform==p & benchmark==b & size==s & zsize==z )
                  sub <- as.data.frame(sub) # may not be necessary?
                  num <- sub[which( sub$type=="original"),]$runtime[1]
                  denom <- sub[which( sub$type=="MSS"),]$runtime[1] # need a "[1]" here because there are multiple copies of the lift! should really only be one
                  speedup <- num / denom 
                  #cat(paste(num," ",denom," ",speedup))
                  #cat("\n")
    
                  if(!is.na(speedup))
                  {
                      data[idx,] <- c( p, b, s, z, speedup )
                      idx <- idx + 1
                  }
                  else
                  {
                      cat(paste(b," ",p," ",s," ",z," ",num," ",denom," ",speedup))
                      cat("\n")
                  }
                }
            }
        }
    }

data$speedup <- as.numeric(data$speedup)
data$size <- as.numeric(data$size)

data$benchmark <- as.factor(data$benchmark)
data$platform <- as.factor(data$platform)

data$benchmark= factor(data$benchmark, levels=levels(data$benchmark)[c(4,2,1,3,5)])
data$platform = factor(data$platform, levels=levels(data$platform)[c(3,1,2)])

# cross cut:
dataCut256 <- data[data$size==256,]

dfDense256 <- aggregate(speedup ~ benchmark + platform + zsize, dataCut256, FUN=max)
dfDense256$platform <- as.factor(dfDense256$platform)
dfDense256$speedup <- as.numeric(dfDense256$speedup)

#TODO :: Figure out how to reorder this !!!
#dfDense256$zsize <- dfDense256[as.numeric(as.character(dfDense256$zsize)),]
dfDense256[3] <- sapply(dfDense256[3],as.numeric)

zsizes <- sort(sapply(dfDense256, function(x) unique(x))$zsize)

leggydense_plot256 <- ggplot(data=dfDense256, aes(x=as.factor(zsize), y=speedup, colour=benchmark, group=benchmark)) +
    geom_line(aes(linetype=benchmark))+
    facet_wrap(~platform)+
    coord_cartesian(ylim=c(0,2))+
#    scale_x_discrete()+
#    scale_x_continuous(breaks=zsizes,labels=zsizes) +
    #geom_bar(stat="identity", position="dodge", colour="black", width=0.8) +
    facet_wrap(~ platform) +
    geom_hline(yintercept=1) +
    ylab("2.5D Tiling Speedup") +
    xlab("Z-Size") +
    xlab(NULL) +
    scale_color_manual(values=c("#a6cee3","#1f78b4","#b2df8a","#33a02c","#e31a1c"))+
    scale_linetype_manual(values=c("longdash", "dashed","dotdash","dotted","solid"))+
    theme_bw() +
    theme(
        legend.position="right",
        strip.background=element_rect(fill=NA),
	axis.text.x=element_text(size=8, angle = 45, hjust=1),
        axis.title.y=element_text(size=10,face="bold")
        )

ggsave("~/workspace/papers/taco/15997511mvqxpdpqchgn/figures/twopointfive_compare_leggydense256.pdf", width=10, height=2.35, device = cairo_pdf)


# cross cut:
dataCut512 <- data[data$size==512,]

dfDense512 <- aggregate(speedup ~ benchmark + platform + zsize, dataCut512, FUN=max)
dfDense512$platform <- as.factor(dfDense512$platform)
dfDense512$speedup <- as.numeric(dfDense512$speedup)


#TODO :: Figure out how to reorder this !!!
#dfDense512$zsize <- dfDense512[as.numeric(as.character(dfDense512$zsize)),]
dfDense512[3] <- sapply(dfDense512[3],as.numeric)

zsizes <- sort(sapply(dfDense512, function(x) unique(x))$zsize)

leggydense_plot512 <- ggplot(data=dfDense512, aes(x=as.factor(zsize), y=speedup, colour=benchmark, group=benchmark)) +
    geom_line()+
    facet_wrap(~platform)+
    coord_cartesian(ylim=c(0,2))+
#    scale_x_discrete()+
#    scale_x_continuous(breaks=zsizes,labels=zsizes) +
    #geom_bar(stat="identity", position="dodge", colour="black", width=0.8) +
    facet_wrap(~ platform) +
    geom_hline(yintercept=1) +
    ylab("2.5D Tiling Speedup") +
    xlab("Z-Size") +
    xlab(NULL) +
    scale_color_manual(values=c("#a6cee3","#1f78b4","#b2df8a","#33a02c","#e31a1c"))+
    scale_linetype_manual(values=c("longdash", "dashed","dotdash","dotted","solid"))+
    theme_bw() +
    theme(
        legend.position="right",
        strip.background=element_rect(fill=NA),
	axis.text.x=element_text(size=8, angle = 45, hjust=1),
        axis.title.y=element_text(size=10,face="bold")
        )

ggsave("~/workspace/papers/taco/15997511mvqxpdpqchgn/figures/twopointfive_compare_leggydense512.pdf", width=10, height=2.35, device = cairo_pdf)





