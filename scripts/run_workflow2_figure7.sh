#!/bin/bash
if [ $# -eq 2 ] && ([ "$1" == "nvidia" ] || [ "$1" == "amd" ] || [ "$1" == "arm" ]) &&
	 ([ "$2" == "hotspot" ] || [ "$2" == "hotspot3D" ] || [ "$2" == "srad1" ] ||
	 [ "$2" == "srad2" ] || [ "$2" == "stencil2d" ])
then 
				# adjust to folder structure
				if [ "$1" == "nvidia" ]			
				then
								ARCH="kepler"
				fi
				if [ "$1" == "amd" ]			
				then
								ARCH="tahiti"
				fi
				if [ "$1" == "arm" ]			
				then
								ARCH="mali"
				fi
				BENCH=$2

############################################
# 2. Run Lift implementations
############################################
				# tune generated kernel for this specific benchmark
				CL=Cl
				pushd $ROOTDIR/benchmarks/figure7/workflow2/$BENCH/$ARCH/lift/$BENCH$CL > /dev/null
				TUNE=$PWD
				popd > /dev/null
				pushd $ATF/build
                # clean
                rm *.cl
                rm *.csv
                rm *.log
				do_you_even_tune.sh $TUNE
				popd > /dev/null

				# analyze tuning results
				pushd $TUNE
				analyze_tuning.sh

                # store runtime files
				mkdir $ROOTDIR/output_data/tuned > /dev/null
				mkdir $ROOTDIR/output_data/tuned/$1 > /dev/null
				mkdir $ROOTDIR/output_data/tuned/$1/lift > /dev/null
				cat summary.txt | tail -1 | awk '{ print $3 }' > $ROOTDIR/output_data/tuned/$1/lift/$BENCH.out

                ############################################
                # 1. Run reference implementations
                ############################################
                # run references if not already happened
                if ! [ -d "$ROOTDIR/output_data/tuned/$1/reference" ]; then
                    run_references_workflow2_figure7.sh $1
                fi

				# plot tuned benchmarks
				pushd $ROOTDIR/scripts/r_scripts > /dev/null
				Rscript plotTunedHandwritten2.r
                
                echo "done! workflow2-figure7.sh has finished"
else
    		echo "Wrong arguments supplied"
				echo "Usage: ./tune_benchmark [architecture] [benchmark]"
				echo ""
				echo "Which architecture? (nvidia | amd | arm)"
				echo "Which benchmark? (hotspot | hotspot3D | srad1 | srad2 | stencil2d)"
    		exit -1
fi
