#!/bin/bash
if [ $# -eq 3 ] && ([ "$1" == "nvidia" ] || [ "$1" == "amd" ]) &&
	 ([ "$2" == "gaussian" ] || [ "$2" == "grad2d" ] || [ "$2" == "heat3d" ] ||
	 [ "$2" == "j2d5pt" ] || [ "$2" == "j2d9pt" ] || [ "$2" == "j3d13pt" ] ||
	 [ "$2" == "j3d7pt" ] || [ "$2" == "poisson3d" ]) &&
	 ([ "$3" == "small" ] || [ "$3" == "large" ]) 
then 
				# adjust to folder structure
				if [ "$1" == "nvidia" ]			
				then
								ARCH="kepler"
				fi
				if [ "$1" == "amd" ]			
				then
								ARCH="tahiti"
				fi
				BENCH=$2
				SIZE=$3
				if [ "$SIZE" == "large" ] 
				then
								SIZE=big # we used 'big' in folder structure
				fi
                ############################################
                # 1. Run reference implementations
                ############################################
				# tune ppcg now
				pushd $ROOTDIR/benchmarks/figure8/workflow2/$BENCH/$SIZE > /dev/null
				tune_this_ppcg_bench.sh
				mkdir $ARCH/tunedRef
				mv cost.csv meta.csv tune.out $ARCH/tunedRef
				cp $BENCH.c $ARCH/tunedRef
				pushd $ARCH/tunedRef > /dev/null
                # run best ppcg more often
				run_best_ppcg_multiple_times.sh
				popd > /dev/null
				popd > /dev/null
else
    		echo "Wrong arguments supplied"
				echo "Usage: ./tune_benchmark [architecture] [benchmark] [inputsize]"
				echo ""
				echo "Which architecture? (nvidia | amd | arm)"
				echo "Which benchmark? (gaussian | grad2d | heat3d | j2d5pt | j2d9pt | j3d13pt | j3d7pt | poisson3d)"
				echo "Which inputsize? (small | large)"
    		exit -1
fi
