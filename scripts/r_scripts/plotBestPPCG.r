library(data.table)
library(plyr)
library(reshape2)
library(ggplot2)
library("RColorBrewer") 

# we'll use this function to extract info from our data path
# thanks, stack overflow!
split_path <- function(x) if (dirname(x)==x) x else c(basename(x),split_path(dirname(x))) 

# directory with data
dir <- "../../benchmarks/figure8/workflow1/"
ext <- "runtime"

# directory to output plot(s)
plotDir <- "../../plots"

# pull out all relevant files
dataFiles <- system(paste("find ",dir,"/*"," | grep ",ext,"$"," | grep -E \"lift|tunedRef\" ",sep=""),intern=TRUE)

# create initial data frame
columns <- c("platform", "version", "benchmark", "size", "runtime") # version = { lift, reference }
                                                                    # size = { big, small }
frame <- data.frame(matrix(ncol = length(columns)))
colnames(frame) <- columns

# loop over files 
for (i in 1:length(dataFiles))
{
    # save whole path, then will carve out the info we need
    wholePathName <- dataFiles[i]

    # get rid of working directory in front of string 
    workingPathName <- gsub(dir,"",wholePathName) 

    pathArray <- split_path(workingPathName)
    #cat(pathArray)
    #cat("\n")

    platform <- pathArray[length(pathArray)-3] 
    size <- pathArray[length(pathArray)-2] 
    benchmark <- pathArray[length(pathArray)-1] 
    version <- pathArray[1] 
    version <- strsplit(version,"[.]")[[1]][1] # throw away the extension in this really convoluted way

    medValue <- 0
    # now open the file and get the results
    if (!file.size(wholePathName) == 0) 
    {
        values <- read.csv(wholePathName, header=FALSE)
        medValue <- median(values[[1]]) 
    }
   
    # add row to the dataframe 
    frame[i,] <- c(platform, version, benchmark, size, medValue)
}

# rename platforms (for now!)
frame$platform[like(frame$platform, "kepler")] <- "Nvidia"
frame$platform[like(frame$platform, "tahiti")] <- "AMD"
frame$platform[like(frame$platform, "mali")] <- "ARM"

# rename big -> large
frame$size[like(frame$size, "big")] <- "large"

# add new column for speedup
frame$speedup <- 0

# reset type of runtime column
frame$runtime <- as.numeric(frame$runtime)

# collect all values in columns { platform, benchmark, size }
platforms <- sapply(frame, function(x) unique(x))$platform
benchmarks <- sapply(frame, function(x) unique(x))$benchmark
size <- sapply(frame, function(x) unique(x))$size

# screw it, let's make a new data frame for this
columnsData <- c("platform", "benchmark", "size", "speedup") 
data <- data.frame(matrix(ncol = length(columnsData)))
colnames(data) <- columnsData

idx <- 1
for( p in platforms)
{
    for( b in benchmarks)
    {
        for( s in size)
        {
            
            sub <- subset(frame, platform==p & benchmark==b & size==s)
            sub <- as.data.frame(sub) # may not be necessary?
            num <- sub[which( sub$version=="ref"),]$runtime[1]
            denom <- sub[which( sub$version=="lift"),]$runtime[1] # need a "[1]" here because there are multiple copies of the lift! should really only be one
            speedup <- num / denom 
#            cat(paste(num," ",denom," ",speedup))
#            cat("\n")

            if(!is.na(speedup))
            {
                data[idx,] <- c( p, b, s, speedup )
                idx <- idx + 1
            }
        }
    }
}


data <- aggregate(speedup ~ benchmark + platform + size, data, FUN=max)
data$platform <- as.factor(data$platform)

data$platform = factor(data$platform, levels=levels(data$platform)[c(3,1,2)])

data$size = factor(data$size, levels=c("small", "large"))

#ann_text <- data.frame(benchmark = "Jacobi3D7pt", speedup=6, size="large", lab="12.2", platform=factor("AMD",levels = c("Nvidia","AMD","ARM")))

data$speedup <- as.numeric(data$speedup)

plot <- ggplot(data, aes(x=benchmark, y=speedup, fill=platform, alpha=size)) +
    geom_bar(stat="identity", position="dodge", colour="black", width=0.8) +
#    coord_cartesian(ylim=c(0,10))+
    #scale_fill_brewer("Input size", palette="YlOrRd") +
    #scale_fill_manual(values = brewer.pal(name="Set1", n=3), guide="none") +
    scale_fill_manual(values=c("#76B900", "#ED1C24","#0091BD")) +
    scale_alpha_manual(values = c(0.6, 1.0)) +
    facet_wrap(~ platform) +
    # scale_y_log10(breaks=c(1,2,3,4,5,10,20)) +
    geom_hline(yintercept=1) +
    ylab("Speedup over PPCG") +
    xlab(NULL) +
    theme_bw() +
    theme(
        legend.position="right",
        strip.background=element_rect(fill=NA),
	axis.text.x=element_text(size=8, angle = 45, hjust=1),
        axis.title.y=element_text(size=10,face="bold")
        )

ggsave(paste(plotDir,"/","workflow1-figure8.pdf",sep=""), width=10, height=2.5)
