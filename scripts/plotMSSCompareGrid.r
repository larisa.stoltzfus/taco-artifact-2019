############# PLOTTING
library(data.table)
library(plyr)
library(reshape2)
library(ggplot2)

#dataFile <- "~/workspace/taco_kernels/data/grid_data/benchmark_comparison_grid_latest.csv"


dataFile <- "~/workspace/taco_kernels/data/taco_paper_data/benchmark_comparison/mss_compare_benchmark.csv"
#dataFile <- "~/workspace/taco_kernels/data/acoustic_test.csv"

frame <- read.csv(dataFile,header=FALSE,col.names=c("benchmark","type","size","zsize","platform","runtime"),sep=",",stringsAsFactors = FALSE)

# rename all platforms
frame$platform[like(frame$platform,"spa")] <- "Nvidia"
frame$platform[like(frame$platform,"fuji")] <- "AMD"
frame$platform[like(frame$platform,"mali")] <- "ARM"

frame <-frame[!(frame$benchmark=="j3d27pt"),]

frame[which(frame$benchmark=="j3d7pt"),]$benchmark <- "Jacobi3D7pt"
frame[which(frame$benchmark=="j3d13pt"),]$benchmark <- "Jacobi3D13pt"
frame[which(frame$benchmark=="heat3d"),]$benchmark <- "Heat"
frame[which(frame$benchmark=="acoustic"),]$benchmark <- "Acoustic"
frame[which(frame$benchmark=="hotspot3D"),]$benchmark <- "Hotspot3D"
frame[which(frame$benchmark=="poisson"),]$benchmark <- "Poisson"


# collect all values in columns { platform, benchmark, size }
platforms <- sapply(frame, function(x) unique(x))$platform
benchmarks <- sapply(frame, function(x) unique(x))$benchmark
sizes <- sapply(frame, function(x) unique(x))$size
zsizes <- sapply(frame, function(x) unique(x))$zsize
types <- sapply(frame, function(x) unique(x))$type

# screw it, let's make a new data frame for this
columnsData <- c("platform", "benchmark", "size","zsize", "speedup") 
data <- data.frame(matrix(ncol = length(columnsData)))
colnames(data) <- columnsData

idx <- 1
    for( p in platforms)
    {
        for( b in benchmarks)
        {
            for( s in sizes)
            {
                for( z in zsizes)
                {
                
                  sub <- subset(frame, platform==p & benchmark==b & size==s & zsize==z )
                  sub <- as.data.frame(sub) # may not be necessary?
                  num <- sub[which( sub$type=="original"),]$runtime[1]
                  denom <- sub[which( sub$type=="MSS"),]$runtime[1] # need a "[1]" here because there are multiple copies of the lift! should really only be one
                  speedup <- num / denom 
                  #cat(paste(num," ",denom," ",speedup))
                  #cat("\n")
    
                  if(!is.na(speedup))
                  {
                      data[idx,] <- c( p, b, s, z, speedup )
                      idx <- idx + 1
                  }
                  else
                  {
                      cat(paste(b," ",p," ",s," ",z," ",num," ",denom," ",speedup))
                      cat("\n")
                  }
                }
            }
        }
    }

data$speedup <- as.numeric(data$speedup)
#data$size <- as.numeric(data$size)

melted <- aggregate(speedup ~ benchmark + platform + zsize + size , data, FUN=max)
melted$platform <- as.factor(melted$platform)
melted$size <- as.factor(melted$size)
melted$zsize <- as.factor(melted$zsize)
melted$speedup <- as.numeric(melted$speedup)
melted$benchmark <- as.factor(melted$benchmark)

melted$platform = factor(melted$platform, levels=levels(melted$platform)[c(3,1,2)])
melted$size = factor(melted$size, levels=levels(melted$size)[c(3,2,1,4)])
melted$benchmark = factor(melted$benchmark, levels=levels(melted$benchmark)[c(1,3,2,4,5,6)])

#TODO :: Figure out how to reorder this !!!
#melted$zsize <- melted[as.numeric(as.character(melted$zsize)),]
#melted[3] <- sapply(melted[3],as.numeric)

zsizes <- sort(sapply(melted, function(x) unique(x))$zsize)

#size_plot <- ggplot(data=dfZSizeHalf, aes(x=as.factor(size), y=speedup, colour=benchmark, group=benchmark)) +
#    geom_line()+
# facet_wrap(~platform)

zsize_plot <- ggplot(data=melted, mapping= aes(x=as.factor(zsize), y=speedup,  color=size)) + #, colour=benchmark, group=benchmark)) +
#    facet_wrap(~platform)
    facet_grid(platform~benchmark, scales="free") +
    coord_cartesian(ylim=c(0.27,1.7))+
    geom_line(aes(group=size))+
    scale_color_manual(values=c("#ca0020","#f4a582","#92c5de","#0571b0"),name="X-Y Size")+
    #geom_line(aes(group=size, colour=))+
    scale_x_discrete(name="Z-Size", limits=c("8","16","32","64","128","256","512"))+
    ylab("2.5D Tiling Speedup") +
    geom_hline(yintercept=1, linetype="dashed") +
   # scale_fill_manual(values=c("#76B900", "#ED1C24","#0091BD"),guide="none")+ 
    theme_bw() +
    theme(
        legend.position="right",
        strip.background=element_rect(fill=NA),
#         panel.grid.major = element_blank(),
	axis.text.x=element_text(size=8, angle = 45, hjust=1),
        axis.title.y=element_text(size=10,face="bold")
        )


melted$zsize = factor(melted$zsize, levels=levels(melted$zsize)[rev(c(7,2,4,6,1,3,5))])

ggsave("~/workspace/papers/taco/15997511mvqxpdpqchgn/figures/twopointfive_compare_grid_byzsize.pdf", width=7, height=3, device = cairo_pdf)

size_plot <- ggplot(data=melted, mapping= aes(x=as.factor(size), y=speedup,  color=zsize)) + #, colour=benchmark, group=benchmark)) +
    facet_grid(platform~benchmark, scales="free") +
    coord_cartesian(ylim=c(0,2))+
    geom_line(aes(group=zsize))+
    #geom_line(aes(group=size, colour=))+
    scale_x_discrete(name="Domain Size", limits=c("64","128","256","512"))+
    scale_color_manual(values=c("#b2182b","#ef8a62","#fddbc7","#f7f7f7","#d1e5f0","#67a9cf","#2166ac"))+
    ylab("2.5D Tiling Speedup") +
    geom_hline(yintercept=1, linetype="dashed") +
   # scale_fill_manual(values=c("#76B900", "#ED1C24","#0091BD"),guide="none")+ 
    theme_bw() +
    theme(
        legend.position="right",
        strip.background=element_rect(fill=NA),
#         panel.grid.major = element_blank(),
	axis.text.x=element_text(size=8, angle = 45, hjust=1),
        axis.title.y=element_text(size=10,face="bold")
        )

#    geom_line()+
#    ylab("2.5D Tiling Speedup") +
#    xlab(NULL) +

#zsize_plot <- ggplot(melted, aes(x=benchmark, y=speedup, fill=platform, alpha=zsize)) +
#    geom_bar(stat="identity", position="dodge", colour="black", width=0.8) +
#    scale_fill_manual(values=c("#76B900", "#ED1C24","#0091BD"),guide="none") +
#    scale_alpha_manual(values = c(.15,.3,.45,.6,.75,1.0),labels=c("64","128","256"),name="z-size") +
#    facet_wrap(~ platform) +
#    geom_hline(yintercept=1) +
#    ylab("2.5D Tiling Speedup") +
#    xlab(NULL) +
#    theme_bw() +
#    theme(
#        legend.position="right",
#        strip.background=element_rect(fill=NA),
#	axis.text.x=element_text(size=8, angle = 45, hjust=1),
#        axis.title.y=element_text(size=10,face="bold")
#        )

#ggsave("~/workspace/papers/taco/15997511mvqxpdpqchgn/figures/twopointfive_compare_grid_bysize.pdf", width=10, height=10, device = cairo_pdf)



