#!/bin/bash
### README ###################################################################
# This script reproduces the results of Figure 8 for a given GPU architecture.
# The plot will be generated in $ROOTDIR/plots/figure8-workflow1.pdf
##############################################################################
# check input
if [ $# -ne 1 ]
then
    echo "No arguments supplied - which architecture? (nvidia | amd | arm)?"
    exit -1
fi
if ! ([ "$1" == "nvidia" ] || [ "$1" == "amd" ] || [ "$1" == "arm" ])
then 
    echo "Wrong architecture! valid: (nvidia | amd | arm)?"
    exit -1
fi

# adjust input to folder structure
if [ "$1" == "nvidia" ]			
then
				ARCH="kepler"
fi
if [ "$1" == "amd" ]			
then
				ARCH="tahiti"
fi
if [ "$1" == "arm" ]			
then
				ARCH="mali"
fi

cd $ROOTDIR/benchmarks/figure8/workflow1
############################################
# 1. Run reference implementations
############################################
echo "Running PPCG references"
run_all_best_ppcg_kernels.sh $ARCH

############################################
# 2. Run Lift implementations
############################################
echo "Running Lift generated kernels"
rerun_all_lift_ppcg_kernels.sh $ARCH

############################################
# 3. Plot results
############################################
cd $ROOTDIR/scripts/r_scripts
Rscript plotBestPPCG.r

echo "done! workflow1-figure8.sh has finished"
