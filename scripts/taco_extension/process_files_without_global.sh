#!/bin/bash

DIR=$1


function process_file {

        FILE=$1
        LENGTH="$(cat $FILE | wc -l)"
        # throw away csv which only contain header -> invalid runs
        if [ $LENGTH -gt 1 ] 
        then
                POSITION="$(cat $FILE | grep cost | sed -e 's/;/ /g' | wc -w)"
                echo -n $FILE | sed -e 's/.cost.csv//g'
                echo -n " -> "
                BEST="$(cat $FILE | \
                        sort -n -r --field-separator=';' -k $POSITION | \
                        tail -n 2 | head -1)"
                echo -n $BEST | sed -e 's/;/ /g' | awk -v pos=$POSITION '{printf $pos,"\t"}'
                echo -n ' ('
                echo -n $BEST
                echo -e ")"
        fi
}

export -f process_file

find $DIR -type f | xargs -n 1 bash -c 'process_file "$@"' _ | awk -F'/' '{ print $NF}' | awk -F'-|.cl| |;' '{ print $1","$2","$3","$5","$7","$10 }' 
