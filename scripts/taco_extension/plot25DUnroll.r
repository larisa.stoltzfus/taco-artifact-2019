############# PLOTTING
library(data.table)
library(plyr)
library(reshape2)
library(ggplot2)


#dataFile <- "~/workspace/taco_kernels/data/unroll.txt"
dataFile <- "~/workspace/taco_kernels/data/unroll_latest2.txt"

frame <- read.csv(dataFile,header=FALSE,col.names=c("benchmark","type","size","zsize","platform","runtime"),sep=",",stringsAsFactors = FALSE)

# rename all platforms
frame$platform[like(frame$platform,"spa")] <- "Nvidia"
frame$platform[like(frame$platform,"fuji")] <- "AMD"
frame$platform[like(frame$platform,"mali")] <- "ARM"

frame <-frame[!(frame$benchmark=="j3d21pt"),]

frame[which(frame$benchmark=="j3d7pt"),]$benchmark <- "Jacobi3D7pt"
frame[which(frame$benchmark=="j3d27pt"),]$benchmark <- "Jacobi3D27pt"
frame[which(frame$benchmark=="j3d13pt"),]$benchmark <- "Jacobi3D13pt"
frame[which(frame$benchmark=="heat3d"),]$benchmark <- "Heat"
frame[which(frame$benchmark=="acoustic"),]$benchmark <- "Acoustic"
frame[which(frame$benchmark=="hotspot3D"),]$benchmark <- "Hotspot3D"
frame[which(frame$benchmark=="poisson"),]$benchmark <- "Poisson"


# collect all values in columns { platform, benchmark, size }
platforms <- sapply(frame, function(x) unique(x))$platform
benchmarks <- sapply(frame, function(x) unique(x))$benchmark
sizes <- sapply(frame, function(x) unique(x))$size
zsizes <- sapply(frame, function(x) unique(x))$zsize
types <- sapply(frame, function(x) unique(x))$type

# screw it, let's make a new data frame for this
columnsData <- c("platform", "benchmark", "size","zsize", "speedup") 
data <- data.frame(matrix(ncol = length(columnsData)))
colnames(data) <- columnsData

idx <- 1
    for( p in platforms)
    {
        for( b in benchmarks)
        {
            for( s in sizes)
            {
                for( z in zsizes)
                {
                
                  sub <- subset(frame, platform==p & benchmark==b & size==s & zsize==z )
                  sub <- as.data.frame(sub) # may not be necessary?
                  denom <- sub[which( sub$type=="MSS"),]$runtime[1]
                  num <- sub[which( sub$type=="MSS_notunroll"),]$runtime[1] # need a "[1]" here because there are multiple copies of the lift! should really only be one
                  speedup <- num / denom 
                  #cat(paste(num," ",denom," ",speedup))
                  #cat("\n")
    
                  if(!is.na(speedup))
                  {
                      data[idx,] <- c( p, b, s, z, speedup )
                      idx <- idx + 1
                  }
                  else
                  {
                      cat(paste(b," ",p," ",s," ",z," ",num," ",denom," ",speedup))
                      cat("\n")
                  }
                }
            }
        }
    }

data$speedup <- as.numeric(data$speedup)

# workaround for setting y limits in an underhand and shady way
fakeData <- rbind(data,c("Nvidia","Acoustic","512","64","2.0"))
fakeData <- rbind(fakeData,c("Nvidia","Acoustic","512","64","0.0"))
fakeData <- rbind(fakeData,c("AMD","Acoustic","512","64","2.0"))
fakeData <- rbind(fakeData,c("AMD","Acoustic","512","64","0.0"))
fakeData <- rbind(fakeData,c("ARM","Acoustic","512","64","8.0"))
fakeData <- rbind(fakeData,c("ARM","Acoustic","512","64","0.0"))

fakeData$speedup <- as.numeric(fakeData$speedup)
fakeData$platform <- as.factor(fakeData$platform)
fakeData$platform = factor(fakeData$platform, levels=levels(fakeData$platform)[c(3,1,2)])

melted <- aggregate(speedup ~ benchmark + platform + zsize + size , data, FUN=max)
melted$platform <- as.factor(melted$platform)
melted$size <- as.factor(melted$size)
melted$zsize <- as.factor(melted$zsize)
melted$speedup <- as.numeric(melted$speedup)

melted$platform = factor(melted$platform, levels=levels(melted$platform)[c(3,1,2)])
melted$size = factor(melted$size, levels=levels(melted$size)[c(2,1)])


# workaround for factoring facets which do not want to be factored!!
melted$benchmark_f = factor(melted$benchmark, levels=c("Acoustic","Heat","Hotspot3D","Jacobi3D13pt","Jacobi3D7pt","Poisson","Jacobi3D27pt")) 

zsizes <- sort(sapply(melted, function(x) unique(x))$zsize)

zsize_plot <- ggplot(data=melted, mapping= aes(x=as.factor(zsize), y=speedup,  color=size)) + #, colour=benchmark, group=benchmark)) +
    facet_grid(platform~benchmark_f, scales="free") +
    geom_line(aes(group=size))+
    scale_color_manual(values=c("#ca0020","#f4a582","#92c5de","#0571b0"),name="X-Y Size")+
    scale_x_discrete(name="Z-Size", limits=c("8","16","32","64","128","256","512"))+
    ylab("Speedup of Unrolled 2.5D Tiling Optimization") +
    geom_hline(yintercept=1, linetype="dashed") +
    theme_bw() +
    geom_blank(data=fakeData) +
    theme(
        legend.position="right",
        strip.background=element_rect(fill=NA),
	axis.text.x=element_text(size=8, angle = 45, hjust=1),
        axis.title.y=element_text(size=10,face="bold")
        )


melted$zsize = factor(melted$zsize, levels=levels(melted$zsize)[rev(c(7,2,4,6,1,3,5))])

ggsave("~/workspace/papers/taco/15997511mvqxpdpqchgn/figures/unrollcomparison_byzsize.pdf", width=7.6, height=3, device = cairo_pdf)

