#!/bin/bash

FILE=$1
DIR=$2

function update_file {
    FILE=$1
    GLOBAL1=$2
    GLOBAL2=$3
    GLOBAL3=$4
    LOCAL1=$5
    LOCAL2=$6
    LOCAL3=$7

    sed -i "6s/200/50/" $FILE
    sed -i "6s/1\.005/1\.2/" $FILE
    sed -i "6s/10800/3600/" $FILE

    sed -i "8s/(1,.*)\"/("$GLOBAL3","$GLOBAL3")\"/" $FILE
    sed -i "9s/(1,.*)\"/("$GLOBAL2","$GLOBAL2")\"/" $FILE
    sed -i "10s/(1,.*)\"/("$GLOBAL1","$GLOBAL1")\"/" $FILE
    
    sed -i "13s/(1,.*)\"/("$LOCAL1","$LOCAL1")\"/" $FILE
    sed -i "17s/(1,.*)\"/("$LOCAL2","$LOCAL2")\"/" $FILE
    sed -i "21s/(1,.*)\"/("$LOCAL3","$LOCAL3")\"/" $FILE
    
}

export -f update_file

# loop over list 
## recreate file names
## update file with new name and update global & local sizes all at once in special function
cat $FILE | awk -v DIR="$DIR" -F, '{ print DIR"/"$1"-"$2"-"$3"-n-"$4".cl " $7" "$8" "$9" "$10" "$11" "$12  }' | xargs -n 7 bash -c 'update_file "$@"' _ 
