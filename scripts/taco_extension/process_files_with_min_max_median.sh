#!/bin/bash

DIR=$1

function process_file {

        FILE=$1
        LENGTH="$(cat $FILE | wc -l)"
        MIN=$(awk -F';' '{print $NF}' $FILE | grep -v 18446744073709551615 | tail -n +2 | sort -n | awk -f min.awk)
        MEDIAN=$(awk -F';' '{print $NF}' $FILE | grep -v 18446744073709551615 | tail -n +2 | sort -n | awk -f median.awk)
        MAX=$(awk -F';' '{print $NF}' $FILE | grep -v 18446744073709551615 | tail -n +2 |sort -n | awk -f max.awk)
                TEST="$(echo $MIN","$MEDIAN","$MAX)"
        # throw away csv which only contain header -> invalid runs
        if [ $LENGTH -gt 1 ] 
        then
                FILENAME="$(echo -n $FILE | sed -e 's/.cost.csv//g')"
                POSITION="$(cat $FILE | grep cost | sed -e 's/;/ /g' | wc -w)"
                BEST="$(cat $FILE | \
                        sort -n -r --field-separator=';' -k $POSITION | \
                        tail -n 2 | head -1)"
                MORE="$(echo $BEST | sed -e 's/;/ /g' | awk -v pos=$POSITION '{printf $pos,"\t"}')"
                echo $FILENAME";"$BEST"->" $TEST
        fi
}

export -f process_file

# first get median 
# MEDIAN=awk -F';' '{print $NF}' $FILE | grep -v 18446744073709551615 | sort -n | awk -f median.awk
# remember && 

find $DIR -type f | xargs -n 1 bash -c 'process_file "$@"' _ | awk -F'/' '{ print $NF}' | awk -F'-|.cl| |;' '{ print $1","$2","$3","$5","$7","$9","$10","$11","$12","$13","$14","$17 }' 
