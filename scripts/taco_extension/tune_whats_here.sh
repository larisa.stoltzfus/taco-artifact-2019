#!/bin/bash

HOST=$(hostname)

function tune {
	NAME=$1
	./atfc -I -i $NAME >> tune.out 
	mv cost.csv $NAME-$HOST.cost.csv
	mv meta.csv $NAME-$HOST.meta.csv
}

# copy all kernels to this directory
#cp $1/*.cl .

# tune them one after each other 
export -f tune
ls -l | awk '{print $9}' | grep cl | xargs -n 1 bash -c 'tune "$@"' _

