#!/bin/bash

DIR=$1

OUTPUTTO="../"
OUTPUTTONAME="taco_take_2_acoustic"

function process_and_copy_file {

        RAW_FILE_HEADERS="raw_headers/"

        FILE=$1
        BENCHMARK=$2
        PLATFORM=$3
        OUTPUTDIR=$4 


        FILENAME=$( printf $FILE | awk -F[/] '{ print $NF }' )
        TYPE="$(printf $FILE | awk -F"-" '{ print $2 }')"
        
        FILETYPE="original"
        BENCHMARKTYPE="normal"
        PLATFORMTYPE="other"
        
        if [ $TYPE = "MSS" ]
        then
            FILETYPE="mss"
        fi

        if [ $BENCHMARK = "acoustic" ] || [ $BENCHMARK = "hotspot3D" ]
        then
            BENCHMARKTYPE="acoustic_hs3d"
        fi

        if [ $PLATFORM = "nvidia" ]
        then
            PLATFORMTYPE="nvidia"
        fi
        
        CONCATFILE=$FILETYPE"_"$BENCHMARKTYPE"_"$PLATFORMTYPE"_atfheader.txt"

        echo "Copying: cat "$RAW_FILE_HEADERS$CONCATFILE" "$FILE" > "$OUTPUTDIR"/"$FILENAME
        cat $RAW_FILE_HEADERS$CONCATFILE $FILE > $OUTPUTDIR"/"$FILENAME
}

export -f process_and_copy_file

function update_file {

        FILE=$1

        GLOBAL="$(printf $FILE | awk -F[/] '{ print $NF }' | awk -F[-.] '{ print $3 }')"
        ZSIZE="$(printf $FILE | awk -F[/] '{ print $NF }' | awk -F[-.] '{ print $5 }')"

        echo "GLOBAL: "$GLOBAL" ZSIZE: "$ZSIZE
        sed -i "1s/v_M_0 =/v_M_0 = "$GLOBAL"/" $FILE
        sed -i "2s/v_N_1 =/v_N_1 = "$GLOBAL"/" $FILE
        sed -i "3s/v_O_2 =/v_O_2 = "$ZSIZE"/" $FILE
}

export -f update_file

# loop over benchmarks - array of names or whatever
BENCHMARKS=( "acoustic" ) #"heat3d" "hotspot3D" "j3d7pt" "j3d13pt" "j3d27pt" "poisson" "j3d21pt")

# loop over platforms - array of names or whatever
PLATFORMS=( "nvidia" "amd" "mali" )

# find all relevant files (per benchmark) 
# loop over relevant files pulling out if "MSS" or "original
# # concatenate "dummy" MSS or original ATF header to all files, output in new location with original name
for PLATFORM in "${PLATFORMS[@]}"; do
    for BENCHMARK in "${BENCHMARKS[@]}"; do
        OUTPUT_DIR=$OUTPUTTO$PLATFORM"/"$OUTPUTTONAME"/"$BENCHMARK
    
        # create new directory if one doesn't exist for new files
        if [ ! -d $OUTPUT_DIR ];then 
            mkdir -p $OUTPUT_DIR
            echo "Creating ... "$OUTPUT_DIR
        fi
        find $DIR -name $BENCHMARK* | while read FILE;do 
            echo "TOUPDATE: "$FILE" "$BENCHMARK" "$PLATFORM" "$OUTPUT_DIR 
            printf $FILE" "$BENCHMARK" "$PLATFORM" "$OUTPUT_DIR | xargs -n 4 bash -c 'process_and_copy_file "$@"' _ 
        done
    done
done

# loop over new files pulling out global values from name and update global values accordingly at top of file
for PLATFORM in "${PLATFORMS[@]}"; do

    OUTPUT_DIR=$OUTPUTTO$PLATFORM"/"$OUTPUTTONAME
    find $OUTPUT_DIR -type f | while read FILE;do 
            
           echo "Updating file...."$FILE
           printf $FILE | xargs -n 1 bash -c 'update_file "$@"' _ 
    
    done
done
