#!/bin/bash

DIR1=$1
DIR2=$2
OUTPUTDIR=$3


# loop over DIR1 and collect files
# if same named file exists in DIR2, pull this file out too
# cat whatever file(s) exist into new file in OUTPUTDIR

find $DIR1 -type f | while read FILE;do 
    
    RAWFILE=$( printf $FILE | awk -F[/] '{ print $NF }' )

    FILE1=$FILE
    FILE2=$DIR2"/"$RAWFILE
    OUTPUTFILE=$OUTPUTDIR"/"$RAWFILE


    if [ -f $FILE2 ];
    then
        echo "cat "$FILE1" "$FILE2" > "$OUTPUTFILE
        cat $FILE1 > $OUTPUTFILE
        head -1 $FILE2 >> $OUTPUTFILE
    else
        echo "cat "$FILE1" > "$OUTPUTFILE
        cat $FILE1 > $OUTPUTFILE
    fi

done
