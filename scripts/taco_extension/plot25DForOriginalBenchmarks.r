############# PLOTTING
library(ggplot2)
dataFile <- "original_speedup.csv"
data <- read.csv(dataFile,header=TRUE,sep=",",stringsAsFactors = FALSE)

data["speedup"] <- data$lift/data$lift25D
data["percentage_speedup"] <- (data$speedup - 1) * 100
data <- within(data, percentage_speedup[speedup == 0] <- 0)

data <- aggregate(percentage_speedup ~ benchmark + platform + size, data, FUN=max)
data$platform <- as.factor(data$platform)

data$platform = factor(data$platform, levels=levels(data$platform)[c(3,1,2)])
data$size = factor(data$size, levels=c("small", "large"))

data$benchmark <- as.factor(data$benchmark)
data$benchmark = factor(data$benchmark, levels=levels(data$benchmark)[c(1,3,2,4,5,6)])

speedup_plot <- ggplot(data, aes(x=benchmark, y=percentage_speedup, fill=platform, alpha=size)) +
    geom_bar(stat="identity", position="dodge", colour="black", width=0.8) +
    scale_fill_manual(values=c("#76B900", "#ED1C24","#0091BD"), guide = "none") +
    #scale_alpha_manual(values = c(0.2,0.4,0.6, 1.0)) +
    scale_alpha_manual(values = c(0.6, 1.0)) +
    #scale_fill_brewer("Platform", palette="YlOrRd") +
    facet_wrap(~ platform) + #, scales = "free") +
    geom_hline(yintercept=0) +
    coord_cartesian(ylim=c(-20,80))+
    # scale_y_log10(breaks=c(1,2,3,4,5,10,20)) +
    ylab("% Improvement with 2.5DT") +
    xlab("Platform") +
    xlab(NULL) +
#    scale_y_continuous(breaks = round(seq(min(data$percentage_speedup), max(data$percentage_speedup), by = 5),1))+
 #   scale_x_discrete(name="Benchmark", limits=c("acoustic","hotspot3D","heat","J3D7pt","J3D13pt","poisson"))+
    theme_bw() +
    theme(
        legend.position="right",
        strip.background=element_rect(fill=NA),
#         panel.grid.major = element_blank(),
	axis.text.x=element_text(size=8, angle = 45, hjust=1),
        axis.title.y=element_text(size=10,face="bold")
        )


ggsave("~/workspace/papers/taco/15997511mvqxpdpqchgn/figures/speedup_original.pdf", width=10, height=2.5, device = cairo_pdf)

