#!/bin/bash
: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"

if [ -f $ROOTDIR/tools/llvm-3.7.0/bin/llvm-config ]; then
  LLVM_CONFIG=$ROOTDIR/tools/llvm-3.7.0/bin/llvm-config
elif [ -x "$(command -v llvm-config)" ]; then
  LLVM_CONFIG=$(command -v llvm-config)
elif [ -x "$(command -v llvm-config-3.8)" ]; then
  LLVM_CONFIG=$(command -v llvm-config-3.8)
elif [ -x "$(command -v llvm-config-3.7)" ]; then
  LLVM_CONFIG=$(command -v llvm-config-3.7)
fi

: "${LLVM_CONFIG:?Need to set LLVM_CONFIG}"

pushd $ROOTDIR/tools > /dev/null
git clone https://github.com/bastianhagedorn/ppcg.git
cd ppcg
./get_submodules.sh
./autogen.sh
./configure --with-clang-prefix=`$LLVM_CONFIG --prefix` CPPFLAGS=-I`$LLVM_CONFIG --includedir`

JOBS=$(nproc --all)
make -j$JOBS
popd > /dev/null
