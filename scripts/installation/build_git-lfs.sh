#!/bin/bash
: "${ROOTDIR:?Please set artifact env-vars: $ source environment.env}"

pushd $ROOTDIR/tools > /dev/null
wget https://github.com/git-lfs/git-lfs/releases/download/v2.3.4/git-lfs-linux-amd64-2.3.4.tar.gz
tar -xvf git-lfs-linux-amd64-2.3.4.tar.gz 
rm git-lfs-linux-amd64-2.3.4.tar.gz
cd git-lfs-2.3.4
./install.sh
git lfs install
