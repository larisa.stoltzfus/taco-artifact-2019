#!/bin/bash
if [ $# -eq 2 ] && ([ "$1" == "nvidia" ] || [ "$1" == "amd" ] || [ "$1" == "arm" ]) &&
	 ([ "$2" == "hotspot" ] || [ "$2" == "hotspot3D" ] || [ "$2" == "srad1" ] ||
	 [ "$2" == "srad2" ] || [ "$2" == "stencil2d" ])
then 
				# adjust to folder structure
				if [ "$1" == "nvidia" ]			
				then
								ARCH="kepler"
				fi
				if [ "$1" == "amd" ]			
				then
								ARCH="tahiti"
				fi
				if [ "$1" == "arm" ]			
				then
								ARCH="mali"
				fi
				BENCH=$2

                ############################################
                # 1. Run reference implementations
                ############################################
                # run references if not already happened
                if ! [ -d "$ROOTDIR/output_data/tuned/$1/reference" ]; then
                    run_references_workflow2_figure7.sh $1
                fi
else
    		echo "Wrong arguments supplied"
				echo "Usage: ./tune_benchmark [architecture] [benchmark]"
				echo ""
				echo "Which architecture? (nvidia | amd | arm)"
				echo "Which benchmark? (hotspot | hotspot3D | srad1 | srad2 | stencil2d)"
    		exit -1
fi
