#!/bin/bash
if [ $# -eq 3 ] && ([ "$1" == "nvidia" ] || [ "$1" == "amd" ]) &&
	 ([ "$2" == "gaussian" ] || [ "$2" == "grad2d" ] || [ "$2" == "heat3d" ] ||
	 [ "$2" == "j2d5pt" ] || [ "$2" == "j2d9pt" ] || [ "$2" == "j3d13pt" ] ||
	 [ "$2" == "j3d7pt" ] || [ "$2" == "poisson3d" ]) &&
	 ([ "$3" == "small" ] || [ "$3" == "large" ]) 
then 
				# adjust to folder structure
				if [ "$1" == "nvidia" ]			
				then
								ARCH="kepler"
				fi
				if [ "$1" == "amd" ]			
				then
								ARCH="tahiti"
				fi
				BENCH=$2
				SIZE=$3
				if [ "$SIZE" == "large" ] 
				then
								SIZE=big # we used 'big' in folder structure
				fi
                ############################################
                # 2. Run Lift implementations
                ############################################
				# tune generated kernel for this specific benchmark
				CL=Cl
				pushd $ROOTDIR/benchmarks/figure8/workflow2/$BENCH/$SIZE/$ARCH/$BENCH$CL > /dev/null
				TUNE=$PWD
				popd > /dev/null
				pushd $ATF/build
                # this folder tends to get messy
                rm *.cl
                rm *.csv
                rm *.log
				do_you_even_tune.sh $TUNE
				popd > /dev/null

				# analyze tuning results
				pushd $TUNE
				analyze_tuning.sh

				# rerun best found kernel multiple times
				rerun_best_lift_ppcg_kernel.sh
				popd > /dev/null
else
    		echo "Wrong arguments supplied"
				echo "Usage: ./tune_benchmark [architecture] [benchmark] [inputsize]"
				echo ""
				echo "Which architecture? (nvidia | amd | arm)"
				echo "Which benchmark? (gaussian | grad2d | heat3d | j2d5pt | j2d9pt | j3d13pt | j3d7pt | poisson3d)"
				echo "Which inputsize? (small | large)"
    		exit -1
fi
