#!/bin/bash
N=10
BIN=./BasicRoom

if [ "$#" -lt 1 ]; then
    echo "Illegal number of parameters (enter platform, device, iterations and 'use files for input flag')"
    exit -1
fi

FILENAME=$(basename "$PWD")

PLATFORM=0
DEVICE=0
USEFILESFLAG=0
ITERATIONS=$N

TIMESTRING="KERNEL"

if [ "$#" -eq 4 ]; then
    PLATFORM=$1
    DEVICE=$2
    ITERATIONS=$3
    USEFILESFLAG=$4
fi


# prints kernel runtime in nanoseconds to hotspot.out using kernel event times

RAWDATA="${FILENAME}.raw"
OUTDATA="${FILENAME}.out"

if [ -e $RAWDATA ]
then
    rm $RAWDATA 
fi
if [ -e $OUTDATA ]
then
    rm $OUTDATA 
fi

#make clean && make

#echo "$BIN $PLATFORM $DEVICE $ITERATIONS $USEFILESFLAG >> ${RAWDATA}"

for i in $(seq 1 $ITERATIONS)
do
    $BIN $PLATFORM $DEVICE $ITERATIONS $USEFILESFLAG >> $RAWDATA
done

cat $RAWDATA | grep $TIMESTRING  | awk '{print $3}' > $OUTDATA 
cat $OUTDATA
