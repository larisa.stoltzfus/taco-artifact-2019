#atf::var<int> v_M_0 = 128 
#atf::var<int> v_N_1 = 128 
#atf::var<int> v_O_2 = 32 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(32,32)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(2,2)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(8,8)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float calculateHotspot(float tInC, float cc, float tInN, float cn, float tInS, float cs, float tInE, float ce, float tInW, float cw, float tInT, float ct, float tInB, float cb, float stepDivCap, float pInC, float amb_temp){
    {
        { return  tInC*cc + tInN*cn + tInS*cs + tInE*ce + tInW*cw + tInT*ct + tInB*cb + stepDivCap * pInC + ct*amb_temp; }; 
    }
}
kernel void KERNEL(const global float* restrict v__62, const global float* restrict v__63, global float* v__77){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        float v__67; 
        float v__68; 
        float v__69; 
        float v__70; 
        float v__71; 
        float v__72; 
        float v__73; 
        float v__74; 
        float v__75; 
        // Private Memory
        for (int v_gl_id_59 = get_global_id(2); (v_gl_id_59 < 32); v_gl_id_59 = (v_gl_id_59 + get_global_size(2))){
            for (int v_gl_id_60 = get_global_id(1); (v_gl_id_60 < 128); v_gl_id_60 = (v_gl_id_60 + get_global_size(1))){
                for (int v_gl_id_61 = get_global_id(0); (v_gl_id_61 < 128); v_gl_id_61 = (v_gl_id_61 + get_global_size(0))){
                    float v_tmp_119 = 0.86186665f; 
                    v__67 = v_tmp_119; 
                    float v_tmp_120 = 0.03413333f; 
                    v__68 = v_tmp_120; 
                    float v_tmp_121 = 0.03413333f; 
                    v__69 = v_tmp_121; 
                    float v_tmp_122 = 0.03413333f; 
                    v__70 = v_tmp_122; 
                    float v_tmp_123 = 0.03413333f; 
                    v__71 = v_tmp_123; 
                    float v_tmp_124 = 5.333333E-4f; 
                    v__72 = v_tmp_124; 
                    float v_tmp_125 = 5.333333E-4f; 
                    v__73 = v_tmp_125; 
                    float v_tmp_126 = 0.3413333f; 
                    v__74 = v_tmp_126; 
                    float v_tmp_127 = 80.0f; 
                    v__75 = v_tmp_127; 
                    v__77[(v_gl_id_61 + (16384 * v_gl_id_59) + (128 * v_gl_id_60))] = calculateHotspot(v__62[(v_gl_id_61 + (16384 * v_gl_id_59) + (128 * v_gl_id_60))], v__67, v__62[(v_gl_id_61 + (16384 * v_gl_id_59) + (128 * ( ((-1 + v_gl_id_60) >= 0) ? (-1 + v_gl_id_60) : 0 )))], v__68, v__62[(v_gl_id_61 + (16384 * v_gl_id_59) + (128 * ( ((1 + v_gl_id_60) < 128) ? (1 + v_gl_id_60) : 127 )))], v__69, v__62[(v_gl_id_61 + (128 * v_gl_id_60) + (16384 * ( ((1 + v_gl_id_59) < 32) ? (1 + v_gl_id_59) : 31 )))], v__70, v__62[(v_gl_id_61 + (128 * v_gl_id_60) + (16384 * ( ((-1 + v_gl_id_59) >= 0) ? (-1 + v_gl_id_59) : 0 )))], v__71, v__62[((16384 * v_gl_id_59) + (128 * v_gl_id_60) + ( ((1 + v_gl_id_61) < 128) ? (1 + v_gl_id_61) : 127 ))], v__72, v__62[((16384 * v_gl_id_59) + (128 * v_gl_id_60) + ( ((-1 + v_gl_id_61) >= 0) ? (-1 + v_gl_id_61) : 0 ))], v__73, v__74, v__63[(v_gl_id_61 + (16384 * v_gl_id_59) + (128 * v_gl_id_60))], v__75); 
                }
            }
        }
    }
}