// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: int
#ifndef Tuple3_float_float_int_DEFINED
#define Tuple3_float_float_int_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
    int _2;
} Tuple3_float_float_int;
#endif

float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float addTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 + x._1;}; 
    }
}
float subtract(float l, float r){
    {
        { return l - r; }; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
float subtractTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 - x._1;}; 
    }
}
int idxF(int i, int j, int k, int m, int n, int o){
    {
        {int count = 6; if(i == (m-1) || i == 0){ count--; } if(j == (n-1) || j == 0){ count--; } if(k == (o-1) || k == 0){ count--; }return count; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
float getCF(int neigh, float cfB, float cfI){
    {
        { if(neigh < 6) { return cfB; } else{ return cfI;} }; 
    }
}
float idIF(int x){
    {
        { return (float)(x*1.0); }; 
    }
}
kernel void KERNEL(const global float* restrict v__204, const global float* restrict v__205, global float* v__247){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        float v__210; 
        float v__213; 
        float v__228; 
        float v__231; 
        float v__232; 
        float v__241; 
        float v__242; 
        // Private Memory
        float v__209; 
        float v__212; 
        float v__215; 
        float v__217; 
        float v__219; 
        float v__221; 
        float v__223; 
        float v__225; 
        float v__227; 
        float v__230; 
        float v__234; 
        float v__236; 
        float v__238; 
        float v__240; 
        float v__244; 
        float v__246; 
        for (int v_gl_id_201 = get_global_id(0); (v_gl_id_201 < 128); v_gl_id_201 = (v_gl_id_201 + get_global_size(0))){
            for (int v_gl_id_202 = get_global_id(1); (v_gl_id_202 < 128); v_gl_id_202 = (v_gl_id_202 + get_global_size(1))){
                // mapSeqSlide
                float v_v_v_window_28_1_452_0_479; 
                float v_v_v_window_28_1_452_1_480; 
                int v_v_v_window_28_1_452_2_481; 
                float v_v_v_window_28_2_453_0_482; 
                float v_v_v_window_28_2_453_1_483; 
                int v_v_v_window_28_2_453_2_484; 
                float v_v_v_window_28_3_454_0_485; 
                float v_v_v_window_28_3_454_1_486; 
                int v_v_v_window_28_3_454_2_487; 
                float v_v_v_window_28_4_455_0_488; 
                float v_v_v_window_28_4_455_1_489; 
                int v_v_v_window_28_4_455_2_490; 
                float v_v_v_window_28_5_456_0_491; 
                float v_v_v_window_28_5_456_1_492; 
                int v_v_v_window_28_5_456_2_493; 
                float v_v_v_window_28_6_457_0_494; 
                float v_v_v_window_28_6_457_1_495; 
                int v_v_v_window_28_6_457_2_496; 
                float v_v_v_window_28_7_458_0_497; 
                float v_v_v_window_28_7_458_1_498; 
                int v_v_v_window_28_7_458_2_499; 
                float v_v_v_window_28_8_459_0_500; 
                float v_v_v_window_28_8_459_1_501; 
                int v_v_v_window_28_8_459_2_502; 
                float v_v_v_window_28_9_460_0_503; 
                float v_v_v_window_28_9_460_1_504; 
                int v_v_v_window_28_9_460_2_505; 
                float v_v_v_window_28_10_461_0_506; 
                float v_v_v_window_28_10_461_1_507; 
                int v_v_v_window_28_10_461_2_508; 
                float v_v_v_window_28_11_462_0_509; 
                float v_v_v_window_28_11_462_1_510; 
                int v_v_v_window_28_11_462_2_511; 
                float v_v_v_window_28_12_463_0_512; 
                float v_v_v_window_28_12_463_1_513; 
                int v_v_v_window_28_12_463_2_514; 
                float v_v_v_window_28_13_464_0_515; 
                float v_v_v_window_28_13_464_1_516; 
                int v_v_v_window_28_13_464_2_517; 
                float v_v_v_window_28_14_465_0_518; 
                float v_v_v_window_28_14_465_1_519; 
                int v_v_v_window_28_14_465_2_520; 
                float v_v_v_window_28_15_466_0_521; 
                float v_v_v_window_28_15_466_1_522; 
                int v_v_v_window_28_15_466_2_523; 
                float v_v_v_window_28_16_467_0_524; 
                float v_v_v_window_28_16_467_1_525; 
                int v_v_v_window_28_16_467_2_526; 
                float v_v_v_window_28_17_468_0_527; 
                float v_v_v_window_28_17_468_1_528; 
                int v_v_v_window_28_17_468_2_529; 
                float v_v_v_window_28_18_469_0_530; 
                float v_v_v_window_28_18_469_1_531; 
                int v_v_v_window_28_18_469_2_532; 
                float v_v_v_window_28_19_470_0_533; 
                float v_v_v_window_28_19_470_1_534; 
                int v_v_v_window_28_19_470_2_535; 
                float v_v_v_window_28_20_471_0_536; 
                float v_v_v_window_28_20_471_1_537; 
                int v_v_v_window_28_20_471_2_538; 
                float v_v_v_window_28_21_472_0_539; 
                float v_v_v_window_28_21_472_1_540; 
                int v_v_v_window_28_21_472_2_541; 
                float v_v_v_window_28_22_473_0_542; 
                float v_v_v_window_28_22_473_1_543; 
                int v_v_v_window_28_22_473_2_544; 
                float v_v_v_window_28_23_474_0_545; 
                float v_v_v_window_28_23_474_1_546; 
                int v_v_v_window_28_23_474_2_547; 
                float v_v_v_window_28_24_475_0_548; 
                float v_v_v_window_28_24_475_1_549; 
                int v_v_v_window_28_24_475_2_550; 
                float v_v_v_window_28_25_476_0_551; 
                float v_v_v_window_28_25_476_1_552; 
                int v_v_v_window_28_25_476_2_553; 
                float v_v_v_window_28_26_477_0_554; 
                float v_v_v_window_28_26_477_1_555; 
                int v_v_v_window_28_26_477_2_556; 
                float v_v_v_window_28_27_478_0_557; 
                float v_v_v_window_28_27_478_1_558; 
                int v_v_v_window_28_27_478_2_559; 
                {
                    Tuple3_float_float_int v_v_v_window_28_1_452_tmp_560; 
                    v_v_v_window_28_1_452_tmp_560 = (Tuple3_float_float_int){v__204[(v_gl_id_201 + (130 * v_gl_id_202))], v__205[(v_gl_id_201 + (130 * v_gl_id_202))], idxF(0, v_gl_id_202, v_gl_id_201, 34, 130, 130)}; 
                    v_v_v_window_28_1_452_0_479 = v_v_v_window_28_1_452_tmp_560._0; 
                    v_v_v_window_28_1_452_1_480 = v_v_v_window_28_1_452_tmp_560._1; 
                    v_v_v_window_28_1_452_2_481 = v_v_v_window_28_1_452_tmp_560._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_10_461_tmp_561; 
                    v_v_v_window_28_10_461_tmp_561 = (Tuple3_float_float_int){v__204[(16900 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(16900 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(1, v_gl_id_202, v_gl_id_201, 34, 130, 130)}; 
                    v_v_v_window_28_10_461_0_506 = v_v_v_window_28_10_461_tmp_561._0; 
                    v_v_v_window_28_10_461_1_507 = v_v_v_window_28_10_461_tmp_561._1; 
                    v_v_v_window_28_10_461_2_508 = v_v_v_window_28_10_461_tmp_561._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_4_455_tmp_562; 
                    v_v_v_window_28_4_455_tmp_562 = (Tuple3_float_float_int){v__204[(130 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(130 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(0, (1 + v_gl_id_202), v_gl_id_201, 34, 130, 130)}; 
                    v_v_v_window_28_4_455_0_488 = v_v_v_window_28_4_455_tmp_562._0; 
                    v_v_v_window_28_4_455_1_489 = v_v_v_window_28_4_455_tmp_562._1; 
                    v_v_v_window_28_4_455_2_490 = v_v_v_window_28_4_455_tmp_562._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_13_464_tmp_563; 
                    v_v_v_window_28_13_464_tmp_563 = (Tuple3_float_float_int){v__204[(17030 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(17030 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(1, (1 + v_gl_id_202), v_gl_id_201, 34, 130, 130)}; 
                    v_v_v_window_28_13_464_0_515 = v_v_v_window_28_13_464_tmp_563._0; 
                    v_v_v_window_28_13_464_1_516 = v_v_v_window_28_13_464_tmp_563._1; 
                    v_v_v_window_28_13_464_2_517 = v_v_v_window_28_13_464_tmp_563._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_7_458_tmp_564; 
                    v_v_v_window_28_7_458_tmp_564 = (Tuple3_float_float_int){v__204[(260 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(260 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(0, (2 + v_gl_id_202), v_gl_id_201, 34, 130, 130)}; 
                    v_v_v_window_28_7_458_0_497 = v_v_v_window_28_7_458_tmp_564._0; 
                    v_v_v_window_28_7_458_1_498 = v_v_v_window_28_7_458_tmp_564._1; 
                    v_v_v_window_28_7_458_2_499 = v_v_v_window_28_7_458_tmp_564._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_16_467_tmp_565; 
                    v_v_v_window_28_16_467_tmp_565 = (Tuple3_float_float_int){v__204[(17160 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(17160 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(1, (2 + v_gl_id_202), v_gl_id_201, 34, 130, 130)}; 
                    v_v_v_window_28_16_467_0_524 = v_v_v_window_28_16_467_tmp_565._0; 
                    v_v_v_window_28_16_467_1_525 = v_v_v_window_28_16_467_tmp_565._1; 
                    v_v_v_window_28_16_467_2_526 = v_v_v_window_28_16_467_tmp_565._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_2_453_tmp_566; 
                    v_v_v_window_28_2_453_tmp_566 = (Tuple3_float_float_int){v__204[(1 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(1 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(0, v_gl_id_202, (1 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_2_453_0_482 = v_v_v_window_28_2_453_tmp_566._0; 
                    v_v_v_window_28_2_453_1_483 = v_v_v_window_28_2_453_tmp_566._1; 
                    v_v_v_window_28_2_453_2_484 = v_v_v_window_28_2_453_tmp_566._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_11_462_tmp_567; 
                    v_v_v_window_28_11_462_tmp_567 = (Tuple3_float_float_int){v__204[(16901 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(16901 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(1, v_gl_id_202, (1 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_11_462_0_509 = v_v_v_window_28_11_462_tmp_567._0; 
                    v_v_v_window_28_11_462_1_510 = v_v_v_window_28_11_462_tmp_567._1; 
                    v_v_v_window_28_11_462_2_511 = v_v_v_window_28_11_462_tmp_567._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_5_456_tmp_568; 
                    v_v_v_window_28_5_456_tmp_568 = (Tuple3_float_float_int){v__204[(131 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(131 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(0, (1 + v_gl_id_202), (1 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_5_456_0_491 = v_v_v_window_28_5_456_tmp_568._0; 
                    v_v_v_window_28_5_456_1_492 = v_v_v_window_28_5_456_tmp_568._1; 
                    v_v_v_window_28_5_456_2_493 = v_v_v_window_28_5_456_tmp_568._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_14_465_tmp_569; 
                    v_v_v_window_28_14_465_tmp_569 = (Tuple3_float_float_int){v__204[(17031 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(17031 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(1, (1 + v_gl_id_202), (1 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_14_465_0_518 = v_v_v_window_28_14_465_tmp_569._0; 
                    v_v_v_window_28_14_465_1_519 = v_v_v_window_28_14_465_tmp_569._1; 
                    v_v_v_window_28_14_465_2_520 = v_v_v_window_28_14_465_tmp_569._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_8_459_tmp_570; 
                    v_v_v_window_28_8_459_tmp_570 = (Tuple3_float_float_int){v__204[(261 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(261 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(0, (2 + v_gl_id_202), (1 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_8_459_0_500 = v_v_v_window_28_8_459_tmp_570._0; 
                    v_v_v_window_28_8_459_1_501 = v_v_v_window_28_8_459_tmp_570._1; 
                    v_v_v_window_28_8_459_2_502 = v_v_v_window_28_8_459_tmp_570._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_17_468_tmp_571; 
                    v_v_v_window_28_17_468_tmp_571 = (Tuple3_float_float_int){v__204[(17161 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(17161 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(1, (2 + v_gl_id_202), (1 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_17_468_0_527 = v_v_v_window_28_17_468_tmp_571._0; 
                    v_v_v_window_28_17_468_1_528 = v_v_v_window_28_17_468_tmp_571._1; 
                    v_v_v_window_28_17_468_2_529 = v_v_v_window_28_17_468_tmp_571._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_3_454_tmp_572; 
                    v_v_v_window_28_3_454_tmp_572 = (Tuple3_float_float_int){v__204[(2 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(2 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(0, v_gl_id_202, (2 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_3_454_0_485 = v_v_v_window_28_3_454_tmp_572._0; 
                    v_v_v_window_28_3_454_1_486 = v_v_v_window_28_3_454_tmp_572._1; 
                    v_v_v_window_28_3_454_2_487 = v_v_v_window_28_3_454_tmp_572._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_12_463_tmp_573; 
                    v_v_v_window_28_12_463_tmp_573 = (Tuple3_float_float_int){v__204[(16902 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(16902 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(1, v_gl_id_202, (2 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_12_463_0_512 = v_v_v_window_28_12_463_tmp_573._0; 
                    v_v_v_window_28_12_463_1_513 = v_v_v_window_28_12_463_tmp_573._1; 
                    v_v_v_window_28_12_463_2_514 = v_v_v_window_28_12_463_tmp_573._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_6_457_tmp_574; 
                    v_v_v_window_28_6_457_tmp_574 = (Tuple3_float_float_int){v__204[(132 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(132 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(0, (1 + v_gl_id_202), (2 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_6_457_0_494 = v_v_v_window_28_6_457_tmp_574._0; 
                    v_v_v_window_28_6_457_1_495 = v_v_v_window_28_6_457_tmp_574._1; 
                    v_v_v_window_28_6_457_2_496 = v_v_v_window_28_6_457_tmp_574._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_15_466_tmp_575; 
                    v_v_v_window_28_15_466_tmp_575 = (Tuple3_float_float_int){v__204[(17032 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(17032 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(1, (1 + v_gl_id_202), (2 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_15_466_0_521 = v_v_v_window_28_15_466_tmp_575._0; 
                    v_v_v_window_28_15_466_1_522 = v_v_v_window_28_15_466_tmp_575._1; 
                    v_v_v_window_28_15_466_2_523 = v_v_v_window_28_15_466_tmp_575._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_9_460_tmp_576; 
                    v_v_v_window_28_9_460_tmp_576 = (Tuple3_float_float_int){v__204[(262 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(262 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(0, (2 + v_gl_id_202), (2 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_9_460_0_503 = v_v_v_window_28_9_460_tmp_576._0; 
                    v_v_v_window_28_9_460_1_504 = v_v_v_window_28_9_460_tmp_576._1; 
                    v_v_v_window_28_9_460_2_505 = v_v_v_window_28_9_460_tmp_576._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_18_469_tmp_577; 
                    v_v_v_window_28_18_469_tmp_577 = (Tuple3_float_float_int){v__204[(17162 + v_gl_id_201 + (130 * v_gl_id_202))], v__205[(17162 + v_gl_id_201 + (130 * v_gl_id_202))], idxF(1, (2 + v_gl_id_202), (2 + v_gl_id_201), 34, 130, 130)}; 
                    v_v_v_window_28_18_469_0_530 = v_v_v_window_28_18_469_tmp_577._0; 
                    v_v_v_window_28_18_469_1_531 = v_v_v_window_28_18_469_tmp_577._1; 
                    v_v_v_window_28_18_469_2_532 = v_v_v_window_28_18_469_tmp_577._2; 
                }
                for (int v_i_203 = 0; (v_i_203 < 32); v_i_203 = (1 + v_i_203)){
                    {
                        Tuple3_float_float_int v_v_v_window_28_19_470_tmp_578; 
                        v_v_v_window_28_19_470_tmp_578 = (Tuple3_float_float_int){v__204[(33800 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], v__205[(33800 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], idxF((2 + v_i_203), v_gl_id_202, v_gl_id_201, 34, 130, 130)}; 
                        v_v_v_window_28_19_470_0_533 = v_v_v_window_28_19_470_tmp_578._0; 
                        v_v_v_window_28_19_470_1_534 = v_v_v_window_28_19_470_tmp_578._1; 
                        v_v_v_window_28_19_470_2_535 = v_v_v_window_28_19_470_tmp_578._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_22_473_tmp_579; 
                        v_v_v_window_28_22_473_tmp_579 = (Tuple3_float_float_int){v__204[(33930 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], v__205[(33930 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], idxF((2 + v_i_203), (1 + v_gl_id_202), v_gl_id_201, 34, 130, 130)}; 
                        v_v_v_window_28_22_473_0_542 = v_v_v_window_28_22_473_tmp_579._0; 
                        v_v_v_window_28_22_473_1_543 = v_v_v_window_28_22_473_tmp_579._1; 
                        v_v_v_window_28_22_473_2_544 = v_v_v_window_28_22_473_tmp_579._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_25_476_tmp_580; 
                        v_v_v_window_28_25_476_tmp_580 = (Tuple3_float_float_int){v__204[(34060 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], v__205[(34060 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], idxF((2 + v_i_203), (2 + v_gl_id_202), v_gl_id_201, 34, 130, 130)}; 
                        v_v_v_window_28_25_476_0_551 = v_v_v_window_28_25_476_tmp_580._0; 
                        v_v_v_window_28_25_476_1_552 = v_v_v_window_28_25_476_tmp_580._1; 
                        v_v_v_window_28_25_476_2_553 = v_v_v_window_28_25_476_tmp_580._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_20_471_tmp_581; 
                        v_v_v_window_28_20_471_tmp_581 = (Tuple3_float_float_int){v__204[(33801 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], v__205[(33801 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], idxF((2 + v_i_203), v_gl_id_202, (1 + v_gl_id_201), 34, 130, 130)}; 
                        v_v_v_window_28_20_471_0_536 = v_v_v_window_28_20_471_tmp_581._0; 
                        v_v_v_window_28_20_471_1_537 = v_v_v_window_28_20_471_tmp_581._1; 
                        v_v_v_window_28_20_471_2_538 = v_v_v_window_28_20_471_tmp_581._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_23_474_tmp_582; 
                        v_v_v_window_28_23_474_tmp_582 = (Tuple3_float_float_int){v__204[(33931 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], v__205[(33931 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], idxF((2 + v_i_203), (1 + v_gl_id_202), (1 + v_gl_id_201), 34, 130, 130)}; 
                        v_v_v_window_28_23_474_0_545 = v_v_v_window_28_23_474_tmp_582._0; 
                        v_v_v_window_28_23_474_1_546 = v_v_v_window_28_23_474_tmp_582._1; 
                        v_v_v_window_28_23_474_2_547 = v_v_v_window_28_23_474_tmp_582._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_26_477_tmp_583; 
                        v_v_v_window_28_26_477_tmp_583 = (Tuple3_float_float_int){v__204[(34061 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], v__205[(34061 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], idxF((2 + v_i_203), (2 + v_gl_id_202), (1 + v_gl_id_201), 34, 130, 130)}; 
                        v_v_v_window_28_26_477_0_554 = v_v_v_window_28_26_477_tmp_583._0; 
                        v_v_v_window_28_26_477_1_555 = v_v_v_window_28_26_477_tmp_583._1; 
                        v_v_v_window_28_26_477_2_556 = v_v_v_window_28_26_477_tmp_583._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_21_472_tmp_584; 
                        v_v_v_window_28_21_472_tmp_584 = (Tuple3_float_float_int){v__204[(33802 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], v__205[(33802 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], idxF((2 + v_i_203), v_gl_id_202, (2 + v_gl_id_201), 34, 130, 130)}; 
                        v_v_v_window_28_21_472_0_539 = v_v_v_window_28_21_472_tmp_584._0; 
                        v_v_v_window_28_21_472_1_540 = v_v_v_window_28_21_472_tmp_584._1; 
                        v_v_v_window_28_21_472_2_541 = v_v_v_window_28_21_472_tmp_584._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_24_475_tmp_585; 
                        v_v_v_window_28_24_475_tmp_585 = (Tuple3_float_float_int){v__204[(33932 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], v__205[(33932 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], idxF((2 + v_i_203), (1 + v_gl_id_202), (2 + v_gl_id_201), 34, 130, 130)}; 
                        v_v_v_window_28_24_475_0_548 = v_v_v_window_28_24_475_tmp_585._0; 
                        v_v_v_window_28_24_475_1_549 = v_v_v_window_28_24_475_tmp_585._1; 
                        v_v_v_window_28_24_475_2_550 = v_v_v_window_28_24_475_tmp_585._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_27_478_tmp_586; 
                        v_v_v_window_28_27_478_tmp_586 = (Tuple3_float_float_int){v__204[(34062 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], v__205[(34062 + v_gl_id_201 + (130 * v_gl_id_202) + (16900 * v_i_203))], idxF((2 + v_i_203), (2 + v_gl_id_202), (2 + v_gl_id_201), 34, 130, 130)}; 
                        v_v_v_window_28_27_478_0_557 = v_v_v_window_28_27_478_tmp_586._0; 
                        v_v_v_window_28_27_478_1_558 = v_v_v_window_28_27_478_tmp_586._1; 
                        v_v_v_window_28_27_478_2_559 = v_v_v_window_28_27_478_tmp_586._2; 
                    }
                    v__209 = idIF(v_v_v_window_28_14_465_2_520); 
                    float v_tmp_445 = 0.3333333f; 
                    v__210 = v_tmp_445; 
                    v__212 = mult(v__209, v__210); 
                    float v_tmp_446 = 2.0f; 
                    v__213 = v_tmp_446; 
                    v__215 = subtract(v__213, v__212); 
                    v__217 = mult(v__215, v_v_v_window_28_14_465_1_519); 
                    v__219 = add(v_v_v_window_28_23_474_1_546, v_v_v_window_28_17_468_1_528); 
                    v__221 = add(v__219, v_v_v_window_28_15_466_1_522); 
                    v__223 = add(v__221, v_v_v_window_28_13_464_1_516); 
                    v__225 = add(v__223, v_v_v_window_28_11_462_1_510); 
                    v__227 = add(v__225, v_v_v_window_28_5_456_1_492); 
                    float v_tmp_447 = 0.3333333f; 
                    v__228 = v_tmp_447; 
                    v__230 = mult(v__227, v__228); 
                    float v_tmp_448 = 0.9971132f; 
                    v__231 = v_tmp_448; 
                    float v_tmp_449 = 1.0f; 
                    v__232 = v_tmp_449; 
                    v__234 = getCF(v_v_v_window_28_14_465_2_520, v__231, v__232); 
                    v__236 = mult(v_v_v_window_28_14_465_0_518, v__234); 
                    v__238 = subtractTuple((Tuple2_float_float){v__230, v__236}); 
                    v__240 = addTuple((Tuple2_float_float){v__217, v__238}); 
                    float v_tmp_450 = 0.9971216f; 
                    v__241 = v_tmp_450; 
                    float v_tmp_451 = 1.0f; 
                    v__242 = v_tmp_451; 
                    v__244 = getCF(v_v_v_window_28_14_465_2_520, v__241, v__242); 
                    v__246 = mult(v__240, v__244); 
                    v__247[(v_gl_id_201 + (128 * v_gl_id_202) + (16384 * v_i_203))] = id(v__246); 
                    v_v_v_window_28_1_452_0_479 = v_v_v_window_28_10_461_0_506; 
                    v_v_v_window_28_1_452_1_480 = v_v_v_window_28_10_461_1_507; 
                    v_v_v_window_28_1_452_2_481 = v_v_v_window_28_10_461_2_508; 
                    v_v_v_window_28_10_461_0_506 = v_v_v_window_28_19_470_0_533; 
                    v_v_v_window_28_10_461_1_507 = v_v_v_window_28_19_470_1_534; 
                    v_v_v_window_28_10_461_2_508 = v_v_v_window_28_19_470_2_535; 
                    v_v_v_window_28_2_453_0_482 = v_v_v_window_28_11_462_0_509; 
                    v_v_v_window_28_2_453_1_483 = v_v_v_window_28_11_462_1_510; 
                    v_v_v_window_28_2_453_2_484 = v_v_v_window_28_11_462_2_511; 
                    v_v_v_window_28_11_462_0_509 = v_v_v_window_28_20_471_0_536; 
                    v_v_v_window_28_11_462_1_510 = v_v_v_window_28_20_471_1_537; 
                    v_v_v_window_28_11_462_2_511 = v_v_v_window_28_20_471_2_538; 
                    v_v_v_window_28_3_454_0_485 = v_v_v_window_28_12_463_0_512; 
                    v_v_v_window_28_3_454_1_486 = v_v_v_window_28_12_463_1_513; 
                    v_v_v_window_28_3_454_2_487 = v_v_v_window_28_12_463_2_514; 
                    v_v_v_window_28_12_463_0_512 = v_v_v_window_28_21_472_0_539; 
                    v_v_v_window_28_12_463_1_513 = v_v_v_window_28_21_472_1_540; 
                    v_v_v_window_28_12_463_2_514 = v_v_v_window_28_21_472_2_541; 
                    v_v_v_window_28_4_455_0_488 = v_v_v_window_28_13_464_0_515; 
                    v_v_v_window_28_4_455_1_489 = v_v_v_window_28_13_464_1_516; 
                    v_v_v_window_28_4_455_2_490 = v_v_v_window_28_13_464_2_517; 
                    v_v_v_window_28_13_464_0_515 = v_v_v_window_28_22_473_0_542; 
                    v_v_v_window_28_13_464_1_516 = v_v_v_window_28_22_473_1_543; 
                    v_v_v_window_28_13_464_2_517 = v_v_v_window_28_22_473_2_544; 
                    v_v_v_window_28_5_456_0_491 = v_v_v_window_28_14_465_0_518; 
                    v_v_v_window_28_5_456_1_492 = v_v_v_window_28_14_465_1_519; 
                    v_v_v_window_28_5_456_2_493 = v_v_v_window_28_14_465_2_520; 
                    v_v_v_window_28_14_465_0_518 = v_v_v_window_28_23_474_0_545; 
                    v_v_v_window_28_14_465_1_519 = v_v_v_window_28_23_474_1_546; 
                    v_v_v_window_28_14_465_2_520 = v_v_v_window_28_23_474_2_547; 
                    v_v_v_window_28_6_457_0_494 = v_v_v_window_28_15_466_0_521; 
                    v_v_v_window_28_6_457_1_495 = v_v_v_window_28_15_466_1_522; 
                    v_v_v_window_28_6_457_2_496 = v_v_v_window_28_15_466_2_523; 
                    v_v_v_window_28_15_466_0_521 = v_v_v_window_28_24_475_0_548; 
                    v_v_v_window_28_15_466_1_522 = v_v_v_window_28_24_475_1_549; 
                    v_v_v_window_28_15_466_2_523 = v_v_v_window_28_24_475_2_550; 
                    v_v_v_window_28_7_458_0_497 = v_v_v_window_28_16_467_0_524; 
                    v_v_v_window_28_7_458_1_498 = v_v_v_window_28_16_467_1_525; 
                    v_v_v_window_28_7_458_2_499 = v_v_v_window_28_16_467_2_526; 
                    v_v_v_window_28_16_467_0_524 = v_v_v_window_28_25_476_0_551; 
                    v_v_v_window_28_16_467_1_525 = v_v_v_window_28_25_476_1_552; 
                    v_v_v_window_28_16_467_2_526 = v_v_v_window_28_25_476_2_553; 
                    v_v_v_window_28_8_459_0_500 = v_v_v_window_28_17_468_0_527; 
                    v_v_v_window_28_8_459_1_501 = v_v_v_window_28_17_468_1_528; 
                    v_v_v_window_28_8_459_2_502 = v_v_v_window_28_17_468_2_529; 
                    v_v_v_window_28_17_468_0_527 = v_v_v_window_28_26_477_0_554; 
                    v_v_v_window_28_17_468_1_528 = v_v_v_window_28_26_477_1_555; 
                    v_v_v_window_28_17_468_2_529 = v_v_v_window_28_26_477_2_556; 
                    v_v_v_window_28_9_460_0_503 = v_v_v_window_28_18_469_0_530; 
                    v_v_v_window_28_9_460_1_504 = v_v_v_window_28_18_469_1_531; 
                    v_v_v_window_28_9_460_2_505 = v_v_v_window_28_18_469_2_532; 
                    v_v_v_window_28_18_469_0_530 = v_v_v_window_28_27_478_0_557; 
                    v_v_v_window_28_18_469_1_531 = v_v_v_window_28_27_478_1_558; 
                    v_v_v_window_28_18_469_2_532 = v_v_v_window_28_27_478_2_559; 
                }
                // end mapSeqSlide
            }
        }
    }
}