#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 64 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,300) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(256,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float heat(float C, float S, float N, float E, float W, float B, float F){
    {
        return 0.125f * (B - 2.0f * C + F) +
       0.125f * (S - 2.0f * C + N) +
       0.125f * (E - 2.0f * C + W) + C;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__70, global float* v__73){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__72; 
        for (int v_gl_id_67 = get_global_id(0); (v_gl_id_67 < 510); v_gl_id_67 = (v_gl_id_67 + get_global_size(0))){
            for (int v_gl_id_68 = get_global_id(1); (v_gl_id_68 < 510); v_gl_id_68 = (v_gl_id_68 + get_global_size(1))){
                // slideSeq_plus
                float v_window_1_95; 
                float v_window_2_96; 
                float v_window_3_97; 
                float v_window_4_98; 
                float v_window_5_99; 
                float v_window_6_100; 
                float v_window_7_101; 
                float v_window_8_102; 
                float v_window_9_103; 
                float v_window_10_104; 
                float v_window_11_105; 
                float v_window_12_106; 
                float v_window_13_107; 
                float v_window_14_108; 
                float v_window_15_109; 
                float v_window_16_110; 
                float v_window_17_111; 
                float v_window_18_112; 
                float v_window_19_113; 
                float v_window_20_114; 
                float v_window_21_115; 
                float v_window_22_116; 
                float v_window_23_117; 
                float v_window_24_118; 
                float v_window_25_119; 
                float v_window_26_120; 
                float v_window_27_121; 
                v_window_1_95 = v__70[(v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_2_96 = v__70[(262144 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_4_98 = v__70[(512 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_5_99 = v__70[(262656 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_7_101 = v__70[(1024 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_8_102 = v__70[(263168 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_10_104 = v__70[(1 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_11_105 = v__70[(262145 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_13_107 = v__70[(513 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_14_108 = v__70[(262657 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_16_110 = v__70[(1025 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_17_111 = v__70[(263169 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_19_113 = v__70[(2 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_20_114 = v__70[(262146 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_22_116 = v__70[(514 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_23_117 = v__70[(262658 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_25_119 = v__70[(1026 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_26_120 = v__70[(263170 + v_gl_id_67 + (512 * v_gl_id_68))]; 
#pragma unroll 1
                for (int v_i_69 = 0; (v_i_69 < 62); v_i_69 = (1 + v_i_69)){
                    v_window_3_97 = v__70[(524288 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_6_100 = v__70[(524800 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_9_103 = v__70[(525312 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_12_106 = v__70[(524289 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_15_109 = v__70[(524801 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_18_112 = v__70[(525313 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_21_115 = v__70[(524290 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_24_118 = v__70[(524802 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_27_121 = v__70[(525314 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v__72 = heat(v_window_14_108, v_window_11_105, v_window_17_111, v_window_15_109, v_window_13_107, v_window_5_99, v_window_23_117); 
                    v__73[(262657 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))] = id(v__72); 
                    v_window_1_95 = v_window_2_96; 
                    v_window_2_96 = v_window_3_97; 
                    v_window_4_98 = v_window_5_99; 
                    v_window_5_99 = v_window_6_100; 
                    v_window_7_101 = v_window_8_102; 
                    v_window_8_102 = v_window_9_103; 
                    v_window_10_104 = v_window_11_105; 
                    v_window_11_105 = v_window_12_106; 
                    v_window_13_107 = v_window_14_108; 
                    v_window_14_108 = v_window_15_109; 
                    v_window_16_110 = v_window_17_111; 
                    v_window_17_111 = v_window_18_112; 
                    v_window_19_113 = v_window_20_114; 
                    v_window_20_114 = v_window_21_115; 
                    v_window_22_116 = v_window_23_117; 
                    v_window_23_117 = v_window_24_118; 
                    v_window_25_119 = v_window_26_120; 
                    v_window_26_120 = v_window_27_121; 
                }
                // end slideSeq_plus
            }
        }
    }
}
