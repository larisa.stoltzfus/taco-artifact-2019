#atf::var<int> v_M_0 = 128 
#atf::var<int> v_N_1 = 128 
#atf::var<int> v_O_2 = 32 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(128,128)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(128,128)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(128,128)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float poisson(float C, float N, float S, float E, float W, float F, float B, float FN, float BN, float FS, float BS, float FW, float BW, float NW, float SW, float FE, float BE, float NE, float SE){
    {
        return 2.666f * C - 0.166f * (F + B + N + S + E + W) -
       0.0833f * (FN + BN + FS + BS + FW + BW +
                  NW + SW + FE + BE + NE + SE);; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__82, global float* v__85){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__84; 
        for (int v_gl_id_79 = get_global_id(0); (v_gl_id_79 < 126); v_gl_id_79 = (v_gl_id_79 + get_global_size(0))){
            for (int v_gl_id_80 = get_global_id(1); (v_gl_id_80 < 126); v_gl_id_80 = (v_gl_id_80 + get_global_size(1))){
                // slideSeq_plus
                float v_window_1_105; 
                float v_window_2_106; 
                float v_window_3_107; 
                float v_window_4_108; 
                float v_window_5_109; 
                float v_window_6_110; 
                float v_window_7_111; 
                float v_window_8_112; 
                float v_window_9_113; 
                float v_window_10_114; 
                float v_window_11_115; 
                float v_window_12_116; 
                float v_window_13_117; 
                float v_window_14_118; 
                float v_window_15_119; 
                float v_window_16_120; 
                float v_window_17_121; 
                float v_window_18_122; 
                float v_window_19_123; 
                float v_window_20_124; 
                float v_window_21_125; 
                float v_window_22_126; 
                float v_window_23_127; 
                float v_window_24_128; 
                float v_window_25_129; 
                float v_window_26_130; 
                float v_window_27_131; 
                v_window_1_105 = v__82[(v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_2_106 = v__82[(16384 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_4_108 = v__82[(128 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_5_109 = v__82[(16512 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_7_111 = v__82[(256 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_8_112 = v__82[(16640 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_10_114 = v__82[(1 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_11_115 = v__82[(16385 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_13_117 = v__82[(129 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_14_118 = v__82[(16513 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_16_120 = v__82[(257 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_17_121 = v__82[(16641 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_19_123 = v__82[(2 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_20_124 = v__82[(16386 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_22_126 = v__82[(130 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_23_127 = v__82[(16514 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_25_129 = v__82[(258 + v_gl_id_79 + (128 * v_gl_id_80))]; 
                v_window_26_130 = v__82[(16642 + v_gl_id_79 + (128 * v_gl_id_80))]; 
#pragma unroll 1
for (int v_i_81 = 0; (v_i_81 < 30); v_i_81 = (1 + v_i_81)){
                    v_window_3_107 = v__82[(32768 + v_gl_id_79 + (128 * v_gl_id_80) + (16384 * v_i_81))]; 
                    v_window_6_110 = v__82[(32896 + v_gl_id_79 + (128 * v_gl_id_80) + (16384 * v_i_81))]; 
                    v_window_9_113 = v__82[(33024 + v_gl_id_79 + (128 * v_gl_id_80) + (16384 * v_i_81))]; 
                    v_window_12_116 = v__82[(32769 + v_gl_id_79 + (128 * v_gl_id_80) + (16384 * v_i_81))]; 
                    v_window_15_119 = v__82[(32897 + v_gl_id_79 + (128 * v_gl_id_80) + (16384 * v_i_81))]; 
                    v_window_18_122 = v__82[(33025 + v_gl_id_79 + (128 * v_gl_id_80) + (16384 * v_i_81))]; 
                    v_window_21_125 = v__82[(32770 + v_gl_id_79 + (128 * v_gl_id_80) + (16384 * v_i_81))]; 
                    v_window_24_128 = v__82[(32898 + v_gl_id_79 + (128 * v_gl_id_80) + (16384 * v_i_81))]; 
                    v_window_27_131 = v__82[(33026 + v_gl_id_79 + (128 * v_gl_id_80) + (16384 * v_i_81))]; 
                    v__84 = poisson(v_window_14_118, v_window_11_115, v_window_17_121, v_window_15_119, v_window_13_117, v_window_5_109, v_window_23_127, v_window_2_106, v_window_20_124, v_window_8_112, v_window_26_130, v_window_4_108, v_window_22_126, v_window_10_114, v_window_16_120, v_window_6_110, v_window_24_128, v_window_12_116, v_window_18_122); 
                    v__85[(16513 + v_gl_id_79 + (128 * v_gl_id_80) + (16384 * v_i_81))] = id(v__84); 
                    v_window_1_105 = v_window_2_106; 
                    v_window_2_106 = v_window_3_107; 
                    v_window_4_108 = v_window_5_109; 
                    v_window_5_109 = v_window_6_110; 
                    v_window_7_111 = v_window_8_112; 
                    v_window_8_112 = v_window_9_113; 
                    v_window_10_114 = v_window_11_115; 
                    v_window_11_115 = v_window_12_116; 
                    v_window_13_117 = v_window_14_118; 
                    v_window_14_118 = v_window_15_119; 
                    v_window_16_120 = v_window_17_121; 
                    v_window_17_121 = v_window_18_122; 
                    v_window_19_123 = v_window_20_124; 
                    v_window_20_124 = v_window_21_125; 
                    v_window_22_126 = v_window_23_127; 
                    v_window_23_127 = v_window_24_128; 
                    v_window_25_129 = v_window_26_130; 
                    v_window_26_130 = v_window_27_131; 
                }
                // end slideSeq_plus
            }
        }
    }
}
