#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 64 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.000,500) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(128,128)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: int
#ifndef Tuple3_float_float_int_DEFINED
#define Tuple3_float_float_int_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
    int _2;
} Tuple3_float_float_int;
#endif

float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float addTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 + x._1;}; 
    }
}
float subtract(float l, float r){
    {
        { return l - r; }; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
float subtractTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 - x._1;}; 
    }
}
int idxF(int i, int j, int k, int m, int n, int o){
    {
        {int count = 6; if(i == (m-1) || i == 0){ count--; } if(j == (n-1) || j == 0){ count--; } if(k == (o-1) || k == 0){ count--; }return count; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
float getCF(int neigh, float cfB, float cfI){
    {
        { if(neigh < 6) { return cfB; } else{ return cfI;} }; 
    }
}
float idIF(int x){
    {
        { return (float)(x*1.0); }; 
    }
}
kernel void KERNEL(const global float* restrict v__204, const global float* restrict v__205, global float* v__247){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        float v__210; 
        float v__213; 
        float v__228; 
        float v__231; 
        float v__232; 
        float v__241; 
        float v__242; 
        // Private Memory
<<<<<<< HEAD:raw_files/acoustic-MSS-512-n-64.cl
        float v__209; 
        float v__212; 
        float v__215; 
        float v__217; 
        float v__219; 
        float v__221; 
        float v__223; 
        float v__225; 
        float v__227; 
        float v__230; 
        float v__234; 
        float v__236; 
        float v__238; 
        float v__240; 
        float v__244; 
        float v__246; 
        for (int v_gl_id_201 = get_global_id(0); (v_gl_id_201 < 512); v_gl_id_201 = (v_gl_id_201 + get_global_size(0))){
            for (int v_gl_id_202 = get_global_id(1); (v_gl_id_202 < 512); v_gl_id_202 = (v_gl_id_202 + get_global_size(1))){
                // mapSeqSlide
                float v_v_v_window_28_1_452_0_479; 
                float v_v_v_window_28_1_452_1_480; 
                int v_v_v_window_28_1_452_2_481; 
                float v_v_v_window_28_2_453_0_482; 
                float v_v_v_window_28_2_453_1_483; 
                int v_v_v_window_28_2_453_2_484; 
                float v_v_v_window_28_3_454_0_485; 
                float v_v_v_window_28_3_454_1_486; 
                int v_v_v_window_28_3_454_2_487; 
                float v_v_v_window_28_4_455_0_488; 
                float v_v_v_window_28_4_455_1_489; 
                int v_v_v_window_28_4_455_2_490; 
                float v_v_v_window_28_5_456_0_491; 
                float v_v_v_window_28_5_456_1_492; 
                int v_v_v_window_28_5_456_2_493; 
                float v_v_v_window_28_6_457_0_494; 
                float v_v_v_window_28_6_457_1_495; 
                int v_v_v_window_28_6_457_2_496; 
                float v_v_v_window_28_7_458_0_497; 
                float v_v_v_window_28_7_458_1_498; 
                int v_v_v_window_28_7_458_2_499; 
                float v_v_v_window_28_8_459_0_500; 
                float v_v_v_window_28_8_459_1_501; 
                int v_v_v_window_28_8_459_2_502; 
                float v_v_v_window_28_9_460_0_503; 
                float v_v_v_window_28_9_460_1_504; 
                int v_v_v_window_28_9_460_2_505; 
                float v_v_v_window_28_10_461_0_506; 
                float v_v_v_window_28_10_461_1_507; 
                int v_v_v_window_28_10_461_2_508; 
                float v_v_v_window_28_11_462_0_509; 
                float v_v_v_window_28_11_462_1_510; 
                int v_v_v_window_28_11_462_2_511; 
                float v_v_v_window_28_12_463_0_512; 
                float v_v_v_window_28_12_463_1_513; 
                int v_v_v_window_28_12_463_2_514; 
                float v_v_v_window_28_13_464_0_515; 
                float v_v_v_window_28_13_464_1_516; 
                int v_v_v_window_28_13_464_2_517; 
                float v_v_v_window_28_14_465_0_518; 
                float v_v_v_window_28_14_465_1_519; 
                int v_v_v_window_28_14_465_2_520; 
                float v_v_v_window_28_15_466_0_521; 
                float v_v_v_window_28_15_466_1_522; 
                int v_v_v_window_28_15_466_2_523; 
                float v_v_v_window_28_16_467_0_524; 
                float v_v_v_window_28_16_467_1_525; 
                int v_v_v_window_28_16_467_2_526; 
                float v_v_v_window_28_17_468_0_527; 
                float v_v_v_window_28_17_468_1_528; 
                int v_v_v_window_28_17_468_2_529; 
                float v_v_v_window_28_18_469_0_530; 
                float v_v_v_window_28_18_469_1_531; 
                int v_v_v_window_28_18_469_2_532; 
                float v_v_v_window_28_19_470_0_533; 
                float v_v_v_window_28_19_470_1_534; 
                int v_v_v_window_28_19_470_2_535; 
                float v_v_v_window_28_20_471_0_536; 
                float v_v_v_window_28_20_471_1_537; 
                int v_v_v_window_28_20_471_2_538; 
                float v_v_v_window_28_21_472_0_539; 
                float v_v_v_window_28_21_472_1_540; 
                int v_v_v_window_28_21_472_2_541; 
                float v_v_v_window_28_22_473_0_542; 
                float v_v_v_window_28_22_473_1_543; 
                int v_v_v_window_28_22_473_2_544; 
                float v_v_v_window_28_23_474_0_545; 
                float v_v_v_window_28_23_474_1_546; 
                int v_v_v_window_28_23_474_2_547; 
                float v_v_v_window_28_24_475_0_548; 
                float v_v_v_window_28_24_475_1_549; 
                int v_v_v_window_28_24_475_2_550; 
                float v_v_v_window_28_25_476_0_551; 
                float v_v_v_window_28_25_476_1_552; 
                int v_v_v_window_28_25_476_2_553; 
                float v_v_v_window_28_26_477_0_554; 
                float v_v_v_window_28_26_477_1_555; 
                int v_v_v_window_28_26_477_2_556; 
                float v_v_v_window_28_27_478_0_557; 
                float v_v_v_window_28_27_478_1_558; 
                int v_v_v_window_28_27_478_2_559; 
                {
                    Tuple3_float_float_int v_v_v_window_28_1_452_tmp_560; 
                    v_v_v_window_28_1_452_tmp_560 = (Tuple3_float_float_int){v__204[(v_gl_id_201 + (514 * v_gl_id_202))], v__205[(v_gl_id_201 + (514 * v_gl_id_202))], idxF(0, v_gl_id_202, v_gl_id_201, 66, 514, 514)}; 
                    v_v_v_window_28_1_452_0_479 = v_v_v_window_28_1_452_tmp_560._0; 
                    v_v_v_window_28_1_452_1_480 = v_v_v_window_28_1_452_tmp_560._1; 
                    v_v_v_window_28_1_452_2_481 = v_v_v_window_28_1_452_tmp_560._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_10_461_tmp_561; 
                    v_v_v_window_28_10_461_tmp_561 = (Tuple3_float_float_int){v__204[(264196 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(264196 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(1, v_gl_id_202, v_gl_id_201, 66, 514, 514)}; 
                    v_v_v_window_28_10_461_0_506 = v_v_v_window_28_10_461_tmp_561._0; 
                    v_v_v_window_28_10_461_1_507 = v_v_v_window_28_10_461_tmp_561._1; 
                    v_v_v_window_28_10_461_2_508 = v_v_v_window_28_10_461_tmp_561._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_4_455_tmp_562; 
                    v_v_v_window_28_4_455_tmp_562 = (Tuple3_float_float_int){v__204[(514 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(514 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(0, (1 + v_gl_id_202), v_gl_id_201, 66, 514, 514)}; 
                    v_v_v_window_28_4_455_0_488 = v_v_v_window_28_4_455_tmp_562._0; 
                    v_v_v_window_28_4_455_1_489 = v_v_v_window_28_4_455_tmp_562._1; 
                    v_v_v_window_28_4_455_2_490 = v_v_v_window_28_4_455_tmp_562._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_13_464_tmp_563; 
                    v_v_v_window_28_13_464_tmp_563 = (Tuple3_float_float_int){v__204[(264710 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(264710 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(1, (1 + v_gl_id_202), v_gl_id_201, 66, 514, 514)}; 
                    v_v_v_window_28_13_464_0_515 = v_v_v_window_28_13_464_tmp_563._0; 
                    v_v_v_window_28_13_464_1_516 = v_v_v_window_28_13_464_tmp_563._1; 
                    v_v_v_window_28_13_464_2_517 = v_v_v_window_28_13_464_tmp_563._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_7_458_tmp_564; 
                    v_v_v_window_28_7_458_tmp_564 = (Tuple3_float_float_int){v__204[(1028 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(1028 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(0, (2 + v_gl_id_202), v_gl_id_201, 66, 514, 514)}; 
                    v_v_v_window_28_7_458_0_497 = v_v_v_window_28_7_458_tmp_564._0; 
                    v_v_v_window_28_7_458_1_498 = v_v_v_window_28_7_458_tmp_564._1; 
                    v_v_v_window_28_7_458_2_499 = v_v_v_window_28_7_458_tmp_564._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_16_467_tmp_565; 
                    v_v_v_window_28_16_467_tmp_565 = (Tuple3_float_float_int){v__204[(265224 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(265224 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(1, (2 + v_gl_id_202), v_gl_id_201, 66, 514, 514)}; 
                    v_v_v_window_28_16_467_0_524 = v_v_v_window_28_16_467_tmp_565._0; 
                    v_v_v_window_28_16_467_1_525 = v_v_v_window_28_16_467_tmp_565._1; 
                    v_v_v_window_28_16_467_2_526 = v_v_v_window_28_16_467_tmp_565._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_2_453_tmp_566; 
                    v_v_v_window_28_2_453_tmp_566 = (Tuple3_float_float_int){v__204[(1 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(1 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(0, v_gl_id_202, (1 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_2_453_0_482 = v_v_v_window_28_2_453_tmp_566._0; 
                    v_v_v_window_28_2_453_1_483 = v_v_v_window_28_2_453_tmp_566._1; 
                    v_v_v_window_28_2_453_2_484 = v_v_v_window_28_2_453_tmp_566._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_11_462_tmp_567; 
                    v_v_v_window_28_11_462_tmp_567 = (Tuple3_float_float_int){v__204[(264197 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(264197 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(1, v_gl_id_202, (1 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_11_462_0_509 = v_v_v_window_28_11_462_tmp_567._0; 
                    v_v_v_window_28_11_462_1_510 = v_v_v_window_28_11_462_tmp_567._1; 
                    v_v_v_window_28_11_462_2_511 = v_v_v_window_28_11_462_tmp_567._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_5_456_tmp_568; 
                    v_v_v_window_28_5_456_tmp_568 = (Tuple3_float_float_int){v__204[(515 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(515 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(0, (1 + v_gl_id_202), (1 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_5_456_0_491 = v_v_v_window_28_5_456_tmp_568._0; 
                    v_v_v_window_28_5_456_1_492 = v_v_v_window_28_5_456_tmp_568._1; 
                    v_v_v_window_28_5_456_2_493 = v_v_v_window_28_5_456_tmp_568._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_14_465_tmp_569; 
                    v_v_v_window_28_14_465_tmp_569 = (Tuple3_float_float_int){v__204[(264711 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(264711 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(1, (1 + v_gl_id_202), (1 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_14_465_0_518 = v_v_v_window_28_14_465_tmp_569._0; 
                    v_v_v_window_28_14_465_1_519 = v_v_v_window_28_14_465_tmp_569._1; 
                    v_v_v_window_28_14_465_2_520 = v_v_v_window_28_14_465_tmp_569._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_8_459_tmp_570; 
                    v_v_v_window_28_8_459_tmp_570 = (Tuple3_float_float_int){v__204[(1029 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(1029 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(0, (2 + v_gl_id_202), (1 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_8_459_0_500 = v_v_v_window_28_8_459_tmp_570._0; 
                    v_v_v_window_28_8_459_1_501 = v_v_v_window_28_8_459_tmp_570._1; 
                    v_v_v_window_28_8_459_2_502 = v_v_v_window_28_8_459_tmp_570._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_17_468_tmp_571; 
                    v_v_v_window_28_17_468_tmp_571 = (Tuple3_float_float_int){v__204[(265225 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(265225 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(1, (2 + v_gl_id_202), (1 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_17_468_0_527 = v_v_v_window_28_17_468_tmp_571._0; 
                    v_v_v_window_28_17_468_1_528 = v_v_v_window_28_17_468_tmp_571._1; 
                    v_v_v_window_28_17_468_2_529 = v_v_v_window_28_17_468_tmp_571._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_3_454_tmp_572; 
                    v_v_v_window_28_3_454_tmp_572 = (Tuple3_float_float_int){v__204[(2 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(2 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(0, v_gl_id_202, (2 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_3_454_0_485 = v_v_v_window_28_3_454_tmp_572._0; 
                    v_v_v_window_28_3_454_1_486 = v_v_v_window_28_3_454_tmp_572._1; 
                    v_v_v_window_28_3_454_2_487 = v_v_v_window_28_3_454_tmp_572._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_12_463_tmp_573; 
                    v_v_v_window_28_12_463_tmp_573 = (Tuple3_float_float_int){v__204[(264198 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(264198 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(1, v_gl_id_202, (2 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_12_463_0_512 = v_v_v_window_28_12_463_tmp_573._0; 
                    v_v_v_window_28_12_463_1_513 = v_v_v_window_28_12_463_tmp_573._1; 
                    v_v_v_window_28_12_463_2_514 = v_v_v_window_28_12_463_tmp_573._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_6_457_tmp_574; 
                    v_v_v_window_28_6_457_tmp_574 = (Tuple3_float_float_int){v__204[(516 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(516 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(0, (1 + v_gl_id_202), (2 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_6_457_0_494 = v_v_v_window_28_6_457_tmp_574._0; 
                    v_v_v_window_28_6_457_1_495 = v_v_v_window_28_6_457_tmp_574._1; 
                    v_v_v_window_28_6_457_2_496 = v_v_v_window_28_6_457_tmp_574._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_15_466_tmp_575; 
                    v_v_v_window_28_15_466_tmp_575 = (Tuple3_float_float_int){v__204[(264712 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(264712 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(1, (1 + v_gl_id_202), (2 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_15_466_0_521 = v_v_v_window_28_15_466_tmp_575._0; 
                    v_v_v_window_28_15_466_1_522 = v_v_v_window_28_15_466_tmp_575._1; 
                    v_v_v_window_28_15_466_2_523 = v_v_v_window_28_15_466_tmp_575._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_9_460_tmp_576; 
                    v_v_v_window_28_9_460_tmp_576 = (Tuple3_float_float_int){v__204[(1030 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(1030 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(0, (2 + v_gl_id_202), (2 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_9_460_0_503 = v_v_v_window_28_9_460_tmp_576._0; 
                    v_v_v_window_28_9_460_1_504 = v_v_v_window_28_9_460_tmp_576._1; 
                    v_v_v_window_28_9_460_2_505 = v_v_v_window_28_9_460_tmp_576._2; 
                }
                {
                    Tuple3_float_float_int v_v_v_window_28_18_469_tmp_577; 
                    v_v_v_window_28_18_469_tmp_577 = (Tuple3_float_float_int){v__204[(265226 + v_gl_id_201 + (514 * v_gl_id_202))], v__205[(265226 + v_gl_id_201 + (514 * v_gl_id_202))], idxF(1, (2 + v_gl_id_202), (2 + v_gl_id_201), 66, 514, 514)}; 
                    v_v_v_window_28_18_469_0_530 = v_v_v_window_28_18_469_tmp_577._0; 
                    v_v_v_window_28_18_469_1_531 = v_v_v_window_28_18_469_tmp_577._1; 
                    v_v_v_window_28_18_469_2_532 = v_v_v_window_28_18_469_tmp_577._2; 
                }
                for (int v_i_203 = 0; (v_i_203 < 64); v_i_203 = (1 + v_i_203)){
                    {
                        Tuple3_float_float_int v_v_v_window_28_19_470_tmp_578; 
                        v_v_v_window_28_19_470_tmp_578 = (Tuple3_float_float_int){v__204[(528392 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], v__205[(528392 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], idxF((2 + v_i_203), v_gl_id_202, v_gl_id_201, 66, 514, 514)}; 
                        v_v_v_window_28_19_470_0_533 = v_v_v_window_28_19_470_tmp_578._0; 
                        v_v_v_window_28_19_470_1_534 = v_v_v_window_28_19_470_tmp_578._1; 
                        v_v_v_window_28_19_470_2_535 = v_v_v_window_28_19_470_tmp_578._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_22_473_tmp_579; 
                        v_v_v_window_28_22_473_tmp_579 = (Tuple3_float_float_int){v__204[(528906 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], v__205[(528906 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], idxF((2 + v_i_203), (1 + v_gl_id_202), v_gl_id_201, 66, 514, 514)}; 
                        v_v_v_window_28_22_473_0_542 = v_v_v_window_28_22_473_tmp_579._0; 
                        v_v_v_window_28_22_473_1_543 = v_v_v_window_28_22_473_tmp_579._1; 
                        v_v_v_window_28_22_473_2_544 = v_v_v_window_28_22_473_tmp_579._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_25_476_tmp_580; 
                        v_v_v_window_28_25_476_tmp_580 = (Tuple3_float_float_int){v__204[(529420 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], v__205[(529420 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], idxF((2 + v_i_203), (2 + v_gl_id_202), v_gl_id_201, 66, 514, 514)}; 
                        v_v_v_window_28_25_476_0_551 = v_v_v_window_28_25_476_tmp_580._0; 
                        v_v_v_window_28_25_476_1_552 = v_v_v_window_28_25_476_tmp_580._1; 
                        v_v_v_window_28_25_476_2_553 = v_v_v_window_28_25_476_tmp_580._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_20_471_tmp_581; 
                        v_v_v_window_28_20_471_tmp_581 = (Tuple3_float_float_int){v__204[(528393 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], v__205[(528393 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], idxF((2 + v_i_203), v_gl_id_202, (1 + v_gl_id_201), 66, 514, 514)}; 
                        v_v_v_window_28_20_471_0_536 = v_v_v_window_28_20_471_tmp_581._0; 
                        v_v_v_window_28_20_471_1_537 = v_v_v_window_28_20_471_tmp_581._1; 
                        v_v_v_window_28_20_471_2_538 = v_v_v_window_28_20_471_tmp_581._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_23_474_tmp_582; 
                        v_v_v_window_28_23_474_tmp_582 = (Tuple3_float_float_int){v__204[(528907 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], v__205[(528907 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], idxF((2 + v_i_203), (1 + v_gl_id_202), (1 + v_gl_id_201), 66, 514, 514)}; 
                        v_v_v_window_28_23_474_0_545 = v_v_v_window_28_23_474_tmp_582._0; 
                        v_v_v_window_28_23_474_1_546 = v_v_v_window_28_23_474_tmp_582._1; 
                        v_v_v_window_28_23_474_2_547 = v_v_v_window_28_23_474_tmp_582._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_26_477_tmp_583; 
                        v_v_v_window_28_26_477_tmp_583 = (Tuple3_float_float_int){v__204[(529421 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], v__205[(529421 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], idxF((2 + v_i_203), (2 + v_gl_id_202), (1 + v_gl_id_201), 66, 514, 514)}; 
                        v_v_v_window_28_26_477_0_554 = v_v_v_window_28_26_477_tmp_583._0; 
                        v_v_v_window_28_26_477_1_555 = v_v_v_window_28_26_477_tmp_583._1; 
                        v_v_v_window_28_26_477_2_556 = v_v_v_window_28_26_477_tmp_583._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_21_472_tmp_584; 
                        v_v_v_window_28_21_472_tmp_584 = (Tuple3_float_float_int){v__204[(528394 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], v__205[(528394 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], idxF((2 + v_i_203), v_gl_id_202, (2 + v_gl_id_201), 66, 514, 514)}; 
                        v_v_v_window_28_21_472_0_539 = v_v_v_window_28_21_472_tmp_584._0; 
                        v_v_v_window_28_21_472_1_540 = v_v_v_window_28_21_472_tmp_584._1; 
                        v_v_v_window_28_21_472_2_541 = v_v_v_window_28_21_472_tmp_584._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_24_475_tmp_585; 
                        v_v_v_window_28_24_475_tmp_585 = (Tuple3_float_float_int){v__204[(528908 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], v__205[(528908 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], idxF((2 + v_i_203), (1 + v_gl_id_202), (2 + v_gl_id_201), 66, 514, 514)}; 
                        v_v_v_window_28_24_475_0_548 = v_v_v_window_28_24_475_tmp_585._0; 
                        v_v_v_window_28_24_475_1_549 = v_v_v_window_28_24_475_tmp_585._1; 
                        v_v_v_window_28_24_475_2_550 = v_v_v_window_28_24_475_tmp_585._2; 
                    }
                    {
                        Tuple3_float_float_int v_v_v_window_28_27_478_tmp_586; 
                        v_v_v_window_28_27_478_tmp_586 = (Tuple3_float_float_int){v__204[(529422 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], v__205[(529422 + v_gl_id_201 + (514 * v_gl_id_202) + (264196 * v_i_203))], idxF((2 + v_i_203), (2 + v_gl_id_202), (2 + v_gl_id_201), 66, 514, 514)}; 
                        v_v_v_window_28_27_478_0_557 = v_v_v_window_28_27_478_tmp_586._0; 
                        v_v_v_window_28_27_478_1_558 = v_v_v_window_28_27_478_tmp_586._1; 
                        v_v_v_window_28_27_478_2_559 = v_v_v_window_28_27_478_tmp_586._2; 
                    }
                    v__209 = idIF(v_v_v_window_28_14_465_2_520); 
                    float v_tmp_445 = 0.3333333f; 
                    v__210 = v_tmp_445; 
                    v__212 = mult(v__209, v__210); 
                    float v_tmp_446 = 2.0f; 
                    v__213 = v_tmp_446; 
                    v__215 = subtract(v__213, v__212); 
                    v__217 = mult(v__215, v_v_v_window_28_14_465_1_519); 
                    v__219 = add(v_v_v_window_28_23_474_1_546, v_v_v_window_28_17_468_1_528); 
                    v__221 = add(v__219, v_v_v_window_28_15_466_1_522); 
                    v__223 = add(v__221, v_v_v_window_28_13_464_1_516); 
                    v__225 = add(v__223, v_v_v_window_28_11_462_1_510); 
                    v__227 = add(v__225, v_v_v_window_28_5_456_1_492); 
                    float v_tmp_447 = 0.3333333f; 
                    v__228 = v_tmp_447; 
                    v__230 = mult(v__227, v__228); 
                    float v_tmp_448 = 0.9971132f; 
                    v__231 = v_tmp_448; 
                    float v_tmp_449 = 1.0f; 
                    v__232 = v_tmp_449; 
                    v__234 = getCF(v_v_v_window_28_14_465_2_520, v__231, v__232); 
                    v__236 = mult(v_v_v_window_28_14_465_0_518, v__234); 
                    v__238 = subtractTuple((Tuple2_float_float){v__230, v__236}); 
                    v__240 = addTuple((Tuple2_float_float){v__217, v__238}); 
                    float v_tmp_450 = 0.9971216f; 
                    v__241 = v_tmp_450; 
                    float v_tmp_451 = 1.0f; 
                    v__242 = v_tmp_451; 
                    v__244 = getCF(v_v_v_window_28_14_465_2_520, v__241, v__242); 
                    v__246 = mult(v__240, v__244); 
                    v__247[(v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))] = id(v__246); 
                    v_v_v_window_28_1_452_0_479 = v_v_v_window_28_10_461_0_506; 
                    v_v_v_window_28_1_452_1_480 = v_v_v_window_28_10_461_1_507; 
                    v_v_v_window_28_1_452_2_481 = v_v_v_window_28_10_461_2_508; 
                    v_v_v_window_28_10_461_0_506 = v_v_v_window_28_19_470_0_533; 
                    v_v_v_window_28_10_461_1_507 = v_v_v_window_28_19_470_1_534; 
                    v_v_v_window_28_10_461_2_508 = v_v_v_window_28_19_470_2_535; 
                    v_v_v_window_28_2_453_0_482 = v_v_v_window_28_11_462_0_509; 
                    v_v_v_window_28_2_453_1_483 = v_v_v_window_28_11_462_1_510; 
                    v_v_v_window_28_2_453_2_484 = v_v_v_window_28_11_462_2_511; 
                    v_v_v_window_28_11_462_0_509 = v_v_v_window_28_20_471_0_536; 
                    v_v_v_window_28_11_462_1_510 = v_v_v_window_28_20_471_1_537; 
                    v_v_v_window_28_11_462_2_511 = v_v_v_window_28_20_471_2_538; 
                    v_v_v_window_28_3_454_0_485 = v_v_v_window_28_12_463_0_512; 
                    v_v_v_window_28_3_454_1_486 = v_v_v_window_28_12_463_1_513; 
                    v_v_v_window_28_3_454_2_487 = v_v_v_window_28_12_463_2_514; 
                    v_v_v_window_28_12_463_0_512 = v_v_v_window_28_21_472_0_539; 
                    v_v_v_window_28_12_463_1_513 = v_v_v_window_28_21_472_1_540; 
                    v_v_v_window_28_12_463_2_514 = v_v_v_window_28_21_472_2_541; 
                    v_v_v_window_28_4_455_0_488 = v_v_v_window_28_13_464_0_515; 
                    v_v_v_window_28_4_455_1_489 = v_v_v_window_28_13_464_1_516; 
                    v_v_v_window_28_4_455_2_490 = v_v_v_window_28_13_464_2_517; 
                    v_v_v_window_28_13_464_0_515 = v_v_v_window_28_22_473_0_542; 
                    v_v_v_window_28_13_464_1_516 = v_v_v_window_28_22_473_1_543; 
                    v_v_v_window_28_13_464_2_517 = v_v_v_window_28_22_473_2_544; 
                    v_v_v_window_28_5_456_0_491 = v_v_v_window_28_14_465_0_518; 
                    v_v_v_window_28_5_456_1_492 = v_v_v_window_28_14_465_1_519; 
                    v_v_v_window_28_5_456_2_493 = v_v_v_window_28_14_465_2_520; 
                    v_v_v_window_28_14_465_0_518 = v_v_v_window_28_23_474_0_545; 
                    v_v_v_window_28_14_465_1_519 = v_v_v_window_28_23_474_1_546; 
                    v_v_v_window_28_14_465_2_520 = v_v_v_window_28_23_474_2_547; 
                    v_v_v_window_28_6_457_0_494 = v_v_v_window_28_15_466_0_521; 
                    v_v_v_window_28_6_457_1_495 = v_v_v_window_28_15_466_1_522; 
                    v_v_v_window_28_6_457_2_496 = v_v_v_window_28_15_466_2_523; 
                    v_v_v_window_28_15_466_0_521 = v_v_v_window_28_24_475_0_548; 
                    v_v_v_window_28_15_466_1_522 = v_v_v_window_28_24_475_1_549; 
                    v_v_v_window_28_15_466_2_523 = v_v_v_window_28_24_475_2_550; 
                    v_v_v_window_28_7_458_0_497 = v_v_v_window_28_16_467_0_524; 
                    v_v_v_window_28_7_458_1_498 = v_v_v_window_28_16_467_1_525; 
                    v_v_v_window_28_7_458_2_499 = v_v_v_window_28_16_467_2_526; 
                    v_v_v_window_28_16_467_0_524 = v_v_v_window_28_25_476_0_551; 
                    v_v_v_window_28_16_467_1_525 = v_v_v_window_28_25_476_1_552; 
                    v_v_v_window_28_16_467_2_526 = v_v_v_window_28_25_476_2_553; 
                    v_v_v_window_28_8_459_0_500 = v_v_v_window_28_17_468_0_527; 
                    v_v_v_window_28_8_459_1_501 = v_v_v_window_28_17_468_1_528; 
                    v_v_v_window_28_8_459_2_502 = v_v_v_window_28_17_468_2_529; 
                    v_v_v_window_28_17_468_0_527 = v_v_v_window_28_26_477_0_554; 
                    v_v_v_window_28_17_468_1_528 = v_v_v_window_28_26_477_1_555; 
                    v_v_v_window_28_17_468_2_529 = v_v_v_window_28_26_477_2_556; 
                    v_v_v_window_28_9_460_0_503 = v_v_v_window_28_18_469_0_530; 
                    v_v_v_window_28_9_460_1_504 = v_v_v_window_28_18_469_1_531; 
                    v_v_v_window_28_9_460_2_505 = v_v_v_window_28_18_469_2_532; 
                    v_v_v_window_28_18_469_0_530 = v_v_v_window_28_27_478_0_557; 
                    v_v_v_window_28_18_469_1_531 = v_v_v_window_28_27_478_1_558; 
                    v_v_v_window_28_18_469_2_532 = v_v_v_window_28_27_478_2_559; 
=======
        float v__269; 
        float v__272; 
        float v__275; 
        float v__277; 
        float v__279; 
        float v__281; 
        float v__283; 
        float v__285; 
        float v__287; 
        float v__290; 
        float v__294; 
        float v__296; 
        float v__298; 
        float v__300; 
        float v__304; 
        float v__306; 
        for (int v_gl_id_261 = get_global_id(0); (v_gl_id_261 < 512); v_gl_id_261 = (v_gl_id_261 + get_global_size(0))){
            for (int v_gl_id_262 = get_global_id(1); (v_gl_id_262 < 512); v_gl_id_262 = (v_gl_id_262 + get_global_size(1))){
                // slideSeq_plus
                float v_window_1_0_591; 
                float v_window_1_1_592; 
                int v_window_1_2_593; 
                float v_window_2_0_594; 
                float v_window_2_1_595; 
                int v_window_2_2_596; 
                float v_window_3_0_597; 
                float v_window_3_1_598; 
                int v_window_3_2_599; 
                float v_window_4_0_600; 
                float v_window_4_1_601; 
                int v_window_4_2_602; 
                float v_window_5_0_603; 
                float v_window_5_1_604; 
                int v_window_5_2_605; 
                float v_window_6_0_606; 
                float v_window_6_1_607; 
                int v_window_6_2_608; 
                float v_window_7_0_609; 
                float v_window_7_1_610; 
                int v_window_7_2_611; 
                float v_window_8_0_612; 
                float v_window_8_1_613; 
                int v_window_8_2_614; 
                float v_window_9_0_615; 
                float v_window_9_1_616; 
                int v_window_9_2_617; 
                float v_window_10_0_618; 
                float v_window_10_1_619; 
                int v_window_10_2_620; 
                float v_window_11_0_621; 
                float v_window_11_1_622; 
                int v_window_11_2_623; 
                float v_window_12_0_624; 
                float v_window_12_1_625; 
                int v_window_12_2_626; 
                float v_window_13_0_627; 
                float v_window_13_1_628; 
                int v_window_13_2_629; 
                float v_window_14_0_630; 
                float v_window_14_1_631; 
                int v_window_14_2_632; 
                float v_window_15_0_633; 
                float v_window_15_1_634; 
                int v_window_15_2_635; 
                float v_window_16_0_636; 
                float v_window_16_1_637; 
                int v_window_16_2_638; 
                float v_window_17_0_639; 
                float v_window_17_1_640; 
                int v_window_17_2_641; 
                float v_window_18_0_642; 
                float v_window_18_1_643; 
                int v_window_18_2_644; 
                float v_window_19_0_645; 
                float v_window_19_1_646; 
                int v_window_19_2_647; 
                float v_window_20_0_648; 
                float v_window_20_1_649; 
                int v_window_20_2_650; 
                float v_window_21_0_651; 
                float v_window_21_1_652; 
                int v_window_21_2_653; 
                float v_window_22_0_654; 
                float v_window_22_1_655; 
                int v_window_22_2_656; 
                float v_window_23_0_657; 
                float v_window_23_1_658; 
                int v_window_23_2_659; 
                float v_window_24_0_660; 
                float v_window_24_1_661; 
                int v_window_24_2_662; 
                float v_window_25_0_663; 
                float v_window_25_1_664; 
                int v_window_25_2_665; 
                float v_window_26_0_666; 
                float v_window_26_1_667; 
                int v_window_26_2_668; 
                float v_window_27_0_669; 
                float v_window_27_1_670; 
                int v_window_27_2_671; 
                v_window_1_0_591 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__264[(-262657 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_1_1_592 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__265[(-262657 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_1_2_593 = idxF(0, v_gl_id_262, v_gl_id_261, 66, 514, 514); 
                v_window_2_0_594 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__264[(-513 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_2_1_595 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__265[(-513 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_2_2_596 = idxF(1, v_gl_id_262, v_gl_id_261, 66, 514, 514); 
                v_window_4_0_600 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__264[(-262145 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_4_1_601 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__265[(-262145 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_4_2_602 = idxF(0, (1 + v_gl_id_262), v_gl_id_261, 66, 514, 514); 
                v_window_5_0_603 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__264[(-1 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_5_1_604 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__265[(-1 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_5_2_605 = idxF(1, (1 + v_gl_id_262), v_gl_id_261, 66, 514, 514); 
                v_window_7_0_609 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__264[(-261633 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_7_1_610 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__265[(-261633 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_7_2_611 = idxF(0, (2 + v_gl_id_262), v_gl_id_261, 66, 514, 514); 
                v_window_8_0_612 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__264[(511 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_8_1_613 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__265[(511 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_8_2_614 = idxF(1, (2 + v_gl_id_262), v_gl_id_261, 66, 514, 514); 
                v_window_10_0_618 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__264[(-262656 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_10_1_619 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__265[(-262656 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_10_2_620 = idxF(0, v_gl_id_262, (1 + v_gl_id_261), 66, 514, 514); 
                v_window_11_0_621 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__264[(-512 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_11_1_622 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__265[(-512 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_11_2_623 = idxF(1, v_gl_id_262, (1 + v_gl_id_261), 66, 514, 514); 
                v_window_13_0_627 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__264[(-262144 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_13_1_628 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__265[(-262144 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_13_2_629 = idxF(0, (1 + v_gl_id_262), (1 + v_gl_id_261), 66, 514, 514); 
                v_window_14_0_630 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__264[(v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_14_1_631 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__265[(v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_14_2_632 = idxF(1, (1 + v_gl_id_262), (1 + v_gl_id_261), 66, 514, 514); 
                v_window_16_0_636 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__264[(-261632 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_16_1_637 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__265[(-261632 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_16_2_638 = idxF(0, (2 + v_gl_id_262), (1 + v_gl_id_261), 66, 514, 514); 
                v_window_17_0_639 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__264[(512 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_17_1_640 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__265[(512 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_17_2_641 = idxF(1, (2 + v_gl_id_262), (1 + v_gl_id_261), 66, 514, 514); 
                v_window_19_0_645 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__264[(-262655 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_19_1_646 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__265[(-262655 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_19_2_647 = idxF(0, v_gl_id_262, (2 + v_gl_id_261), 66, 514, 514); 
                v_window_20_0_648 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__264[(-511 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_20_1_649 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__265[(-511 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_20_2_650 = idxF(1, v_gl_id_262, (2 + v_gl_id_261), 66, 514, 514); 
                v_window_22_0_654 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__264[(-262143 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_22_1_655 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__265[(-262143 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_22_2_656 = idxF(0, (1 + v_gl_id_262), (2 + v_gl_id_261), 66, 514, 514); 
                v_window_23_0_657 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__264[(1 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_23_1_658 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__265[(1 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_23_2_659 = idxF(1, (1 + v_gl_id_262), (2 + v_gl_id_261), 66, 514, 514); 
                v_window_25_0_663 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__264[(-261631 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_25_1_664 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 64)) ? 0.0f : v__265[(-261631 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_25_2_665 = idxF(0, (2 + v_gl_id_262), (2 + v_gl_id_261), 66, 514, 514); 
                v_window_26_0_666 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__264[(513 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_26_1_667 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 64)) ? 0.0f : v__265[(513 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_26_2_668 = idxF(1, (2 + v_gl_id_262), (2 + v_gl_id_261), 66, 514, 514); 
#pragma unroll 1
                for (int v_i_263 = 0; (v_i_263 < 64); v_i_263 = (1 + v_i_263)){
                    v_window_3_0_597 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__264[(261631 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_3_1_598 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__265[(261631 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_3_2_599 = idxF((2 + v_i_263), v_gl_id_262, v_gl_id_261, 66, 514, 514); 
                    v_window_6_0_606 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__264[(262143 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_6_1_607 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__265[(262143 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_6_2_608 = idxF((2 + v_i_263), (1 + v_gl_id_262), v_gl_id_261, 66, 514, 514); 
                    v_window_9_0_615 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__264[(262655 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_9_1_616 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__265[(262655 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_9_2_617 = idxF((2 + v_i_263), (2 + v_gl_id_262), v_gl_id_261, 66, 514, 514); 
                    v_window_12_0_624 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__264[(261632 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_12_1_625 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__265[(261632 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_12_2_626 = idxF((2 + v_i_263), v_gl_id_262, (1 + v_gl_id_261), 66, 514, 514); 
                    v_window_15_0_633 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__264[(262144 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_15_1_634 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__265[(262144 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_15_2_635 = idxF((2 + v_i_263), (1 + v_gl_id_262), (1 + v_gl_id_261), 66, 514, 514); 
                    v_window_18_0_642 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__264[(262656 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_18_1_643 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__265[(262656 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_18_2_644 = idxF((2 + v_i_263), (2 + v_gl_id_262), (1 + v_gl_id_261), 66, 514, 514); 
                    v_window_21_0_651 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__264[(261633 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_21_1_652 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__265[(261633 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_21_2_653 = idxF((2 + v_i_263), v_gl_id_262, (2 + v_gl_id_261), 66, 514, 514); 
                    v_window_24_0_660 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__264[(262145 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_24_1_661 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__265[(262145 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_24_2_662 = idxF((2 + v_i_263), (1 + v_gl_id_262), (2 + v_gl_id_261), 66, 514, 514); 
                    v_window_27_0_669 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__264[(262657 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_27_1_670 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 64)) ? 0.0f : v__265[(262657 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_27_2_671 = idxF((2 + v_i_263), (2 + v_gl_id_262), (2 + v_gl_id_261), 66, 514, 514); 
                    v__269 = idIF(v_window_14_2_632); 
                    float v_tmp_557 = 0.3333333f; 
                    v__270 = v_tmp_557; 
                    v__272 = mult(v__269, v__270); 
                    float v_tmp_558 = 2.0f; 
                    v__273 = v_tmp_558; 
                    v__275 = subtract(v__273, v__272); 
                    v__277 = mult(v__275, v_window_14_1_631); 
                    v__279 = add(v_window_23_1_658, v_window_17_1_640); 
                    v__281 = add(v__279, v_window_15_1_634); 
                    v__283 = add(v__281, v_window_13_1_628); 
                    v__285 = add(v__283, v_window_11_1_622); 
                    v__287 = add(v__285, v_window_5_1_604); 
                    float v_tmp_559 = 0.3333333f; 
                    v__288 = v_tmp_559; 
                    v__290 = mult(v__287, v__288); 
                    float v_tmp_560 = 0.9971132f; 
                    v__291 = v_tmp_560; 
                    float v_tmp_561 = 1.0f; 
                    v__292 = v_tmp_561; 
                    v__294 = getCF(v_window_14_2_632, v__291, v__292); 
                    v__296 = mult(v_window_14_0_630, v__294); 
                    v__298 = subtractTuple((Tuple2_float_float){v__290, v__296}); 
                    v__300 = addTuple((Tuple2_float_float){v__277, v__298}); 
                    float v_tmp_562 = 0.9971216f; 
                    v__301 = v_tmp_562; 
                    float v_tmp_563 = 1.0f; 
                    v__302 = v_tmp_563; 
                    v__304 = getCF(v_window_14_2_632, v__301, v__302); 
                    v__306 = mult(v__300, v__304); 
                    v__307[(v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))] = id(v__306); 
                    v_window_1_0_591 = v_window_2_0_594; 
                    v_window_1_1_592 = v_window_2_1_595; 
                    v_window_1_2_593 = v_window_2_2_596; 
                    v_window_2_0_594 = v_window_3_0_597; 
                    v_window_2_1_595 = v_window_3_1_598; 
                    v_window_2_2_596 = v_window_3_2_599; 
                    v_window_4_0_600 = v_window_5_0_603; 
                    v_window_4_1_601 = v_window_5_1_604; 
                    v_window_4_2_602 = v_window_5_2_605; 
                    v_window_5_0_603 = v_window_6_0_606; 
                    v_window_5_1_604 = v_window_6_1_607; 
                    v_window_5_2_605 = v_window_6_2_608; 
                    v_window_7_0_609 = v_window_8_0_612; 
                    v_window_7_1_610 = v_window_8_1_613; 
                    v_window_7_2_611 = v_window_8_2_614; 
                    v_window_8_0_612 = v_window_9_0_615; 
                    v_window_8_1_613 = v_window_9_1_616; 
                    v_window_8_2_614 = v_window_9_2_617; 
                    v_window_10_0_618 = v_window_11_0_621; 
                    v_window_10_1_619 = v_window_11_1_622; 
                    v_window_10_2_620 = v_window_11_2_623; 
                    v_window_11_0_621 = v_window_12_0_624; 
                    v_window_11_1_622 = v_window_12_1_625; 
                    v_window_11_2_623 = v_window_12_2_626; 
                    v_window_13_0_627 = v_window_14_0_630; 
                    v_window_13_1_628 = v_window_14_1_631; 
                    v_window_13_2_629 = v_window_14_2_632; 
                    v_window_14_0_630 = v_window_15_0_633; 
                    v_window_14_1_631 = v_window_15_1_634; 
                    v_window_14_2_632 = v_window_15_2_635; 
                    v_window_16_0_636 = v_window_17_0_639; 
                    v_window_16_1_637 = v_window_17_1_640; 
                    v_window_16_2_638 = v_window_17_2_641; 
                    v_window_17_0_639 = v_window_18_0_642; 
                    v_window_17_1_640 = v_window_18_1_643; 
                    v_window_17_2_641 = v_window_18_2_644; 
                    v_window_19_0_645 = v_window_20_0_648; 
                    v_window_19_1_646 = v_window_20_1_649; 
                    v_window_19_2_647 = v_window_20_2_650; 
                    v_window_20_0_648 = v_window_21_0_651; 
                    v_window_20_1_649 = v_window_21_1_652; 
                    v_window_20_2_650 = v_window_21_2_653; 
                    v_window_22_0_654 = v_window_23_0_657; 
                    v_window_22_1_655 = v_window_23_1_658; 
                    v_window_22_2_656 = v_window_23_2_659; 
                    v_window_23_0_657 = v_window_24_0_660; 
                    v_window_23_1_658 = v_window_24_1_661; 
                    v_window_23_2_659 = v_window_24_2_662; 
                    v_window_25_0_663 = v_window_26_0_666; 
                    v_window_25_1_664 = v_window_26_1_667; 
                    v_window_25_2_665 = v_window_26_2_668; 
                    v_window_26_0_666 = v_window_27_0_669; 
                    v_window_26_1_667 = v_window_27_1_670; 
                    v_window_26_2_668 = v_window_27_2_671; 
>>>>>>> b4ed44e8eb11eaa68562ff76879ee9f3215b7401:nvidia/benchmark_comparison_unroll_rerun/acoustic-MSS-512-n-64.cl
                }
                // end mapSeqSlide
            }
        }
    }
}
