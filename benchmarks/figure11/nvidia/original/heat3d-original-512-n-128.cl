#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 128 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(128,128)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(32,32)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(64,64)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(64,64)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float heat(float C, float S, float N, float E, float W, float B, float F){
    {
        return 0.125f * (B - 2.0f * C + F) +
       0.125f * (S - 2.0f * C + N) +
       0.125f * (E - 2.0f * C + W) + C;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__45, global float* v__48){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__47; 
        for (int v_gl_id_42 = get_global_id(2); (v_gl_id_42 < 126); v_gl_id_42 = (v_gl_id_42 + get_global_size(2))){
            for (int v_gl_id_43 = get_global_id(1); (v_gl_id_43 < 510); v_gl_id_43 = (v_gl_id_43 + get_global_size(1))){
                for (int v_gl_id_44 = get_global_id(0); (v_gl_id_44 < 510); v_gl_id_44 = (v_gl_id_44 + get_global_size(0))){
                    v__47 = heat(v__45[(262657 + v_gl_id_44 + (262144 * v_gl_id_42) + (512 * v_gl_id_43))], v__45[(262145 + v_gl_id_44 + (262144 * v_gl_id_42) + (512 * v_gl_id_43))], v__45[(263169 + v_gl_id_44 + (262144 * v_gl_id_42) + (512 * v_gl_id_43))], v__45[(262658 + v_gl_id_44 + (262144 * v_gl_id_42) + (512 * v_gl_id_43))], v__45[(262656 + v_gl_id_44 + (512 * v_gl_id_43) + (262144 * v_gl_id_42))], v__45[(513 + v_gl_id_44 + (512 * v_gl_id_43) + (262144 * v_gl_id_42))], v__45[(524801 + v_gl_id_44 + (262144 * v_gl_id_42) + (512 * v_gl_id_43))]); 
                    v__48[(262657 + v_gl_id_44 + (262144 * v_gl_id_42) + (512 * v_gl_id_43))] = id(v__47); 
                }
            }
        }
    }
}