#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 16 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(128,128)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: int
#ifndef Tuple3_float_float_int_DEFINED
#define Tuple3_float_float_int_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
    int _2;
} Tuple3_float_float_int;
#endif

float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float addTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 + x._1;}; 
    }
}
float subtract(float l, float r){
    {
        { return l - r; }; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
float subtractTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 - x._1;}; 
    }
}
int idxF(int i, int j, int k, int m, int n, int o){
    {
        {int count = 6; if(i == (m-1) || i == 0){ count--; } if(j == (n-1) || j == 0){ count--; } if(k == (o-1) || k == 0){ count--; }return count; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
float getCF(int neigh, float cfB, float cfI){
    {
        { if(neigh < 6) { return cfB; } else{ return cfI;} }; 
    }
}
float idIF(int x){
    {
        { return (float)(x*1.0); }; 
    }
}
kernel void KERNEL(const global float* restrict v__53, const global float* restrict v__54, global float* v__97){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        float v__60; 
        float v__63; 
        float v__78; 
        float v__81; 
        float v__82; 
        float v__91; 
        float v__92; 
        // Private Memory
        float v__59; 
        float v__62; 
        float v__65; 
        float v__67; 
        float v__69; 
        float v__71; 
        float v__73; 
        float v__75; 
        float v__77; 
        float v__80; 
        float v__84; 
        float v__86; 
        float v__88; 
        float v__90; 
        float v__94; 
        float v__96; 
        for (int v_gl_id_50 = get_global_id(2); (v_gl_id_50 < 14); v_gl_id_50 = (v_gl_id_50 + get_global_size(2))){
            for (int v_gl_id_51 = get_global_id(1); (v_gl_id_51 < 510); v_gl_id_51 = (v_gl_id_51 + get_global_size(1))){
                for (int v_gl_id_52 = get_global_id(0); (v_gl_id_52 < 510); v_gl_id_52 = (v_gl_id_52 + get_global_size(0))){
                    v__59 = idIF(idxF((1 + v_gl_id_50), (1 + v_gl_id_51), (1 + v_gl_id_52), 16, 512, 512)); 
                    float v_tmp_192 = 0.3333333f; 
                    v__60 = v_tmp_192; 
                    v__62 = mult(v__59, v__60); 
                    float v_tmp_193 = 2.0f; 
                    v__63 = v_tmp_193; 
                    v__65 = subtract(v__63, v__62); 
                    v__67 = mult(v__65, v__54[(262657 + v_gl_id_52 + (262144 * v_gl_id_50) + (512 * v_gl_id_51))]); 
                    v__69 = add(v__54[(524801 + v_gl_id_52 + (262144 * v_gl_id_50) + (512 * v_gl_id_51))], v__54[(263169 + v_gl_id_52 + (262144 * v_gl_id_50) + (512 * v_gl_id_51))]); 
                    v__71 = add(v__69, v__54[(262658 + v_gl_id_52 + (262144 * v_gl_id_50) + (512 * v_gl_id_51))]); 
                    v__73 = add(v__71, v__54[(262656 + v_gl_id_52 + (512 * v_gl_id_51) + (262144 * v_gl_id_50))]); 
                    v__75 = add(v__73, v__54[(262145 + v_gl_id_52 + (262144 * v_gl_id_50) + (512 * v_gl_id_51))]); 
                    v__77 = add(v__75, v__54[(513 + v_gl_id_52 + (512 * v_gl_id_51) + (262144 * v_gl_id_50))]); 
                    float v_tmp_194 = 0.3333333f; 
                    v__78 = v_tmp_194; 
                    v__80 = mult(v__77, v__78); 
                    float v_tmp_195 = 0.9971132f; 
                    v__81 = v_tmp_195; 
                    float v_tmp_196 = 1.0f; 
                    v__82 = v_tmp_196; 
                    v__84 = getCF(idxF((1 + v_gl_id_50), (1 + v_gl_id_51), (1 + v_gl_id_52), 16, 512, 512), v__81, v__82); 
                    v__86 = mult(v__53[(262657 + v_gl_id_52 + (262144 * v_gl_id_50) + (512 * v_gl_id_51))], v__84); 
                    v__88 = subtractTuple((Tuple2_float_float){v__80, v__86}); 
                    v__90 = addTuple((Tuple2_float_float){v__67, v__88}); 
                    float v_tmp_197 = 0.9971216f; 
                    v__91 = v_tmp_197; 
                    float v_tmp_198 = 1.0f; 
                    v__92 = v_tmp_198; 
                    v__94 = getCF(idxF((1 + v_gl_id_50), (1 + v_gl_id_51), (1 + v_gl_id_52), 16, 512, 512), v__91, v__92); 
                    v__96 = mult(v__90, v__94); 
                    v__97[(v_gl_id_52 + (260100 * v_gl_id_50) + (510 * v_gl_id_51))] = id(v__96); 
                }
            }
        }
    }
}