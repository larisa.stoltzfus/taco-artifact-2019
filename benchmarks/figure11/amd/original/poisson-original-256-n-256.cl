#atf::var<int> v_M_0 = 256 
#atf::var<int> v_N_1 = 256 
#atf::var<int> v_O_2 = 256 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(8,8)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(16,16)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(128,128)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(32,32)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(8,8)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float poisson(float C, float N, float S, float E, float W, float F, float B, float FN, float BN, float FS, float BS, float FW, float BW, float NW, float SW, float FE, float BE, float NE, float SE){
    {
        return 2.666f * C - 0.166f * (F + B + N + S + E + W) -
       0.0833f * (FN + BN + FS + BS + FW + BW +
                  NW + SW + FE + BE + NE + SE);; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__54, global float* v__57){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__56; 
        for (int v_gl_id_51 = get_global_id(2); (v_gl_id_51 < 254); v_gl_id_51 = (v_gl_id_51 + get_global_size(2))){
            for (int v_gl_id_52 = get_global_id(1); (v_gl_id_52 < 254); v_gl_id_52 = (v_gl_id_52 + get_global_size(1))){
                for (int v_gl_id_53 = get_global_id(0); (v_gl_id_53 < 254); v_gl_id_53 = (v_gl_id_53 + get_global_size(0))){
                    v__56 = poisson(v__54[(65793 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__54[(65537 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__54[(66049 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__54[(65794 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__54[(65792 + v_gl_id_53 + (256 * v_gl_id_52) + (65536 * v_gl_id_51))], v__54[(257 + v_gl_id_53 + (256 * v_gl_id_52) + (65536 * v_gl_id_51))], v__54[(131329 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__54[(1 + v_gl_id_53 + (256 * v_gl_id_52) + (65536 * v_gl_id_51))], v__54[(131073 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__54[(513 + v_gl_id_53 + (256 * v_gl_id_52) + (65536 * v_gl_id_51))], v__54[(131585 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__54[(256 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__54[(131328 + v_gl_id_53 + (256 * v_gl_id_52) + (65536 * v_gl_id_51))], v__54[(65536 + v_gl_id_53 + (256 * v_gl_id_52) + (65536 * v_gl_id_51))], v__54[(66048 + v_gl_id_53 + (256 * v_gl_id_52) + (65536 * v_gl_id_51))], v__54[(258 + v_gl_id_53 + (256 * v_gl_id_52) + (65536 * v_gl_id_51))], v__54[(131330 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__54[(65538 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))], v__54[(66050 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))]); 
                    v__57[(65793 + v_gl_id_53 + (65536 * v_gl_id_51) + (256 * v_gl_id_52))] = id(v__56); 
                }
            }
        }
    }
}