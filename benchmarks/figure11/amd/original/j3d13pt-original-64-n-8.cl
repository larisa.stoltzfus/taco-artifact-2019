#atf::var<int> v_M_0 = 64 
#atf::var<int> v_N_1 = 64 
#atf::var<int> v_O_2 = 8 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(8,8)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(64,64)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(64,64)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(64,64)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float jacobi13(float EE, float E, float W, float WW, float SS, float S, float N, float NN, float BB, float B, float F, float FF, float C){
    {
        return 0.083f * EE + 0.083f * E + 0.083f * W + 0.083f * WW +
       0.083f * SS + 0.083f * S + 0.083f * N + 0.083f * NN +
       0.083f * BB + 0.083f * B + 0.083f * F + 0.083f * FF -
       0.996f * C;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__45, global float* v__48){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__47; 
        for (int v_gl_id_42 = get_global_id(2); (v_gl_id_42 < 4); v_gl_id_42 = (v_gl_id_42 + get_global_size(2))){
            for (int v_gl_id_43 = get_global_id(1); (v_gl_id_43 < 60); v_gl_id_43 = (v_gl_id_43 + get_global_size(1))){
                for (int v_gl_id_44 = get_global_id(0); (v_gl_id_44 < 60); v_gl_id_44 = (v_gl_id_44 + get_global_size(0))){
                    v__47 = jacobi13(v__45[(8324 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))], v__45[(8323 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))], v__45[(8321 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))], v__45[(8320 + v_gl_id_44 + (64 * v_gl_id_43) + (4096 * v_gl_id_42))], v__45[(8450 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))], v__45[(8386 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))], v__45[(8258 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))], v__45[(8194 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))], v__45[(16514 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))], v__45[(12418 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))], v__45[(4226 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))], v__45[(130 + v_gl_id_44 + (64 * v_gl_id_43) + (4096 * v_gl_id_42))], v__45[(8322 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))]); 
                    v__48[(8322 + v_gl_id_44 + (4096 * v_gl_id_42) + (64 * v_gl_id_43))] = id(v__47); 
                }
            }
        }
    }
}