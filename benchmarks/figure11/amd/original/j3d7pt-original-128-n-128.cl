#atf::var<int> v_M_0 = 128 
#atf::var<int> v_N_1 = 128 
#atf::var<int> v_O_2 = 128 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(16,16)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(32,32)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(32,32)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(32,32)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float jacobi(float C, float N, float S, float E, float W, float F, float B){
    {
        return C+N+S+E+W+F+B;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__45, global float* v__48){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__47; 
        for (int v_gl_id_42 = get_global_id(2); (v_gl_id_42 < 126); v_gl_id_42 = (v_gl_id_42 + get_global_size(2))){
            for (int v_gl_id_43 = get_global_id(1); (v_gl_id_43 < 126); v_gl_id_43 = (v_gl_id_43 + get_global_size(1))){
                for (int v_gl_id_44 = get_global_id(0); (v_gl_id_44 < 126); v_gl_id_44 = (v_gl_id_44 + get_global_size(0))){
                    v__47 = jacobi(v__45[(16513 + v_gl_id_44 + (16384 * v_gl_id_42) + (128 * v_gl_id_43))], v__45[(16385 + v_gl_id_44 + (16384 * v_gl_id_42) + (128 * v_gl_id_43))], v__45[(16641 + v_gl_id_44 + (16384 * v_gl_id_42) + (128 * v_gl_id_43))], v__45[(16514 + v_gl_id_44 + (16384 * v_gl_id_42) + (128 * v_gl_id_43))], v__45[(16512 + v_gl_id_44 + (128 * v_gl_id_43) + (16384 * v_gl_id_42))], v__45[(129 + v_gl_id_44 + (128 * v_gl_id_43) + (16384 * v_gl_id_42))], v__45[(32897 + v_gl_id_44 + (16384 * v_gl_id_42) + (128 * v_gl_id_43))]); 
                    v__48[(16513 + v_gl_id_44 + (16384 * v_gl_id_42) + (128 * v_gl_id_43))] = id(v__47); 
                }
            }
        }
    }
}