#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 512 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(16,16)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(128,128)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: int
#ifndef Tuple3_float_float_int_DEFINED
#define Tuple3_float_float_int_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
    int _2;
} Tuple3_float_float_int;
#endif

float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float addTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 + x._1;}; 
    }
}
float subtract(float l, float r){
    {
        { return l - r; }; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
float subtractTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 - x._1;}; 
    }
}
int idxF(int i, int j, int k, int m, int n, int o){
    {
        {int count = 6; if(i == (m-1) || i == 0){ count--; } if(j == (n-1) || j == 0){ count--; } if(k == (o-1) || k == 0){ count--; }return count; }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
float getCF(int neigh, float cfB, float cfI){
    {
        { if(neigh < 6) { return cfB; } else{ return cfI;} }; 
    }
}
float idIF(int x){
    {
        { return (float)(x*1.0); }; 
    }
}
kernel void KERNEL(const global float* restrict v__204, const global float* restrict v__205, global float* v__247){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        float v__210; 
        float v__213; 
        float v__228; 
        float v__231; 
        float v__232; 
        float v__241; 
        float v__242; 
        // Private Memory
        float v__209; 
        float v__212; 
        float v__215; 
        float v__217; 
        float v__219; 
        float v__221; 
        float v__223; 
        float v__225; 
        float v__227; 
        float v__230; 
        float v__234; 
        float v__236; 
        float v__238; 
        float v__240; 
        float v__244; 
        float v__246; 
        for (int v_gl_id_201 = get_global_id(0); (v_gl_id_201 < 510); v_gl_id_201 = (v_gl_id_201 + get_global_size(0))){
            for (int v_gl_id_202 = get_global_id(1); (v_gl_id_202 < 510); v_gl_id_202 = (v_gl_id_202 + get_global_size(1))){
                // slideSeq_plus
                float v_window_1_0_479; 
                float v_window_1_1_480; 
                int v_window_1_2_481; 
                float v_window_2_0_482; 
                float v_window_2_1_483; 
                int v_window_2_2_484; 
                float v_window_3_0_485; 
                float v_window_3_1_486; 
                int v_window_3_2_487; 
                float v_window_4_0_488; 
                float v_window_4_1_489; 
                int v_window_4_2_490; 
                float v_window_5_0_491; 
                float v_window_5_1_492; 
                int v_window_5_2_493; 
                float v_window_6_0_494; 
                float v_window_6_1_495; 
                int v_window_6_2_496; 
                float v_window_7_0_497; 
                float v_window_7_1_498; 
                int v_window_7_2_499; 
                float v_window_8_0_500; 
                float v_window_8_1_501; 
                int v_window_8_2_502; 
                float v_window_9_0_503; 
                float v_window_9_1_504; 
                int v_window_9_2_505; 
                float v_window_10_0_506; 
                float v_window_10_1_507; 
                int v_window_10_2_508; 
                float v_window_11_0_509; 
                float v_window_11_1_510; 
                int v_window_11_2_511; 
                float v_window_12_0_512; 
                float v_window_12_1_513; 
                int v_window_12_2_514; 
                float v_window_13_0_515; 
                float v_window_13_1_516; 
                int v_window_13_2_517; 
                float v_window_14_0_518; 
                float v_window_14_1_519; 
                int v_window_14_2_520; 
                float v_window_15_0_521; 
                float v_window_15_1_522; 
                int v_window_15_2_523; 
                float v_window_16_0_524; 
                float v_window_16_1_525; 
                int v_window_16_2_526; 
                float v_window_17_0_527; 
                float v_window_17_1_528; 
                int v_window_17_2_529; 
                float v_window_18_0_530; 
                float v_window_18_1_531; 
                int v_window_18_2_532; 
                float v_window_19_0_533; 
                float v_window_19_1_534; 
                int v_window_19_2_535; 
                float v_window_20_0_536; 
                float v_window_20_1_537; 
                int v_window_20_2_538; 
                float v_window_21_0_539; 
                float v_window_21_1_540; 
                int v_window_21_2_541; 
                float v_window_22_0_542; 
                float v_window_22_1_543; 
                int v_window_22_2_544; 
                float v_window_23_0_545; 
                float v_window_23_1_546; 
                int v_window_23_2_547; 
                float v_window_24_0_548; 
                float v_window_24_1_549; 
                int v_window_24_2_550; 
                float v_window_25_0_551; 
                float v_window_25_1_552; 
                int v_window_25_2_553; 
                float v_window_26_0_554; 
                float v_window_26_1_555; 
                int v_window_26_2_556; 
                float v_window_27_0_557; 
                float v_window_27_1_558; 
                int v_window_27_2_559; 
                v_window_1_0_479 = v__204[(v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_1_1_480 = v__205[(v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_1_2_481 = idxF(0, v_gl_id_202, v_gl_id_201, 512, 512, 512); 
                v_window_2_0_482 = v__204[(262144 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_2_1_483 = v__205[(262144 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_2_2_484 = idxF(1, v_gl_id_202, v_gl_id_201, 512, 512, 512); 
                v_window_4_0_488 = v__204[(512 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_4_1_489 = v__205[(512 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_4_2_490 = idxF(0, (1 + v_gl_id_202), v_gl_id_201, 512, 512, 512); 
                v_window_5_0_491 = v__204[(262656 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_5_1_492 = v__205[(262656 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_5_2_493 = idxF(1, (1 + v_gl_id_202), v_gl_id_201, 512, 512, 512); 
                v_window_7_0_497 = v__204[(1024 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_7_1_498 = v__205[(1024 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_7_2_499 = idxF(0, (2 + v_gl_id_202), v_gl_id_201, 512, 512, 512); 
                v_window_8_0_500 = v__204[(263168 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_8_1_501 = v__205[(263168 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_8_2_502 = idxF(1, (2 + v_gl_id_202), v_gl_id_201, 512, 512, 512); 
                v_window_10_0_506 = v__204[(1 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_10_1_507 = v__205[(1 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_10_2_508 = idxF(0, v_gl_id_202, (1 + v_gl_id_201), 512, 512, 512); 
                v_window_11_0_509 = v__204[(262145 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_11_1_510 = v__205[(262145 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_11_2_511 = idxF(1, v_gl_id_202, (1 + v_gl_id_201), 512, 512, 512); 
                v_window_13_0_515 = v__204[(513 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_13_1_516 = v__205[(513 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_13_2_517 = idxF(0, (1 + v_gl_id_202), (1 + v_gl_id_201), 512, 512, 512); 
                v_window_14_0_518 = v__204[(262657 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_14_1_519 = v__205[(262657 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_14_2_520 = idxF(1, (1 + v_gl_id_202), (1 + v_gl_id_201), 512, 512, 512); 
                v_window_16_0_524 = v__204[(1025 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_16_1_525 = v__205[(1025 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_16_2_526 = idxF(0, (2 + v_gl_id_202), (1 + v_gl_id_201), 512, 512, 512); 
                v_window_17_0_527 = v__204[(263169 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_17_1_528 = v__205[(263169 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_17_2_529 = idxF(1, (2 + v_gl_id_202), (1 + v_gl_id_201), 512, 512, 512); 
                v_window_19_0_533 = v__204[(2 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_19_1_534 = v__205[(2 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_19_2_535 = idxF(0, v_gl_id_202, (2 + v_gl_id_201), 512, 512, 512); 
                v_window_20_0_536 = v__204[(262146 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_20_1_537 = v__205[(262146 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_20_2_538 = idxF(1, v_gl_id_202, (2 + v_gl_id_201), 512, 512, 512); 
                v_window_22_0_542 = v__204[(514 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_22_1_543 = v__205[(514 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_22_2_544 = idxF(0, (1 + v_gl_id_202), (2 + v_gl_id_201), 512, 512, 512); 
                v_window_23_0_545 = v__204[(262658 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_23_1_546 = v__205[(262658 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_23_2_547 = idxF(1, (1 + v_gl_id_202), (2 + v_gl_id_201), 512, 512, 512); 
                v_window_25_0_551 = v__204[(1026 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_25_1_552 = v__205[(1026 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_25_2_553 = idxF(0, (2 + v_gl_id_202), (2 + v_gl_id_201), 512, 512, 512); 
                v_window_26_0_554 = v__204[(263170 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_26_1_555 = v__205[(263170 + v_gl_id_201 + (512 * v_gl_id_202))]; 
                v_window_26_2_556 = idxF(1, (2 + v_gl_id_202), (2 + v_gl_id_201), 512, 512, 512); 
                for (int v_i_203 = 0; (v_i_203 < 510); v_i_203 = (1 + v_i_203)){
                    v_window_3_0_485 = v__204[(524288 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_3_1_486 = v__205[(524288 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_3_2_487 = idxF((2 + v_i_203), v_gl_id_202, v_gl_id_201, 512, 512, 512); 
                    v_window_6_0_494 = v__204[(524800 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_6_1_495 = v__205[(524800 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_6_2_496 = idxF((2 + v_i_203), (1 + v_gl_id_202), v_gl_id_201, 512, 512, 512); 
                    v_window_9_0_503 = v__204[(525312 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_9_1_504 = v__205[(525312 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_9_2_505 = idxF((2 + v_i_203), (2 + v_gl_id_202), v_gl_id_201, 512, 512, 512); 
                    v_window_12_0_512 = v__204[(524289 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_12_1_513 = v__205[(524289 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_12_2_514 = idxF((2 + v_i_203), v_gl_id_202, (1 + v_gl_id_201), 512, 512, 512); 
                    v_window_15_0_521 = v__204[(524801 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_15_1_522 = v__205[(524801 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_15_2_523 = idxF((2 + v_i_203), (1 + v_gl_id_202), (1 + v_gl_id_201), 512, 512, 512); 
                    v_window_18_0_530 = v__204[(525313 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_18_1_531 = v__205[(525313 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_18_2_532 = idxF((2 + v_i_203), (2 + v_gl_id_202), (1 + v_gl_id_201), 512, 512, 512); 
                    v_window_21_0_539 = v__204[(524290 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_21_1_540 = v__205[(524290 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_21_2_541 = idxF((2 + v_i_203), v_gl_id_202, (2 + v_gl_id_201), 512, 512, 512); 
                    v_window_24_0_548 = v__204[(524802 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_24_1_549 = v__205[(524802 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_24_2_550 = idxF((2 + v_i_203), (1 + v_gl_id_202), (2 + v_gl_id_201), 512, 512, 512); 
                    v_window_27_0_557 = v__204[(525314 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_27_1_558 = v__205[(525314 + v_gl_id_201 + (512 * v_gl_id_202) + (262144 * v_i_203))]; 
                    v_window_27_2_559 = idxF((2 + v_i_203), (2 + v_gl_id_202), (2 + v_gl_id_201), 512, 512, 512); 
                    v__209 = idIF(v_window_14_2_520); 
                    float v_tmp_445 = 0.3333333f; 
                    v__210 = v_tmp_445; 
                    v__212 = mult(v__209, v__210); 
                    float v_tmp_446 = 2.0f; 
                    v__213 = v_tmp_446; 
                    v__215 = subtract(v__213, v__212); 
                    v__217 = mult(v__215, v_window_14_1_519); 
                    v__219 = add(v_window_23_1_546, v_window_17_1_528); 
                    v__221 = add(v__219, v_window_15_1_522); 
                    v__223 = add(v__221, v_window_13_1_516); 
                    v__225 = add(v__223, v_window_11_1_510); 
                    v__227 = add(v__225, v_window_5_1_492); 
                    float v_tmp_447 = 0.3333333f; 
                    v__228 = v_tmp_447; 
                    v__230 = mult(v__227, v__228); 
                    float v_tmp_448 = 0.9971132f; 
                    v__231 = v_tmp_448; 
                    float v_tmp_449 = 1.0f; 
                    v__232 = v_tmp_449; 
                    v__234 = getCF(v_window_14_2_520, v__231, v__232); 
                    v__236 = mult(v_window_14_0_518, v__234); 
                    v__238 = subtractTuple((Tuple2_float_float){v__230, v__236}); 
                    v__240 = addTuple((Tuple2_float_float){v__217, v__238}); 
                    float v_tmp_450 = 0.9971216f; 
                    v__241 = v_tmp_450; 
                    float v_tmp_451 = 1.0f; 
                    v__242 = v_tmp_451; 
                    v__244 = getCF(v_window_14_2_520, v__241, v__242); 
                    v__246 = mult(v__240, v__244); 
                    v__247[(v_gl_id_201 + (510 * v_gl_id_202) + (260100 * v_i_203))] = id(v__246); 
                    v_window_1_0_479 = v_window_2_0_482; 
                    v_window_1_1_480 = v_window_2_1_483; 
                    v_window_1_2_481 = v_window_2_2_484; 
                    v_window_2_0_482 = v_window_3_0_485; 
                    v_window_2_1_483 = v_window_3_1_486; 
                    v_window_2_2_484 = v_window_3_2_487; 
                    v_window_4_0_488 = v_window_5_0_491; 
                    v_window_4_1_489 = v_window_5_1_492; 
                    v_window_4_2_490 = v_window_5_2_493; 
                    v_window_5_0_491 = v_window_6_0_494; 
                    v_window_5_1_492 = v_window_6_1_495; 
                    v_window_5_2_493 = v_window_6_2_496; 
                    v_window_7_0_497 = v_window_8_0_500; 
                    v_window_7_1_498 = v_window_8_1_501; 
                    v_window_7_2_499 = v_window_8_2_502; 
                    v_window_8_0_500 = v_window_9_0_503; 
                    v_window_8_1_501 = v_window_9_1_504; 
                    v_window_8_2_502 = v_window_9_2_505; 
                    v_window_10_0_506 = v_window_11_0_509; 
                    v_window_10_1_507 = v_window_11_1_510; 
                    v_window_10_2_508 = v_window_11_2_511; 
                    v_window_11_0_509 = v_window_12_0_512; 
                    v_window_11_1_510 = v_window_12_1_513; 
                    v_window_11_2_511 = v_window_12_2_514; 
                    v_window_13_0_515 = v_window_14_0_518; 
                    v_window_13_1_516 = v_window_14_1_519; 
                    v_window_13_2_517 = v_window_14_2_520; 
                    v_window_14_0_518 = v_window_15_0_521; 
                    v_window_14_1_519 = v_window_15_1_522; 
                    v_window_14_2_520 = v_window_15_2_523; 
                    v_window_16_0_524 = v_window_17_0_527; 
                    v_window_16_1_525 = v_window_17_1_528; 
                    v_window_16_2_526 = v_window_17_2_529; 
                    v_window_17_0_527 = v_window_18_0_530; 
                    v_window_17_1_528 = v_window_18_1_531; 
                    v_window_17_2_529 = v_window_18_2_532; 
                    v_window_19_0_533 = v_window_20_0_536; 
                    v_window_19_1_534 = v_window_20_1_537; 
                    v_window_19_2_535 = v_window_20_2_538; 
                    v_window_20_0_536 = v_window_21_0_539; 
                    v_window_20_1_537 = v_window_21_1_540; 
                    v_window_20_2_538 = v_window_21_2_541; 
                    v_window_22_0_542 = v_window_23_0_545; 
                    v_window_22_1_543 = v_window_23_1_546; 
                    v_window_22_2_544 = v_window_23_2_547; 
                    v_window_23_0_545 = v_window_24_0_548; 
                    v_window_23_1_546 = v_window_24_1_549; 
                    v_window_23_2_547 = v_window_24_2_550; 
                    v_window_25_0_551 = v_window_26_0_554; 
                    v_window_25_1_552 = v_window_26_1_555; 
                    v_window_25_2_553 = v_window_26_2_556; 
                    v_window_26_0_554 = v_window_27_0_557; 
                    v_window_26_1_555 = v_window_27_1_558; 
                    v_window_26_2_556 = v_window_27_2_559; 
                }
                // end slideSeq_plus
            }
        }
    }
}