#atf::var<int> v_M_0 = 64 
#atf::var<int> v_N_1 = 64 
#atf::var<int> v_O_2 = 16 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(64,64)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(64,64)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(16,16)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float jacobi(float C, float N, float S, float E, float W, float F, float B){
    {
        return C+N+S+E+W+F+B;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__70, global float* v__73){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__72; 
        for (int v_gl_id_67 = get_global_id(0); (v_gl_id_67 < 62); v_gl_id_67 = (v_gl_id_67 + get_global_size(0))){
            for (int v_gl_id_68 = get_global_id(1); (v_gl_id_68 < 62); v_gl_id_68 = (v_gl_id_68 + get_global_size(1))){
                // slideSeq_plus
                float v_window_1_95; 
                float v_window_2_96; 
                float v_window_3_97; 
                float v_window_4_98; 
                float v_window_5_99; 
                float v_window_6_100; 
                float v_window_7_101; 
                float v_window_8_102; 
                float v_window_9_103; 
                float v_window_10_104; 
                float v_window_11_105; 
                float v_window_12_106; 
                float v_window_13_107; 
                float v_window_14_108; 
                float v_window_15_109; 
                float v_window_16_110; 
                float v_window_17_111; 
                float v_window_18_112; 
                float v_window_19_113; 
                float v_window_20_114; 
                float v_window_21_115; 
                float v_window_22_116; 
                float v_window_23_117; 
                float v_window_24_118; 
                float v_window_25_119; 
                float v_window_26_120; 
                float v_window_27_121; 
                v_window_1_95 = v__70[(v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_2_96 = v__70[(4096 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_4_98 = v__70[(64 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_5_99 = v__70[(4160 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_7_101 = v__70[(128 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_8_102 = v__70[(4224 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_10_104 = v__70[(1 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_11_105 = v__70[(4097 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_13_107 = v__70[(65 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_14_108 = v__70[(4161 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_16_110 = v__70[(129 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_17_111 = v__70[(4225 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_19_113 = v__70[(2 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_20_114 = v__70[(4098 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_22_116 = v__70[(66 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_23_117 = v__70[(4162 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_25_119 = v__70[(130 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                v_window_26_120 = v__70[(4226 + v_gl_id_67 + (64 * v_gl_id_68))]; 
                for (int v_i_69 = 0; (v_i_69 < 14); v_i_69 = (1 + v_i_69)){
                    v_window_3_97 = v__70[(8192 + v_gl_id_67 + (64 * v_gl_id_68) + (4096 * v_i_69))]; 
                    v_window_6_100 = v__70[(8256 + v_gl_id_67 + (64 * v_gl_id_68) + (4096 * v_i_69))]; 
                    v_window_9_103 = v__70[(8320 + v_gl_id_67 + (64 * v_gl_id_68) + (4096 * v_i_69))]; 
                    v_window_12_106 = v__70[(8193 + v_gl_id_67 + (64 * v_gl_id_68) + (4096 * v_i_69))]; 
                    v_window_15_109 = v__70[(8257 + v_gl_id_67 + (64 * v_gl_id_68) + (4096 * v_i_69))]; 
                    v_window_18_112 = v__70[(8321 + v_gl_id_67 + (64 * v_gl_id_68) + (4096 * v_i_69))]; 
                    v_window_21_115 = v__70[(8194 + v_gl_id_67 + (64 * v_gl_id_68) + (4096 * v_i_69))]; 
                    v_window_24_118 = v__70[(8258 + v_gl_id_67 + (64 * v_gl_id_68) + (4096 * v_i_69))]; 
                    v_window_27_121 = v__70[(8322 + v_gl_id_67 + (64 * v_gl_id_68) + (4096 * v_i_69))]; 
                    v__72 = jacobi(v_window_14_108, v_window_11_105, v_window_17_111, v_window_15_109, v_window_13_107, v_window_5_99, v_window_23_117); 
                    v__73[(4161 + v_gl_id_67 + (64 * v_gl_id_68) + (4096 * v_i_69))] = id(v__72); 
                    v_window_1_95 = v_window_2_96; 
                    v_window_2_96 = v_window_3_97; 
                    v_window_4_98 = v_window_5_99; 
                    v_window_5_99 = v_window_6_100; 
                    v_window_7_101 = v_window_8_102; 
                    v_window_8_102 = v_window_9_103; 
                    v_window_10_104 = v_window_11_105; 
                    v_window_11_105 = v_window_12_106; 
                    v_window_13_107 = v_window_14_108; 
                    v_window_14_108 = v_window_15_109; 
                    v_window_16_110 = v_window_17_111; 
                    v_window_17_111 = v_window_18_112; 
                    v_window_19_113 = v_window_20_114; 
                    v_window_20_114 = v_window_21_115; 
                    v_window_22_116 = v_window_23_117; 
                    v_window_23_117 = v_window_24_118; 
                    v_window_25_119 = v_window_26_120; 
                    v_window_26_120 = v_window_27_121; 
                }
                // end slideSeq_plus
            }
        }
    }
}