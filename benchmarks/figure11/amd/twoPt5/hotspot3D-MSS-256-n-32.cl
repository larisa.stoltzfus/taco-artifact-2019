#atf::var<int> v_M_0 = 256 
#atf::var<int> v_N_1 = 256 
#atf::var<int> v_O_2 = 32 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(128,128)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(256,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(256,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float calculateHotspot(float tInC, float cc, float tInN, float cn, float tInS, float cs, float tInE, float ce, float tInW, float cw, float tInT, float ct, float tInB, float cb, float stepDivCap, float pInC, float amb_temp){
    {
        { return  tInC*cc + tInN*cn + tInS*cs + tInE*ce + tInW*cw + tInT*ct + tInB*cb + stepDivCap * pInC + ct*amb_temp; }; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__143, const global float* restrict v__144, global float* v__159){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        float v__148; 
        float v__149; 
        float v__150; 
        float v__151; 
        float v__152; 
        float v__153; 
        float v__154; 
        float v__155; 
        float v__156; 
        // Private Memory
        float v__158; 
        for (int v_gl_id_140 = get_global_id(0); (v_gl_id_140 < 256); v_gl_id_140 = (v_gl_id_140 + get_global_size(0))){
            for (int v_gl_id_141 = get_global_id(1); (v_gl_id_141 < 256); v_gl_id_141 = (v_gl_id_141 + get_global_size(1))){
                // slideSeq_plus
                float v_window_1_0_215; 
                float v_window_1_1_216; 
                float v_window_2_0_217; 
                float v_window_2_1_218; 
                float v_window_3_0_219; 
                float v_window_3_1_220; 
                float v_window_4_0_221; 
                float v_window_4_1_222; 
                float v_window_5_0_223; 
                float v_window_5_1_224; 
                float v_window_6_0_225; 
                float v_window_6_1_226; 
                float v_window_7_0_227; 
                float v_window_7_1_228; 
                float v_window_8_0_229; 
                float v_window_8_1_230; 
                float v_window_9_0_231; 
                float v_window_9_1_232; 
                float v_window_10_0_233; 
                float v_window_10_1_234; 
                float v_window_11_0_235; 
                float v_window_11_1_236; 
                float v_window_12_0_237; 
                float v_window_12_1_238; 
                float v_window_13_0_239; 
                float v_window_13_1_240; 
                float v_window_14_0_241; 
                float v_window_14_1_242; 
                float v_window_15_0_243; 
                float v_window_15_1_244; 
                float v_window_16_0_245; 
                float v_window_16_1_246; 
                float v_window_17_0_247; 
                float v_window_17_1_248; 
                float v_window_18_0_249; 
                float v_window_18_1_250; 
                float v_window_19_0_251; 
                float v_window_19_1_252; 
                float v_window_20_0_253; 
                float v_window_20_1_254; 
                float v_window_21_0_255; 
                float v_window_21_1_256; 
                float v_window_22_0_257; 
                float v_window_22_1_258; 
                float v_window_23_0_259; 
                float v_window_23_1_260; 
                float v_window_24_0_261; 
                float v_window_24_1_262; 
                float v_window_25_0_263; 
                float v_window_25_1_264; 
                float v_window_26_0_265; 
                float v_window_26_1_266; 
                float v_window_27_0_267; 
                float v_window_27_1_268; 
                v_window_1_0_215 = v__144[((256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_1_1_216 = v__143[((256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_2_0_217 = v__144[((256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_2_1_218 = v__143[((256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_4_0_221 = v__144[((256 * v_gl_id_141) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_4_1_222 = v__143[((256 * v_gl_id_141) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_5_0_223 = v__144[((256 * v_gl_id_141) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_5_1_224 = v__143[((256 * v_gl_id_141) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_7_0_227 = v__144[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_7_1_228 = v__143[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_8_0_229 = v__144[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_8_1_230 = v__143[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                v_window_10_0_233 = v__144[(v_gl_id_140 + (256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )))]; 
                v_window_10_1_234 = v__143[(v_gl_id_140 + (256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )))]; 
                v_window_11_0_235 = v__144[(v_gl_id_140 + (256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )))]; 
                v_window_11_1_236 = v__143[(v_gl_id_140 + (256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )))]; 
                v_window_13_0_239 = v__144[(v_gl_id_140 + (256 * v_gl_id_141))]; 
                v_window_13_1_240 = v__143[(v_gl_id_140 + (256 * v_gl_id_141))]; 
                v_window_14_0_241 = v__144[(v_gl_id_140 + (256 * v_gl_id_141))]; 
                v_window_14_1_242 = v__143[(v_gl_id_140 + (256 * v_gl_id_141))]; 
                v_window_16_0_245 = v__144[(v_gl_id_140 + (256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )))]; 
                v_window_16_1_246 = v__143[(v_gl_id_140 + (256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )))]; 
                v_window_17_0_247 = v__144[(v_gl_id_140 + (256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )))]; 
                v_window_17_1_248 = v__143[(v_gl_id_140 + (256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )))]; 
                v_window_19_0_251 = v__144[((256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                v_window_19_1_252 = v__143[((256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                v_window_20_0_253 = v__144[((256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                v_window_20_1_254 = v__143[((256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                v_window_22_0_257 = v__144[((256 * v_gl_id_141) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                v_window_22_1_258 = v__143[((256 * v_gl_id_141) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                v_window_23_0_259 = v__144[((256 * v_gl_id_141) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                v_window_23_1_260 = v__143[((256 * v_gl_id_141) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                v_window_25_0_263 = v__144[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                v_window_25_1_264 = v__143[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                v_window_26_0_265 = v__144[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                v_window_26_1_266 = v__143[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                for (int v_i_142 = 0; (v_i_142 < 32); v_i_142 = (1 + v_i_142)){
                    v_window_3_0_219 = v__144[((65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + (256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                    v_window_3_1_220 = v__143[((65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + (256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                    v_window_6_0_225 = v__144[((256 * v_gl_id_141) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                    v_window_6_1_226 = v__143[((256 * v_gl_id_141) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                    v_window_9_0_231 = v__144[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                    v_window_9_1_232 = v__143[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + ( ((-1 + v_gl_id_140) >= 0) ? (-1 + v_gl_id_140) : 0 ))]; 
                    v_window_12_0_237 = v__144[(v_gl_id_140 + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + (256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )))]; 
                    v_window_12_1_238 = v__143[(v_gl_id_140 + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + (256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )))]; 
                    v_window_15_0_243 = v__144[(v_gl_id_140 + (256 * v_gl_id_141) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )))]; 
                    v_window_15_1_244 = v__143[(v_gl_id_140 + (256 * v_gl_id_141) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )))]; 
                    v_window_18_0_249 = v__144[(v_gl_id_140 + (256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )))]; 
                    v_window_18_1_250 = v__143[(v_gl_id_140 + (256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )))]; 
                    v_window_21_0_255 = v__144[((65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + (256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                    v_window_21_1_256 = v__143[((65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + (256 * ( ((-1 + v_gl_id_141) >= 0) ? (-1 + v_gl_id_141) : 0 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                    v_window_24_0_261 = v__144[((256 * v_gl_id_141) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                    v_window_24_1_262 = v__143[((256 * v_gl_id_141) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                    v_window_27_0_267 = v__144[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                    v_window_27_1_268 = v__143[((256 * ( ((1 + v_gl_id_141) < 256) ? (1 + v_gl_id_141) : 255 )) + (65536 * ( ((1 + v_i_142) < 32) ? (1 + v_i_142) : 31 )) + ( ((1 + v_gl_id_140) < 256) ? (1 + v_gl_id_140) : 255 ))]; 
                    float v_tmp_179 = 0.86186665f; 
                    v__148 = v_tmp_179; 
                    float v_tmp_180 = 0.03413333f; 
                    v__149 = v_tmp_180; 
                    float v_tmp_181 = 0.03413333f; 
                    v__150 = v_tmp_181; 
                    float v_tmp_182 = 0.03413333f; 
                    v__151 = v_tmp_182; 
                    float v_tmp_183 = 0.03413333f; 
                    v__152 = v_tmp_183; 
                    float v_tmp_184 = 5.333333E-4f; 
                    v__153 = v_tmp_184; 
                    float v_tmp_185 = 5.333333E-4f; 
                    v__154 = v_tmp_185; 
                    float v_tmp_186 = 0.3413333f; 
                    v__155 = v_tmp_186; 
                    float v_tmp_187 = 80.0f; 
                    v__156 = v_tmp_187; 
                    v__158 = calculateHotspot(v_window_14_1_242, v__148, v_window_11_1_236, v__149, v_window_17_1_248, v__150, v_window_23_1_260, v__151, v_window_5_1_224, v__152, v_window_15_1_244, v__153, v_window_13_1_240, v__154, v__155, v_window_14_0_241, v__156); 
                    v__159[(v_gl_id_140 + (256 * v_gl_id_141) + (65536 * v_i_142))] = id(v__158); 
                    v_window_1_0_215 = v_window_2_0_217; 
                    v_window_1_1_216 = v_window_2_1_218; 
                    v_window_2_0_217 = v_window_3_0_219; 
                    v_window_2_1_218 = v_window_3_1_220; 
                    v_window_4_0_221 = v_window_5_0_223; 
                    v_window_4_1_222 = v_window_5_1_224; 
                    v_window_5_0_223 = v_window_6_0_225; 
                    v_window_5_1_224 = v_window_6_1_226; 
                    v_window_7_0_227 = v_window_8_0_229; 
                    v_window_7_1_228 = v_window_8_1_230; 
                    v_window_8_0_229 = v_window_9_0_231; 
                    v_window_8_1_230 = v_window_9_1_232; 
                    v_window_10_0_233 = v_window_11_0_235; 
                    v_window_10_1_234 = v_window_11_1_236; 
                    v_window_11_0_235 = v_window_12_0_237; 
                    v_window_11_1_236 = v_window_12_1_238; 
                    v_window_13_0_239 = v_window_14_0_241; 
                    v_window_13_1_240 = v_window_14_1_242; 
                    v_window_14_0_241 = v_window_15_0_243; 
                    v_window_14_1_242 = v_window_15_1_244; 
                    v_window_16_0_245 = v_window_17_0_247; 
                    v_window_16_1_246 = v_window_17_1_248; 
                    v_window_17_0_247 = v_window_18_0_249; 
                    v_window_17_1_248 = v_window_18_1_250; 
                    v_window_19_0_251 = v_window_20_0_253; 
                    v_window_19_1_252 = v_window_20_1_254; 
                    v_window_20_0_253 = v_window_21_0_255; 
                    v_window_20_1_254 = v_window_21_1_256; 
                    v_window_22_0_257 = v_window_23_0_259; 
                    v_window_22_1_258 = v_window_23_1_260; 
                    v_window_23_0_259 = v_window_24_0_261; 
                    v_window_23_1_260 = v_window_24_1_262; 
                    v_window_25_0_263 = v_window_26_0_265; 
                    v_window_25_1_264 = v_window_26_1_266; 
                    v_window_26_0_265 = v_window_27_0_267; 
                    v_window_26_1_266 = v_window_27_1_268; 
                }
                // end slideSeq_plus
            }
        }
    }
}