#atf::var<int> v_M_0 = 256 
#atf::var<int> v_N_1 = 256 
#atf::var<int> v_O_2 = 256 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(64,64)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(32,32)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float jacobi13(float EE, float E, float W, float WW, float SS, float S, float N, float NN, float BB, float B, float F, float FF, float C){
    {
        return 0.083f * EE + 0.083f * E + 0.083f * W + 0.083f * WW +
       0.083f * SS + 0.083f * S + 0.083f * N + 0.083f * NN +
       0.083f * BB + 0.083f * B + 0.083f * F + 0.083f * FF -
       0.996f * C;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__599, global float* v__602){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__601; 
        for (int v_gl_id_596 = get_global_id(0); (v_gl_id_596 < 252); v_gl_id_596 = (v_gl_id_596 + get_global_size(0))){
            for (int v_gl_id_597 = get_global_id(1); (v_gl_id_597 < 252); v_gl_id_597 = (v_gl_id_597 + get_global_size(1))){
                // mapSeqSlide
                private float v_window_565[125]; 
                v_window_565[0] = v__599[(v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[25] = v__599[(65536 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[50] = v__599[(131072 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[75] = v__599[(196608 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[5] = v__599[(256 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[30] = v__599[(65792 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[55] = v__599[(131328 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[80] = v__599[(196864 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[10] = v__599[(512 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[35] = v__599[(66048 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[60] = v__599[(131584 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[85] = v__599[(197120 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[15] = v__599[(768 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[40] = v__599[(66304 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[65] = v__599[(131840 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[90] = v__599[(197376 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[20] = v__599[(1024 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[45] = v__599[(66560 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[70] = v__599[(132096 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[95] = v__599[(197632 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[1] = v__599[(1 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[26] = v__599[(65537 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[51] = v__599[(131073 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[76] = v__599[(196609 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[6] = v__599[(257 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[31] = v__599[(65793 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[56] = v__599[(131329 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[81] = v__599[(196865 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[11] = v__599[(513 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[36] = v__599[(66049 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[61] = v__599[(131585 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[86] = v__599[(197121 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[16] = v__599[(769 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[41] = v__599[(66305 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[66] = v__599[(131841 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[91] = v__599[(197377 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[21] = v__599[(1025 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[46] = v__599[(66561 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[71] = v__599[(132097 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[96] = v__599[(197633 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[2] = v__599[(2 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[27] = v__599[(65538 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[52] = v__599[(131074 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[77] = v__599[(196610 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[7] = v__599[(258 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[32] = v__599[(65794 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[57] = v__599[(131330 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[82] = v__599[(196866 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[12] = v__599[(514 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[37] = v__599[(66050 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[62] = v__599[(131586 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[87] = v__599[(197122 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[17] = v__599[(770 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[42] = v__599[(66306 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[67] = v__599[(131842 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[92] = v__599[(197378 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[22] = v__599[(1026 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[47] = v__599[(66562 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[72] = v__599[(132098 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[97] = v__599[(197634 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[3] = v__599[(3 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[28] = v__599[(65539 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[53] = v__599[(131075 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[78] = v__599[(196611 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[8] = v__599[(259 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[33] = v__599[(65795 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[58] = v__599[(131331 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[83] = v__599[(196867 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[13] = v__599[(515 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[38] = v__599[(66051 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[63] = v__599[(131587 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[88] = v__599[(197123 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[18] = v__599[(771 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[43] = v__599[(66307 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[68] = v__599[(131843 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[93] = v__599[(197379 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[23] = v__599[(1027 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[48] = v__599[(66563 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[73] = v__599[(132099 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[98] = v__599[(197635 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[4] = v__599[(4 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[29] = v__599[(65540 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[54] = v__599[(131076 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[79] = v__599[(196612 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[9] = v__599[(260 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[34] = v__599[(65796 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[59] = v__599[(131332 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[84] = v__599[(196868 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[14] = v__599[(516 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[39] = v__599[(66052 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[64] = v__599[(131588 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[89] = v__599[(197124 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[19] = v__599[(772 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[44] = v__599[(66308 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[69] = v__599[(131844 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[94] = v__599[(197380 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[24] = v__599[(1028 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[49] = v__599[(66564 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[74] = v__599[(132100 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                v_window_565[99] = v__599[(197636 + v_gl_id_596 + (256 * v_gl_id_597))]; 
                for (int v_i_598 = 0; (v_i_598 < 252); v_i_598 = (1 + v_i_598)){
                    v_window_565[100] = v__599[(262144 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[105] = v__599[(262400 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[110] = v__599[(262656 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[115] = v__599[(262912 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[120] = v__599[(263168 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[101] = v__599[(262145 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[106] = v__599[(262401 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[111] = v__599[(262657 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[116] = v__599[(262913 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[121] = v__599[(263169 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[102] = v__599[(262146 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[107] = v__599[(262402 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[112] = v__599[(262658 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[117] = v__599[(262914 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[122] = v__599[(263170 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[103] = v__599[(262147 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[108] = v__599[(262403 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[113] = v__599[(262659 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[118] = v__599[(262915 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[123] = v__599[(263171 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[104] = v__599[(262148 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[109] = v__599[(262404 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[114] = v__599[(262660 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[119] = v__599[(262916 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v_window_565[124] = v__599[(263172 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))]; 
                    v__601 = jacobi13(v_window_565[64], v_window_565[63], v_window_565[61], v_window_565[60], v_window_565[72], v_window_565[67], v_window_565[57], v_window_565[52], v_window_565[112], v_window_565[87], v_window_565[37], v_window_565[12], v_window_565[62]); 
                    v__602[(131586 + v_gl_id_596 + (256 * v_gl_id_597) + (65536 * v_i_598))] = id(v__601); 
                    v_window_565[0] = v_window_565[25]; 
                    v_window_565[25] = v_window_565[50]; 
                    v_window_565[50] = v_window_565[75]; 
                    v_window_565[75] = v_window_565[100]; 
                    v_window_565[1] = v_window_565[26]; 
                    v_window_565[26] = v_window_565[51]; 
                    v_window_565[51] = v_window_565[76]; 
                    v_window_565[76] = v_window_565[101]; 
                    v_window_565[2] = v_window_565[27]; 
                    v_window_565[27] = v_window_565[52]; 
                    v_window_565[52] = v_window_565[77]; 
                    v_window_565[77] = v_window_565[102]; 
                    v_window_565[3] = v_window_565[28]; 
                    v_window_565[28] = v_window_565[53]; 
                    v_window_565[53] = v_window_565[78]; 
                    v_window_565[78] = v_window_565[103]; 
                    v_window_565[4] = v_window_565[29]; 
                    v_window_565[29] = v_window_565[54]; 
                    v_window_565[54] = v_window_565[79]; 
                    v_window_565[79] = v_window_565[104]; 
                    v_window_565[5] = v_window_565[30]; 
                    v_window_565[30] = v_window_565[55]; 
                    v_window_565[55] = v_window_565[80]; 
                    v_window_565[80] = v_window_565[105]; 
                    v_window_565[6] = v_window_565[31]; 
                    v_window_565[31] = v_window_565[56]; 
                    v_window_565[56] = v_window_565[81]; 
                    v_window_565[81] = v_window_565[106]; 
                    v_window_565[7] = v_window_565[32]; 
                    v_window_565[32] = v_window_565[57]; 
                    v_window_565[57] = v_window_565[82]; 
                    v_window_565[82] = v_window_565[107]; 
                    v_window_565[8] = v_window_565[33]; 
                    v_window_565[33] = v_window_565[58]; 
                    v_window_565[58] = v_window_565[83]; 
                    v_window_565[83] = v_window_565[108]; 
                    v_window_565[9] = v_window_565[34]; 
                    v_window_565[34] = v_window_565[59]; 
                    v_window_565[59] = v_window_565[84]; 
                    v_window_565[84] = v_window_565[109]; 
                    v_window_565[10] = v_window_565[35]; 
                    v_window_565[35] = v_window_565[60]; 
                    v_window_565[60] = v_window_565[85]; 
                    v_window_565[85] = v_window_565[110]; 
                    v_window_565[11] = v_window_565[36]; 
                    v_window_565[36] = v_window_565[61]; 
                    v_window_565[61] = v_window_565[86]; 
                    v_window_565[86] = v_window_565[111]; 
                    v_window_565[12] = v_window_565[37]; 
                    v_window_565[37] = v_window_565[62]; 
                    v_window_565[62] = v_window_565[87]; 
                    v_window_565[87] = v_window_565[112]; 
                    v_window_565[13] = v_window_565[38]; 
                    v_window_565[38] = v_window_565[63]; 
                    v_window_565[63] = v_window_565[88]; 
                    v_window_565[88] = v_window_565[113]; 
                    v_window_565[14] = v_window_565[39]; 
                    v_window_565[39] = v_window_565[64]; 
                    v_window_565[64] = v_window_565[89]; 
                    v_window_565[89] = v_window_565[114]; 
                    v_window_565[15] = v_window_565[40]; 
                    v_window_565[40] = v_window_565[65]; 
                    v_window_565[65] = v_window_565[90]; 
                    v_window_565[90] = v_window_565[115]; 
                    v_window_565[16] = v_window_565[41]; 
                    v_window_565[41] = v_window_565[66]; 
                    v_window_565[66] = v_window_565[91]; 
                    v_window_565[91] = v_window_565[116]; 
                    v_window_565[17] = v_window_565[42]; 
                    v_window_565[42] = v_window_565[67]; 
                    v_window_565[67] = v_window_565[92]; 
                    v_window_565[92] = v_window_565[117]; 
                    v_window_565[18] = v_window_565[43]; 
                    v_window_565[43] = v_window_565[68]; 
                    v_window_565[68] = v_window_565[93]; 
                    v_window_565[93] = v_window_565[118]; 
                    v_window_565[19] = v_window_565[44]; 
                    v_window_565[44] = v_window_565[69]; 
                    v_window_565[69] = v_window_565[94]; 
                    v_window_565[94] = v_window_565[119]; 
                    v_window_565[20] = v_window_565[45]; 
                    v_window_565[45] = v_window_565[70]; 
                    v_window_565[70] = v_window_565[95]; 
                    v_window_565[95] = v_window_565[120]; 
                    v_window_565[21] = v_window_565[46]; 
                    v_window_565[46] = v_window_565[71]; 
                    v_window_565[71] = v_window_565[96]; 
                    v_window_565[96] = v_window_565[121]; 
                    v_window_565[22] = v_window_565[47]; 
                    v_window_565[47] = v_window_565[72]; 
                    v_window_565[72] = v_window_565[97]; 
                    v_window_565[97] = v_window_565[122]; 
                    v_window_565[23] = v_window_565[48]; 
                    v_window_565[48] = v_window_565[73]; 
                    v_window_565[73] = v_window_565[98]; 
                    v_window_565[98] = v_window_565[123]; 
                    v_window_565[24] = v_window_565[49]; 
                    v_window_565[49] = v_window_565[74]; 
                    v_window_565[74] = v_window_565[99]; 
                    v_window_565[99] = v_window_565[124]; 
                }
                // end mapSeqSlide
            }
        }
    }
}
