#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 128 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(4,4)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(64,64)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(32,32)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float jacobi13(float EE, float E, float W, float WW, float SS, float S, float N, float NN, float BB, float B, float F, float FF, float C){
    {
        return 0.083f * EE + 0.083f * E + 0.083f * W + 0.083f * WW +
       0.083f * SS + 0.083f * S + 0.083f * N + 0.083f * NN +
       0.083f * BB + 0.083f * B + 0.083f * F + 0.083f * FF -
       0.996f * C;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__599, global float* v__602){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__601; 
        for (int v_gl_id_596 = get_global_id(0); (v_gl_id_596 < 508); v_gl_id_596 = (v_gl_id_596 + get_global_size(0))){
            for (int v_gl_id_597 = get_global_id(1); (v_gl_id_597 < 508); v_gl_id_597 = (v_gl_id_597 + get_global_size(1))){
                // mapSeqSlide
                private float v_window_565[125]; 
                v_window_565[0] = v__599[(v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[25] = v__599[(262144 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[50] = v__599[(524288 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[75] = v__599[(786432 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[5] = v__599[(512 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[30] = v__599[(262656 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[55] = v__599[(524800 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[80] = v__599[(786944 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[10] = v__599[(1024 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[35] = v__599[(263168 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[60] = v__599[(525312 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[85] = v__599[(787456 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[15] = v__599[(1536 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[40] = v__599[(263680 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[65] = v__599[(525824 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[90] = v__599[(787968 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[20] = v__599[(2048 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[45] = v__599[(264192 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[70] = v__599[(526336 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[95] = v__599[(788480 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[1] = v__599[(1 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[26] = v__599[(262145 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[51] = v__599[(524289 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[76] = v__599[(786433 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[6] = v__599[(513 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[31] = v__599[(262657 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[56] = v__599[(524801 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[81] = v__599[(786945 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[11] = v__599[(1025 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[36] = v__599[(263169 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[61] = v__599[(525313 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[86] = v__599[(787457 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[16] = v__599[(1537 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[41] = v__599[(263681 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[66] = v__599[(525825 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[91] = v__599[(787969 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[21] = v__599[(2049 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[46] = v__599[(264193 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[71] = v__599[(526337 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[96] = v__599[(788481 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[2] = v__599[(2 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[27] = v__599[(262146 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[52] = v__599[(524290 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[77] = v__599[(786434 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[7] = v__599[(514 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[32] = v__599[(262658 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[57] = v__599[(524802 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[82] = v__599[(786946 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[12] = v__599[(1026 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[37] = v__599[(263170 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[62] = v__599[(525314 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[87] = v__599[(787458 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[17] = v__599[(1538 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[42] = v__599[(263682 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[67] = v__599[(525826 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[92] = v__599[(787970 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[22] = v__599[(2050 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[47] = v__599[(264194 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[72] = v__599[(526338 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[97] = v__599[(788482 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[3] = v__599[(3 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[28] = v__599[(262147 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[53] = v__599[(524291 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[78] = v__599[(786435 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[8] = v__599[(515 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[33] = v__599[(262659 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[58] = v__599[(524803 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[83] = v__599[(786947 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[13] = v__599[(1027 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[38] = v__599[(263171 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[63] = v__599[(525315 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[88] = v__599[(787459 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[18] = v__599[(1539 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[43] = v__599[(263683 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[68] = v__599[(525827 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[93] = v__599[(787971 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[23] = v__599[(2051 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[48] = v__599[(264195 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[73] = v__599[(526339 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[98] = v__599[(788483 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[4] = v__599[(4 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[29] = v__599[(262148 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[54] = v__599[(524292 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[79] = v__599[(786436 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[9] = v__599[(516 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[34] = v__599[(262660 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[59] = v__599[(524804 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[84] = v__599[(786948 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[14] = v__599[(1028 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[39] = v__599[(263172 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[64] = v__599[(525316 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[89] = v__599[(787460 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[19] = v__599[(1540 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[44] = v__599[(263684 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[69] = v__599[(525828 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[94] = v__599[(787972 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[24] = v__599[(2052 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[49] = v__599[(264196 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[74] = v__599[(526340 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                v_window_565[99] = v__599[(788484 + v_gl_id_596 + (512 * v_gl_id_597))]; 
                for (int v_i_598 = 0; (v_i_598 < 124); v_i_598 = (1 + v_i_598)){
                    v_window_565[100] = v__599[(1048576 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[105] = v__599[(1049088 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[110] = v__599[(1049600 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[115] = v__599[(1050112 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[120] = v__599[(1050624 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[101] = v__599[(1048577 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[106] = v__599[(1049089 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[111] = v__599[(1049601 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[116] = v__599[(1050113 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[121] = v__599[(1050625 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[102] = v__599[(1048578 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[107] = v__599[(1049090 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[112] = v__599[(1049602 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[117] = v__599[(1050114 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[122] = v__599[(1050626 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[103] = v__599[(1048579 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[108] = v__599[(1049091 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[113] = v__599[(1049603 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[118] = v__599[(1050115 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[123] = v__599[(1050627 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[104] = v__599[(1048580 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[109] = v__599[(1049092 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[114] = v__599[(1049604 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[119] = v__599[(1050116 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v_window_565[124] = v__599[(1050628 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))]; 
                    v__601 = jacobi13(v_window_565[64], v_window_565[63], v_window_565[61], v_window_565[60], v_window_565[72], v_window_565[67], v_window_565[57], v_window_565[52], v_window_565[112], v_window_565[87], v_window_565[37], v_window_565[12], v_window_565[62]); 
                    v__602[(525314 + v_gl_id_596 + (512 * v_gl_id_597) + (262144 * v_i_598))] = id(v__601); 
                    v_window_565[0] = v_window_565[25]; 
                    v_window_565[25] = v_window_565[50]; 
                    v_window_565[50] = v_window_565[75]; 
                    v_window_565[75] = v_window_565[100]; 
                    v_window_565[1] = v_window_565[26]; 
                    v_window_565[26] = v_window_565[51]; 
                    v_window_565[51] = v_window_565[76]; 
                    v_window_565[76] = v_window_565[101]; 
                    v_window_565[2] = v_window_565[27]; 
                    v_window_565[27] = v_window_565[52]; 
                    v_window_565[52] = v_window_565[77]; 
                    v_window_565[77] = v_window_565[102]; 
                    v_window_565[3] = v_window_565[28]; 
                    v_window_565[28] = v_window_565[53]; 
                    v_window_565[53] = v_window_565[78]; 
                    v_window_565[78] = v_window_565[103]; 
                    v_window_565[4] = v_window_565[29]; 
                    v_window_565[29] = v_window_565[54]; 
                    v_window_565[54] = v_window_565[79]; 
                    v_window_565[79] = v_window_565[104]; 
                    v_window_565[5] = v_window_565[30]; 
                    v_window_565[30] = v_window_565[55]; 
                    v_window_565[55] = v_window_565[80]; 
                    v_window_565[80] = v_window_565[105]; 
                    v_window_565[6] = v_window_565[31]; 
                    v_window_565[31] = v_window_565[56]; 
                    v_window_565[56] = v_window_565[81]; 
                    v_window_565[81] = v_window_565[106]; 
                    v_window_565[7] = v_window_565[32]; 
                    v_window_565[32] = v_window_565[57]; 
                    v_window_565[57] = v_window_565[82]; 
                    v_window_565[82] = v_window_565[107]; 
                    v_window_565[8] = v_window_565[33]; 
                    v_window_565[33] = v_window_565[58]; 
                    v_window_565[58] = v_window_565[83]; 
                    v_window_565[83] = v_window_565[108]; 
                    v_window_565[9] = v_window_565[34]; 
                    v_window_565[34] = v_window_565[59]; 
                    v_window_565[59] = v_window_565[84]; 
                    v_window_565[84] = v_window_565[109]; 
                    v_window_565[10] = v_window_565[35]; 
                    v_window_565[35] = v_window_565[60]; 
                    v_window_565[60] = v_window_565[85]; 
                    v_window_565[85] = v_window_565[110]; 
                    v_window_565[11] = v_window_565[36]; 
                    v_window_565[36] = v_window_565[61]; 
                    v_window_565[61] = v_window_565[86]; 
                    v_window_565[86] = v_window_565[111]; 
                    v_window_565[12] = v_window_565[37]; 
                    v_window_565[37] = v_window_565[62]; 
                    v_window_565[62] = v_window_565[87]; 
                    v_window_565[87] = v_window_565[112]; 
                    v_window_565[13] = v_window_565[38]; 
                    v_window_565[38] = v_window_565[63]; 
                    v_window_565[63] = v_window_565[88]; 
                    v_window_565[88] = v_window_565[113]; 
                    v_window_565[14] = v_window_565[39]; 
                    v_window_565[39] = v_window_565[64]; 
                    v_window_565[64] = v_window_565[89]; 
                    v_window_565[89] = v_window_565[114]; 
                    v_window_565[15] = v_window_565[40]; 
                    v_window_565[40] = v_window_565[65]; 
                    v_window_565[65] = v_window_565[90]; 
                    v_window_565[90] = v_window_565[115]; 
                    v_window_565[16] = v_window_565[41]; 
                    v_window_565[41] = v_window_565[66]; 
                    v_window_565[66] = v_window_565[91]; 
                    v_window_565[91] = v_window_565[116]; 
                    v_window_565[17] = v_window_565[42]; 
                    v_window_565[42] = v_window_565[67]; 
                    v_window_565[67] = v_window_565[92]; 
                    v_window_565[92] = v_window_565[117]; 
                    v_window_565[18] = v_window_565[43]; 
                    v_window_565[43] = v_window_565[68]; 
                    v_window_565[68] = v_window_565[93]; 
                    v_window_565[93] = v_window_565[118]; 
                    v_window_565[19] = v_window_565[44]; 
                    v_window_565[44] = v_window_565[69]; 
                    v_window_565[69] = v_window_565[94]; 
                    v_window_565[94] = v_window_565[119]; 
                    v_window_565[20] = v_window_565[45]; 
                    v_window_565[45] = v_window_565[70]; 
                    v_window_565[70] = v_window_565[95]; 
                    v_window_565[95] = v_window_565[120]; 
                    v_window_565[21] = v_window_565[46]; 
                    v_window_565[46] = v_window_565[71]; 
                    v_window_565[71] = v_window_565[96]; 
                    v_window_565[96] = v_window_565[121]; 
                    v_window_565[22] = v_window_565[47]; 
                    v_window_565[47] = v_window_565[72]; 
                    v_window_565[72] = v_window_565[97]; 
                    v_window_565[97] = v_window_565[122]; 
                    v_window_565[23] = v_window_565[48]; 
                    v_window_565[48] = v_window_565[73]; 
                    v_window_565[73] = v_window_565[98]; 
                    v_window_565[98] = v_window_565[123]; 
                    v_window_565[24] = v_window_565[49]; 
                    v_window_565[49] = v_window_565[74]; 
                    v_window_565[74] = v_window_565[99]; 
                    v_window_565[99] = v_window_565[124]; 
                }
                // end mapSeqSlide
            }
        }
    }
}
