#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 128 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(32,32)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(32,32)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float jacobi(float C, float N, float S, float E, float W, float F, float B){
    {
        return 0.161f * E + 0.162f * W +
      0.163f * S + 0.164f * N +
      0.165f * B + 0.166f * F -
      1.67f * C;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__1022, global float* v__1025){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__1024; 
        for (int v_gl_id_1019 = get_global_id(0); (v_gl_id_1019 < 510); v_gl_id_1019 = (v_gl_id_1019 + get_global_size(0))){
            for (int v_gl_id_1020 = get_global_id(1); (v_gl_id_1020 < 510); v_gl_id_1020 = (v_gl_id_1020 + get_global_size(1))){
                // mapSeqSlide
                private float v_window_1012[27]; 
                v_window_1012[0] = v__1022[(v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[9] = v__1022[(262144 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[3] = v__1022[(512 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[12] = v__1022[(262656 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[6] = v__1022[(1024 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[15] = v__1022[(263168 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[1] = v__1022[(1 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[10] = v__1022[(262145 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[4] = v__1022[(513 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[13] = v__1022[(262657 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[7] = v__1022[(1025 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[16] = v__1022[(263169 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[2] = v__1022[(2 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[11] = v__1022[(262146 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[5] = v__1022[(514 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[14] = v__1022[(262658 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[8] = v__1022[(1026 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                v_window_1012[17] = v__1022[(263170 + v_gl_id_1019 + (512 * v_gl_id_1020))]; 
                for (int v_i_1021 = 0; (v_i_1021 < 126); v_i_1021 = (1 + v_i_1021)){
                    v_window_1012[18] = v__1022[(524288 + v_gl_id_1019 + (512 * v_gl_id_1020) + (262144 * v_i_1021))]; 
                    v_window_1012[21] = v__1022[(524800 + v_gl_id_1019 + (512 * v_gl_id_1020) + (262144 * v_i_1021))]; 
                    v_window_1012[24] = v__1022[(525312 + v_gl_id_1019 + (512 * v_gl_id_1020) + (262144 * v_i_1021))]; 
                    v_window_1012[19] = v__1022[(524289 + v_gl_id_1019 + (512 * v_gl_id_1020) + (262144 * v_i_1021))]; 
                    v_window_1012[22] = v__1022[(524801 + v_gl_id_1019 + (512 * v_gl_id_1020) + (262144 * v_i_1021))]; 
                    v_window_1012[25] = v__1022[(525313 + v_gl_id_1019 + (512 * v_gl_id_1020) + (262144 * v_i_1021))]; 
                    v_window_1012[20] = v__1022[(524290 + v_gl_id_1019 + (512 * v_gl_id_1020) + (262144 * v_i_1021))]; 
                    v_window_1012[23] = v__1022[(524802 + v_gl_id_1019 + (512 * v_gl_id_1020) + (262144 * v_i_1021))]; 
                    v_window_1012[26] = v__1022[(525314 + v_gl_id_1019 + (512 * v_gl_id_1020) + (262144 * v_i_1021))]; 
                    v__1024 = jacobi(v_window_1012[13], v_window_1012[10], v_window_1012[16], v_window_1012[14], v_window_1012[12], v_window_1012[4], v_window_1012[22]); 
                    v__1025[(262657 + v_gl_id_1019 + (512 * v_gl_id_1020) + (262144 * v_i_1021))] = id(v__1024); 
                    v_window_1012[0] = v_window_1012[9]; 
                    v_window_1012[9] = v_window_1012[18]; 
                    v_window_1012[1] = v_window_1012[10]; 
                    v_window_1012[10] = v_window_1012[19]; 
                    v_window_1012[2] = v_window_1012[11]; 
                    v_window_1012[11] = v_window_1012[20]; 
                    v_window_1012[3] = v_window_1012[12]; 
                    v_window_1012[12] = v_window_1012[21]; 
                    v_window_1012[4] = v_window_1012[13]; 
                    v_window_1012[13] = v_window_1012[22]; 
                    v_window_1012[5] = v_window_1012[14]; 
                    v_window_1012[14] = v_window_1012[23]; 
                    v_window_1012[6] = v_window_1012[15]; 
                    v_window_1012[15] = v_window_1012[24]; 
                    v_window_1012[7] = v_window_1012[16]; 
                    v_window_1012[16] = v_window_1012[25]; 
                    v_window_1012[8] = v_window_1012[17]; 
                    v_window_1012[17] = v_window_1012[26]; 
                }
                // end mapSeqSlide
            }
        }
    }
}
