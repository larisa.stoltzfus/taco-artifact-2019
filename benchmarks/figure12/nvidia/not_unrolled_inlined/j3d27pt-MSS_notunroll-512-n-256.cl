#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 256 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,300) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(128,128)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(512,512)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(2,2)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float jacobi27(float FNW, float FN, float FNE, float FW, float F, float FE, float FSW, float FS, float FSE, float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE, float BNW, float BN, float BNE, float BW, float B, float BE, float BSW, float BS, float BSE){
    {
        return (0.5 * FNW + 0.7 * FN + 0.9 * FNE +
        1.2 * FW + 1.5 * F + 1.2 * FE +
        0.9 * FSW + 0.7 * FS + 0.5 * FSE +
        0.51 * NW + 0.71 * N + 0.91 * NE +
        1.21 * W + 1.51 * C + 1.21 * E +
        0.91 * SW + 0.71 * S + 0.51 * SE +
        0.52 * BNW + 0.72 * BN + 0.92 * BNE +
        1.22 * BW + 1.52 * B + 1.22 * BE +
        0.92 * BSW + 0.72 * BS + 0.52 * BSE) / 159;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__70, global float* v__73){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__72; 
        for (int v_gl_id_67 = get_global_id(0); (v_gl_id_67 < 510); v_gl_id_67 = (v_gl_id_67 + get_global_size(0))){
            for (int v_gl_id_68 = get_global_id(1); (v_gl_id_68 < 510); v_gl_id_68 = (v_gl_id_68 + get_global_size(1))){
                // mapSeqSlide
                private float v_window_35[27]; 
                v_window_35[0] = v__70[(v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[9] = v__70[(262144 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[3] = v__70[(512 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[12] = v__70[(262656 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[6] = v__70[(1024 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[15] = v__70[(263168 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[1] = v__70[(1 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[10] = v__70[(262145 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[4] = v__70[(513 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[13] = v__70[(262657 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[7] = v__70[(1025 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[16] = v__70[(263169 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[2] = v__70[(2 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[11] = v__70[(262146 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[5] = v__70[(514 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[14] = v__70[(262658 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[8] = v__70[(1026 + v_gl_id_67 + (512 * v_gl_id_68))]; 
                v_window_35[17] = v__70[(263170 + v_gl_id_67 + (512 * v_gl_id_68))]; 
#pragma unroll 1
                for (int v_i_69 = 0; (v_i_69 < 254); v_i_69 = (1 + v_i_69)){
                    v_window_35[18] = v__70[(524288 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_35[21] = v__70[(524800 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_35[24] = v__70[(525312 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_35[19] = v__70[(524289 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_35[22] = v__70[(524801 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_35[25] = v__70[(525313 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_35[20] = v__70[(524290 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_35[23] = v__70[(524802 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v_window_35[26] = v__70[(525314 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))]; 
                    v__72 = jacobi27(v_window_35[0], v_window_35[1], v_window_35[2], v_window_35[3], v_window_35[4], v_window_35[5], v_window_35[6], v_window_35[7], v_window_35[8], v_window_35[9], v_window_35[10], v_window_35[11], v_window_35[12], v_window_35[13], v_window_35[14], v_window_35[15], v_window_35[16], v_window_35[17], v_window_35[18], v_window_35[19], v_window_35[20], v_window_35[21], v_window_35[22], v_window_35[23], v_window_35[24], v_window_35[25], v_window_35[26]); 
                    v__73[(262657 + v_gl_id_67 + (512 * v_gl_id_68) + (262144 * v_i_69))] = id(v__72); 
                    v_window_35[0] = v_window_35[9]; 
                    v_window_35[9] = v_window_35[18]; 
                    v_window_35[1] = v_window_35[10]; 
                    v_window_35[10] = v_window_35[19]; 
                    v_window_35[2] = v_window_35[11]; 
                    v_window_35[11] = v_window_35[20]; 
                    v_window_35[3] = v_window_35[12]; 
                    v_window_35[12] = v_window_35[21]; 
                    v_window_35[4] = v_window_35[13]; 
                    v_window_35[13] = v_window_35[22]; 
                    v_window_35[5] = v_window_35[14]; 
                    v_window_35[14] = v_window_35[23]; 
                    v_window_35[6] = v_window_35[15]; 
                    v_window_35[15] = v_window_35[24]; 
                    v_window_35[7] = v_window_35[16]; 
                    v_window_35[16] = v_window_35[25]; 
                    v_window_35[8] = v_window_35[17]; 
                    v_window_35[17] = v_window_35[26]; 
                }
                // end mapSeqSlide
            }
        }
    }
}
