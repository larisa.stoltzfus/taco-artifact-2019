#atf::var<int> v_M_0 = 256 
#atf::var<int> v_N_1 = 256 
#atf::var<int> v_O_2 = 16 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(256,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(256,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(128,128)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: int
#ifndef Tuple3_float_float_int_DEFINED
#define Tuple3_float_float_int_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
    int _2;
} Tuple3_float_float_int;
#endif

float subtract(float l, float r){
    {
        { return l - r; }; 
    }
}
float getCF(int neigh, float cfB, float cfI){
    {
        { if(neigh < 6) { return cfB; } else{ return cfI;} }; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
float idIF(int x){
    {
        { return (float)(x*1.0); }; 
    }
}
float addTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 + x._1;}; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
int idxF(int i, int j, int k, int m, int n, int o){
    {
        {int count = 6; if(i == (m-1) || i == 0){ count--; } if(j == (n-1) || j == 0){ count--; } if(k == (o-1) || k == 0){ count--; }return count; }; 
    }
}
float subtractTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 - x._1;}; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
kernel void KERNEL(const global float* restrict v__204, const global float* restrict v__205, global float* v__247){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        float v__210; 
        float v__213; 
        float v__228; 
        float v__231; 
        float v__232; 
        float v__241; 
        float v__242; 
        // Private Memory
        float v__209; 
        float v__212; 
        float v__215; 
        float v__217; 
        float v__219; 
        float v__221; 
        float v__223; 
        float v__225; 
        float v__227; 
        float v__230; 
        float v__234; 
        float v__236; 
        float v__238; 
        float v__240; 
        float v__244; 
        float v__246; 
        for (int v_gl_id_201 = get_global_id(0); (v_gl_id_201 < 254); v_gl_id_201 = (v_gl_id_201 + get_global_size(0))){
            for (int v_gl_id_202 = get_global_id(1); (v_gl_id_202 < 254); v_gl_id_202 = (v_gl_id_202 + get_global_size(1))){
                // mapSeqSlide
                private Tuple3_float_float_int v_window_28[27]; 
                v_window_28[0] = (Tuple3_float_float_int){v__204[(v_gl_id_201 + (256 * v_gl_id_202))], v__205[(v_gl_id_201 + (256 * v_gl_id_202))], idxF(0, v_gl_id_202, v_gl_id_201, 16, 256, 256)}; 
                v_window_28[9] = (Tuple3_float_float_int){v__204[(65536 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(65536 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(1, v_gl_id_202, v_gl_id_201, 16, 256, 256)}; 
                v_window_28[3] = (Tuple3_float_float_int){v__204[(256 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(256 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(0, (1 + v_gl_id_202), v_gl_id_201, 16, 256, 256)}; 
                v_window_28[12] = (Tuple3_float_float_int){v__204[(65792 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(65792 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(1, (1 + v_gl_id_202), v_gl_id_201, 16, 256, 256)}; 
                v_window_28[6] = (Tuple3_float_float_int){v__204[(512 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(512 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(0, (2 + v_gl_id_202), v_gl_id_201, 16, 256, 256)}; 
                v_window_28[15] = (Tuple3_float_float_int){v__204[(66048 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(66048 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(1, (2 + v_gl_id_202), v_gl_id_201, 16, 256, 256)}; 
                v_window_28[1] = (Tuple3_float_float_int){v__204[(1 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(1 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(0, v_gl_id_202, (1 + v_gl_id_201), 16, 256, 256)}; 
                v_window_28[10] = (Tuple3_float_float_int){v__204[(65537 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(65537 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(1, v_gl_id_202, (1 + v_gl_id_201), 16, 256, 256)}; 
                v_window_28[4] = (Tuple3_float_float_int){v__204[(257 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(257 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(0, (1 + v_gl_id_202), (1 + v_gl_id_201), 16, 256, 256)}; 
                v_window_28[13] = (Tuple3_float_float_int){v__204[(65793 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(65793 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(1, (1 + v_gl_id_202), (1 + v_gl_id_201), 16, 256, 256)}; 
                v_window_28[7] = (Tuple3_float_float_int){v__204[(513 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(513 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(0, (2 + v_gl_id_202), (1 + v_gl_id_201), 16, 256, 256)}; 
                v_window_28[16] = (Tuple3_float_float_int){v__204[(66049 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(66049 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(1, (2 + v_gl_id_202), (1 + v_gl_id_201), 16, 256, 256)}; 
                v_window_28[2] = (Tuple3_float_float_int){v__204[(2 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(2 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(0, v_gl_id_202, (2 + v_gl_id_201), 16, 256, 256)}; 
                v_window_28[11] = (Tuple3_float_float_int){v__204[(65538 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(65538 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(1, v_gl_id_202, (2 + v_gl_id_201), 16, 256, 256)}; 
                v_window_28[5] = (Tuple3_float_float_int){v__204[(258 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(258 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(0, (1 + v_gl_id_202), (2 + v_gl_id_201), 16, 256, 256)}; 
                v_window_28[14] = (Tuple3_float_float_int){v__204[(65794 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(65794 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(1, (1 + v_gl_id_202), (2 + v_gl_id_201), 16, 256, 256)}; 
                v_window_28[8] = (Tuple3_float_float_int){v__204[(514 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(514 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(0, (2 + v_gl_id_202), (2 + v_gl_id_201), 16, 256, 256)}; 
                v_window_28[17] = (Tuple3_float_float_int){v__204[(66050 + v_gl_id_201 + (256 * v_gl_id_202))], v__205[(66050 + v_gl_id_201 + (256 * v_gl_id_202))], idxF(1, (2 + v_gl_id_202), (2 + v_gl_id_201), 16, 256, 256)}; 

                #pragma unroll 1
                for (int v_i_203 = 0; (v_i_203 < 14); v_i_203 = (1 + v_i_203)){
                    v_window_28[18] = (Tuple3_float_float_int){v__204[(131072 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], v__205[(131072 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], idxF((2 + v_i_203), v_gl_id_202, v_gl_id_201, 16, 256, 256)}; 
                    v_window_28[21] = (Tuple3_float_float_int){v__204[(131328 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], v__205[(131328 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], idxF((2 + v_i_203), (1 + v_gl_id_202), v_gl_id_201, 16, 256, 256)}; 
                    v_window_28[24] = (Tuple3_float_float_int){v__204[(131584 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], v__205[(131584 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], idxF((2 + v_i_203), (2 + v_gl_id_202), v_gl_id_201, 16, 256, 256)}; 
                    v_window_28[19] = (Tuple3_float_float_int){v__204[(131073 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], v__205[(131073 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], idxF((2 + v_i_203), v_gl_id_202, (1 + v_gl_id_201), 16, 256, 256)}; 
                    v_window_28[22] = (Tuple3_float_float_int){v__204[(131329 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], v__205[(131329 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], idxF((2 + v_i_203), (1 + v_gl_id_202), (1 + v_gl_id_201), 16, 256, 256)}; 
                    v_window_28[25] = (Tuple3_float_float_int){v__204[(131585 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], v__205[(131585 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], idxF((2 + v_i_203), (2 + v_gl_id_202), (1 + v_gl_id_201), 16, 256, 256)}; 
                    v_window_28[20] = (Tuple3_float_float_int){v__204[(131074 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], v__205[(131074 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], idxF((2 + v_i_203), v_gl_id_202, (2 + v_gl_id_201), 16, 256, 256)}; 
                    v_window_28[23] = (Tuple3_float_float_int){v__204[(131330 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], v__205[(131330 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], idxF((2 + v_i_203), (1 + v_gl_id_202), (2 + v_gl_id_201), 16, 256, 256)}; 
                    v_window_28[26] = (Tuple3_float_float_int){v__204[(131586 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], v__205[(131586 + v_gl_id_201 + (256 * v_gl_id_202) + (65536 * v_i_203))], idxF((2 + v_i_203), (2 + v_gl_id_202), (2 + v_gl_id_201), 16, 256, 256)}; 
                    v__209 = idIF(v_window_28[13]._2); 
                    float v_tmp_445 = 0.3333333f; 
                    v__210 = v_tmp_445; 
                    v__212 = mult(v__209, v__210); 
                    float v_tmp_446 = 2.0f; 
                    v__213 = v_tmp_446; 
                    v__215 = subtract(v__213, v__212); 
                    v__217 = mult(v__215, v_window_28[13]._1); 
                    v__219 = add(v_window_28[22]._1, v_window_28[16]._1); 
                    v__221 = add(v__219, v_window_28[14]._1); 
                    v__223 = add(v__221, v_window_28[12]._1); 
                    v__225 = add(v__223, v_window_28[10]._1); 
                    v__227 = add(v__225, v_window_28[4]._1); 
                    float v_tmp_447 = 0.3333333f; 
                    v__228 = v_tmp_447; 
                    v__230 = mult(v__227, v__228); 
                    float v_tmp_448 = 0.9971132f; 
                    v__231 = v_tmp_448; 
                    float v_tmp_449 = 1.0f; 
                    v__232 = v_tmp_449; 
                    v__234 = getCF(v_window_28[13]._2, v__231, v__232); 
                    v__236 = mult(v_window_28[13]._0, v__234); 
                    v__238 = subtractTuple((Tuple2_float_float){v__230, v__236}); 
                    v__240 = addTuple((Tuple2_float_float){v__217, v__238}); 
                    float v_tmp_450 = 0.9971216f; 
                    v__241 = v_tmp_450; 
                    float v_tmp_451 = 1.0f; 
                    v__242 = v_tmp_451; 
                    v__244 = getCF(v_window_28[13]._2, v__241, v__242); 
                    v__246 = mult(v__240, v__244); 
                    v__247[(v_gl_id_201 + (254 * v_gl_id_202) + (64516 * v_i_203))] = id(v__246); 
                    v_window_28[0] = v_window_28[9]; 
                    v_window_28[9] = v_window_28[18]; 
                    v_window_28[1] = v_window_28[10]; 
                    v_window_28[10] = v_window_28[19]; 
                    v_window_28[2] = v_window_28[11]; 
                    v_window_28[11] = v_window_28[20]; 
                    v_window_28[3] = v_window_28[12]; 
                    v_window_28[12] = v_window_28[21]; 
                    v_window_28[4] = v_window_28[13]; 
                    v_window_28[13] = v_window_28[22]; 
                    v_window_28[5] = v_window_28[14]; 
                    v_window_28[14] = v_window_28[23]; 
                    v_window_28[6] = v_window_28[15]; 
                    v_window_28[15] = v_window_28[24]; 
                    v_window_28[7] = v_window_28[16]; 
                    v_window_28[16] = v_window_28[25]; 
                    v_window_28[8] = v_window_28[17]; 
                    v_window_28[17] = v_window_28[26]; 
                }
                // end mapSeqSlide
            }
        }
    }
}
