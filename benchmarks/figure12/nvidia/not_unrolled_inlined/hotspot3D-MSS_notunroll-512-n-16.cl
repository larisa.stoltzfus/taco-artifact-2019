#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 16 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,300) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(256,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

float calculateHotspot(float tInC, float cc, float tInN, float cn, float tInS, float cs, float tInE, float ce, float tInW, float cw, float tInT, float ct, float tInB, float cb, float stepDivCap, float pInC, float amb_temp){
    {
        { return  tInC*cc + tInN*cn + tInS*cs + tInE*ce + tInW*cw + tInT*ct + tInB*cb + stepDivCap * pInC + ct*amb_temp; }; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__940, const global float* restrict v__941, global float* v__956){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        float v__945; 
        float v__946; 
        float v__947; 
        float v__948; 
        float v__949; 
        float v__950; 
        float v__951; 
        float v__952; 
        float v__953; 
        // Private Memory
        float v__955; 
        for (int v_gl_id_937 = get_global_id(0); (v_gl_id_937 < 512); v_gl_id_937 = (v_gl_id_937 + get_global_size(0))){
            for (int v_gl_id_938 = get_global_id(1); (v_gl_id_938 < 512); v_gl_id_938 = (v_gl_id_938 + get_global_size(1))){
                // mapSeqSlide
                private Tuple2_float_float v_window_828[27]; 
                v_window_828[0] = (Tuple2_float_float){v__941[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))], v__940[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))]}; 
                v_window_828[9] = (Tuple2_float_float){v__941[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))], v__940[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))]}; 
                v_window_828[3] = (Tuple2_float_float){v__941[((512 * v_gl_id_938) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))], v__940[((512 * v_gl_id_938) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))]}; 
                v_window_828[12] = (Tuple2_float_float){v__941[((512 * v_gl_id_938) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))], v__940[((512 * v_gl_id_938) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))]}; 
                v_window_828[6] = (Tuple2_float_float){v__941[((512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))], v__940[((512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))]}; 
                v_window_828[15] = (Tuple2_float_float){v__941[((512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))], v__940[((512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))]}; 
                v_window_828[1] = (Tuple2_float_float){v__941[(v_gl_id_937 + (512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )))], v__940[(v_gl_id_937 + (512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )))]}; 
                v_window_828[10] = (Tuple2_float_float){v__941[(v_gl_id_937 + (512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )))], v__940[(v_gl_id_937 + (512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )))]}; 
                v_window_828[4] = (Tuple2_float_float){v__941[(v_gl_id_937 + (512 * v_gl_id_938))], v__940[(v_gl_id_937 + (512 * v_gl_id_938))]}; 
                v_window_828[13] = (Tuple2_float_float){v__941[(v_gl_id_937 + (512 * v_gl_id_938))], v__940[(v_gl_id_937 + (512 * v_gl_id_938))]}; 
                v_window_828[7] = (Tuple2_float_float){v__941[(v_gl_id_937 + (512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )))], v__940[(v_gl_id_937 + (512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )))]}; 
                v_window_828[16] = (Tuple2_float_float){v__941[(v_gl_id_937 + (512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )))], v__940[(v_gl_id_937 + (512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )))]}; 
                v_window_828[2] = (Tuple2_float_float){v__941[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))], v__940[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))]}; 
                v_window_828[11] = (Tuple2_float_float){v__941[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))], v__940[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))]}; 
                v_window_828[5] = (Tuple2_float_float){v__941[((512 * v_gl_id_938) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))], v__940[((512 * v_gl_id_938) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))]}; 
                v_window_828[14] = (Tuple2_float_float){v__941[((512 * v_gl_id_938) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))], v__940[((512 * v_gl_id_938) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))]}; 
                v_window_828[8] = (Tuple2_float_float){v__941[((512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))], v__940[((512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))]}; 
                v_window_828[17] = (Tuple2_float_float){v__941[((512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))], v__940[((512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))]}; 
#pragma unroll 1
                for (int v_i_939 = 0; (v_i_939 < 16); v_i_939 = (1 + v_i_939)){
                    v_window_828[18] = (Tuple2_float_float){v__941[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))], v__940[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))]}; 
                    v_window_828[21] = (Tuple2_float_float){v__941[((512 * v_gl_id_938) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))], v__940[((512 * v_gl_id_938) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))]}; 
                    v_window_828[24] = (Tuple2_float_float){v__941[((262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + (512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))], v__940[((262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + (512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((-1 + v_gl_id_937) >= 0) ? (-1 + v_gl_id_937) : 0 ))]}; 
                    v_window_828[19] = (Tuple2_float_float){v__941[(v_gl_id_937 + (512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )))], v__940[(v_gl_id_937 + (512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )))]}; 
                    v_window_828[22] = (Tuple2_float_float){v__941[(v_gl_id_937 + (512 * v_gl_id_938) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )))], v__940[(v_gl_id_937 + (512 * v_gl_id_938) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )))]}; 
                    v_window_828[25] = (Tuple2_float_float){v__941[(v_gl_id_937 + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + (512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )))], v__940[(v_gl_id_937 + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + (512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )))]}; 
                    v_window_828[20] = (Tuple2_float_float){v__941[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))], v__940[((512 * ( ((-1 + v_gl_id_938) >= 0) ? (-1 + v_gl_id_938) : 0 )) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))]}; 
                    v_window_828[23] = (Tuple2_float_float){v__941[((512 * v_gl_id_938) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))], v__940[((512 * v_gl_id_938) + (262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))]}; 
                    v_window_828[26] = (Tuple2_float_float){v__941[((262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + (512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))], v__940[((262144 * ( ((1 + v_i_939) < 16) ? (1 + v_i_939) : 15 )) + (512 * ( ((1 + v_gl_id_938) < 512) ? (1 + v_gl_id_938) : 511 )) + ( ((1 + v_gl_id_937) < 512) ? (1 + v_gl_id_937) : 511 ))]}; 
                    float v_tmp_976 = 0.86186665f; 
                    v__945 = v_tmp_976; 
                    float v_tmp_977 = 0.03413333f; 
                    v__946 = v_tmp_977; 
                    float v_tmp_978 = 0.03413333f; 
                    v__947 = v_tmp_978; 
                    float v_tmp_979 = 0.03413333f; 
                    v__948 = v_tmp_979; 
                    float v_tmp_980 = 0.03413333f; 
                    v__949 = v_tmp_980; 
                    float v_tmp_981 = 5.333333E-4f; 
                    v__950 = v_tmp_981; 
                    float v_tmp_982 = 5.333333E-4f; 
                    v__951 = v_tmp_982; 
                    float v_tmp_983 = 0.3413333f; 
                    v__952 = v_tmp_983; 
                    float v_tmp_984 = 80.0f; 
                    v__953 = v_tmp_984; 
                    v__955 = calculateHotspot(v_window_828[13]._1, v__945, v_window_828[10]._1, v__946, v_window_828[16]._1, v__947, v_window_828[22]._1, v__948, v_window_828[4]._1, v__949, v_window_828[14]._1, v__950, v_window_828[12]._1, v__951, v__952, v_window_828[13]._0, v__953); 
                    v__956[(v_gl_id_937 + (512 * v_gl_id_938) + (262144 * v_i_939))] = id(v__955); 
                    v_window_828[0] = v_window_828[9]; 
                    v_window_828[9] = v_window_828[18]; 
                    v_window_828[1] = v_window_828[10]; 
                    v_window_828[10] = v_window_828[19]; 
                    v_window_828[2] = v_window_828[11]; 
                    v_window_828[11] = v_window_828[20]; 
                    v_window_828[3] = v_window_828[12]; 
                    v_window_828[12] = v_window_828[21]; 
                    v_window_828[4] = v_window_828[13]; 
                    v_window_828[13] = v_window_828[22]; 
                    v_window_828[5] = v_window_828[14]; 
                    v_window_828[14] = v_window_828[23]; 
                    v_window_828[6] = v_window_828[15]; 
                    v_window_828[15] = v_window_828[24]; 
                    v_window_828[7] = v_window_828[16]; 
                    v_window_828[16] = v_window_828[25]; 
                    v_window_828[8] = v_window_828[17]; 
                    v_window_828[17] = v_window_828[26]; 
                }
                // end mapSeqSlide
            }
        }
    }
}
