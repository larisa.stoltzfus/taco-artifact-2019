#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 512 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(16,16)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(64,64)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float heat(float C, float S, float N, float E, float W, float B, float F){
    {
        return 0.125f * (B - 2.0f * C + F) +
       0.125f * (S - 2.0f * C + N) +
       0.125f * (E - 2.0f * C + W) + C;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__685, global float* v__688){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__687; 
        for (int v_gl_id_682 = get_global_id(0); (v_gl_id_682 < 510); v_gl_id_682 = (v_gl_id_682 + get_global_size(0))){
            for (int v_gl_id_683 = get_global_id(1); (v_gl_id_683 < 510); v_gl_id_683 = (v_gl_id_683 + get_global_size(1))){
                // mapSeqSlide
                private float v_window_651[27]; 
                v_window_651[0] = v__685[(v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[9] = v__685[(262144 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[3] = v__685[(512 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[12] = v__685[(262656 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[6] = v__685[(1024 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[15] = v__685[(263168 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[1] = v__685[(1 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[10] = v__685[(262145 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[4] = v__685[(513 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[13] = v__685[(262657 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[7] = v__685[(1025 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[16] = v__685[(263169 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[2] = v__685[(2 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[11] = v__685[(262146 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[5] = v__685[(514 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[14] = v__685[(262658 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[8] = v__685[(1026 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                v_window_651[17] = v__685[(263170 + v_gl_id_682 + (512 * v_gl_id_683))]; 
                for (int v_i_684 = 0; (v_i_684 < 510); v_i_684 = (1 + v_i_684)){
                    v_window_651[18] = v__685[(524288 + v_gl_id_682 + (512 * v_gl_id_683) + (262144 * v_i_684))]; 
                    v_window_651[21] = v__685[(524800 + v_gl_id_682 + (512 * v_gl_id_683) + (262144 * v_i_684))]; 
                    v_window_651[24] = v__685[(525312 + v_gl_id_682 + (512 * v_gl_id_683) + (262144 * v_i_684))]; 
                    v_window_651[19] = v__685[(524289 + v_gl_id_682 + (512 * v_gl_id_683) + (262144 * v_i_684))]; 
                    v_window_651[22] = v__685[(524801 + v_gl_id_682 + (512 * v_gl_id_683) + (262144 * v_i_684))]; 
                    v_window_651[25] = v__685[(525313 + v_gl_id_682 + (512 * v_gl_id_683) + (262144 * v_i_684))]; 
                    v_window_651[20] = v__685[(524290 + v_gl_id_682 + (512 * v_gl_id_683) + (262144 * v_i_684))]; 
                    v_window_651[23] = v__685[(524802 + v_gl_id_682 + (512 * v_gl_id_683) + (262144 * v_i_684))]; 
                    v_window_651[26] = v__685[(525314 + v_gl_id_682 + (512 * v_gl_id_683) + (262144 * v_i_684))]; 
                    v__687 = heat(v_window_651[13], v_window_651[10], v_window_651[16], v_window_651[14], v_window_651[12], v_window_651[4], v_window_651[22]); 
                    v__688[(262657 + v_gl_id_682 + (512 * v_gl_id_683) + (262144 * v_i_684))] = id(v__687); 
                    v_window_651[0] = v_window_651[9]; 
                    v_window_651[9] = v_window_651[18]; 
                    v_window_651[1] = v_window_651[10]; 
                    v_window_651[10] = v_window_651[19]; 
                    v_window_651[2] = v_window_651[11]; 
                    v_window_651[11] = v_window_651[20]; 
                    v_window_651[3] = v_window_651[12]; 
                    v_window_651[12] = v_window_651[21]; 
                    v_window_651[4] = v_window_651[13]; 
                    v_window_651[13] = v_window_651[22]; 
                    v_window_651[5] = v_window_651[14]; 
                    v_window_651[14] = v_window_651[23]; 
                    v_window_651[6] = v_window_651[15]; 
                    v_window_651[15] = v_window_651[24]; 
                    v_window_651[7] = v_window_651[16]; 
                    v_window_651[16] = v_window_651[25]; 
                    v_window_651[8] = v_window_651[17]; 
                    v_window_651[17] = v_window_651[26]; 
                }
                // end mapSeqSlide
            }
        }
    }
}
