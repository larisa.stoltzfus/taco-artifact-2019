#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 64 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(32,32)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(64,64)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float poisson(float C, float N, float S, float E, float W, float F, float B, float FN, float BN, float FS, float BS, float FW, float BW, float NW, float SW, float FE, float BE, float NE, float SE){
    {
        return 2.666f * C - 0.166f * (F + B + N + S + E + W) -
       0.0833f * (FN + BN + FS + BS + FW + BW +
                  NW + SW + FE + BE + NE + SE);; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__783, global float* v__786){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__785; 
        for (int v_gl_id_780 = get_global_id(0); (v_gl_id_780 < 510); v_gl_id_780 = (v_gl_id_780 + get_global_size(0))){
            for (int v_gl_id_781 = get_global_id(1); (v_gl_id_781 < 510); v_gl_id_781 = (v_gl_id_781 + get_global_size(1))){
                // mapSeqSlide
                private float v_window_737[27]; 
                v_window_737[0] = v__783[(v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[9] = v__783[(262144 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[3] = v__783[(512 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[12] = v__783[(262656 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[6] = v__783[(1024 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[15] = v__783[(263168 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[1] = v__783[(1 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[10] = v__783[(262145 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[4] = v__783[(513 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[13] = v__783[(262657 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[7] = v__783[(1025 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[16] = v__783[(263169 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[2] = v__783[(2 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[11] = v__783[(262146 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[5] = v__783[(514 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[14] = v__783[(262658 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[8] = v__783[(1026 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                v_window_737[17] = v__783[(263170 + v_gl_id_780 + (512 * v_gl_id_781))]; 
                for (int v_i_782 = 0; (v_i_782 < 62); v_i_782 = (1 + v_i_782)){
                    v_window_737[18] = v__783[(524288 + v_gl_id_780 + (512 * v_gl_id_781) + (262144 * v_i_782))]; 
                    v_window_737[21] = v__783[(524800 + v_gl_id_780 + (512 * v_gl_id_781) + (262144 * v_i_782))]; 
                    v_window_737[24] = v__783[(525312 + v_gl_id_780 + (512 * v_gl_id_781) + (262144 * v_i_782))]; 
                    v_window_737[19] = v__783[(524289 + v_gl_id_780 + (512 * v_gl_id_781) + (262144 * v_i_782))]; 
                    v_window_737[22] = v__783[(524801 + v_gl_id_780 + (512 * v_gl_id_781) + (262144 * v_i_782))]; 
                    v_window_737[25] = v__783[(525313 + v_gl_id_780 + (512 * v_gl_id_781) + (262144 * v_i_782))]; 
                    v_window_737[20] = v__783[(524290 + v_gl_id_780 + (512 * v_gl_id_781) + (262144 * v_i_782))]; 
                    v_window_737[23] = v__783[(524802 + v_gl_id_780 + (512 * v_gl_id_781) + (262144 * v_i_782))]; 
                    v_window_737[26] = v__783[(525314 + v_gl_id_780 + (512 * v_gl_id_781) + (262144 * v_i_782))]; 
                    v__785 = poisson(v_window_737[13], v_window_737[10], v_window_737[16], v_window_737[14], v_window_737[12], v_window_737[4], v_window_737[22], v_window_737[1], v_window_737[19], v_window_737[7], v_window_737[25], v_window_737[3], v_window_737[21], v_window_737[9], v_window_737[15], v_window_737[5], v_window_737[23], v_window_737[11], v_window_737[17]); 
                    v__786[(262657 + v_gl_id_780 + (512 * v_gl_id_781) + (262144 * v_i_782))] = id(v__785); 
                    v_window_737[0] = v_window_737[9]; 
                    v_window_737[9] = v_window_737[18]; 
                    v_window_737[1] = v_window_737[10]; 
                    v_window_737[10] = v_window_737[19]; 
                    v_window_737[2] = v_window_737[11]; 
                    v_window_737[11] = v_window_737[20]; 
                    v_window_737[3] = v_window_737[12]; 
                    v_window_737[12] = v_window_737[21]; 
                    v_window_737[4] = v_window_737[13]; 
                    v_window_737[13] = v_window_737[22]; 
                    v_window_737[5] = v_window_737[14]; 
                    v_window_737[14] = v_window_737[23]; 
                    v_window_737[6] = v_window_737[15]; 
                    v_window_737[15] = v_window_737[24]; 
                    v_window_737[7] = v_window_737[16]; 
                    v_window_737[16] = v_window_737[25]; 
                    v_window_737[8] = v_window_737[17]; 
                    v_window_737[17] = v_window_737[26]; 
                }
                // end mapSeqSlide
            }
        }
    }
}
