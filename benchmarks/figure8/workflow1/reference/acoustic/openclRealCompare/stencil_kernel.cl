#pragma OPENCL EXTENSION cl_khr_fp64 : enable

typedef float value;
#define area (Nx*Ny)


typedef struct coeffs_type
{
	value l2;
	value loss1;
	value loss2;

} coeffs_type;

__kernel void UpdateScheme(__global value* restrict u,
                           __global value* restrict u1, 
                           __constant struct coeffs_type* cf_d)
{
	
	// get X,Y,Z from thread and block Id's
	int X = get_global_id(0); 
	int Y = get_global_id(1); 
	int Z = get_global_id(2);

	
	// Test that not at halo, Z block excludes Z halo
	if( (X>0) && (X<(Nx-1)) && (Y>0) && (Y<(Ny-1)) && (Z>0) && (Z<(Nz-1)) ){
		
		// get linear position
		int cp   = Z*area+(Y*Nx+X);
		
		// local variables
		value cf  = 1.0;
		value cf2 = 1.0;
		
		int K    = (0||(X-1)) + (0||(X-(Nx-2))) + (0||(Y-1)) + (0||(Y-(Ny-2))) + (0||(Z-1)) + (0||(Z-(Nz-2)));
		
		// set loss coeffs at walls
		if(K<6){
			cf   = cf_d[0].loss1;
			cf2  = cf_d[0].loss2;
		}
        
		// Get sum of neighbours
        value S   = u1[cp-1]+u1[cp+1]+u1[cp-Nx]+u1[cp+Nx]+u1[cp-area]+u1[cp+area];
        
        // Calc update
        u[cp]    = cf*( (2.0-K*cf_d[0].l2)*u1[cp] + cf_d[0].l2*S - cf2*u[cp] );
	
	}
	
}
