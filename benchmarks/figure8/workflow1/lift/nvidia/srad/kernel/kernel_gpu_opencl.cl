//========================================================================================================================================================================================================200
//	DEFINE / INCLUDE
//========================================================================================================================================================================================================200

//======================================================================================================================================================150
//	MAIN FUNCTION HEADER
//======================================================================================================================================================150

#include "./main.h"

//======================================================================================================================================================150
//	End
//======================================================================================================================================================150

//========================================================================================================================================================================================================200
//	Extract KERNEL
//========================================================================================================================================================================================================200

__kernel void 
extract_kernel(long d_Ne,
				__global fp* d_I){											// pointer to input image (DEVICE GLOBAL MEMORY)

	// indexes
	int bx = get_group_id(0);												// get current horizontal block index
	int tx = get_local_id(0);													// get current horizontal thread index
	int ei = (bx*NUMBER_THREADS)+tx;										// unique thread id, more threads than actual elements !!!

	// copy input to output & log uncompress
	if(ei<d_Ne){															// do only for the number of elements, omit extra threads

		d_I[ei] = exp(d_I[ei]/255);												// exponentiate input IMAGE and copy to output image

	}

}

//========================================================================================================================================================================================================200
//	Prepare KERNEL
//========================================================================================================================================================================================================200

__kernel void 
prepare_kernel(	long d_Ne,
				__global fp* d_I,											// pointer to output image (DEVICE GLOBAL MEMORY)
				__global fp* d_sums,										// pointer to input image (DEVICE GLOBAL MEMORY)
				__global fp* d_sums2){

	// indexes
	int bx = get_group_id(0);												// get current horizontal block index
	int tx = get_local_id(0);												// get current horizontal thread index
	int ei = (bx*NUMBER_THREADS)+tx;										// unique thread id, more threads than actual elements !!!

	// copy input to output & log uncompress
	if(ei<d_Ne){															// do only for the number of elements, omit extra threads

		d_sums[ei] = d_I[ei];
		d_sums2[ei] = d_I[ei]*d_I[ei];

	}

}

//========================================================================================================================================================================================================200
//	Reduce KERNEL
//========================================================================================================================================================================================================200

__kernel void 
reduce_kernel(	long d_Ne,													// number of elements in array
				long d_no,													// number of sums to reduce
				int d_mul,													// increment
				__global fp* d_sums,										// pointer to partial sums variable (DEVICE GLOBAL MEMORY)
				__global fp* d_sums2,
				int gridDim){

	// indexes
    int bx = get_group_id(0);												// get current horizontal block index
	int tx = get_local_id(0);												// get current horizontal thread index
	int ei = (bx*NUMBER_THREADS)+tx;										// unique thread id, more threads than actual elements !!!
	// int gridDim = (int)get_group_size(0)/(int)get_local_size(0);			// number of workgroups
	int nf = NUMBER_THREADS-(gridDim*NUMBER_THREADS-d_no);				// number of elements assigned to last block
	int df = 0;															// divisibility factor for the last block

	// statistical
	__local fp d_psum[NUMBER_THREADS];										// data for block calculations allocated by every block in its shared memory
	__local fp d_psum2[NUMBER_THREADS];

	// counters
	int i;

	// copy data to shared memory
	if(ei<d_no){															// do only for the number of elements, omit extra threads

		d_psum[tx] = d_sums[ei*d_mul];
		d_psum2[tx] = d_sums2[ei*d_mul];

	}

    // Lingjie Zhang modificated at Nov 1, 2015
	//	barrier(CLK_LOCAL_MEM_FENCE);
	barrier(CLK_LOCAL_MEM_FENCE | CLK_GLOBAL_MEM_FENCE); // Lukasz proposed, Ke modified 2015/12/12 22:31:00
    // end Lingjie Zhang modification

	// reduction of sums if all blocks are full (rare case)	
	if(nf == NUMBER_THREADS){
		// sum of every 2, 4, ..., NUMBER_THREADS elements
		for(i=2; i<=NUMBER_THREADS; i=2*i){
			// sum of elements
			if((tx+1) % i == 0){											// every ith
				d_psum[tx] = d_psum[tx] + d_psum[tx-i/2];
				d_psum2[tx] = d_psum2[tx] + d_psum2[tx-i/2];
			}
			// synchronization
			barrier(CLK_LOCAL_MEM_FENCE);
		}
		// final sumation by last thread in every block
		if(tx==(NUMBER_THREADS-1)){											// block result stored in global memory
			d_sums[bx*d_mul*NUMBER_THREADS] = d_psum[tx];
			d_sums2[bx*d_mul*NUMBER_THREADS] = d_psum2[tx];
		}
	}
	// reduction of sums if last block is not full (common case)
	else{ 
		// for full blocks (all except for last block)
		if(bx != (gridDim - 1)){											//
			// sum of every 2, 4, ..., NUMBER_THREADS elements
			for(i=2; i<=NUMBER_THREADS; i=2*i){								//
				// sum of elements
				if((tx+1) % i == 0){										// every ith
					d_psum[tx] = d_psum[tx] + d_psum[tx-i/2];
					d_psum2[tx] = d_psum2[tx] + d_psum2[tx-i/2];
				}
				// synchronization
				barrier(CLK_LOCAL_MEM_FENCE);
			}
			// final sumation by last thread in every block
			if(tx==(NUMBER_THREADS-1)){										// block result stored in global memory
				d_sums[bx*d_mul*NUMBER_THREADS] = d_psum[tx];
				d_sums2[bx*d_mul*NUMBER_THREADS] = d_psum2[tx];
			}
		}
		// for not full block (last block)
		else{																//
			// figure out divisibility
			for(i=2; i<=NUMBER_THREADS; i=2*i){								//
				if(nf >= i){
					df = i;
				}
			}
			// sum of every 2, 4, ..., NUMBER_THREADS elements
			for(i=2; i<=df; i=2*i){											//
				// sum of elements (only busy threads)
				if((tx+1) % i == 0 && tx<df){								// every ith
					d_psum[tx] = d_psum[tx] + d_psum[tx-i/2];
					d_psum2[tx] = d_psum2[tx] + d_psum2[tx-i/2];
				}
				// synchronization (all threads)
				barrier(CLK_LOCAL_MEM_FENCE);
			}
			// remainder / final summation by last thread
			if(tx==(df-1)){										//
				// compute the remainder and final summation by last busy thread
				for(i=(bx*NUMBER_THREADS)+df; i<(bx*NUMBER_THREADS)+nf; i++){						//
					d_psum[tx] = d_psum[tx] + d_sums[i];
					d_psum2[tx] = d_psum2[tx] + d_sums2[i];
				}
				// final sumation by last thread in every block
				d_sums[bx*d_mul*NUMBER_THREADS] = d_psum[tx];
				d_sums2[bx*d_mul*NUMBER_THREADS] = d_psum2[tx];
			}
		}
	}

}

//========================================================================================================================================================================================================200
//	SRAD KERNEL
//========================================================================================================================================================================================================200

// BUG, IF STILL PRESENT, COULD BE SOMEWHERE IN THIS CODE, MEMORY ACCESS OUT OF BOUNDS

__kernel void 
srad_kernel(fp d_lambda, 
			int d_Nr, 
			int d_Nc, 
			long d_Ne, 
			__global int* d_iN, 
			__global int* d_iS, 
			__global int* d_jE, 
			__global int* d_jW, 
			__global fp* d_dN, 
			__global fp* d_dS, 
			__global fp* d_dE, 
			__global fp* d_dW, 
			fp d_q0sqr, 
			__global fp* d_c, 
			__global fp* d_I){

	// indexes
    int bx = get_group_id(0);												// get current horizontal block index
	int tx = get_local_id(0);												// get current horizontal thread index
	int ei = bx*NUMBER_THREADS+tx;											// more threads than actual elements !!!
	int row;																// column, x position
	int col;																// row, y position

	// variables
	fp d_Jc;
	fp d_dN_loc, d_dS_loc, d_dW_loc, d_dE_loc;
	fp d_c_loc;
	fp d_G2,d_L,d_num,d_den,d_qsqr;
	
	// figure out row/col location in new matrix
	row = (ei+1) % d_Nr - 1;													// (0-n) row
	col = (ei+1) / d_Nr + 1 - 1;												// (0-n) column
	if((ei+1) % d_Nr == 0){
		row = d_Nr - 1;
		col = col - 1;
	}
	
	if(ei<d_Ne){															// make sure that only threads matching jobs run
		
		// directional derivatives, ICOV, diffusion coefficent
		d_Jc = d_I[ei];														// get value of the current element
		
		// directional derivates (every element of IMAGE)(try to copy to shared memory or temp files)
		d_dN_loc = d_I[d_iN[row] + d_Nr*col] - d_Jc;						// north direction derivative
		d_dS_loc = d_I[d_iS[row] + d_Nr*col] - d_Jc;						// south direction derivative
		d_dW_loc = d_I[row + d_Nr*d_jW[col]] - d_Jc;						// west direction derivative
		d_dE_loc = d_I[row + d_Nr*d_jE[col]] - d_Jc;						// east direction derivative
	         
		// normalized discrete gradient mag squared (equ 52,53)
		d_G2 = (d_dN_loc*d_dN_loc + d_dS_loc*d_dS_loc + d_dW_loc*d_dW_loc + d_dE_loc*d_dE_loc) / (d_Jc*d_Jc);	// gradient (based on derivatives)
		
		// normalized discrete laplacian (equ 54)
		d_L = (d_dN_loc + d_dS_loc + d_dW_loc + d_dE_loc) / d_Jc;			// laplacian (based on derivatives)

		// ICOV (equ 31/35)
		d_num  = (0.5*d_G2) - ((1.0/16.0)*(d_L*d_L)) ;						// num (based on gradient and laplacian)
		fp d_den1  = 1 + (0.25*d_L);												// den (based on laplacian)
		d_qsqr = d_num/(d_den1*d_den1);										// qsqr (based on num and den)
	 
		// diffusion coefficent (equ 33) (every element of IMAGE)
		d_den = (d_qsqr-d_q0sqr) / (d_q0sqr * (1+d_q0sqr)) ;				// den (based on qsqr and q0sqr)
		d_c_loc = 1.0 / (1.0+d_den) ;										// diffusion coefficient (based on den)
	    
		// saturate diffusion coefficent to 0-1 range
		if (d_c_loc < 0){													// if diffusion coefficient < 0
			d_c_loc = 0;													// ... set to 0
		}
		else if (d_c_loc > 1){												// if diffusion coefficient > 1
			d_c_loc = 1;													// ... set to 1
		}

		// save data to global memory
		d_dN[ei] = d_dN_loc; 
		d_dS[ei] = d_dS_loc; 
		d_dW[ei] = d_dW_loc; 
		d_dE[ei] = d_dE_loc;
		d_c[ei] = d_c_loc;
		    
	}
	
}

// *************************
// REESE SRAD KERNEL 1 
// *************************

float subtract(float l, float r){
  { return l - r; }
}
float calculateScoeff(float dN, float dS, float dE, float dW, float jC, float q0sqr){
  { float g2 = (dN*dN + dS*dS + dW*dW + dE*dE) / (jC * jC);float l = (dN + dS + dW + dE ) / jC; float num = (0.5*g2) - ((1.0/16.0)*(l*l)); float  den = 1 + (0.25*l);float qsqr = num/(den*den); den = (qsqr-q0sqr) / (q0sqr * (1+q0sqr));float coeff = 1.0 / (1.0+den);  if(coeff > 1) { return 1.0f; } else if(coeff < 0 ) { return 0.0f; } else { return coeff;}  }
}
float id(float x){
  { return x; }
}
kernel void srad_kernel_lift1(const global float* restrict v__14, global float* v__27, int v_M_3, int v_N_4){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  float v__24;
  /* Private Memory */
  float v__17;
  float v__19;
  float v__21;
  float v__23;
  float v__26;
  if(get_global_id(0) < v_M_3 && get_global_id(1) < v_N_4)
  {
  for (int v_gl_id_12 = get_global_id(1);v_gl_id_12<v_N_4;v_gl_id_12 = (v_gl_id_12 + get_global_size(1))){
    for (int v_gl_id_13 = get_global_id(0);v_gl_id_13<v_M_3;v_gl_id_13 = (v_gl_id_13 + get_global_size(0))){
      v__17 = subtract(v__14[((v_M_3 * ( ((-1 + v_gl_id_12) >= 0) ? ( ((-1 + v_gl_id_12) < v_N_4) ? (-1 + v_gl_id_12) : ((-1 * v_gl_id_12) + (2 * v_N_4)) ) : (-1 * v_gl_id_12) )) + ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_M_3) ? v_gl_id_13 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) ))], v__14[((v_M_3 * ( (v_gl_id_12 >= 0) ? ( (v_gl_id_12 < v_N_4) ? v_gl_id_12 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_12)) ) : (-1 + (-1 * v_gl_id_12)) )) + ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_M_3) ? v_gl_id_13 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) ))]);
      v__19 = subtract(v__14[((v_M_3 * ( ((1 + v_gl_id_12) >= 0) ? ( ((1 + v_gl_id_12) < v_N_4) ? (1 + v_gl_id_12) : (-2 + (2 * v_N_4) + (-1 * v_gl_id_12)) ) : (-2 + (-1 * v_gl_id_12)) )) + ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_M_3) ? v_gl_id_13 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) ))], v__14[((v_M_3 * ( (v_gl_id_12 >= 0) ? ( (v_gl_id_12 < v_N_4) ? v_gl_id_12 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_12)) ) : (-1 + (-1 * v_gl_id_12)) )) + ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_M_3) ? v_gl_id_13 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) ))]);
      v__21 = subtract(v__14[((v_M_3 * ( (v_gl_id_12 >= 0) ? ( (v_gl_id_12 < v_N_4) ? v_gl_id_12 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_12)) ) : (-1 + (-1 * v_gl_id_12)) )) + ( ((1 + v_gl_id_13) >= 0) ? ( ((1 + v_gl_id_13) < v_M_3) ? (1 + v_gl_id_13) : (-2 + (2 * v_M_3) + (-1 * v_gl_id_13)) ) : (-2 + (-1 * v_gl_id_13)) ))], v__14[((v_M_3 * ( (v_gl_id_12 >= 0) ? ( (v_gl_id_12 < v_N_4) ? v_gl_id_12 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_12)) ) : (-1 + (-1 * v_gl_id_12)) )) + ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_M_3) ? v_gl_id_13 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) ))]);
      v__23 = subtract(v__14[((v_M_3 * ( (v_gl_id_12 >= 0) ? ( (v_gl_id_12 < v_N_4) ? v_gl_id_12 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_12)) ) : (-1 + (-1 * v_gl_id_12)) )) + ( ((-1 + v_gl_id_13) >= 0) ? ( ((-1 + v_gl_id_13) < v_M_3) ? (-1 + v_gl_id_13) : ((-1 * v_gl_id_13) + (2 * v_M_3)) ) : (-1 * v_gl_id_13) ))], v__14[((v_M_3 * ( (v_gl_id_12 >= 0) ? ( (v_gl_id_12 < v_N_4) ? v_gl_id_12 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_12)) ) : (-1 + (-1 * v_gl_id_12)) )) + ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_M_3) ? v_gl_id_13 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) ))]);
      float v_tmp_92 = 0.05378722f;
      v__24 = v_tmp_92;
      v__26 = calculateScoeff(v__17, v__19, v__21, v__23, v__14[((v_M_3 * ( (v_gl_id_12 >= 0) ? ( (v_gl_id_12 < v_N_4) ? v_gl_id_12 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_12)) ) : (-1 + (-1 * v_gl_id_12)) )) + ( (v_gl_id_13 >= 0) ? ( (v_gl_id_13 < v_M_3) ? v_gl_id_13 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_13)) ) : (-1 + (-1 * v_gl_id_13)) ))], v__24);
      v__27[(v_gl_id_13 + (v_M_3 * v_gl_id_12))] = id(v__26);
    }
  }}
}}

// ******************

//========================================================================================================================================================================================================200
//	SRAD2 KERNEL
//========================================================================================================================================================================================================200

// BUG, IF STILL PRESENT, COULD BE SOMEWHERE IN THIS CODE, MEMORY ACCESS OUT OF BOUNDS

__kernel void 
srad2_kernel(	fp d_lambda, 
				int d_Nr, 
				int d_Nc, 
				long d_Ne, 
				__global int* d_iN, 
				__global int* d_iS, 
				__global int* d_jE, 
				__global int* d_jW,
				__global fp* d_dN, 
				__global fp* d_dS, 
				__global fp* d_dE, 
				__global fp* d_dW, 
				__global fp* d_c, 
				__global fp* d_I){

	// indexes
    int bx = get_group_id(0);												// get current horizontal block index
	int tx = get_local_id(0);												// get current horizontal thread index
	int ei = bx*NUMBER_THREADS+tx;											// more threads than actual elements !!!
	int row;																// column, x position
	int col;																// row, y position

	// variables
	fp d_cN,d_cS,d_cW,d_cE;
	fp d_D;

	// figure out row/col location in new matrix
	row = (ei+1) % d_Nr - 1;												// (0-n) row
	col = (ei+1) / d_Nr + 1 - 1;											// (0-n) column
	if((ei+1) % d_Nr == 0){
		row = d_Nr - 1;
		col = col - 1;
	}

	if(ei<d_Ne){															// make sure that only threads matching jobs run

		// diffusion coefficent
		d_cN = d_c[ei];														// north diffusion coefficient
		d_cS = d_c[d_iS[row] + d_Nr*col];										// south diffusion coefficient
		d_cW = d_c[ei];														// west diffusion coefficient
		d_cE = d_c[row + d_Nr * d_jE[col]];									// east diffusion coefficient

		// divergence (equ 58)
		d_D = d_cN*d_dN[ei] + d_cS*d_dS[ei] + d_cW*d_dW[ei] + d_cE*d_dE[ei];// divergence

		// image update (equ 61) (every element of IMAGE)
		d_I[ei] = d_I[ei] + 0.25*d_lambda*d_D;								// updates image (based on input time step and divergence)

	}

}

// *************************
// REESE SRAD KERNEL FLY 2 
// *************************


float calculateImageUpdate(float img, float div){
  { return img + 0.125 * div; }
}
float calculateDiv(float dN, float dS, float dW, float dE, float orgDn, float orgDs, float orgDw, float orgDe){
  { return  (dN*orgDn + dS*orgDs + dW*orgDw + dE*orgDe) ; }
}
kernel void srad_kernel_lift2(const global float* restrict v__19, const global float* restrict v__20, global float* v__36, int v_M_3, int v_N_4){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__25;
  float v__27;
  float v__29;
  float v__31;
  float v__33;
  float v__35;
  if(get_global_id(0) < v_M_3 && get_global_id(1) < v_N_4)
  {
  for (int v_gl_id_17 = get_global_id(1);v_gl_id_17<v_N_4;v_gl_id_17 = (v_gl_id_17 + get_global_size(1))){
    for (int v_gl_id_18 = get_global_id(0);v_gl_id_18<v_M_3;v_gl_id_18 = (v_gl_id_18 + get_global_size(0))){
      v__25 = subtract(v__19[((v_M_3 * ( ((-1 + v_gl_id_17) >= 0) ? ( ((-1 + v_gl_id_17) < v_N_4) ? (-1 + v_gl_id_17) : ((-1 * v_gl_id_17) + (2 * v_N_4)) ) : (-1 * v_gl_id_17) )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_3) ? v_gl_id_18 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-1 + (-1 * v_gl_id_18)) ))], v__19[((v_M_3 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_4) ? v_gl_id_17 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-1 + (-1 * v_gl_id_17)) )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_3) ? v_gl_id_18 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-1 + (-1 * v_gl_id_18)) ))]);
      v__27 = subtract(v__19[((v_M_3 * ( ((1 + v_gl_id_17) >= 0) ? ( ((1 + v_gl_id_17) < v_N_4) ? (1 + v_gl_id_17) : (-2 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-2 + (-1 * v_gl_id_17)) )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_3) ? v_gl_id_18 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-1 + (-1 * v_gl_id_18)) ))], v__19[((v_M_3 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_4) ? v_gl_id_17 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-1 + (-1 * v_gl_id_17)) )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_3) ? v_gl_id_18 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-1 + (-1 * v_gl_id_18)) ))]);
      v__29 = subtract(v__19[((v_M_3 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_4) ? v_gl_id_17 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-1 + (-1 * v_gl_id_17)) )) + ( ((-1 + v_gl_id_18) >= 0) ? ( ((-1 + v_gl_id_18) < v_M_3) ? (-1 + v_gl_id_18) : ((-1 * v_gl_id_18) + (2 * v_M_3)) ) : (-1 * v_gl_id_18) ))], v__19[((v_M_3 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_4) ? v_gl_id_17 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-1 + (-1 * v_gl_id_17)) )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_3) ? v_gl_id_18 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-1 + (-1 * v_gl_id_18)) ))]);
      v__31 = subtract(v__19[((v_M_3 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_4) ? v_gl_id_17 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-1 + (-1 * v_gl_id_17)) )) + ( ((1 + v_gl_id_18) >= 0) ? ( ((1 + v_gl_id_18) < v_M_3) ? (1 + v_gl_id_18) : (-2 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-2 + (-1 * v_gl_id_18)) ))], v__19[((v_M_3 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_4) ? v_gl_id_17 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-1 + (-1 * v_gl_id_17)) )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_3) ? v_gl_id_18 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-1 + (-1 * v_gl_id_18)) ))]);
      v__33 = calculateDiv(v__20[((v_M_3 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_4) ? v_gl_id_17 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-1 + (-1 * v_gl_id_17)) )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_3) ? v_gl_id_18 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-1 + (-1 * v_gl_id_18)) ))], v__20[((v_M_3 * ( ((1 + v_gl_id_17) >= 0) ? ( ((1 + v_gl_id_17) < v_N_4) ? (1 + v_gl_id_17) : (-2 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-2 + (-1 * v_gl_id_17)) )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_3) ? v_gl_id_18 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-1 + (-1 * v_gl_id_18)) ))], v__20[((v_M_3 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_4) ? v_gl_id_17 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-1 + (-1 * v_gl_id_17)) )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_3) ? v_gl_id_18 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-1 + (-1 * v_gl_id_18)) ))], v__20[((v_M_3 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_4) ? v_gl_id_17 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-1 + (-1 * v_gl_id_17)) )) + ( ((1 + v_gl_id_18) >= 0) ? ( ((1 + v_gl_id_18) < v_M_3) ? (1 + v_gl_id_18) : (-2 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-2 + (-1 * v_gl_id_18)) ))], v__25, v__27, v__29, v__31);
      v__35 = calculateImageUpdate(v__19[((v_M_3 * ( (v_gl_id_17 >= 0) ? ( (v_gl_id_17 < v_N_4) ? v_gl_id_17 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_17)) ) : (-1 + (-1 * v_gl_id_17)) )) + ( (v_gl_id_18 >= 0) ? ( (v_gl_id_18 < v_M_3) ? v_gl_id_18 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_18)) ) : (-1 + (-1 * v_gl_id_18)) ))], v__33);
      v__36[(v_gl_id_18 + (v_M_3 * v_gl_id_17))] = id(v__35);
    }
  }}
}}



// *************************
// REESE SRAD KERNEL INPT  2 
// *************************
kernel void srad_kernel_lift2inp(const global float* restrict v__23, const global float* restrict v__24, const global float* restrict v__25, const global float* restrict v__26, const global float* restrict v__27, const global float* restrict v__28, global float* v__36, int v_M_3, int v_N_4){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__33;
  float v__35;
  if(get_global_id(0) < v_M_3 && get_global_id(1) < v_N_4)
  {
  for (int v_gl_id_21 = get_global_id(1);v_gl_id_21<v_N_4;v_gl_id_21 = (v_gl_id_21 + get_global_size(1))){
    for (int v_gl_id_22 = get_global_id(0);v_gl_id_22<v_M_3;v_gl_id_22 = (v_gl_id_22 + get_global_size(0))){
      v__33 = calculateDiv(v__24[((v_M_3 * ( (v_gl_id_21 >= 0) ? ( (v_gl_id_21 < v_N_4) ? v_gl_id_21 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_21)) ) : (-1 + (-1 * v_gl_id_21)) )) + ( (v_gl_id_22 >= 0) ? ( (v_gl_id_22 < v_M_3) ? v_gl_id_22 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_22)) ) : (-1 + (-1 * v_gl_id_22)) ))], v__24[((v_M_3 * ( ((1 + v_gl_id_21) >= 0) ? ( ((1 + v_gl_id_21) < v_N_4) ? (1 + v_gl_id_21) : (-2 + (2 * v_N_4) + (-1 * v_gl_id_21)) ) : (-2 + (-1 * v_gl_id_21)) )) + ( (v_gl_id_22 >= 0) ? ( (v_gl_id_22 < v_M_3) ? v_gl_id_22 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_22)) ) : (-1 + (-1 * v_gl_id_22)) ))], v__24[((v_M_3 * ( (v_gl_id_21 >= 0) ? ( (v_gl_id_21 < v_N_4) ? v_gl_id_21 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_21)) ) : (-1 + (-1 * v_gl_id_21)) )) + ( (v_gl_id_22 >= 0) ? ( (v_gl_id_22 < v_M_3) ? v_gl_id_22 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_22)) ) : (-1 + (-1 * v_gl_id_22)) ))], v__24[((v_M_3 * ( (v_gl_id_21 >= 0) ? ( (v_gl_id_21 < v_N_4) ? v_gl_id_21 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_21)) ) : (-1 + (-1 * v_gl_id_21)) )) + ( ((1 + v_gl_id_22) >= 0) ? ( ((1 + v_gl_id_22) < v_M_3) ? (1 + v_gl_id_22) : (-2 + (2 * v_M_3) + (-1 * v_gl_id_22)) ) : (-2 + (-1 * v_gl_id_22)) ))], v__25[(v_gl_id_22 + (v_M_3 * v_gl_id_21))], v__26[(v_gl_id_22 + (v_M_3 * v_gl_id_21))], v__28[(v_gl_id_22 + (v_M_3 * v_gl_id_21))], v__27[(v_gl_id_22 + (v_M_3 * v_gl_id_21))]);
      v__35 = calculateImageUpdate(v__23[((v_M_3 * ( (v_gl_id_21 >= 0) ? ( (v_gl_id_21 < v_N_4) ? v_gl_id_21 : (-1 + (2 * v_N_4) + (-1 * v_gl_id_21)) ) : (-1 + (-1 * v_gl_id_21)) )) + ( (v_gl_id_22 >= 0) ? ( (v_gl_id_22 < v_M_3) ? v_gl_id_22 : (-1 + (2 * v_M_3) + (-1 * v_gl_id_22)) ) : (-1 + (-1 * v_gl_id_22)) ))], v__33);
      v__36[(v_gl_id_22 + (v_M_3 * v_gl_id_21))] = id(v__35);
    }
  }}
}}

//========================================================================================================================================================================================================200
//	Compress KERNEL
//========================================================================================================================================================================================================200

__kernel void 
compress_kernel(long d_Ne,
				__global fp* d_I){										// pointer to output image (DEVICE GLOBAL MEMORY)

	// indexes
	int bx = get_group_id(0);												// get current horizontal block index
	int tx = get_local_id(0);												// get current horizontal thread index
	int ei = (bx*NUMBER_THREADS)+tx;										// unique thread id, more threads than actual elements !!!

	// copy input to output & log uncompress
	if(ei<d_Ne){															// do only for the number of elements, omit extra threads

		d_I[ei] = log(d_I[ei])*255;												// exponentiate input IMAGE and copy to output image

	}

}

//========================================================================================================================================================================================================200
//	End
//========================================================================================================================================================================================================200
