#!/bin/bash

FILE=blocks.h

BLOCKX=( 2 4 8 16 32 64 128 )
BLOCKY=( 2 4 8 16 32 64 128 )
Z=1

tmpFile=tmp.txt

for Y in "${BLOCKY[@]}"; do
    for X in "${BLOCKX[@]}"; do
        echo -e "#define BLOCK_X" $X "\n#define BLOCK_Y" $Y "\n#define BLOCK_Z" $Z "\n"
        echo -e "#define BLOCK_X" $X "\n#define BLOCK_Y" $Y "\n#define BLOCK_Z" $Z "\n" > $FILE
        make clean && make
        outfile="blockx_"$X"_blocky_"$Y"_blockz_"$Z".txt"
        ../../bin/BasicRoom > $tmpFile
        value=$( grep -ic "time:" $tmpFile )
         if [ $value -gt 0 ]
         then  
             cp $tmpFile $outfile
         fi

     done
done

rm $tmpFile

