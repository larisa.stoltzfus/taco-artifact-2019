#atf::var<int> v_M_0 = 512 
#atf::var<int> v_N_1 = 512 
#atf::var<int> v_O_2 = 404 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.000,500) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(512,512)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(32,32)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(16,16)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
#ifndef Tuple2_float_float_DEFINED
#define Tuple2_float_float_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
} Tuple2_float_float;
#endif

// NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: float
 // NOTE: trying to print unprintable type: int
#ifndef Tuple3_float_float_int_DEFINED
#define Tuple3_float_float_int_DEFINED
typedef struct __attribute__((aligned(4))){
    float _0;
    float _1;
    int _2;
} Tuple3_float_float_int;
#endif

float getCF(int neigh, float cfB, float cfI){
    {
        { if(neigh < 6) { return cfB; } else{ return cfI;} }; 
    }
}
float mult(float l, float r){
    {
        { return l * r; }; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
float add(float x, float y){
    {
        { return x+y; }; 
    }
}
float idIF(int x){
    {
        { return (float)(x*1.0); }; 
    }
}
float subtractTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 - x._1;}; 
    }
}
float subtract(float l, float r){
    {
        { return l - r; }; 
    }
}
float addTuple(Tuple2_float_float x){
    {
        typedef Tuple2_float_float Tuple;
        {return x._0 + x._1;}; 
    }
}
int idxF(int i, int j, int k, int m, int n, int o){
    {
        {int count = 6; if(i == (m-1) || i == 0){ count--; } if(j == (n-1) || j == 0){ count--; } if(k == (o-1) || k == 0){ count--; }return count; }; 
    }
}
kernel void KERNEL(const global float* restrict v__264, const global float* restrict v__265, global float* v__307){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        float v__270; 
        float v__273; 
        float v__288; 
        float v__291; 
        float v__292; 
        float v__301; 
        float v__302; 
        // Private Memory
        float v__269; 
        float v__272; 
        float v__275; 
        float v__277; 
        float v__279; 
        float v__281; 
        float v__283; 
        float v__285; 
        float v__287; 
        float v__290; 
        float v__294; 
        float v__296; 
        float v__298; 
        float v__300; 
        float v__304; 
        float v__306; 
        for (int v_gl_id_261 = get_global_id(0); (v_gl_id_261 < 512); v_gl_id_261 = (v_gl_id_261 + get_global_size(0))){
            for (int v_gl_id_262 = get_global_id(1); (v_gl_id_262 < 512); v_gl_id_262 = (v_gl_id_262 + get_global_size(1))){
                // slideSeq_plus
                float v_window_1_0_591; 
                float v_window_1_1_592; 
                int v_window_1_2_593; 
                float v_window_2_0_594; 
                float v_window_2_1_595; 
                int v_window_2_2_596; 
                float v_window_3_0_597; 
                float v_window_3_1_598; 
                int v_window_3_2_599; 
                float v_window_4_0_600; 
                float v_window_4_1_601; 
                int v_window_4_2_602; 
                float v_window_5_0_603; 
                float v_window_5_1_604; 
                int v_window_5_2_605; 
                float v_window_6_0_606; 
                float v_window_6_1_607; 
                int v_window_6_2_608; 
                float v_window_7_0_609; 
                float v_window_7_1_610; 
                int v_window_7_2_611; 
                float v_window_8_0_612; 
                float v_window_8_1_613; 
                int v_window_8_2_614; 
                float v_window_9_0_615; 
                float v_window_9_1_616; 
                int v_window_9_2_617; 
                float v_window_10_0_618; 
                float v_window_10_1_619; 
                int v_window_10_2_620; 
                float v_window_11_0_621; 
                float v_window_11_1_622; 
                int v_window_11_2_623; 
                float v_window_12_0_624; 
                float v_window_12_1_625; 
                int v_window_12_2_626; 
                float v_window_13_0_627; 
                float v_window_13_1_628; 
                int v_window_13_2_629; 
                float v_window_14_0_630; 
                float v_window_14_1_631; 
                int v_window_14_2_632; 
                float v_window_15_0_633; 
                float v_window_15_1_634; 
                int v_window_15_2_635; 
                float v_window_16_0_636; 
                float v_window_16_1_637; 
                int v_window_16_2_638; 
                float v_window_17_0_639; 
                float v_window_17_1_640; 
                int v_window_17_2_641; 
                float v_window_18_0_642; 
                float v_window_18_1_643; 
                int v_window_18_2_644; 
                float v_window_19_0_645; 
                float v_window_19_1_646; 
                int v_window_19_2_647; 
                float v_window_20_0_648; 
                float v_window_20_1_649; 
                int v_window_20_2_650; 
                float v_window_21_0_651; 
                float v_window_21_1_652; 
                int v_window_21_2_653; 
                float v_window_22_0_654; 
                float v_window_22_1_655; 
                int v_window_22_2_656; 
                float v_window_23_0_657; 
                float v_window_23_1_658; 
                int v_window_23_2_659; 
                float v_window_24_0_660; 
                float v_window_24_1_661; 
                int v_window_24_2_662; 
                float v_window_25_0_663; 
                float v_window_25_1_664; 
                int v_window_25_2_665; 
                float v_window_26_0_666; 
                float v_window_26_1_667; 
                int v_window_26_2_668; 
                float v_window_27_0_669; 
                float v_window_27_1_670; 
                int v_window_27_2_671; 
                v_window_1_0_591 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__264[(-262657 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_1_1_592 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__265[(-262657 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_1_2_593 = idxF(0, v_gl_id_262, v_gl_id_261, 406, 514, 514); 
                v_window_2_0_594 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__264[(-513 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_2_1_595 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__265[(-513 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_2_2_596 = idxF(1, v_gl_id_262, v_gl_id_261, 406, 514, 514); 
                v_window_4_0_600 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__264[(-262145 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_4_1_601 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__265[(-262145 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_4_2_602 = idxF(0, (1 + v_gl_id_262), v_gl_id_261, 406, 514, 514); 
                v_window_5_0_603 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__264[(-1 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_5_1_604 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__265[(-1 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_5_2_605 = idxF(1, (1 + v_gl_id_262), v_gl_id_261, 406, 514, 514); 
                v_window_7_0_609 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__264[(-261633 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_7_1_610 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__265[(-261633 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_7_2_611 = idxF(0, (2 + v_gl_id_262), v_gl_id_261, 406, 514, 514); 
                v_window_8_0_612 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__264[(511 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_8_1_613 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__265[(511 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_8_2_614 = idxF(1, (2 + v_gl_id_262), v_gl_id_261, 406, 514, 514); 
                v_window_10_0_618 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__264[(-262656 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_10_1_619 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__265[(-262656 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_10_2_620 = idxF(0, v_gl_id_262, (1 + v_gl_id_261), 406, 514, 514); 
                v_window_11_0_621 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__264[(-512 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_11_1_622 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__265[(-512 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_11_2_623 = idxF(1, v_gl_id_262, (1 + v_gl_id_261), 406, 514, 514); 
                v_window_13_0_627 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__264[(-262144 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_13_1_628 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__265[(-262144 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_13_2_629 = idxF(0, (1 + v_gl_id_262), (1 + v_gl_id_261), 406, 514, 514); 
                v_window_14_0_630 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__264[(v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_14_1_631 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__265[(v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_14_2_632 = idxF(1, (1 + v_gl_id_262), (1 + v_gl_id_261), 406, 514, 514); 
                v_window_16_0_636 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__264[(-261632 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_16_1_637 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__265[(-261632 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_16_2_638 = idxF(0, (2 + v_gl_id_262), (1 + v_gl_id_261), 406, 514, 514); 
                v_window_17_0_639 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__264[(512 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_17_1_640 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__265[(512 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_17_2_641 = idxF(1, (2 + v_gl_id_262), (1 + v_gl_id_261), 406, 514, 514); 
                v_window_19_0_645 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__264[(-262655 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_19_1_646 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__265[(-262655 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_19_2_647 = idxF(0, v_gl_id_262, (2 + v_gl_id_261), 406, 514, 514); 
                v_window_20_0_648 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__264[(-511 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_20_1_649 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__265[(-511 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_20_2_650 = idxF(1, v_gl_id_262, (2 + v_gl_id_261), 406, 514, 514); 
                v_window_22_0_654 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__264[(-262143 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_22_1_655 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__265[(-262143 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_22_2_656 = idxF(0, (1 + v_gl_id_262), (2 + v_gl_id_261), 406, 514, 514); 
                v_window_23_0_657 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__264[(1 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_23_1_658 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__265[(1 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_23_2_659 = idxF(1, (1 + v_gl_id_262), (2 + v_gl_id_261), 406, 514, 514); 
                v_window_25_0_663 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__264[(-261631 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_25_1_664 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((-1 < 0) || (-1 >= 404)) ? 0.0f : v__265[(-261631 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_25_2_665 = idxF(0, (2 + v_gl_id_262), (2 + v_gl_id_261), 406, 514, 514); 
                v_window_26_0_666 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__264[(513 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_26_1_667 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : (((0 < 0) || (0 >= 404)) ? 0.0f : v__265[(513 + v_gl_id_261 + (512 * v_gl_id_262))]))); 
                v_window_26_2_668 = idxF(1, (2 + v_gl_id_262), (2 + v_gl_id_261), 406, 514, 514); 
#pragma unroll 1
                for (int v_i_263 = 0; (v_i_263 < 404); v_i_263 = (1 + v_i_263)){
                    v_window_3_0_597 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__264[(261631 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_3_1_598 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__265[(261631 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_3_2_599 = idxF((2 + v_i_263), v_gl_id_262, v_gl_id_261, 406, 514, 514); 
                    v_window_6_0_606 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__264[(262143 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_6_1_607 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__265[(262143 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_6_2_608 = idxF((2 + v_i_263), (1 + v_gl_id_262), v_gl_id_261, 406, 514, 514); 
                    v_window_9_0_615 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__264[(262655 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_9_1_616 = ((((-1 + v_gl_id_261) < 0) || ((-1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__265[(262655 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_9_2_617 = idxF((2 + v_i_263), (2 + v_gl_id_262), v_gl_id_261, 406, 514, 514); 
                    v_window_12_0_624 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__264[(261632 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_12_1_625 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__265[(261632 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_12_2_626 = idxF((2 + v_i_263), v_gl_id_262, (1 + v_gl_id_261), 406, 514, 514); 
                    v_window_15_0_633 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__264[(262144 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_15_1_634 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__265[(262144 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_15_2_635 = idxF((2 + v_i_263), (1 + v_gl_id_262), (1 + v_gl_id_261), 406, 514, 514); 
                    v_window_18_0_642 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__264[(262656 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_18_1_643 = (((v_gl_id_261 < 0) || (v_gl_id_261 >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__265[(262656 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_18_2_644 = idxF((2 + v_i_263), (2 + v_gl_id_262), (1 + v_gl_id_261), 406, 514, 514); 
                    v_window_21_0_651 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__264[(261633 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_21_1_652 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((-1 + v_gl_id_262) < 0) || ((-1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__265[(261633 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_21_2_653 = idxF((2 + v_i_263), v_gl_id_262, (2 + v_gl_id_261), 406, 514, 514); 
                    v_window_24_0_660 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__264[(262145 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_24_1_661 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : (((v_gl_id_262 < 0) || (v_gl_id_262 >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__265[(262145 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_24_2_662 = idxF((2 + v_i_263), (1 + v_gl_id_262), (2 + v_gl_id_261), 406, 514, 514); 
                    v_window_27_0_669 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__264[(262657 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_27_1_670 = ((((1 + v_gl_id_261) < 0) || ((1 + v_gl_id_261) >= 512)) ? 0.0f : ((((1 + v_gl_id_262) < 0) || ((1 + v_gl_id_262) >= 512)) ? 0.0f : ((((1 + v_i_263) < 0) || ((1 + v_i_263) >= 404)) ? 0.0f : v__265[(262657 + v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))]))); 
                    v_window_27_2_671 = idxF((2 + v_i_263), (2 + v_gl_id_262), (2 + v_gl_id_261), 406, 514, 514); 
                    v__269 = idIF(v_window_14_2_632); 
                    float v_tmp_557 = 0.3333333f; 
                    v__270 = v_tmp_557; 
                    v__272 = mult(v__269, v__270); 
                    float v_tmp_558 = 2.0f; 
                    v__273 = v_tmp_558; 
                    v__275 = subtract(v__273, v__272); 
                    v__277 = mult(v__275, v_window_14_1_631); 
                    v__279 = add(v_window_23_1_658, v_window_17_1_640); 
                    v__281 = add(v__279, v_window_15_1_634); 
                    v__283 = add(v__281, v_window_13_1_628); 
                    v__285 = add(v__283, v_window_11_1_622); 
                    v__287 = add(v__285, v_window_5_1_604); 
                    float v_tmp_559 = 0.3333333f; 
                    v__288 = v_tmp_559; 
                    v__290 = mult(v__287, v__288); 
                    float v_tmp_560 = 0.9971132f; 
                    v__291 = v_tmp_560; 
                    float v_tmp_561 = 1.0f; 
                    v__292 = v_tmp_561; 
                    v__294 = getCF(v_window_14_2_632, v__291, v__292); 
                    v__296 = mult(v_window_14_0_630, v__294); 
                    v__298 = subtractTuple((Tuple2_float_float){v__290, v__296}); 
                    v__300 = addTuple((Tuple2_float_float){v__277, v__298}); 
                    float v_tmp_562 = 0.9971216f; 
                    v__301 = v_tmp_562; 
                    float v_tmp_563 = 1.0f; 
                    v__302 = v_tmp_563; 
                    v__304 = getCF(v_window_14_2_632, v__301, v__302); 
                    v__306 = mult(v__300, v__304); 
                    v__307[(v_gl_id_261 + (512 * v_gl_id_262) + (262144 * v_i_263))] = id(v__306); 
                    v_window_1_0_591 = v_window_2_0_594; 
                    v_window_1_1_592 = v_window_2_1_595; 
                    v_window_1_2_593 = v_window_2_2_596; 
                    v_window_2_0_594 = v_window_3_0_597; 
                    v_window_2_1_595 = v_window_3_1_598; 
                    v_window_2_2_596 = v_window_3_2_599; 
                    v_window_4_0_600 = v_window_5_0_603; 
                    v_window_4_1_601 = v_window_5_1_604; 
                    v_window_4_2_602 = v_window_5_2_605; 
                    v_window_5_0_603 = v_window_6_0_606; 
                    v_window_5_1_604 = v_window_6_1_607; 
                    v_window_5_2_605 = v_window_6_2_608; 
                    v_window_7_0_609 = v_window_8_0_612; 
                    v_window_7_1_610 = v_window_8_1_613; 
                    v_window_7_2_611 = v_window_8_2_614; 
                    v_window_8_0_612 = v_window_9_0_615; 
                    v_window_8_1_613 = v_window_9_1_616; 
                    v_window_8_2_614 = v_window_9_2_617; 
                    v_window_10_0_618 = v_window_11_0_621; 
                    v_window_10_1_619 = v_window_11_1_622; 
                    v_window_10_2_620 = v_window_11_2_623; 
                    v_window_11_0_621 = v_window_12_0_624; 
                    v_window_11_1_622 = v_window_12_1_625; 
                    v_window_11_2_623 = v_window_12_2_626; 
                    v_window_13_0_627 = v_window_14_0_630; 
                    v_window_13_1_628 = v_window_14_1_631; 
                    v_window_13_2_629 = v_window_14_2_632; 
                    v_window_14_0_630 = v_window_15_0_633; 
                    v_window_14_1_631 = v_window_15_1_634; 
                    v_window_14_2_632 = v_window_15_2_635; 
                    v_window_16_0_636 = v_window_17_0_639; 
                    v_window_16_1_637 = v_window_17_1_640; 
                    v_window_16_2_638 = v_window_17_2_641; 
                    v_window_17_0_639 = v_window_18_0_642; 
                    v_window_17_1_640 = v_window_18_1_643; 
                    v_window_17_2_641 = v_window_18_2_644; 
                    v_window_19_0_645 = v_window_20_0_648; 
                    v_window_19_1_646 = v_window_20_1_649; 
                    v_window_19_2_647 = v_window_20_2_650; 
                    v_window_20_0_648 = v_window_21_0_651; 
                    v_window_20_1_649 = v_window_21_1_652; 
                    v_window_20_2_650 = v_window_21_2_653; 
                    v_window_22_0_654 = v_window_23_0_657; 
                    v_window_22_1_655 = v_window_23_1_658; 
                    v_window_22_2_656 = v_window_23_2_659; 
                    v_window_23_0_657 = v_window_24_0_660; 
                    v_window_23_1_658 = v_window_24_1_661; 
                    v_window_23_2_659 = v_window_24_2_662; 
                    v_window_25_0_663 = v_window_26_0_666; 
                    v_window_25_1_664 = v_window_26_1_667; 
                    v_window_25_2_665 = v_window_26_2_668; 
                    v_window_26_0_666 = v_window_27_0_669; 
                    v_window_26_1_667 = v_window_27_1_670; 
                    v_window_26_2_668 = v_window_27_2_671; 
                }
                // end slideSeq_plus
            }
        }
    }
}
