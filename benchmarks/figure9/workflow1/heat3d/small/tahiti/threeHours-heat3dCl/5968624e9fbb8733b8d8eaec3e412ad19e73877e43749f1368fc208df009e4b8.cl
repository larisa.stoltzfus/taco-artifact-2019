
// High-level hash: 660cae58392acd1b84ff4618386905f6b02940bfec7a333667bc0e3cffc65801
// Low-level hash: 5968624e9fbb8733b8d8eaec3e412ad19e73877e43749f1368fc208df009e4b8

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(10800)"


#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"


float heat(float C, float S, float N, float E, float W, float B, float F){
  return 0.125f * (B - 2.0f * C + F) +
       0.125f * (S - 2.0f * C + N) +
       0.125f * (E - 2.0f * C + W) + C;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__27, global float* v__30){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__29; 
  for (int v_gl_id_24 = get_global_id(2); v_gl_id_24 < 254; v_gl_id_24 = (v_gl_id_24 + GLOBAL_SIZE_2)) {
    for (int v_gl_id_25 = get_global_id(1); v_gl_id_25 < 254; v_gl_id_25 = (v_gl_id_25 + GLOBAL_SIZE_1)) {
      for (int v_gl_id_26 = get_global_id(0); v_gl_id_26 < 254; v_gl_id_26 = (v_gl_id_26 + GLOBAL_SIZE_0)) {
        v__29 = heat(v__27[(65793 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65537 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(66049 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65794 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))], v__27[(65792 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(257 + v_gl_id_26 + (256 * v_gl_id_25) + (65536 * v_gl_id_24))], v__27[(131329 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))]); 
        v__30[(65793 + v_gl_id_26 + (65536 * v_gl_id_24) + (256 * v_gl_id_25))] = id(v__29); 
      }
    }
  }
}}


