#include <assert.h>
#include <stdio.h>
#include "ocl_utilities.h"
// clang-format off
/*
 * grad-2d-sp.sdsl.c: This file is part of the SDSLC project.
 *
 * SDSLC: A compiler for high performance stencil computations
 *
 * Copyright (C) 2011-2013 Ohio State University
 *
 * This program can be redistributed and/or modified under the terms
 * of the license specified in the LICENSE.txt file at the root of the
 * project.
 *
 * Contact: P Sadayappan <saday@cse.ohio-state.edu>
 */

/*
 * @file: grad-2d-sp.sdsl.c
 * @author: Tom Henretty <henretty@cse.ohio-state.edu>
 */
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <math.h>
#include <assert.h>

// Problem parameters
#ifndef H
#define H (8192)
#endif

#ifndef W
#define W (8192)
#endif

#ifndef T
//#define T (4)
#define T (1)
#endif



float aref[2][H][W];
float a[2][H][W];



/** Main program */
int main(int argc, char *argv[]) {
  int i, j, ii, jj, t, tt;
  double refElapsed, sdslElapsed;
  double refGFLOPS, sdslGFLOPS;
  float (*inref)[W], (*outref)[W];
  
  // Initialize arrays
  for (i = 0; i < H; i++) {
    for (j = 0; j < W; j++) {
      aref[0][i][j] = aref[1][i][j] = sin(i)*cos(j);
      a[0][i][j] = a[1][i][j] = sin(i)*cos(j);
    }
  }
  
  // Compute reference
  #ifndef NOREF
  for (t = 0; t < T; t++) {
    inref  = aref[t & 1];
    outref = aref[(t + 1) & 1];
    for (i = 1; i < H - 1; i++) {
      #pragma ivdep
      #pragma vector always
      for (j = 1; j < W - 1; j++) {
         outref[i][j] = inref[i][j] + 1.0f/sqrt(0.0001f +
              (inref[i][j]-inref[i-1][j])*(inref[i][j]-inref[i-1][j]) +
              (inref[i][j]-inref[i+1][j])*(inref[i][j]-inref[i+1][j]) +
              (inref[i][j]-inref[i][j+1])*(inref[i][j]-inref[i][j+1]) +
              (inref[i][j]-inref[i][j-1])*(inref[i][j]-inref[i][j-1]));
      }
    }
  }
  #endif
  
  // Compute reference
  #ifndef NOREF
  for (t = 0; t < T; t++) {
    inref  = a[t & 1];
    outref = a[(t + 1) & 1];
    {
      #define openclCheckReturn(ret) \
  if (ret != CL_SUCCESS) {\
    fprintf(stderr, "OpenCL error: %s\n", opencl_error_string(ret)); \
    fflush(stderr); \
    assert(ret == CL_SUCCESS);\
  }

      cl_mem dev_inref;
      cl_mem dev_outref;
      
      cl_device_id device;
      cl_context context;
      cl_program program;
      cl_command_queue queue;
      cl_int err;
      device = opencl_create_device(1);
      char deviceName[1024];
      clGetDeviceInfo(device, CL_DEVICE_NAME, sizeof(deviceName), deviceName, NULL);
      printf("Using: %s\n",deviceName);
      context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
      openclCheckReturn(err);
      queue = clCreateCommandQueue(context, device, CL_QUEUE_PROFILING_ENABLE, &err);
      openclCheckReturn(err);
      program = opencl_build_program_from_file(context, device, "grad2d_kernel.cl", "");
      
      {
        dev_inref = clCreateBuffer(context, CL_MEM_READ_WRITE, (8192) * (8192) * sizeof(float), NULL, &err);
        openclCheckReturn(err);
      }
      {
        dev_outref = clCreateBuffer(context, CL_MEM_READ_WRITE, (8191) * (8192) * sizeof(float), NULL, &err);
        openclCheckReturn(err);
      }
      
      openclCheckReturn(clEnqueueWriteBuffer(queue, dev_inref, CL_TRUE, 0, (8192) * (8192) * sizeof(float), inref, 0, NULL, NULL));
      openclCheckReturn(clEnqueueWriteBuffer(queue, dev_outref, CL_TRUE, 0, (8191) * (8192) * sizeof(float), outref, 0, NULL, NULL));
      {
        cl_kernel kernel0 = clCreateKernel(program, "kernel0", &err);
         size_t global_work_size[3];
         size_t block_size[3];
         if(argc == 7) {
             global_work_size[0] = atoi(argv[1]);
             global_work_size[1] = atoi(argv[2]);
             global_work_size[2] = atoi(argv[3]); 
             block_size[0] = atoi(argv[4]);
             block_size[1] = atoi(argv[5]);
             block_size[2] = atoi(argv[6]);
         } else {
             global_work_size[0] = (256) * 32;
             global_work_size[1] = (256) * 16;
             global_work_size[2] = 1;
             block_size[0] = 32;
             block_size[1] = 16;
             block_size[2] = 1;
         }
        openclCheckReturn(err);
        openclCheckReturn(clSetKernelArg(kernel0, 0, sizeof(cl_mem), (void *) &dev_inref));
        openclCheckReturn(clSetKernelArg(kernel0, 1, sizeof(cl_mem), (void *) &dev_outref));
        cl_event mainKernelEvt;
openclCheckReturn(clEnqueueNDRangeKernel(queue, kernel0, 2, NULL, global_work_size, block_size, 0, NULL, &mainKernelEvt));
        unsigned long long elapsed = 0;
        cl_ulong time_start, time_end;
        clWaitForEvents(1, &mainKernelEvt);
        clGetEventProfilingInfo(mainKernelEvt, CL_PROFILING_COMMAND_START, sizeof(time_start), &time_start, NULL);
        clGetEventProfilingInfo(mainKernelEvt, CL_PROFILING_COMMAND_END, sizeof(time_end), &time_end, NULL);
        elapsed = (time_end - time_start);
        printf("[DEBUG] kernel runtime: %llu\n",elapsed);
        openclCheckReturn(clReleaseKernel(kernel0));
        clFinish(queue);
      }
      
      openclCheckReturn(clEnqueueReadBuffer(queue, dev_outref, CL_TRUE, 0, (8191) * (8192) * sizeof(float), outref, 0, NULL, NULL));
      i = (8191);
      j = (8191);
      openclCheckReturn(clReleaseMemObject(dev_inref));
      openclCheckReturn(clReleaseMemObject(dev_outref));
      openclCheckReturn(clReleaseCommandQueue(queue));
      openclCheckReturn(clReleaseProgram(program));
      openclCheckReturn(clReleaseContext(context));
    }
  }
  #endif 
  
  // Check correctness
  inref = a[T & 1]; outref = aref[T & 1];float maxDiff = 0.0, sumDiff = 0.0, diff, meanDiff;
  int numDiff = 0;
  for (i = 4; i < H-4; i++) {
    for (j = 4; j < W-4; j++) {
      diff = fabs(outref[i][j] - inref[i][j]);
      if (diff > 0.0001f) {
        numDiff++;
        sumDiff += diff;
      }
      if (diff > maxDiff) {
        maxDiff = diff;
      }
    }
  }
  meanDiff = sumDiff/((float)(H*W));
  
  printf("Num diff  = %d\n",numDiff);
  printf("Sum diff  = %.8f\n",sumDiff);
  printf("Mean diff = %.8f\n",meanDiff);
  printf("Max diff  = %.8f\n",maxDiff);
  return 0;
}
