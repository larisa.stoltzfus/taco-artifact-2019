
// High-level hash: c720d531d84c51f53e6069501314edc331cd4463c76acd4222a33cce8c14745f
// Low-level hash: dceb149d908c352f30a31aa49b38fb953e5b81169961616b083e737382fb23a4

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_2117" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"
#atf::tp name "v_TP_2116" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__2167, global float* v__2173){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__2171_0;
  
  for (int v_gl_id_2163 = get_global_id(1); v_gl_id_2163 < (4092 / v_TP_2116); v_gl_id_2163 = (v_gl_id_2163 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_2164 = get_global_id(0); v_gl_id_2164 < (4092 / v_TP_2117); v_gl_id_2164 = (v_gl_id_2164 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_2165 = 0; v_i_2165 < v_TP_2116; v_i_2165 = (1 + v_i_2165)) {
        /* map_seq */
        for (int v_i_2166 = 0; v_i_2166 < v_TP_2117; v_i_2166 = (1 + v_i_2166)) {
          v__2171_0 = jacobi(v__2167[((v_i_2166 % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (((4092 / v_TP_2117) * (v_i_2165 % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * (v_i_2165 % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(1 + (v_i_2166 % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (((4092 / v_TP_2117) * (v_i_2165 % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * (v_i_2165 % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(2 + (v_i_2166 % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (((4092 / v_TP_2117) * (v_i_2165 % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * (v_i_2165 % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(3 + (v_i_2166 % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (((4092 / v_TP_2117) * (v_i_2165 % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * (v_i_2165 % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(4 + (v_i_2166 % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (((4092 / v_TP_2117) * (v_i_2165 % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * (v_i_2165 % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(((v_TP_2117 + v_i_2166) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((1 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((1 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(1 + ((v_TP_2117 + v_i_2166) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((1 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((1 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(2 + ((v_TP_2117 + v_i_2166) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((1 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((1 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(3 + ((v_TP_2117 + v_i_2166) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((1 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((1 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(4 + ((v_TP_2117 + v_i_2166) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((1 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((1 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(((v_i_2166 + (2 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((2 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((2 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(1 + ((v_i_2166 + (2 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((2 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((2 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(2 + ((v_i_2166 + (2 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((2 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((2 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(3 + ((v_i_2166 + (2 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((2 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((2 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(4 + ((v_i_2166 + (2 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((2 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((2 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(((v_i_2166 + (3 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((3 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((3 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(1 + ((v_i_2166 + (3 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((3 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((3 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(2 + ((v_i_2166 + (3 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((3 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((3 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(3 + ((v_i_2166 + (3 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((3 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((3 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(4 + ((v_i_2166 + (3 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((3 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((3 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(((v_i_2166 + (4 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((4 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((4 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(1 + ((v_i_2166 + (4 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((4 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((4 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(2 + ((v_i_2166 + (4 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((4 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((4 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(3 + ((v_i_2166 + (4 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((4 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((4 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))], v__2167[(4 + ((v_i_2166 + (4 * v_TP_2117)) % v_TP_2117) + (4096 * v_TP_2116 * v_gl_id_2163) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((4 + v_i_2165) % (4 + v_TP_2116))) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((4 + v_i_2165) % (4 + v_TP_2116)))) % (4092 / v_TP_2117))))]); 
          v__2173[(8194 + v_i_2166 + (4096 * v_TP_2116 * v_gl_id_2163) + (-16777216 * (((v_gl_id_2164 / (4092 / v_TP_2117)) + (((4092 / v_TP_2117) * ((v_i_2165 + (v_TP_2116 * v_gl_id_2164)) % v_TP_2116)) / (4092 / v_TP_2117)) + (v_TP_2116 * v_gl_id_2163)) / 4095)) + (-4096 * ((v_i_2166 + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((v_i_2165 + (v_TP_2116 * v_gl_id_2164)) % v_TP_2116))) % (4092 / v_TP_2117)))) / 4095)) + (4096 * (v_gl_id_2164 / (4092 / v_TP_2117))) + (4096 * (((4092 / v_TP_2117) * ((v_i_2165 + (v_TP_2116 * v_gl_id_2164)) % v_TP_2116)) / (4092 / v_TP_2117))) + (v_TP_2117 * ((v_gl_id_2164 + ((4092 / v_TP_2117) * ((v_i_2165 + (v_TP_2116 * v_gl_id_2164)) % v_TP_2116))) % (4092 / v_TP_2117))))] = id(v__2171_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


