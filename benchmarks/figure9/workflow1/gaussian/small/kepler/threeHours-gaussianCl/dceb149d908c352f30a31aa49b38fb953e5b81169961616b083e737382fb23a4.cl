
// High-level hash: b25aafd5344e2613607aeacf0ac8b39af4010fc888fe7aaa7d2630b91f7a6de9
// Low-level hash: dceb149d908c352f30a31aa49b38fb953e5b81169961616b083e737382fb23a4

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_53" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"
#atf::tp name "v_TP_52" \
 type "int" \
 range "atf::interval<int>(1,4092)" \
 constraint "atf::divides(4092)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__82, global float* v__85){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__84_0;
  
  for (int v_gl_id_78 = get_global_id(1); v_gl_id_78 < (4092 / v_TP_52); v_gl_id_78 = (v_gl_id_78 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_79 = get_global_id(0); v_gl_id_79 < (4092 / v_TP_53); v_gl_id_79 = (v_gl_id_79 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_80 = 0; v_i_80 < v_TP_52; v_i_80 = (1 + v_i_80)) {
        /* map_seq */
        for (int v_i_81 = 0; v_i_81 < v_TP_53; v_i_81 = (1 + v_i_81)) {
          v__84_0 = jacobi(v__82[((v_i_81 % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (((4092 / v_TP_53) * (v_i_80 % (4 + v_TP_52))) / (4092 / v_TP_53))) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * (v_i_80 % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(1 + (v_i_81 % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (((4092 / v_TP_53) * (v_i_80 % (4 + v_TP_52))) / (4092 / v_TP_53))) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * (v_i_80 % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(2 + (v_i_81 % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (((4092 / v_TP_53) * (v_i_80 % (4 + v_TP_52))) / (4092 / v_TP_53))) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * (v_i_80 % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(3 + (v_i_81 % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (((4092 / v_TP_53) * (v_i_80 % (4 + v_TP_52))) / (4092 / v_TP_53))) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * (v_i_80 % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(4 + (v_i_81 % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (((4092 / v_TP_53) * (v_i_80 % (4 + v_TP_52))) / (4092 / v_TP_53))) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * (v_i_80 % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(((v_TP_53 + v_i_81) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((1 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((1 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(1 + ((v_TP_53 + v_i_81) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((1 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((1 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(2 + ((v_TP_53 + v_i_81) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((1 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((1 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(3 + ((v_TP_53 + v_i_81) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((1 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((1 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(4 + ((v_TP_53 + v_i_81) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((1 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((1 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(((v_i_81 + (2 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((2 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((2 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(1 + ((v_i_81 + (2 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((2 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((2 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(2 + ((v_i_81 + (2 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((2 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((2 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(3 + ((v_i_81 + (2 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((2 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((2 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(4 + ((v_i_81 + (2 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((2 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((2 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(((v_i_81 + (3 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((3 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((3 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(1 + ((v_i_81 + (3 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((3 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((3 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(2 + ((v_i_81 + (3 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((3 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((3 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(3 + ((v_i_81 + (3 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((3 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((3 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(4 + ((v_i_81 + (3 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((3 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((3 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(((v_i_81 + (4 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((4 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((4 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(1 + ((v_i_81 + (4 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((4 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((4 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(2 + ((v_i_81 + (4 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((4 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((4 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(3 + ((v_i_81 + (4 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((4 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((4 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))], v__82[(4 + ((v_i_81 + (4 * v_TP_53)) % v_TP_53) + (4096 * v_TP_52 * v_gl_id_78) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((4 + v_i_80) % (4 + v_TP_52))) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((4 + v_i_80) % (4 + v_TP_52)))) % (4092 / v_TP_53))))]); 
          v__85[(8194 + v_i_81 + (4096 * v_TP_52 * v_gl_id_78) + (-16777216 * (((v_gl_id_79 / (4092 / v_TP_53)) + (((4092 / v_TP_53) * ((v_i_80 + (v_TP_52 * v_gl_id_79)) % v_TP_52)) / (4092 / v_TP_53)) + (v_TP_52 * v_gl_id_78)) / 4095)) + (-4096 * ((v_i_81 + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((v_i_80 + (v_TP_52 * v_gl_id_79)) % v_TP_52))) % (4092 / v_TP_53)))) / 4095)) + (4096 * (v_gl_id_79 / (4092 / v_TP_53))) + (4096 * (((4092 / v_TP_53) * ((v_i_80 + (v_TP_52 * v_gl_id_79)) % v_TP_52)) / (4092 / v_TP_53))) + (v_TP_53 * ((v_gl_id_79 + ((4092 / v_TP_53) * ((v_i_80 + (v_TP_52 * v_gl_id_79)) % v_TP_52))) % (4092 / v_TP_53))))] = id(v__84_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


