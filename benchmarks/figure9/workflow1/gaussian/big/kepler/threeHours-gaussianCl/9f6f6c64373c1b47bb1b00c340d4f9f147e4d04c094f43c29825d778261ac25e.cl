
// High-level hash: bfab1cbf6d9d15128fbfb958c120c81491403df4ac45ed65e33661f888707a43
// Low-level hash: 9f6f6c64373c1b47bb1b00c340d4f9f147e4d04c094f43c29825d778261ac25e

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_182" \
 type "int" \
 range "atf::interval<int>(1,8188)" \
 constraint "atf::divides(8188)"
#atf::tp name "v_TP_181" \
 type "int" \
 range "atf::interval<int>(1,8188)" \
 constraint "atf::divides(8188)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__258, global float* v__300){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__296[(16+(4*v_TP_181)+(4*v_TP_182)+(v_TP_181*v_TP_182))];
  /* Typed Value memory */
  /* Private Memory */
  float v__299_0;
  
  for (int v_wg_id_243 = get_group_id(1); v_wg_id_243 < (8188 / v_TP_181); v_wg_id_243 = (v_wg_id_243 + NUM_GROUPS_1)) {
    for (int v_wg_id_244 = get_group_id(0); v_wg_id_244 < (8188 / v_TP_182); v_wg_id_244 = (v_wg_id_244 + NUM_GROUPS_0)) {
      for (int v_l_id_245 = get_local_id(1); v_l_id_245 < (4 + v_TP_181); v_l_id_245 = (v_l_id_245 + LOCAL_SIZE_1)) {
        for (int v_l_id_247 = get_local_id(0); v_l_id_247 < (4 + v_TP_182); v_l_id_247 = (v_l_id_247 + LOCAL_SIZE_0)) {
          v__296[(v_l_id_247 + (4 * v_l_id_245) + (v_TP_182 * v_l_id_245))] = idfloat(v__258[(v_l_id_247 + (8192 * v_TP_181 * v_wg_id_243) + (8192 * (v_wg_id_244 / (8188 / v_TP_182))) + (8192 * (((8188 / v_TP_182) * (v_l_id_245 % (4 + v_TP_181))) / (8188 / v_TP_182))) + (v_TP_182 * ((v_wg_id_244 + ((8188 / v_TP_182) * (v_l_id_245 % (4 + v_TP_181)))) % (8188 / v_TP_182))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_256 = get_local_id(1); v_l_id_256 < v_TP_181; v_l_id_256 = (v_l_id_256 + LOCAL_SIZE_1)) {
        for (int v_l_id_257 = get_local_id(0); v_l_id_257 < v_TP_182; v_l_id_257 = (v_l_id_257 + LOCAL_SIZE_0)) {
          v__299_0 = jacobi(v__296[((v_l_id_257 % v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(1 + (v_l_id_257 % v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(2 + (v_l_id_257 % v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(3 + (v_l_id_257 % v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(4 + (v_l_id_257 % v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(4 + v_TP_182 + ((v_TP_182 + v_l_id_257) % v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(5 + v_TP_182 + ((v_TP_182 + v_l_id_257) % v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(6 + v_TP_182 + ((v_TP_182 + v_l_id_257) % v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(7 + v_TP_182 + ((v_TP_182 + v_l_id_257) % v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(8 + v_TP_182 + ((v_TP_182 + v_l_id_257) % v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(8 + v_l_id_257 + (2 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(9 + v_l_id_257 + (2 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(10 + v_l_id_257 + (2 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(11 + v_l_id_257 + (2 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(12 + v_l_id_257 + (2 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(12 + v_l_id_257 + (3 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(13 + v_l_id_257 + (3 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(14 + v_l_id_257 + (3 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(15 + v_l_id_257 + (3 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(16 + v_l_id_257 + (3 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(16 + v_l_id_257 + (4 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(17 + v_l_id_257 + (4 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(18 + v_l_id_257 + (4 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(19 + v_l_id_257 + (4 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))], v__296[(20 + v_l_id_257 + (4 * v_TP_182) + (4 * v_l_id_256) + (v_TP_182 * v_l_id_256))]); 
          v__300[(16386 + v_l_id_257 + (-8192 * ((v_l_id_257 + (v_TP_182 * ((v_wg_id_244 + ((8188 / v_TP_182) * ((v_l_id_256 + (v_TP_181 * v_wg_id_244)) % v_TP_181))) % (8188 / v_TP_182)))) / 8191)) + (8192 * v_TP_181 * v_wg_id_243) + (-67108864 * (((((8188 / v_TP_182) * ((v_l_id_256 + (v_TP_181 * v_wg_id_244)) % v_TP_181)) / (8188 / v_TP_182)) + (v_wg_id_244 / (8188 / v_TP_182)) + (v_TP_181 * v_wg_id_243)) / 8191)) + (8192 * (((8188 / v_TP_182) * ((v_l_id_256 + (v_TP_181 * v_wg_id_244)) % v_TP_181)) / (8188 / v_TP_182))) + (8192 * (v_wg_id_244 / (8188 / v_TP_182))) + (v_TP_182 * ((v_wg_id_244 + ((8188 / v_TP_182) * ((v_l_id_256 + (v_TP_181 * v_wg_id_244)) % v_TP_181))) % (8188 / v_TP_182))))] = id(v__299_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


