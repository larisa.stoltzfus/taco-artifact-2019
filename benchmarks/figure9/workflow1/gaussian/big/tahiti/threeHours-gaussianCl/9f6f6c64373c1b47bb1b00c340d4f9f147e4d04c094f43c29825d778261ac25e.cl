
// High-level hash: dcc51678d87162f9202787a158fe83780cdccf4c3ddcf9f7384c8a0ae764019e
// Low-level hash: 9f6f6c64373c1b47bb1b00c340d4f9f147e4d04c094f43c29825d778261ac25e

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_171" \
 type "int" \
 range "atf::interval<int>(1,8188)" \
 constraint "atf::divides(8188)"
#atf::tp name "v_TP_170" \
 type "int" \
 range "atf::interval<int>(1,8188)" \
 constraint "atf::divides(8188)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float NNWW, float NNW, float NN, float NNE, float NNEE, float NWW, float NW, float N, float NE, float NEE, float WW, float W, float C, float E, float EE, float SWW, float SW, float S, float SE, float SEE, float SSWW, float SSW, float SS, float SSE, float SSEE){
  return (2*NNWW + 4*NNW + 5*NN + 4*NNE + 2*NNEE +
 4*NWW + 9*NW + 12*N + 9*NE + 4*NEE +
 5*WW + 12*W + 15*C + 12*E + 5*EE +
 4*SWW + 9*SW + 12*S + 9*SE + 4*SEE +
 2*SSWW + 4*SSW + 5*SS + 4*SSE + 2*SSEE) / 159;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__214, global float* v__245){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__242[(16+(4*v_TP_170)+(4*v_TP_171)+(v_TP_170*v_TP_171))];
  /* Typed Value memory */
  /* Private Memory */
  float v__244_0;
  
  for (int v_wg_id_206 = get_group_id(1); v_wg_id_206 < (8188 / v_TP_170); v_wg_id_206 = (v_wg_id_206 + NUM_GROUPS_1)) {
    for (int v_wg_id_208 = get_group_id(0); v_wg_id_208 < (8188 / v_TP_171); v_wg_id_208 = (v_wg_id_208 + NUM_GROUPS_0)) {
      for (int v_l_id_209 = get_local_id(1); v_l_id_209 < (4 + v_TP_170); v_l_id_209 = (v_l_id_209 + LOCAL_SIZE_1)) {
        for (int v_l_id_210 = get_local_id(0); v_l_id_210 < (4 + v_TP_171); v_l_id_210 = (v_l_id_210 + LOCAL_SIZE_0)) {
          v__242[(v_l_id_210 + (4 * v_l_id_209) + (v_TP_171 * v_l_id_209))] = idfloat(v__214[(v_l_id_210 + (8192 * v_TP_170 * v_wg_id_206) + (8192 * (v_wg_id_208 / (8188 / v_TP_171))) + (8192 * (((8188 / v_TP_171) * (v_l_id_209 % (4 + v_TP_170))) / (8188 / v_TP_171))) + (v_TP_171 * ((v_wg_id_208 + ((8188 / v_TP_171) * (v_l_id_209 % (4 + v_TP_170)))) % (8188 / v_TP_171))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_211 = get_local_id(1); v_l_id_211 < v_TP_170; v_l_id_211 = (v_l_id_211 + LOCAL_SIZE_1)) {
        for (int v_l_id_212 = get_local_id(0); v_l_id_212 < v_TP_171; v_l_id_212 = (v_l_id_212 + LOCAL_SIZE_0)) {
          v__244_0 = jacobi(v__242[((v_l_id_212 % v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(1 + (v_l_id_212 % v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(2 + (v_l_id_212 % v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(3 + (v_l_id_212 % v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(4 + (v_l_id_212 % v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(4 + v_TP_171 + ((v_TP_171 + v_l_id_212) % v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(5 + v_TP_171 + ((v_TP_171 + v_l_id_212) % v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(6 + v_TP_171 + ((v_TP_171 + v_l_id_212) % v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(7 + v_TP_171 + ((v_TP_171 + v_l_id_212) % v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(8 + v_TP_171 + ((v_TP_171 + v_l_id_212) % v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(8 + v_l_id_212 + (2 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(9 + v_l_id_212 + (2 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(10 + v_l_id_212 + (2 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(11 + v_l_id_212 + (2 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(12 + v_l_id_212 + (2 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(12 + v_l_id_212 + (3 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(13 + v_l_id_212 + (3 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(14 + v_l_id_212 + (3 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(15 + v_l_id_212 + (3 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(16 + v_l_id_212 + (3 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(16 + v_l_id_212 + (4 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(17 + v_l_id_212 + (4 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(18 + v_l_id_212 + (4 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(19 + v_l_id_212 + (4 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))], v__242[(20 + v_l_id_212 + (4 * v_TP_171) + (4 * v_l_id_211) + (v_TP_171 * v_l_id_211))]); 
          v__245[(16386 + v_l_id_212 + (-8192 * ((v_l_id_212 + (v_TP_171 * ((v_wg_id_208 + ((8188 / v_TP_171) * ((v_l_id_211 + (v_TP_170 * v_wg_id_208)) % v_TP_170))) % (8188 / v_TP_171)))) / 8191)) + (8192 * v_TP_170 * v_wg_id_206) + (-67108864 * (((((8188 / v_TP_171) * ((v_l_id_211 + (v_TP_170 * v_wg_id_208)) % v_TP_170)) / (8188 / v_TP_171)) + (v_wg_id_208 / (8188 / v_TP_171)) + (v_TP_170 * v_wg_id_206)) / 8191)) + (8192 * (((8188 / v_TP_171) * ((v_l_id_211 + (v_TP_170 * v_wg_id_208)) % v_TP_170)) / (8188 / v_TP_171))) + (8192 * (v_wg_id_208 / (8188 / v_TP_171))) + (v_TP_171 * ((v_wg_id_208 + ((8188 / v_TP_171) * ((v_l_id_211 + (v_TP_170 * v_wg_id_208)) % v_TP_170))) % (8188 / v_TP_171))))] = id(v__244_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


