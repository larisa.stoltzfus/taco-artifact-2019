#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(10800)"

#atf::tp name "TILE_2" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto TILE_2){ return (TILE_2 & (TILE_2 - 1)) == 0; }"
#atf::tp name "TILE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto TILE_1){ return (TILE_1 & (TILE_1 - 1)) == 0; }"
#atf::tp name "TILE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto TILE_0){ return (TILE_0 & (TILE_0 - 1)) == 0; }"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"

#atf::tp name "LOCAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "atf::divides(GLOBAL_SIZE_0)" 
#atf::tp name "LOCAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "atf::divides(GLOBAL_SIZE_1)" 
#atf::tp name "LOCAL_SIZE_2" type "int" range "atf::interval<int>(1,256)" constraint "atf::divides(GLOBAL_SIZE_2) && [&](auto LOCAL_SIZE_2){ return LOCAL_SIZE_0 * LOCAL_SIZE_1 * LOCAL_SIZE_2 <= 256; }"

#atf::cost_file "costfile.txt"
#atf::run_script "./run.sh"

#!/bin/bash
GRID_0="$(python -c "print (GLOBAL_SIZE_0 / LOCAL_SIZE_0)")"
GRID_1="$(python -c "print (GLOBAL_SIZE_1 / LOCAL_SIZE_1)")"
GRID_2="$(python -c "print (GLOBAL_SIZE_2 / LOCAL_SIZE_2)")"

ppcg --target=opencl --sizes "{ kernel[0] -> tile[TILE_0,TILE_1,TILE_2]; kernel[0] -> block[LOCAL_SIZE_0,LOCAL_SIZE_1,LOCAL_SIZE_2]; kernel[0] -> grid[$GRID_0,$GRID_1,$GRID_2] }" j3d13pt.c

timeout -k 2m 2m ./j3d13pt GLOBAL_SIZE_0 GLOBAL_SIZE_1 GLOBAL_SIZE_2 LOCAL_SIZE_0 LOCAL_SIZE_1 LOCAL_SIZE_2 > tmp.txt
cat tmp.txt >> tune.out # save output
cat tmp.txt | tail -5 > result.txt # use output
rm tmp.txt
(>&2 cat result.txt )

GOLDMAX="$(cat gold.txt | tail -1 | awk '{print $4}')"
OURMAX="$(cat result.txt | tail -1 | awk '{print $4}')"
RESULT="$(python -c "print (abs($GOLDMAX-$OURMAX) > 0.005)")"
if [ $RESULT == False ] ; then
				(>&2 echo -e "\e[32mPASSED\e[0m" ) # print to stderr
				echo "PASSED" 										 # print to stdout > gets caught in file
				cat result.txt | head -1 | awk '{print $4}' > costfile.txt
else
				(>&2 echo -e "\e[31mFAILED!\e[0m" )
				echo "FAILED"
				echo 18446744073709551615 > costfile.txt
fi
