
// High-level hash: fa2766cf081e60976c85fc2be011368f23dc2d03ee4a10ee662dfe4702eb3524
// Low-level hash: 2ddd9947c1b1ffd418ed644285c052ebe8336329b0a58ebf827431b94726b219

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,256)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"


float jacobi(float C, float N, float S, float E, float W, float F, float B, float FN, float BN, float FS, float BS, float FW, float BW, float NW, float SW, float FE, float BE, float NE, float SE){
  return 2.666f * C - 0.166f * (F + B + N + S + E + W) -
       0.0833f * (FN + BN + FS + BS + FW + BW +
                  NW + SW + FE + BE + NE + SE);
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__59, global float* v__68){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__67; 
  for (int v_gl_id_55 = get_global_id(0); v_gl_id_55 < 254; v_gl_id_55 = (v_gl_id_55 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_57 = get_global_id(1); v_gl_id_57 < 254; v_gl_id_57 = (v_gl_id_57 + GLOBAL_SIZE_1)) {
      for (int v_gl_id_58 = get_global_id(2); v_gl_id_58 < 254; v_gl_id_58 = (v_gl_id_58 + GLOBAL_SIZE_2)) {
        v__67 = jacobi(v__59[(65793 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))], v__59[(65537 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))], v__59[(66049 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))], v__59[(65794 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))], v__59[(65792 + v_gl_id_58 + (256 * v_gl_id_57) + (65536 * v_gl_id_55))], v__59[(257 + v_gl_id_58 + (256 * v_gl_id_57) + (65536 * v_gl_id_55))], v__59[(131329 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))], v__59[(1 + v_gl_id_58 + (256 * v_gl_id_57) + (65536 * v_gl_id_55))], v__59[(131073 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))], v__59[(513 + v_gl_id_58 + (256 * v_gl_id_57) + (65536 * v_gl_id_55))], v__59[(131585 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))], v__59[(256 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))], v__59[(131328 + v_gl_id_58 + (256 * v_gl_id_57) + (65536 * v_gl_id_55))], v__59[(65536 + v_gl_id_58 + (256 * v_gl_id_57) + (65536 * v_gl_id_55))], v__59[(66048 + v_gl_id_58 + (256 * v_gl_id_57) + (65536 * v_gl_id_55))], v__59[(258 + v_gl_id_58 + (256 * v_gl_id_57) + (65536 * v_gl_id_55))], v__59[(131330 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))], v__59[(65538 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))], v__59[(66050 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))]); 
        v__68[(65793 + v_gl_id_58 + (65536 * v_gl_id_55) + (256 * v_gl_id_57))] = id(v__67); 
      }
    }
  }
}}


