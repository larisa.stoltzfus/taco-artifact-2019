
// High-level hash: d9610667c34db21b3b85dc5bdf67a09b199cf48e2fe915ba50f720758fe1c5d7
// Low-level hash: 8f702eeaf65eb4007fdea250a45cbc19678f09db4507eb9e0fd1bc5fe314df8b

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_382" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_381" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__401, global float* v__405){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__402[(4+(2*v_TP_381)+(2*v_TP_382)+(v_TP_381*v_TP_382))];
  /* Typed Value memory */
  /* Private Memory */
  float v__404_0;
  
  for (int v_wg_id_395 = get_group_id(1); v_wg_id_395 < (4094 / v_TP_381); v_wg_id_395 = (v_wg_id_395 + NUM_GROUPS_1)) {
    for (int v_wg_id_396 = get_group_id(0); v_wg_id_396 < (4094 / v_TP_382); v_wg_id_396 = (v_wg_id_396 + NUM_GROUPS_0)) {
      for (int v_l_id_397 = get_local_id(1); v_l_id_397 < (2 + v_TP_381); v_l_id_397 = (v_l_id_397 + LOCAL_SIZE_1)) {
        for (int v_l_id_398 = get_local_id(0); v_l_id_398 < (2 + v_TP_382); v_l_id_398 = (v_l_id_398 + LOCAL_SIZE_0)) {
          v__402[(v_l_id_398 + (2 * v_l_id_397) + (v_TP_382 * v_l_id_397))] = idfloat(v__401[(v_l_id_398 + (4096 * v_TP_381 * v_wg_id_395) + (4096 * (v_wg_id_396 / (4094 / v_TP_382))) + (4096 * (((4094 / v_TP_382) * (v_l_id_397 % (2 + v_TP_381))) / (4094 / v_TP_382))) + (v_TP_382 * ((v_wg_id_396 + ((4094 / v_TP_382) * (v_l_id_397 % (2 + v_TP_381)))) % (4094 / v_TP_382))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_399 = get_local_id(1); v_l_id_399 < v_TP_381; v_l_id_399 = (v_l_id_399 + LOCAL_SIZE_1)) {
        for (int v_l_id_400 = get_local_id(0); v_l_id_400 < v_TP_382; v_l_id_400 = (v_l_id_400 + LOCAL_SIZE_0)) {
          v__404_0 = jacobi(v__402[(1 + (v_l_id_400 % v_TP_382) + (2 * v_l_id_399) + (v_TP_382 * v_l_id_399))], v__402[(5 + v_l_id_400 + (2 * v_TP_382) + (2 * v_l_id_399) + (v_TP_382 * v_l_id_399))], v__402[(2 + v_TP_382 + ((v_TP_382 + v_l_id_400) % v_TP_382) + (2 * v_l_id_399) + (v_TP_382 * v_l_id_399))], v__402[(4 + v_TP_382 + ((v_TP_382 + v_l_id_400) % v_TP_382) + (2 * v_l_id_399) + (v_TP_382 * v_l_id_399))], v__402[(3 + v_TP_382 + ((v_TP_382 + v_l_id_400) % v_TP_382) + (2 * v_l_id_399) + (v_TP_382 * v_l_id_399))]); 
          v__405[(4097 + v_l_id_400 + (-4096 * ((v_l_id_400 + (v_TP_382 * ((v_wg_id_396 + ((4094 / v_TP_382) * ((v_l_id_399 + (v_TP_381 * v_wg_id_396)) % v_TP_381))) % (4094 / v_TP_382)))) / 4095)) + (4096 * v_TP_381 * v_wg_id_395) + (-16777216 * (((((4094 / v_TP_382) * ((v_l_id_399 + (v_TP_381 * v_wg_id_396)) % v_TP_381)) / (4094 / v_TP_382)) + (v_wg_id_396 / (4094 / v_TP_382)) + (v_TP_381 * v_wg_id_395)) / 4095)) + (4096 * (((4094 / v_TP_382) * ((v_l_id_399 + (v_TP_381 * v_wg_id_396)) % v_TP_381)) / (4094 / v_TP_382))) + (4096 * (v_wg_id_396 / (4094 / v_TP_382))) + (v_TP_382 * ((v_wg_id_396 + ((4094 / v_TP_382) * ((v_l_id_399 + (v_TP_381 * v_wg_id_396)) % v_TP_381))) % (4094 / v_TP_382))))] = id(v__404_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


