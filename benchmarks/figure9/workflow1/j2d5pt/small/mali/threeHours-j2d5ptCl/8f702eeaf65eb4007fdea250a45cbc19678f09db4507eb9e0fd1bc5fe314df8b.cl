
// High-level hash: d9610667c34db21b3b85dc5bdf67a09b199cf48e2fe915ba50f720758fe1c5d7
// Low-level hash: 8f702eeaf65eb4007fdea250a45cbc19678f09db4507eb9e0fd1bc5fe314df8b

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_422" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_421" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float idfloat(float x){
  { return x; }
}
float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__441, global float* v__454){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  local float v__451[(4+(2*v_TP_421)+(2*v_TP_422)+(v_TP_421*v_TP_422))];
  /* Typed Value memory */
  /* Private Memory */
  float v__453_0;
  
  for (int v_wg_id_435 = get_group_id(1); v_wg_id_435 < (4094 / v_TP_421); v_wg_id_435 = (v_wg_id_435 + NUM_GROUPS_1)) {
    for (int v_wg_id_436 = get_group_id(0); v_wg_id_436 < (4094 / v_TP_422); v_wg_id_436 = (v_wg_id_436 + NUM_GROUPS_0)) {
      for (int v_l_id_437 = get_local_id(1); v_l_id_437 < (2 + v_TP_421); v_l_id_437 = (v_l_id_437 + LOCAL_SIZE_1)) {
        for (int v_l_id_438 = get_local_id(0); v_l_id_438 < (2 + v_TP_422); v_l_id_438 = (v_l_id_438 + LOCAL_SIZE_0)) {
          v__451[(v_l_id_438 + (2 * v_l_id_437) + (v_TP_422 * v_l_id_437))] = idfloat(v__441[(v_l_id_438 + (4096 * v_TP_421 * v_wg_id_435) + (4096 * (v_wg_id_436 / (4094 / v_TP_422))) + (4096 * (((4094 / v_TP_422) * (v_l_id_437 % (2 + v_TP_421))) / (4094 / v_TP_422))) + (v_TP_422 * ((v_wg_id_436 + ((4094 / v_TP_422) * (v_l_id_437 % (2 + v_TP_421)))) % (4094 / v_TP_422))))]); 
        }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      
      for (int v_l_id_439 = get_local_id(1); v_l_id_439 < v_TP_421; v_l_id_439 = (v_l_id_439 + LOCAL_SIZE_1)) {
        for (int v_l_id_440 = get_local_id(0); v_l_id_440 < v_TP_422; v_l_id_440 = (v_l_id_440 + LOCAL_SIZE_0)) {
          v__453_0 = jacobi(v__451[(1 + (v_l_id_440 % v_TP_422) + (2 * v_l_id_439) + (v_TP_422 * v_l_id_439))], v__451[(5 + v_l_id_440 + (2 * v_TP_422) + (2 * v_l_id_439) + (v_TP_422 * v_l_id_439))], v__451[(2 + v_TP_422 + ((v_TP_422 + v_l_id_440) % v_TP_422) + (2 * v_l_id_439) + (v_TP_422 * v_l_id_439))], v__451[(4 + v_TP_422 + ((v_TP_422 + v_l_id_440) % v_TP_422) + (2 * v_l_id_439) + (v_TP_422 * v_l_id_439))], v__451[(3 + v_TP_422 + ((v_TP_422 + v_l_id_440) % v_TP_422) + (2 * v_l_id_439) + (v_TP_422 * v_l_id_439))]); 
          v__454[(4097 + v_l_id_440 + (-4096 * ((v_l_id_440 + (v_TP_422 * ((v_wg_id_436 + ((4094 / v_TP_422) * ((v_l_id_439 + (v_TP_421 * v_wg_id_436)) % v_TP_421))) % (4094 / v_TP_422)))) / 4095)) + (4096 * v_TP_421 * v_wg_id_435) + (-16777216 * (((((4094 / v_TP_422) * ((v_l_id_439 + (v_TP_421 * v_wg_id_436)) % v_TP_421)) / (4094 / v_TP_422)) + (v_wg_id_436 / (4094 / v_TP_422)) + (v_TP_421 * v_wg_id_435)) / 4095)) + (4096 * (((4094 / v_TP_422) * ((v_l_id_439 + (v_TP_421 * v_wg_id_436)) % v_TP_421)) / (4094 / v_TP_422))) + (4096 * (v_wg_id_436 / (4094 / v_TP_422))) + (v_TP_422 * ((v_wg_id_436 + ((4094 / v_TP_422) * ((v_l_id_439 + (v_TP_421 * v_wg_id_436)) % v_TP_421))) % (4094 / v_TP_422))))] = id(v__453_0); 
        }
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
      
    }
  }
}}


