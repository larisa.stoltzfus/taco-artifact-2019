
// High-level hash: 56c60216968d65daf19c2e65b478557715a8db4825662df35b449853a5bed9af
// Low-level hash: 7775ab189281604133d2bdd7a44e1f3915e9a4fb40777a858a09b33d5fbf8d6b

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"


#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float top, float bottom, float left, float right, float center){
  return (5 * top + 12 * left + 15 * center + 5 * bottom + 12 * right) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__22, global float* v__29){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__25; 
  for (int v_gl_id_19 = get_global_id(0); v_gl_id_19 < 4094; v_gl_id_19 = (v_gl_id_19 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_20 = get_global_id(1); v_gl_id_20 < 4094; v_gl_id_20 = (v_gl_id_20 + GLOBAL_SIZE_1)) {
      v__25 = jacobi(v__22[(1 + v_gl_id_20 + (4096 * v_gl_id_19))], v__22[(8193 + v_gl_id_20 + (4096 * v_gl_id_19))], v__22[(4096 + v_gl_id_20 + (4096 * v_gl_id_19))], v__22[(4098 + v_gl_id_20 + (4096 * v_gl_id_19))], v__22[(4097 + v_gl_id_20 + (4096 * v_gl_id_19))]); 
      v__29[(4097 + v_gl_id_20 + (4096 * v_gl_id_19))] = id(v__25); 
    }
  }
}}


