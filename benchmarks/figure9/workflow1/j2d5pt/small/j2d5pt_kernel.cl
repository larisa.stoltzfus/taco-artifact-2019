__kernel void kernel0(__global float *inref, __global float *outref)
{
    int b0 = get_group_id(0), b1 = get_group_id(1);
    int t0 = get_local_id(0), t1 = get_local_id(1);

    #define ppcg_min(x,y)    min((__typeof__(x + y)) x, (__typeof__(x + y)) y)
    #define ppcg_max(x,y)    max((__typeof__(x + y)) x, (__typeof__(x + y)) y)
    if (32 * b0 + t0 >= 1 && 32 * b0 + t0 <= 4094)
      for (int c3 = ppcg_max(t1, ((t1 + 15) % 16) - 32 * b1 + 1); c3 <= ppcg_min(31, -32 * b1 + 4094); c3 += 16)
        outref[(32 * b0 + t0) * 4096 + (32 * b1 + c3)] = ((((((5 * inref[(32 * b0 + t0 - 1) * 4096 + (32 * b1 + c3)]) + (12 * inref[(32 * b0 + t0) * 4096 + (32 * b1 + c3 - 1)])) + (15 * inref[(32 * b0 + t0) * 4096 + (32 * b1 + c3)])) + (12 * inref[(32 * b0 + t0) * 4096 + (32 * b1 + c3 + 1)])) + (5 * inref[(32 * b0 + t0 + 1) * 4096 + (32 * b1 + c3)])) / 118);
}
