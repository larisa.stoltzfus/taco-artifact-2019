
// High-level hash: 240eaceda3c42cdd13f7e09bcaa2ed2c945d4005470ea7188d3cf3c3e8d75d40
// Low-level hash: c223634dca9e5329fea4a0cc19ea7f3eca94e500a46370ac40158151eb62039a

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_113" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_112" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__160, global float* v__166){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__165_0;
  
  for (int v_gl_id_156 = get_global_id(0); v_gl_id_156 < (4094 / v_TP_112); v_gl_id_156 = (v_gl_id_156 + GLOBAL_SIZE_0)) {
    for (int v_gl_id_157 = get_global_id(1); v_gl_id_157 < (4094 / v_TP_113); v_gl_id_157 = (v_gl_id_157 + GLOBAL_SIZE_1)) {
      /* map_seq */
      for (int v_i_158 = 0; v_i_158 < v_TP_112; v_i_158 = (1 + v_i_158)) {
        /* map_seq */
        for (int v_i_159 = 0; v_i_159 < v_TP_113; v_i_159 = (1 + v_i_159)) {
          v__165_0 = jacobi(v__160[((v_i_159 % v_TP_113) + (4096 * v_TP_112 * v_gl_id_156) + (4096 * (((4094 / v_TP_113) * (v_i_158 % (2 + v_TP_112))) / (4094 / v_TP_113))) + (4096 * (v_gl_id_157 / (4094 / v_TP_113))) + (v_TP_113 * ((v_gl_id_157 + ((4094 / v_TP_113) * (v_i_158 % (2 + v_TP_112)))) % (4094 / v_TP_113))))], v__160[(1 + (v_i_159 % v_TP_113) + (4096 * v_TP_112 * v_gl_id_156) + (4096 * (((4094 / v_TP_113) * (v_i_158 % (2 + v_TP_112))) / (4094 / v_TP_113))) + (4096 * (v_gl_id_157 / (4094 / v_TP_113))) + (v_TP_113 * ((v_gl_id_157 + ((4094 / v_TP_113) * (v_i_158 % (2 + v_TP_112)))) % (4094 / v_TP_113))))], v__160[(2 + (v_i_159 % v_TP_113) + (4096 * v_TP_112 * v_gl_id_156) + (4096 * (((4094 / v_TP_113) * (v_i_158 % (2 + v_TP_112))) / (4094 / v_TP_113))) + (4096 * (v_gl_id_157 / (4094 / v_TP_113))) + (v_TP_113 * ((v_gl_id_157 + ((4094 / v_TP_113) * (v_i_158 % (2 + v_TP_112)))) % (4094 / v_TP_113))))], v__160[(((v_TP_113 + v_i_159) % v_TP_113) + (4096 * v_TP_112 * v_gl_id_156) + (4096 * (v_gl_id_157 / (4094 / v_TP_113))) + (4096 * (((4094 / v_TP_113) * ((1 + v_i_158) % (2 + v_TP_112))) / (4094 / v_TP_113))) + (v_TP_113 * ((v_gl_id_157 + ((4094 / v_TP_113) * ((1 + v_i_158) % (2 + v_TP_112)))) % (4094 / v_TP_113))))], v__160[(1 + ((v_TP_113 + v_i_159) % v_TP_113) + (4096 * v_TP_112 * v_gl_id_156) + (4096 * (v_gl_id_157 / (4094 / v_TP_113))) + (4096 * (((4094 / v_TP_113) * ((1 + v_i_158) % (2 + v_TP_112))) / (4094 / v_TP_113))) + (v_TP_113 * ((v_gl_id_157 + ((4094 / v_TP_113) * ((1 + v_i_158) % (2 + v_TP_112)))) % (4094 / v_TP_113))))], v__160[(2 + ((v_TP_113 + v_i_159) % v_TP_113) + (4096 * v_TP_112 * v_gl_id_156) + (4096 * (v_gl_id_157 / (4094 / v_TP_113))) + (4096 * (((4094 / v_TP_113) * ((1 + v_i_158) % (2 + v_TP_112))) / (4094 / v_TP_113))) + (v_TP_113 * ((v_gl_id_157 + ((4094 / v_TP_113) * ((1 + v_i_158) % (2 + v_TP_112)))) % (4094 / v_TP_113))))], v__160[(((v_i_159 + (2 * v_TP_113)) % v_TP_113) + (4096 * v_TP_112 * v_gl_id_156) + (4096 * (v_gl_id_157 / (4094 / v_TP_113))) + (4096 * (((4094 / v_TP_113) * ((2 + v_i_158) % (2 + v_TP_112))) / (4094 / v_TP_113))) + (v_TP_113 * ((v_gl_id_157 + ((4094 / v_TP_113) * ((2 + v_i_158) % (2 + v_TP_112)))) % (4094 / v_TP_113))))], v__160[(1 + ((v_i_159 + (2 * v_TP_113)) % v_TP_113) + (4096 * v_TP_112 * v_gl_id_156) + (4096 * (v_gl_id_157 / (4094 / v_TP_113))) + (4096 * (((4094 / v_TP_113) * ((2 + v_i_158) % (2 + v_TP_112))) / (4094 / v_TP_113))) + (v_TP_113 * ((v_gl_id_157 + ((4094 / v_TP_113) * ((2 + v_i_158) % (2 + v_TP_112)))) % (4094 / v_TP_113))))], v__160[(2 + ((v_i_159 + (2 * v_TP_113)) % v_TP_113) + (4096 * v_TP_112 * v_gl_id_156) + (4096 * (v_gl_id_157 / (4094 / v_TP_113))) + (4096 * (((4094 / v_TP_113) * ((2 + v_i_158) % (2 + v_TP_112))) / (4094 / v_TP_113))) + (v_TP_113 * ((v_gl_id_157 + ((4094 / v_TP_113) * ((2 + v_i_158) % (2 + v_TP_112)))) % (4094 / v_TP_113))))]); 
          v__166[(4097 + v_i_159 + (4096 * v_TP_112 * v_gl_id_156) + (-16777216 * (((v_gl_id_157 / (4094 / v_TP_113)) + (((4094 / v_TP_113) * ((v_i_158 + (v_TP_112 * v_gl_id_157)) % v_TP_112)) / (4094 / v_TP_113)) + (v_TP_112 * v_gl_id_156)) / 4095)) + (-4096 * ((v_i_159 + (v_TP_113 * ((v_gl_id_157 + ((4094 / v_TP_113) * ((v_i_158 + (v_TP_112 * v_gl_id_157)) % v_TP_112))) % (4094 / v_TP_113)))) / 4095)) + (4096 * (v_gl_id_157 / (4094 / v_TP_113))) + (4096 * (((4094 / v_TP_113) * ((v_i_158 + (v_TP_112 * v_gl_id_157)) % v_TP_112)) / (4094 / v_TP_113))) + (v_TP_113 * ((v_gl_id_157 + ((4094 / v_TP_113) * ((v_i_158 + (v_TP_112 * v_gl_id_157)) % v_TP_112))) % (4094 / v_TP_113))))] = id(v__165_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


