
// High-level hash: 240eaceda3c42cdd13f7e09bcaa2ed2c945d4005470ea7188d3cf3c3e8d75d40
// Low-level hash: 68d105507624fdc148ea9e35ef116bf024a6d0695052d39799ffbe4fd4f5f9b7

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(1350)"

#atf::tp name "v_TP_954" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"
#atf::tp name "v_TP_953" \
 type "int" \
 range "atf::interval<int>(1,4094)" \
 constraint "atf::divides(4094)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,4096)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "ARM" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::input "atf::buffer<float>(16777216)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__969, global float* v__972){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__971_0;
  
  for (int v_gl_id_965 = get_global_id(1); v_gl_id_965 < (4094 / v_TP_953); v_gl_id_965 = (v_gl_id_965 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_966 = get_global_id(0); v_gl_id_966 < (4094 / v_TP_954); v_gl_id_966 = (v_gl_id_966 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_967 = 0; v_i_967 < v_TP_953; v_i_967 = (1 + v_i_967)) {
        /* map_seq */
        for (int v_i_968 = 0; v_i_968 < v_TP_954; v_i_968 = (1 + v_i_968)) {
          v__971_0 = jacobi(v__969[((v_i_968 % v_TP_954) + (4096 * v_TP_953 * v_gl_id_965) + (4096 * (((4094 / v_TP_954) * (v_i_967 % (2 + v_TP_953))) / (4094 / v_TP_954))) + (4096 * (v_gl_id_966 / (4094 / v_TP_954))) + (v_TP_954 * ((v_gl_id_966 + ((4094 / v_TP_954) * (v_i_967 % (2 + v_TP_953)))) % (4094 / v_TP_954))))], v__969[(1 + (v_i_968 % v_TP_954) + (4096 * v_TP_953 * v_gl_id_965) + (4096 * (((4094 / v_TP_954) * (v_i_967 % (2 + v_TP_953))) / (4094 / v_TP_954))) + (4096 * (v_gl_id_966 / (4094 / v_TP_954))) + (v_TP_954 * ((v_gl_id_966 + ((4094 / v_TP_954) * (v_i_967 % (2 + v_TP_953)))) % (4094 / v_TP_954))))], v__969[(2 + (v_i_968 % v_TP_954) + (4096 * v_TP_953 * v_gl_id_965) + (4096 * (((4094 / v_TP_954) * (v_i_967 % (2 + v_TP_953))) / (4094 / v_TP_954))) + (4096 * (v_gl_id_966 / (4094 / v_TP_954))) + (v_TP_954 * ((v_gl_id_966 + ((4094 / v_TP_954) * (v_i_967 % (2 + v_TP_953)))) % (4094 / v_TP_954))))], v__969[(((v_TP_954 + v_i_968) % v_TP_954) + (4096 * v_TP_953 * v_gl_id_965) + (4096 * (v_gl_id_966 / (4094 / v_TP_954))) + (4096 * (((4094 / v_TP_954) * ((1 + v_i_967) % (2 + v_TP_953))) / (4094 / v_TP_954))) + (v_TP_954 * ((v_gl_id_966 + ((4094 / v_TP_954) * ((1 + v_i_967) % (2 + v_TP_953)))) % (4094 / v_TP_954))))], v__969[(1 + ((v_TP_954 + v_i_968) % v_TP_954) + (4096 * v_TP_953 * v_gl_id_965) + (4096 * (v_gl_id_966 / (4094 / v_TP_954))) + (4096 * (((4094 / v_TP_954) * ((1 + v_i_967) % (2 + v_TP_953))) / (4094 / v_TP_954))) + (v_TP_954 * ((v_gl_id_966 + ((4094 / v_TP_954) * ((1 + v_i_967) % (2 + v_TP_953)))) % (4094 / v_TP_954))))], v__969[(2 + ((v_TP_954 + v_i_968) % v_TP_954) + (4096 * v_TP_953 * v_gl_id_965) + (4096 * (v_gl_id_966 / (4094 / v_TP_954))) + (4096 * (((4094 / v_TP_954) * ((1 + v_i_967) % (2 + v_TP_953))) / (4094 / v_TP_954))) + (v_TP_954 * ((v_gl_id_966 + ((4094 / v_TP_954) * ((1 + v_i_967) % (2 + v_TP_953)))) % (4094 / v_TP_954))))], v__969[(((v_i_968 + (2 * v_TP_954)) % v_TP_954) + (4096 * v_TP_953 * v_gl_id_965) + (4096 * (v_gl_id_966 / (4094 / v_TP_954))) + (4096 * (((4094 / v_TP_954) * ((2 + v_i_967) % (2 + v_TP_953))) / (4094 / v_TP_954))) + (v_TP_954 * ((v_gl_id_966 + ((4094 / v_TP_954) * ((2 + v_i_967) % (2 + v_TP_953)))) % (4094 / v_TP_954))))], v__969[(1 + ((v_i_968 + (2 * v_TP_954)) % v_TP_954) + (4096 * v_TP_953 * v_gl_id_965) + (4096 * (v_gl_id_966 / (4094 / v_TP_954))) + (4096 * (((4094 / v_TP_954) * ((2 + v_i_967) % (2 + v_TP_953))) / (4094 / v_TP_954))) + (v_TP_954 * ((v_gl_id_966 + ((4094 / v_TP_954) * ((2 + v_i_967) % (2 + v_TP_953)))) % (4094 / v_TP_954))))], v__969[(2 + ((v_i_968 + (2 * v_TP_954)) % v_TP_954) + (4096 * v_TP_953 * v_gl_id_965) + (4096 * (v_gl_id_966 / (4094 / v_TP_954))) + (4096 * (((4094 / v_TP_954) * ((2 + v_i_967) % (2 + v_TP_953))) / (4094 / v_TP_954))) + (v_TP_954 * ((v_gl_id_966 + ((4094 / v_TP_954) * ((2 + v_i_967) % (2 + v_TP_953)))) % (4094 / v_TP_954))))]); 
          v__972[(4097 + v_i_968 + (4096 * v_TP_953 * v_gl_id_965) + (-16777216 * (((v_gl_id_966 / (4094 / v_TP_954)) + (((4094 / v_TP_954) * ((v_i_967 + (v_TP_953 * v_gl_id_966)) % v_TP_953)) / (4094 / v_TP_954)) + (v_TP_953 * v_gl_id_965)) / 4095)) + (-4096 * ((v_i_968 + (v_TP_954 * ((v_gl_id_966 + ((4094 / v_TP_954) * ((v_i_967 + (v_TP_953 * v_gl_id_966)) % v_TP_953))) % (4094 / v_TP_954)))) / 4095)) + (4096 * (v_gl_id_966 / (4094 / v_TP_954))) + (4096 * (((4094 / v_TP_954) * ((v_i_967 + (v_TP_953 * v_gl_id_966)) % v_TP_953)) / (4094 / v_TP_954))) + (v_TP_954 * ((v_gl_id_966 + ((4094 / v_TP_954) * ((v_i_967 + (v_TP_953 * v_gl_id_966)) % v_TP_953))) % (4094 / v_TP_954))))] = id(v__971_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


