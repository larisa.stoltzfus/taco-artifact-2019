
// High-level hash: c8811d8271478b4ed36deb17467fc194c184f2f00cdfce1fcf12efcacb29b052
// Low-level hash: f19ee4bcc1af1e8dc1e57bf8b19fcf5721cd778c1fd82a4df115569196979b0c

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_47" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_46" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1024)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__96, global float* v__101){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__100_0;
  
  for (int v_wg_id_82 = get_group_id(1); v_wg_id_82 < (8190 / v_TP_46); v_wg_id_82 = (v_wg_id_82 + NUM_GROUPS_1)) {
    for (int v_wg_id_83 = get_group_id(0); v_wg_id_83 < (8190 / v_TP_47); v_wg_id_83 = (v_wg_id_83 + NUM_GROUPS_0)) {
      for (int v_l_id_94 = get_local_id(1); v_l_id_94 < v_TP_46; v_l_id_94 = (v_l_id_94 + LOCAL_SIZE_1)) {
        for (int v_l_id_95 = get_local_id(0); v_l_id_95 < v_TP_47; v_l_id_95 = (v_l_id_95 + LOCAL_SIZE_0)) {
          v__100_0 = jacobi(v__96[((v_l_id_95 % v_TP_47) + (8192 * v_TP_46 * v_wg_id_82) + (8192 * (v_wg_id_83 / (8190 / v_TP_47))) + (8192 * (((8190 / v_TP_47) * (v_l_id_94 % (2 + v_TP_46))) / (8190 / v_TP_47))) + (v_TP_47 * ((v_wg_id_83 + ((8190 / v_TP_47) * (v_l_id_94 % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__96[(1 + (v_l_id_95 % v_TP_47) + (8192 * v_TP_46 * v_wg_id_82) + (8192 * (v_wg_id_83 / (8190 / v_TP_47))) + (8192 * (((8190 / v_TP_47) * (v_l_id_94 % (2 + v_TP_46))) / (8190 / v_TP_47))) + (v_TP_47 * ((v_wg_id_83 + ((8190 / v_TP_47) * (v_l_id_94 % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__96[(2 + (v_l_id_95 % v_TP_47) + (8192 * v_TP_46 * v_wg_id_82) + (8192 * (v_wg_id_83 / (8190 / v_TP_47))) + (8192 * (((8190 / v_TP_47) * (v_l_id_94 % (2 + v_TP_46))) / (8190 / v_TP_47))) + (v_TP_47 * ((v_wg_id_83 + ((8190 / v_TP_47) * (v_l_id_94 % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__96[(((v_TP_47 + v_l_id_95) % v_TP_47) + (8192 * v_TP_46 * v_wg_id_82) + (8192 * (((8190 / v_TP_47) * ((1 + v_l_id_94) % (2 + v_TP_46))) / (8190 / v_TP_47))) + (8192 * (v_wg_id_83 / (8190 / v_TP_47))) + (v_TP_47 * ((v_wg_id_83 + ((8190 / v_TP_47) * ((1 + v_l_id_94) % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__96[(1 + ((v_TP_47 + v_l_id_95) % v_TP_47) + (8192 * v_TP_46 * v_wg_id_82) + (8192 * (((8190 / v_TP_47) * ((1 + v_l_id_94) % (2 + v_TP_46))) / (8190 / v_TP_47))) + (8192 * (v_wg_id_83 / (8190 / v_TP_47))) + (v_TP_47 * ((v_wg_id_83 + ((8190 / v_TP_47) * ((1 + v_l_id_94) % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__96[(2 + ((v_TP_47 + v_l_id_95) % v_TP_47) + (8192 * v_TP_46 * v_wg_id_82) + (8192 * (((8190 / v_TP_47) * ((1 + v_l_id_94) % (2 + v_TP_46))) / (8190 / v_TP_47))) + (8192 * (v_wg_id_83 / (8190 / v_TP_47))) + (v_TP_47 * ((v_wg_id_83 + ((8190 / v_TP_47) * ((1 + v_l_id_94) % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__96[(((v_l_id_95 + (2 * v_TP_47)) % v_TP_47) + (8192 * v_TP_46 * v_wg_id_82) + (8192 * (((8190 / v_TP_47) * ((2 + v_l_id_94) % (2 + v_TP_46))) / (8190 / v_TP_47))) + (8192 * (v_wg_id_83 / (8190 / v_TP_47))) + (v_TP_47 * ((v_wg_id_83 + ((8190 / v_TP_47) * ((2 + v_l_id_94) % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__96[(1 + ((v_l_id_95 + (2 * v_TP_47)) % v_TP_47) + (8192 * v_TP_46 * v_wg_id_82) + (8192 * (((8190 / v_TP_47) * ((2 + v_l_id_94) % (2 + v_TP_46))) / (8190 / v_TP_47))) + (8192 * (v_wg_id_83 / (8190 / v_TP_47))) + (v_TP_47 * ((v_wg_id_83 + ((8190 / v_TP_47) * ((2 + v_l_id_94) % (2 + v_TP_46)))) % (8190 / v_TP_47))))], v__96[(2 + ((v_l_id_95 + (2 * v_TP_47)) % v_TP_47) + (8192 * v_TP_46 * v_wg_id_82) + (8192 * (((8190 / v_TP_47) * ((2 + v_l_id_94) % (2 + v_TP_46))) / (8190 / v_TP_47))) + (8192 * (v_wg_id_83 / (8190 / v_TP_47))) + (v_TP_47 * ((v_wg_id_83 + ((8190 / v_TP_47) * ((2 + v_l_id_94) % (2 + v_TP_46)))) % (8190 / v_TP_47))))]); 
          v__101[(8193 + v_l_id_95 + (-8192 * ((v_l_id_95 + (v_TP_47 * ((v_wg_id_83 + ((8190 / v_TP_47) * ((v_l_id_94 + (v_TP_46 * v_wg_id_83)) % v_TP_46))) % (8190 / v_TP_47)))) / 8191)) + (8192 * v_TP_46 * v_wg_id_82) + (-67108864 * (((((8190 / v_TP_47) * ((v_l_id_94 + (v_TP_46 * v_wg_id_83)) % v_TP_46)) / (8190 / v_TP_47)) + (v_wg_id_83 / (8190 / v_TP_47)) + (v_TP_46 * v_wg_id_82)) / 8191)) + (8192 * (((8190 / v_TP_47) * ((v_l_id_94 + (v_TP_46 * v_wg_id_83)) % v_TP_46)) / (8190 / v_TP_47))) + (8192 * (v_wg_id_83 / (8190 / v_TP_47))) + (v_TP_47 * ((v_wg_id_83 + ((8190 / v_TP_47) * ((v_l_id_94 + (v_TP_46 * v_wg_id_83)) % v_TP_46))) % (8190 / v_TP_47))))] = id(v__100_0); 
        }
      }
    }
  }
}}


