
// High-level hash: 00a98ff96921e14b0a740f74ed8a1e924740c8254c099f4a4cdfe88ed7c9badb
// Low-level hash: fc7159b0c6fc7d1c250faddc848796bc1d87e5b588305ce9010f05ffaad239f1

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_42" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_41" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__57, global float* v__60){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__59_0;
  
  for (int v_gl_id_53 = get_global_id(1); v_gl_id_53 < (8190 / v_TP_41); v_gl_id_53 = (v_gl_id_53 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_54 = get_global_id(0); v_gl_id_54 < (8190 / v_TP_42); v_gl_id_54 = (v_gl_id_54 + GLOBAL_SIZE_0)) {
      /* map_seq */
      for (int v_i_55 = 0; v_i_55 < v_TP_41; v_i_55 = (1 + v_i_55)) {
        /* map_seq */
        for (int v_i_56 = 0; v_i_56 < v_TP_42; v_i_56 = (1 + v_i_56)) {
          v__59_0 = jacobi(v__57[((v_i_56 % v_TP_42) + (8192 * v_TP_41 * v_gl_id_53) + (8192 * (((8190 / v_TP_42) * (v_i_55 % (2 + v_TP_41))) / (8190 / v_TP_42))) + (8192 * (v_gl_id_54 / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((8190 / v_TP_42) * (v_i_55 % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__57[(1 + (v_i_56 % v_TP_42) + (8192 * v_TP_41 * v_gl_id_53) + (8192 * (((8190 / v_TP_42) * (v_i_55 % (2 + v_TP_41))) / (8190 / v_TP_42))) + (8192 * (v_gl_id_54 / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((8190 / v_TP_42) * (v_i_55 % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__57[(2 + (v_i_56 % v_TP_42) + (8192 * v_TP_41 * v_gl_id_53) + (8192 * (((8190 / v_TP_42) * (v_i_55 % (2 + v_TP_41))) / (8190 / v_TP_42))) + (8192 * (v_gl_id_54 / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((8190 / v_TP_42) * (v_i_55 % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__57[(((v_TP_42 + v_i_56) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_53) + (8192 * (v_gl_id_54 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((8190 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__57[(1 + ((v_TP_42 + v_i_56) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_53) + (8192 * (v_gl_id_54 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((8190 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__57[(2 + ((v_TP_42 + v_i_56) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_53) + (8192 * (v_gl_id_54 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((8190 / v_TP_42) * ((1 + v_i_55) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__57[(((v_i_56 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_53) + (8192 * (v_gl_id_54 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((2 + v_i_55) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((8190 / v_TP_42) * ((2 + v_i_55) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__57[(1 + ((v_i_56 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_53) + (8192 * (v_gl_id_54 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((2 + v_i_55) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((8190 / v_TP_42) * ((2 + v_i_55) % (2 + v_TP_41)))) % (8190 / v_TP_42))))], v__57[(2 + ((v_i_56 + (2 * v_TP_42)) % v_TP_42) + (8192 * v_TP_41 * v_gl_id_53) + (8192 * (v_gl_id_54 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((2 + v_i_55) % (2 + v_TP_41))) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((8190 / v_TP_42) * ((2 + v_i_55) % (2 + v_TP_41)))) % (8190 / v_TP_42))))]); 
          v__60[(8193 + v_i_56 + (8192 * v_TP_41 * v_gl_id_53) + (-67108864 * (((v_gl_id_54 / (8190 / v_TP_42)) + (((8190 / v_TP_42) * ((v_i_55 + (v_TP_41 * v_gl_id_54)) % v_TP_41)) / (8190 / v_TP_42)) + (v_TP_41 * v_gl_id_53)) / 8191)) + (-8192 * ((v_i_56 + (v_TP_42 * ((v_gl_id_54 + ((8190 / v_TP_42) * ((v_i_55 + (v_TP_41 * v_gl_id_54)) % v_TP_41))) % (8190 / v_TP_42)))) / 8191)) + (8192 * (v_gl_id_54 / (8190 / v_TP_42))) + (8192 * (((8190 / v_TP_42) * ((v_i_55 + (v_TP_41 * v_gl_id_54)) % v_TP_41)) / (8190 / v_TP_42))) + (v_TP_42 * ((v_gl_id_54 + ((8190 / v_TP_42) * ((v_i_55 + (v_TP_41 * v_gl_id_54)) % v_TP_41))) % (8190 / v_TP_42))))] = id(v__59_0); 
        }
        /* end map_seq */
      }
      /* end map_seq */
    }
  }
}}


