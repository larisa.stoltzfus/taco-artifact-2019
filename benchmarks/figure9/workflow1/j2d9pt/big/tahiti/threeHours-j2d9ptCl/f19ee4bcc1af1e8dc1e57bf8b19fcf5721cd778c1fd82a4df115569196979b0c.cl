
// High-level hash: 00a98ff96921e14b0a740f74ed8a1e924740c8254c099f4a4cdfe88ed7c9badb
// Low-level hash: f19ee4bcc1af1e8dc1e57bf8b19fcf5721cd778c1fd82a4df115569196979b0c

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"

#atf::tp name "v_TP_258" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"
#atf::tp name "v_TP_257" \
 type "int" \
 range "atf::interval<int>(1,8190)" \
 constraint "atf::divides(8190)"

#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"

#define NUM_GROUPS_0 (GLOBAL_SIZE_0 / LOCAL_SIZE_0)
#define NUM_GROUPS_1 (GLOBAL_SIZE_1 / LOCAL_SIZE_1)
float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__304, global float* v__307){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__306_0;
  
  for (int v_wg_id_299 = get_group_id(1); v_wg_id_299 < (8190 / v_TP_257); v_wg_id_299 = (v_wg_id_299 + NUM_GROUPS_1)) {
    for (int v_wg_id_300 = get_group_id(0); v_wg_id_300 < (8190 / v_TP_258); v_wg_id_300 = (v_wg_id_300 + NUM_GROUPS_0)) {
      for (int v_l_id_302 = get_local_id(1); v_l_id_302 < v_TP_257; v_l_id_302 = (v_l_id_302 + LOCAL_SIZE_1)) {
        for (int v_l_id_303 = get_local_id(0); v_l_id_303 < v_TP_258; v_l_id_303 = (v_l_id_303 + LOCAL_SIZE_0)) {
          v__306_0 = jacobi(v__304[((v_l_id_303 % v_TP_258) + (8192 * v_TP_257 * v_wg_id_299) + (8192 * (v_wg_id_300 / (8190 / v_TP_258))) + (8192 * (((8190 / v_TP_258) * (v_l_id_302 % (2 + v_TP_257))) / (8190 / v_TP_258))) + (v_TP_258 * ((v_wg_id_300 + ((8190 / v_TP_258) * (v_l_id_302 % (2 + v_TP_257)))) % (8190 / v_TP_258))))], v__304[(1 + (v_l_id_303 % v_TP_258) + (8192 * v_TP_257 * v_wg_id_299) + (8192 * (v_wg_id_300 / (8190 / v_TP_258))) + (8192 * (((8190 / v_TP_258) * (v_l_id_302 % (2 + v_TP_257))) / (8190 / v_TP_258))) + (v_TP_258 * ((v_wg_id_300 + ((8190 / v_TP_258) * (v_l_id_302 % (2 + v_TP_257)))) % (8190 / v_TP_258))))], v__304[(2 + (v_l_id_303 % v_TP_258) + (8192 * v_TP_257 * v_wg_id_299) + (8192 * (v_wg_id_300 / (8190 / v_TP_258))) + (8192 * (((8190 / v_TP_258) * (v_l_id_302 % (2 + v_TP_257))) / (8190 / v_TP_258))) + (v_TP_258 * ((v_wg_id_300 + ((8190 / v_TP_258) * (v_l_id_302 % (2 + v_TP_257)))) % (8190 / v_TP_258))))], v__304[(((v_TP_258 + v_l_id_303) % v_TP_258) + (8192 * v_TP_257 * v_wg_id_299) + (8192 * (((8190 / v_TP_258) * ((1 + v_l_id_302) % (2 + v_TP_257))) / (8190 / v_TP_258))) + (8192 * (v_wg_id_300 / (8190 / v_TP_258))) + (v_TP_258 * ((v_wg_id_300 + ((8190 / v_TP_258) * ((1 + v_l_id_302) % (2 + v_TP_257)))) % (8190 / v_TP_258))))], v__304[(1 + ((v_TP_258 + v_l_id_303) % v_TP_258) + (8192 * v_TP_257 * v_wg_id_299) + (8192 * (((8190 / v_TP_258) * ((1 + v_l_id_302) % (2 + v_TP_257))) / (8190 / v_TP_258))) + (8192 * (v_wg_id_300 / (8190 / v_TP_258))) + (v_TP_258 * ((v_wg_id_300 + ((8190 / v_TP_258) * ((1 + v_l_id_302) % (2 + v_TP_257)))) % (8190 / v_TP_258))))], v__304[(2 + ((v_TP_258 + v_l_id_303) % v_TP_258) + (8192 * v_TP_257 * v_wg_id_299) + (8192 * (((8190 / v_TP_258) * ((1 + v_l_id_302) % (2 + v_TP_257))) / (8190 / v_TP_258))) + (8192 * (v_wg_id_300 / (8190 / v_TP_258))) + (v_TP_258 * ((v_wg_id_300 + ((8190 / v_TP_258) * ((1 + v_l_id_302) % (2 + v_TP_257)))) % (8190 / v_TP_258))))], v__304[(((v_l_id_303 + (2 * v_TP_258)) % v_TP_258) + (8192 * v_TP_257 * v_wg_id_299) + (8192 * (((8190 / v_TP_258) * ((2 + v_l_id_302) % (2 + v_TP_257))) / (8190 / v_TP_258))) + (8192 * (v_wg_id_300 / (8190 / v_TP_258))) + (v_TP_258 * ((v_wg_id_300 + ((8190 / v_TP_258) * ((2 + v_l_id_302) % (2 + v_TP_257)))) % (8190 / v_TP_258))))], v__304[(1 + ((v_l_id_303 + (2 * v_TP_258)) % v_TP_258) + (8192 * v_TP_257 * v_wg_id_299) + (8192 * (((8190 / v_TP_258) * ((2 + v_l_id_302) % (2 + v_TP_257))) / (8190 / v_TP_258))) + (8192 * (v_wg_id_300 / (8190 / v_TP_258))) + (v_TP_258 * ((v_wg_id_300 + ((8190 / v_TP_258) * ((2 + v_l_id_302) % (2 + v_TP_257)))) % (8190 / v_TP_258))))], v__304[(2 + ((v_l_id_303 + (2 * v_TP_258)) % v_TP_258) + (8192 * v_TP_257 * v_wg_id_299) + (8192 * (((8190 / v_TP_258) * ((2 + v_l_id_302) % (2 + v_TP_257))) / (8190 / v_TP_258))) + (8192 * (v_wg_id_300 / (8190 / v_TP_258))) + (v_TP_258 * ((v_wg_id_300 + ((8190 / v_TP_258) * ((2 + v_l_id_302) % (2 + v_TP_257)))) % (8190 / v_TP_258))))]); 
          v__307[(8193 + v_l_id_303 + (-8192 * ((v_l_id_303 + (v_TP_258 * ((v_wg_id_300 + ((8190 / v_TP_258) * ((v_l_id_302 + (v_TP_257 * v_wg_id_300)) % v_TP_257))) % (8190 / v_TP_258)))) / 8191)) + (8192 * v_TP_257 * v_wg_id_299) + (-67108864 * (((((8190 / v_TP_258) * ((v_l_id_302 + (v_TP_257 * v_wg_id_300)) % v_TP_257)) / (8190 / v_TP_258)) + (v_wg_id_300 / (8190 / v_TP_258)) + (v_TP_257 * v_wg_id_299)) / 8191)) + (8192 * (((8190 / v_TP_258) * ((v_l_id_302 + (v_TP_257 * v_wg_id_300)) % v_TP_257)) / (8190 / v_TP_258))) + (8192 * (v_wg_id_300 / (8190 / v_TP_258))) + (v_TP_258 * ((v_wg_id_300 + ((8190 / v_TP_258) * ((v_l_id_302 + (v_TP_257 * v_wg_id_300)) % v_TP_257))) % (8190 / v_TP_258))))] = id(v__306_0); 
        }
      }
    }
  }
}}


