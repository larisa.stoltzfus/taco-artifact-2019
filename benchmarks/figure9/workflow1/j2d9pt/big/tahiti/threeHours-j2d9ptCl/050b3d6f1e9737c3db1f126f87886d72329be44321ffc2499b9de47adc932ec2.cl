
// High-level hash: 8a53f90fe83b38756400e27cc0794a048dc69f054193908c0f5251ae49378687
// Low-level hash: 050b3d6f1e9737c3db1f126f87886d72329be44321ffc2499b9de47adc932ec2

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,500) || atf::cond::duration<::std::chrono::seconds>(2700)"


#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,8192)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::ocl::device_info vendor "Advanced Micro Devices" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::input "atf::buffer<float>(67108864)"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1"


float jacobi(float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE){
  return (7 * NW + 5 * N + 9 * NE + 12 * W + 15 * C + 12 * E + 9 * SW + 5 * S + 7 * SE) / 118;
}
float id(float x){
  { return x; }
}
kernel void KERNEL(const global float* restrict v__14, global float* v__17){ 
#ifndef WORKGROUP_GUARD
#define WORKGROUP_GUARD
#endif
WORKGROUP_GUARD
{
  /* Static local memory */
  /* Typed Value memory */
  /* Private Memory */
  float v__16; 
  for (int v_gl_id_12 = get_global_id(1); v_gl_id_12 < 8190; v_gl_id_12 = (v_gl_id_12 + GLOBAL_SIZE_1)) {
    for (int v_gl_id_13 = get_global_id(0); v_gl_id_13 < 8190; v_gl_id_13 = (v_gl_id_13 + GLOBAL_SIZE_0)) {
      v__16 = jacobi(v__14[(v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(1 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(2 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8192 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8193 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(8194 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16384 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16385 + v_gl_id_13 + (8192 * v_gl_id_12))], v__14[(16386 + v_gl_id_13 + (8192 * v_gl_id_12))]); 
      v__17[(8193 + v_gl_id_13 + (8192 * v_gl_id_12))] = id(v__16); 
    }
  }
}}


