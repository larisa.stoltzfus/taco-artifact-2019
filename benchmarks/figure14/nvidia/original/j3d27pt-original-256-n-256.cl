#atf::var<int> v_M_0 = 256 
#atf::var<int> v_N_1 = 256 
#atf::var<int> v_O_2 = 256 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.2,50) || atf::cond::duration<::std::chrono::seconds>(3600)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(256,256)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(128,128)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(32,32)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(32,32)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(4,4)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float jacobi27(float FNW, float FN, float FNE, float FW, float F, float FE, float FSW, float FS, float FSE, float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE, float BNW, float BN, float BNE, float BW, float B, float BE, float BSW, float BS, float BSE){
    {
        return (0.5 * FNW + 0.7 * FN + 0.9 * FNE +
        1.2 * FW + 1.5 * F + 1.2 * FE +
        0.9 * FSW + 0.7 * FS + 0.5 * FSE +
        0.51 * NW + 0.71 * N + 0.91 * NE +
        1.21 * W + 1.51 * C + 1.21 * E +
        0.91 * SW + 0.71 * S + 0.51 * SE +
        0.52 * BNW + 0.72 * BN + 0.92 * BNE +
        1.22 * BW + 1.52 * B + 1.22 * BE +
        0.92 * BSW + 0.72 * BS + 0.52 * BSE) / 159;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__45, global float* v__48){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__47; 
        for (int v_gl_id_42 = get_global_id(2); (v_gl_id_42 < 254); v_gl_id_42 = (v_gl_id_42 + get_global_size(2))){
            for (int v_gl_id_43 = get_global_id(1); (v_gl_id_43 < 254); v_gl_id_43 = (v_gl_id_43 + get_global_size(1))){
                for (int v_gl_id_44 = get_global_id(0); (v_gl_id_44 < 254); v_gl_id_44 = (v_gl_id_44 + get_global_size(0))){
                    v__47 = jacobi27(v__45[(v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(1 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(2 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(256 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(257 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(258 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(512 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(513 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(514 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(65536 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(65537 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(65538 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(65792 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(65793 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(65794 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(66048 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(66049 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(66050 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(131072 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(131073 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(131074 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(131328 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(131329 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(131330 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(131584 + v_gl_id_44 + (256 * v_gl_id_43) + (65536 * v_gl_id_42))], v__45[(131585 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))], v__45[(131586 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))]); 
                    v__48[(65793 + v_gl_id_44 + (65536 * v_gl_id_42) + (256 * v_gl_id_43))] = id(v__47); 
                }
            }
        }
    }
}