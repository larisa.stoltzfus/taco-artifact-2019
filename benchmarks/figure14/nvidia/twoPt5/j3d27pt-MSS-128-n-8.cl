#atf::var<int> v_M_0 = 128 
#atf::var<int> v_N_1 = 128 
#atf::var<int> v_O_2 = 8 

#atf::search_technique "atf::open_tuner"
#atf::abort_condition "atf::cond::speedup(1.0,300) || atf::cond::duration<::std::chrono::seconds>(10800)"

#atf::tp name "GLOBAL_SIZE_2" type "int" range "atf::interval<int>(1,1)" constraint "[&](auto GLOBAL_SIZE_2){ return (GLOBAL_SIZE_2 & (GLOBAL_SIZE_2 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_1" type "int" range "atf::interval<int>(1,v_N_1)" constraint "[&](auto GLOBAL_SIZE_1){ return (GLOBAL_SIZE_1 & (GLOBAL_SIZE_1 - 1)) == 0; }"
#atf::tp name "GLOBAL_SIZE_0" type "int" range "atf::interval<int>(1,v_M_0)" constraint "[&](auto GLOBAL_SIZE_0){ return (GLOBAL_SIZE_0 & (GLOBAL_SIZE_0 - 1)) == 0; }"
#atf::tp name "LOCAL_SIZE_0" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_0)"
#atf::tp name "LOCAL_SIZE_1" \
 type "int" \
 range "atf::interval<int>(1,256)" \
 constraint "atf::divides(GLOBAL_SIZE_1)"
#atf::tp name "LOCAL_SIZE_2" \
 type "int" \
 range "atf::interval<int>(1,1)" \
 constraint "atf::divides(GLOBAL_SIZE_2)"
#atf::ocl::device_info vendor "NVIDIA" type "GPU" id 0

#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::input "atf::buffer<float>((v_M_0*v_N_1*v_O_2))"
#atf::ocl::kernel_name "KERNEL"
#atf::ocl::ls "LOCAL_SIZE_0, LOCAL_SIZE_1, LOCAL_SIZE_2"
#atf::ocl::gs "GLOBAL_SIZE_0, GLOBAL_SIZE_1, GLOBAL_SIZE_2"

float jacobi27(float FNW, float FN, float FNE, float FW, float F, float FE, float FSW, float FS, float FSE, float NW, float N, float NE, float W, float C, float E, float SW, float S, float SE, float BNW, float BN, float BNE, float BW, float B, float BE, float BSW, float BS, float BSE){
    {
        return (0.5 * FNW + 0.7 * FN + 0.9 * FNE +
        1.2 * FW + 1.5 * F + 1.2 * FE +
        0.9 * FSW + 0.7 * FS + 0.5 * FSE +
        0.51 * NW + 0.71 * N + 0.91 * NE +
        1.21 * W + 1.51 * C + 1.21 * E +
        0.91 * SW + 0.71 * S + 0.51 * SE +
        0.52 * BNW + 0.72 * BN + 0.92 * BNE +
        1.22 * BW + 1.52 * B + 1.22 * BE +
        0.92 * BSW + 0.72 * BS + 0.52 * BSE) / 159;; 
    }
}
float id(float x){
    {
        { return x; }; 
    }
}
kernel void KERNEL(const global float* restrict v__70, global float* v__73){
    #ifndef WORKGROUP_GUARD
    #define WORKGROUP_GUARD
    #endif
    WORKGROUP_GUARD
    {
        // Static local memory
        // Typed Value memory
        // Private Memory
        float v__72; 
        for (int v_gl_id_67 = get_global_id(0); (v_gl_id_67 < 126); v_gl_id_67 = (v_gl_id_67 + get_global_size(0))){
            for (int v_gl_id_68 = get_global_id(1); (v_gl_id_68 < 126); v_gl_id_68 = (v_gl_id_68 + get_global_size(1))){
                // slideSeq_plus
                float v_window_1_95; 
                float v_window_2_96; 
                float v_window_3_97; 
                float v_window_4_98; 
                float v_window_5_99; 
                float v_window_6_100; 
                float v_window_7_101; 
                float v_window_8_102; 
                float v_window_9_103; 
                float v_window_10_104; 
                float v_window_11_105; 
                float v_window_12_106; 
                float v_window_13_107; 
                float v_window_14_108; 
                float v_window_15_109; 
                float v_window_16_110; 
                float v_window_17_111; 
                float v_window_18_112; 
                float v_window_19_113; 
                float v_window_20_114; 
                float v_window_21_115; 
                float v_window_22_116; 
                float v_window_23_117; 
                float v_window_24_118; 
                float v_window_25_119; 
                float v_window_26_120; 
                float v_window_27_121; 
                v_window_1_95 = v__70[(v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_2_96 = v__70[(16384 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_4_98 = v__70[(128 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_5_99 = v__70[(16512 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_7_101 = v__70[(256 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_8_102 = v__70[(16640 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_10_104 = v__70[(1 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_11_105 = v__70[(16385 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_13_107 = v__70[(129 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_14_108 = v__70[(16513 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_16_110 = v__70[(257 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_17_111 = v__70[(16641 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_19_113 = v__70[(2 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_20_114 = v__70[(16386 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_22_116 = v__70[(130 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_23_117 = v__70[(16514 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_25_119 = v__70[(258 + v_gl_id_67 + (128 * v_gl_id_68))]; 
                v_window_26_120 = v__70[(16642 + v_gl_id_67 + (128 * v_gl_id_68))]; 
#pragma unroll 1
                for (int v_i_69 = 0; (v_i_69 < 6); v_i_69 = (1 + v_i_69)){
                    v_window_3_97 = v__70[(32768 + v_gl_id_67 + (128 * v_gl_id_68) + (16384 * v_i_69))]; 
                    v_window_6_100 = v__70[(32896 + v_gl_id_67 + (128 * v_gl_id_68) + (16384 * v_i_69))]; 
                    v_window_9_103 = v__70[(33024 + v_gl_id_67 + (128 * v_gl_id_68) + (16384 * v_i_69))]; 
                    v_window_12_106 = v__70[(32769 + v_gl_id_67 + (128 * v_gl_id_68) + (16384 * v_i_69))]; 
                    v_window_15_109 = v__70[(32897 + v_gl_id_67 + (128 * v_gl_id_68) + (16384 * v_i_69))]; 
                    v_window_18_112 = v__70[(33025 + v_gl_id_67 + (128 * v_gl_id_68) + (16384 * v_i_69))]; 
                    v_window_21_115 = v__70[(32770 + v_gl_id_67 + (128 * v_gl_id_68) + (16384 * v_i_69))]; 
                    v_window_24_118 = v__70[(32898 + v_gl_id_67 + (128 * v_gl_id_68) + (16384 * v_i_69))]; 
                    v_window_27_121 = v__70[(33026 + v_gl_id_67 + (128 * v_gl_id_68) + (16384 * v_i_69))]; 
                    v__72 = jacobi27(v_window_1_95, v_window_2_96, v_window_3_97, v_window_4_98, v_window_5_99, v_window_6_100, v_window_7_101, v_window_8_102, v_window_9_103, v_window_10_104, v_window_11_105, v_window_12_106, v_window_13_107, v_window_14_108, v_window_15_109, v_window_16_110, v_window_17_111, v_window_18_112, v_window_19_113, v_window_20_114, v_window_21_115, v_window_22_116, v_window_23_117, v_window_24_118, v_window_25_119, v_window_26_120, v_window_27_121); 
                    v__73[(16513 + v_gl_id_67 + (128 * v_gl_id_68) + (16384 * v_i_69))] = id(v__72); 
                    v_window_1_95 = v_window_2_96; 
                    v_window_2_96 = v_window_3_97; 
                    v_window_4_98 = v_window_5_99; 
                    v_window_5_99 = v_window_6_100; 
                    v_window_7_101 = v_window_8_102; 
                    v_window_8_102 = v_window_9_103; 
                    v_window_10_104 = v_window_11_105; 
                    v_window_11_105 = v_window_12_106; 
                    v_window_13_107 = v_window_14_108; 
                    v_window_14_108 = v_window_15_109; 
                    v_window_16_110 = v_window_17_111; 
                    v_window_17_111 = v_window_18_112; 
                    v_window_19_113 = v_window_20_114; 
                    v_window_20_114 = v_window_21_115; 
                    v_window_22_116 = v_window_23_117; 
                    v_window_23_117 = v_window_24_118; 
                    v_window_25_119 = v_window_26_120; 
                    v_window_26_120 = v_window_27_121; 
                }
                // end slideSeq_plus
            }
        }
    }
}
