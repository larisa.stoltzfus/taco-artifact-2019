// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

#include <detail/platform.hpp>

#ifdef LIBATF_IS_POSIX

#include <cstdio>
#include <exception>
#include <stdexcept>

#include <cstdlib>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <detail/process.hpp>

namespace atf {
namespace detail {
process::process(const ::std::string &p_path, const ::std::string &p_args)
    : process(defer_exec, p_path, p_args) {
  run();
}

process::process(defer_exec_t, const ::std::string &p_path,
                 const ::std::string &p_args)
    : m_Path{p_path}, m_Args{p_args} {}

process::~process() {
  if (running())
    ::std::terminate();
}

auto process::handle() const -> handle_type { return m_Handle; }

auto process::wait() -> bool {
  if (!running())
    throw ::std::runtime_error("process::wait: Process is not running!");

  // Wait for child process to terminate
  int t_status;
  if (waitpid(static_cast<pid_t>(m_Handle), &t_status, 0) == -1)
    throw ::std::runtime_error("process::wait: Failed to waitpid()!");

  // Clear handle
  m_Handle = handle_type{};

  return WEXITSTATUS(t_status) == EXIT_SUCCESS;
}

auto process::detach() -> void {
  // Clear handle
  m_Handle = handle_type{};
}

auto process::running() const -> bool {
  // This check is enough, since a running process will never
  // have a clear handle (since detached processes dont count as running for us)
  // besides the situation where we didn't wait for running process.
  // But for this case, we already require wait() to be called, so its fine.
  return (m_Handle != handle_type{});
}

auto process::run() -> void {
  // Throw if we are already in running state.
  if (running())
    throw ::std::runtime_error("process::run: Already running!");

  // Fork off new child process
  const auto t_pid = fork();

  if (t_pid == 0) {
    // We are the child. Execute program. This will not return when
    // successful.
    char const *const t_args[] = {m_Args.c_str(), nullptr};

    if (execv(m_Path.c_str(), const_cast<char *const *>(t_args)) == -1) {
      perror("execl error");
      throw ::std::runtime_error("process::run: Failed to execl()!");
    }
  } else if (t_pid < 0) {
    // Something failed.
    throw ::std::runtime_error("process::run: Failed to fork()!");
  }

  // We are the parent.
  m_Handle = static_cast<handle_type>(t_pid);
  return;
}
} // namespace detail
} // namespace atf

#endif
