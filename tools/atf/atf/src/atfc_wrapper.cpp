// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

#include <atfc_wrapper.hpp>
#include <cctype>
#include <chrono>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iterator>
#include <limits>
#include <regex>
#include <sstream>
#include <stdexcept>
#include <string>
#include <thirdparty/json.hpp>

using namespace nlohmann;

namespace atf {
namespace cf {
void atfc_callable::split_rope(const configuration &p_conf) {
  using size_type = typename detail::rope::size_type;
  using ignore_block = ::std::pair<size_type, size_type>;

  // Contains ignore intervals
  ::std::vector<ignore_block> t_ignoreIntervals;

  // Remove all atf statements from the source code
  ::std::string t_src;
  t_src.reserve(m_Source.length());

  ::std::istringstream t_ss(m_Source);

  size_type t_pos = 0;
  // Are we in an ignore block?
  bool t_isIgnore = false;
  // Beginning of current ignore block
  size_type t_ignoreStart = {};
  // Length of begin line
  size_type t_lenBegin = {};
  // Are we in a directive spanning multiple lines?
  bool t_isMultiLineDirective = false;

  // TODO utf8 decoding error!
  for (::std::string t_line; ::std::getline(t_ss, t_line);) {
    if (t_isMultiLineDirective) {
      // We are still in a directive spanning multiple lines.
      // We need to check if we still need to throw away lines.
      // We need to stop if there is no \ at the end of the line anymore.

      if (*(::std::prev(t_line.end(), 2)) == '\\')
        continue;
      else {
        // There was no \ at the end of the line anymore.
        // Throw-away this line and continue as normal.
        t_isMultiLineDirective = false;
        continue;
      }
    }

    if (t_line.find("#atf::ignore_begin") == 0) {
      if (t_isIgnore)
        throw ::std::runtime_error("Nested ignore blocks are not allowed!");
      else {
        t_isIgnore = true;
        t_ignoreStart = t_pos;
        t_lenBegin = t_line.length() + 1;
      }
    } else if (t_line.find("#atf::ignore_end") == 0) {
      if (!t_isIgnore)
        throw ::std::runtime_error(
            "Encountered end of ignore block without matching begin block");
      else {
        t_isIgnore = false;
        t_ignoreIntervals.push_back(::std::make_pair(t_ignoreStart, t_pos));
      }
    } else if (t_line.find("#atf::") != 0) {
      t_src.append(t_line);
      t_src.append("\n");
      t_pos += t_line.length() + 1;
    } else {
      // Line began with #atf::. We need to check now if it ends with '\',
      // because we then have to trash lines until they dont end with '\'
      // anymore
      if (*(::std::prev(t_line.end(), 2)) == '\\') {
        t_isMultiLineDirective = true;
      }
    }
  }
  m_Source = t_src;

  m_Rope = detail::rope{m_Source};

  // --

  // A type representing an insertion point
  // Format is: <beginpos, endpos, name>
  // endpos points to one after the tp sequence
  using insertion_point =
      ::std::tuple<size_type, size_type, detail::string_view>;

  // Vector containing all insertion points
  ::std::vector<insertion_point> t_insPoints;

  // A lambda checking if a character is valid in the sense that it is allowed
  // to preceede a TP
  const auto t_checkChar = [](char p_c) -> bool {
    return !(::std::isalnum(p_c)) && (p_c != '_') && (p_c != '-');
  };

  // A lambda checking if a insertion point is valid
  const auto t_checkInsPoint =
      [this, &t_checkChar](const insertion_point &p_point) -> bool {
    const auto t_begin = ::std::get<0>(p_point);
    const auto t_end = ::std::get<1>(p_point);

    // It is fine if it is the first character of the file
    // TODO maybe end is off by one here?
    return ((t_begin == 0) || t_checkChar(m_Source[t_begin - 1])) &&
           ((t_end == m_Source.length()) || t_checkChar(m_Source[t_end]));
  };

  // A lambda to check if an insertion point is inside of an ignore block
  const auto t_isIgnored = [](const insertion_point &p_point,
                              const ignore_block &p_block) -> bool {
    const auto t_begin = ::std::get<0>(p_point);
    const auto t_end = ::std::get<1>(p_point);

    return (t_begin >= p_block.first) && (t_begin <= p_block.second);
  };

  // A lambda to check if a TP is well formed
  const auto t_isValidTp =
      [&t_checkChar](const detail::string_view p_tp) -> bool {
    return !::std::any_of(p_tp.begin(), p_tp.end(), t_checkChar);
  };

  // Iterate through all known tuning parameters
  for (const auto &t_kvp : p_conf) {
    // Extract TP name
    // It's okay to create a view on the hashmap key, since we will copy it
    // when creating the insertion points (they contain a hardcopy, not a view)
    const detail::string_view t_tp = {t_kvp.first};

    // Check if TP is actually valid (only identifier characters)
    if (!t_isValidTp(t_tp)) {
      ::std::stringstream t_ss;
      t_ss << "Tuningparameter \"" << t_tp << "\" is invalid!";

      throw ::std::runtime_error(t_ss.str());
    }

    // Find all occurences
    // for(size_type t_start = 0, size_type t_pos = m_Source.find(t_tp,
    // t_start); t_pos != detail::string_view::npos; t_start += ())
    // View on source string
    const detail::string_view t_source{m_Source};
    for (size_type t_start = 0;;) {
      const auto t_pos = t_source.find(t_tp, t_start);

      if (t_pos == detail::string_view::npos)
        break;

      // We found an occurence. Update start position for next try.
      t_start = t_pos + t_tp.length();

      // Create insertion_point
      const auto t_ip = ::std::make_tuple(t_pos, t_pos + t_tp.length(), t_tp);

      // Check if TP is inside of an ignore block
      if (::std::any_of(t_ignoreIntervals.begin(), t_ignoreIntervals.end(),
                        [&t_isIgnored, &t_ip](const ignore_block &p_block)
                            -> bool { return t_isIgnored(t_ip, p_block); })) {
        continue;
      }

      // Only insert it if its indeed valid
      if (t_checkInsPoint(t_ip)) {
        t_insPoints.push_back(t_ip);
      }
    }
  }

  // Sort insertion point list. There can't be any sequences that are
  // subsequences of others, so sorting by first key is enough.
  const auto t_comp = [](const insertion_point &p_l,
                         const insertion_point &p_r) -> bool {
    return ::std::get<0>(p_l) < ::std::get<0>(p_r);
  };

  ::std::sort(t_insPoints.begin(), t_insPoints.end(), t_comp);

  // Split the rope
  m_Rope.split(m_Rope.begin(), t_insPoints);
}

auto ccfg(const ::std::string &p_sourceFile, const ::std::string &p_runScript,
          bool p_useCostFile, const ::std::string &p_costFile)
    -> atfc_callable {
  return {p_sourceFile, "", p_runScript, p_useCostFile, p_costFile};
}

auto ccfg(const ::std::string &p_sourceFile,
          ::std::pair<::std::string, ::std::string> p_scripts,
          bool p_useCostFile, const ::std::string &p_costFile)
    -> atfc_callable {
  return {p_sourceFile, p_scripts.first, p_scripts.second, p_useCostFile,
          p_costFile};
}

atfc_callable::atfc_callable(const ::std::string &p_srcFile,
                             const ::std::string &p_compileScript,
                             const ::std::string &p_runScript, bool p_useCf,
                             const ::std::string &p_costFile)
    : m_SourceFile{p_srcFile}, m_CompileScript{p_compileScript},
      m_RunScript{p_runScript}, m_UseCostFile{p_useCf}, m_CostFile{p_costFile} {
  // Load source file
  load_source();

  // Initialize rope
  m_Rope = detail::rope{m_Source};
}

atfc_callable::~atfc_callable() {
  if (!m_SourceFile.empty()) {
    // Write back original source file
    ::std::ofstream t_file;
    t_file.open(m_SourceFile, ::std::ofstream::out | ::std::ofstream::trunc);
    t_file << m_OriginalSrc;
    t_file.flush();
  }
}

auto atfc_callable::load_source() -> void {
  // Read whole source file
  // TODO error checking?
  ::std::stringstream t_stream;
  ::std::ifstream t_file{m_SourceFile};
  t_stream << t_file.rdbuf();
  m_Source = t_stream.str();
  m_OriginalSrc = m_Source;
}

auto atfc_callable::operator()(const configuration &p_config) -> ::std::size_t {
  // Check if the rope was already created.
  // If not, we need to do that before we can continue.
  if (!m_SplitRope) {
    split_rope(p_config);
    m_SplitRope = true;
  }

  try {
    if (m_UseCostFile)
      return process_cf(p_config);
    else
      return process_tm(p_config);
  } catch (...) {
    return ::std::numeric_limits<::std::size_t>::max();
  }
}

auto atfc_callable::load_cost() -> ::std::size_t {
  ::std::size_t t_cost{};

  {
    // Open cost file in text mode and try to read one value
    ::std::ifstream t_cf{m_CostFile};
    t_cf >> t_cost;
  }

  // Truncate file just to be sure.
  ::std::ofstream t_cf;
  t_cf.open(m_CostFile, ::std::ofstream::out | ::std::ofstream::trunc);

  return t_cost;
}

auto atfc_callable::launch_runscript() -> void {
  // detail::process t_run(m_RunScript, "");
  // t_run.wait();
  system(m_RunScript.c_str());
}

auto atfc_callable::launch_compilescript() -> void {
  /*detail::process t_run(m_CompileScript, "");
  const auto t_ret = t_run.wait();*/
  const auto t_ret = system(m_CompileScript.c_str());

  if (t_ret != EXIT_SUCCESS)
    throw ::std::runtime_error("");
}

auto atfc_callable::write_source(const configuration &p_cfg) -> void {
  this->write_source(p_cfg, m_SourceFile);
}

auto atfc_callable::write_source(const configuration &p_cfg,
                                 const ::std::string &p_path) -> void {
  // Since this might be called from the outside, we should check our
  // precondition here
  if (!this->m_SplitRope)
    throw ::std::runtime_error("Tried to write_source on unsplit rope!");

  // Open and truncate source file.
  ::std::ofstream t_srcFile;
  t_srcFile.open(p_path, ::std::ofstream::out | ::std::ofstream::trunc);

  // Generate a new version of the code using the rope
  const auto t_src = m_Rope.build_string(p_cfg);

  // Write out new source code
  t_srcFile << t_src;

  t_srcFile.flush();
}

auto atfc_callable::process_cf(const configuration &p_cfg) -> ::std::size_t {
  // First, generate new source code for this run.
  write_source(p_cfg);

  // Invoke compile script and wait for it to finish.
  if (!m_CompileScript.empty())
    launch_compilescript();

  // Invoke run script and wait for it to finish.
  launch_runscript();

  // Retrieve cost value from the file.
  return load_cost();
}

auto atfc_callable::process_tm(const configuration &p_cfg) -> ::std::size_t {
  std::chrono::steady_clock timer; // TODO think angry thoughts at mingw for
                                   // botching high_resolution_clock under
                                   // windows

  write_source(p_cfg);

  if (!m_CompileScript.empty())
    launch_compilescript();

  auto start = timer.now();
  launch_runscript();
  auto end = timer.now();
  return ::std::chrono::duration_cast<::std::chrono::nanoseconds>(end - start)
      .count();
}

auto atfc_callable::process_source() -> void {}
} // namespace cf
} // namespace atf
