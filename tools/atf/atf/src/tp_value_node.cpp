// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

//
//  tp_value_node.cpp
//  new_atf_lib
//
//  Created by Ari Rasch on 15/03/2017.
//  Copyright © 2017 Ari Rasch. All rights reserved.
//

#include <tp_value_node.hpp>

namespace atf {

size_t tp_value_node::__number_tree_nodes = 0;

} // namespace atf
