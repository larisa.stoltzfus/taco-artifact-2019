// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

#ifndef process_h
#define process_h

#include <cstdint>
#include <string>

namespace atf {
namespace detail {
struct defer_exec_t {};

// Process abstraction.
// This class behaves similary to ::std::thread:
// The user will either .wait() on a process, or .detach() from it.
// If an object of this type is destroyed while still having a non-detached
// child process, ::std::terminate is called, even if the process already
// exited. Always call either call wait() or detach(). (Equivalent to
// ::std::thread::join())
class process {
  using handle_type = ::std::intptr_t;

public:
  process(const ::std::string &p_path, const ::std::string &p_args);
  process(defer_exec_t, const ::std::string &p_path,
          const ::std::string &p_args);
  ~process();

public:
  process(const process &) = delete;
  process(process &&) = default;

  process &operator=(const process &) = delete;
  process &operator=(process &&) = default;

public:
  auto run() -> void;
  auto detach() -> void;
  auto wait() -> bool; /* Returns true when application signaled success */
  auto running() const -> bool;
  auto handle() const -> handle_type;

private:
  handle_type m_Handle{};
  ::std::string m_Path;
  ::std::string m_Args;
};
} // namespace detail

constexpr auto defer_exec = detail::defer_exec_t{};
} // namespace atf

#endif
