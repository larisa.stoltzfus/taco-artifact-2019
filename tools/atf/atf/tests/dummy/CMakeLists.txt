###################################################################
# libatf test template
#
# This template automatically registers a test.
###################################################################

# Set minimum required CMAKE version
cmake_minimum_required(VERSION 3.1)

include("${CMAKE_CURRENT_LIST_DIR}/../template.cmake")
