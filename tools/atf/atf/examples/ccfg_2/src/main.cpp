#include <atf.h>

int main() {
  auto N = atf::tp("N", atf::interval<std::size_t>(1, 5));
  auto M = atf::tp("M", atf::interval<std::size_t>(1, 5));

  auto cf =
      atf::cf::ccfg("test.c", ::std::make_pair("./compile.sh", "./run.sh"),
                    true, "costfile.txt");
  // auto cf = atf::cf::ccfg("test.c", {"./compile.sh", "./run.sh"}, true,
  // "costfile.txt");

  auto best = atf::exhaustive()(N, M)(cf);
}
