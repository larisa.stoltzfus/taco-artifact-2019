## ATF library Example: ccfg
##

cmake_minimum_required(VERSION 3.1)
project(ccfg)


## PROJECT SETUP

# Source files
file(GLOB_RECURSE SOURCE_FILES LIST_DIRECTORIES false src/*.cpp)

# Build as executable
add_executable(ccfg ${SOURCE_FILES})

# Require support for at least C++14
set_property(TARGET ccfg PROPERTY CXX_STANDARD 14)
set_property(TARGET ccfg PROPERTY CXX_STANDARD_REQUIRED ON)

# Assets
file(GLOB DATA_FILES assets/*.*)

foreach(DATA_FILE ${DATA_FILES})
	get_filename_component(DATA_FILE_NAME ${DATA_FILE} NAME)
	configure_file(${DATA_FILE} "${CMAKE_CURRENT_BINARY_DIR}/${DATA_FILE_NAME}" COPYONLY)
endforeach()


## DEPENDENCIES

# ATF library
target_link_libraries(ccfg ${LIBATF_LIBRARIES})







