#include <cctype>
// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

#include <iterator>
#include <limits>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include <grouping.hxx>
#include <union_find.hxx>

namespace atfc {

// group tps in given job by name
::std::vector<::std::vector<::std::string>> group(const job &p_job) {
  /*static const constexpr ::std::size_t name_index = 0;
  static const constexpr ::std::size_t constr_index = 3;*/

  using tp_type = job::tp_type;
  using result_type = ::std::vector<::std::vector<::std::string>>;
  using uf_type = union_find<const tp_type *>;
  using bucket_id_type = typename uf_type::bucket_id_type;

  uf_type uf;
  result_type result;

  if (p_job.m_TuningParams.empty()) {
    return result;
  }

  // insert first tp (cannot have dependencies)
  uf.push_back(&(p_job.m_TuningParams[0]));

  ::std::vector<bucket_id_type>
      dependencies; // to keep track of dependencies during iteration

  // iterate over remaining tps, checking dependencies for each of them
  for (auto tp_it = ::std::next(p_job.m_TuningParams.begin());
       tp_it != p_job.m_TuningParams.end(); ++tp_it) {
    dependencies.clear();
    const auto &constraint = tp_it->m_Constraints;
    // check all previous tps for dependency (i.e. name occurs in constraint)
    for (auto prev_it = p_job.m_TuningParams.begin(); prev_it != tp_it;
         ++prev_it) {
      ::std::string::size_type found_index = 0;
      auto cont = true;
      const auto &prev_name = prev_it->m_Name;
      // search until npos or valid occurence
      do {
        found_index = constraint.find(prev_name, found_index);
        if (found_index != uf_type::npos) {
          auto valid = true;
          // check for valid occurence (not part of identifier)
          // need to check both before and after occurence (if present,
          // respectively)
          if (found_index != 0) {
            valid = valid && !(::std::isalnum(constraint[found_index - 1])) &&
                    (constraint[found_index - 1] != '_') &&
                    (constraint[found_index - 1] != '-');
          }
          if (valid && found_index < constraint.size() - prev_name.size()) {
            valid =
                valid &&
                !(::std::isalnum(constraint[found_index + prev_name.size()])) &&
                (constraint[found_index + prev_name.size()] != '_') &&
                (constraint[found_index + prev_name.size()] != '-');
          }
          // valid occurence -> insert dependency
          if (valid) {
            dependencies.push_back(uf.find(&(*prev_it)).second);
            cont = false;
          } else {
            ++found_index; // make sure we don't find the same occurence
                           // repeatedly
          }
        } // found_index != constraint.npos
        else {
          cont = false;
        }
      } while (cont);
    } // end of inner loop
    // merge dependencies
    if (!dependencies.empty()) {
      auto bucket_id = uf.merge(dependencies.begin(), dependencies.end());
      uf.insert(bucket_id, &(*tp_it));
    } else {
      uf.push_back(&(*tp_it));
    }
  } // end of outer loop

  // convert dependencies to result format
  for (auto uf_it = uf.begin(); uf_it != uf.end(); /*deliberately empty*/) {
    // iterate while bucket_id remains the same (same group)
    ::std::vector<::std::string> group;

    for (auto bucket_id = uf_it.bucket_id(); uf_it.bucket_id() == bucket_id;
         ++uf_it) {
      group.push_back((*uf_it)->m_Name);
    }
    result.push_back(::std::move(group));
  }

  return result;
} // end of grouping
} // namespace atfc
