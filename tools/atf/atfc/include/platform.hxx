// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

// Platform support macros

#ifndef platform_h
#define platform_h

#if defined(__linux) || defined(linux) || defined(__linux__) ||                \
    defined(__CYGWIN__) || defined(__unix__) || defined(__APPLE__)
#define ATFC_IS_POSIX
#elif defined(_WIN32) || defined(_WIN64)
#define ATFC_IS_WINDOWS
#else
#error Current platform is not supported by atfc!
#endif

#endif
