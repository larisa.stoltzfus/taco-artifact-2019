// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

// TODO: job_action, cuda_action.. can be generalized by introducing an
// additional template parameter T and using that in the pointer definition.
// This allows us to use it with all types.

#pragma once

#include "../job.hxx"
#include "grammar.hxx"
#include "parser_state.hxx"
#include <iostream>
#include <sstream>

namespace atfc {
namespace internal {
bool change_mode_safe(parser_state &p_state, mode p_mode) {
  if (p_state.m_Job.m_Mode != mode::base && p_state.m_Job.m_Mode != p_mode)
    return false;
  else {
    p_state.m_Job.m_Mode = p_mode;
    return true;
  }
}

template <::std::string job::*Ptr> struct job_action {
  static void apply(const pegtl::input &p_in, string_view p_src,
                    parser_state &p_state) {
    p_state.m_Job.*Ptr = p_state.string();
    p_state.clear_string();
  }
};

template <::std::string ocl_data::*Ptr> struct ocl_action {
  static void apply(const pegtl::input &p_in, string_view p_src,
                    parser_state &p_state) {
    p_state.m_Job.m_OclData.*Ptr = p_state.string();
    p_state.clear_string();

    if (!change_mode_safe(p_state, mode::ocl)) {
      post_diagnostic(message_type::error, p_in.source(),
                      {p_in.line(), p_in.column()}, {p_in.column(), 1}, p_src,
                      "can't use both OpenCL and CUDA directives");
      throw ::std::runtime_error("");
    }
  }
};

template <::std::string cuda_data::*Ptr> struct cuda_action {
  static void apply(const pegtl::input &p_in, string_view p_src,
                    parser_state &p_state) {
    p_state.m_Job.m_CudaData.*Ptr = p_state.string();
    p_state.clear_string();
    if (!change_mode_safe(p_state, mode::cuda)) {
      post_diagnostic(message_type::error, p_in.source(),
                      {p_in.line(), p_in.column()}, {p_in.column(), 1}, p_src,
                      "can't use both OpenCL and CUDA directives");
      throw ::std::runtime_error("");
    }
  }
};
} // namespace internal

template <typename T> struct parser_action : public nothing<T> {};

template <> struct parser_action<integral> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    // Parse string as positive integral TODO do this without string stream
    ::std::stringstream t_ss;
    t_ss << p_input.string();
    t_ss >> p_state.m_Num;
  }
};

template <> struct parser_action<ocl_di_id> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    ::std::stringstream t_ss;
    t_ss << p_input.string();
    t_ss >> p_state.m_OclId;
  }
};

template <> struct parser_action<ocl_di_pid> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    ::std::stringstream t_ss;
    t_ss << p_input.string();
    t_ss >> p_state.m_OclPId;
  }
};

template <> struct parser_action<ocl_di_did> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    ::std::stringstream t_ss;
    t_ss << p_input.string();
    t_ss >> p_state.m_OclDId;
  }
};

template <> struct parser_action<cuda_id> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    ::std::stringstream t_ss;
    t_ss << p_input.string();
    t_ss >> p_state.m_CudaId;
  }
};

template <bool Inline> struct parser_action<cuda_di_dcl<Inline>> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    p_state.m_Job.m_CudaData.m_Id = p_state.m_CudaId;
  }
};

template <bool Inline> struct parser_action<ocl_di_by_name<Inline>> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    ocl_device_info t_di{
        false, p_state.m_OclVendor, p_state.m_OclType, p_state.m_OclId, 0, 0};

    p_state.m_Job.m_OclData.m_DeviceInfo = t_di;

    p_state.m_Job.m_Mode = mode::ocl;

    ::std::stringstream t_str;
    t_str << "Found device information! Vendor: " << p_state.m_OclVendor
          << ", Type: " << p_state.m_OclType << ", Id: " << p_state.m_OclId;

    post_diagnostic(message_type::debug, p_input.source(),
                    {p_input.line(), p_input.column()},
                    {p_input.column() + 5, 1}, p_source, {t_str.str()});
  }
};

template <bool Inline> struct parser_action<ocl_di_by_id<Inline>> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    ocl_device_info t_di{true, "", "", 0, p_state.m_OclDId, p_state.m_OclPId};

    p_state.m_Job.m_OclData.m_DeviceInfo = t_di;

    p_state.m_Job.m_Mode = mode::ocl;
  }
};

template <> struct parser_action<ocl_di_vendor> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    p_state.m_OclVendor = p_input.string();
  }
};

template <> struct parser_action<ocl_di_type> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    p_state.m_OclType = p_input.string();
  }
};

template <> struct parser_action<bracket_close> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    p_state.m_BracketPos = p_input.column();
  }
};

template <> struct parser_action<argv_dcl_old> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    // We know that an integral has matched.
    // It is not allowed to be zero though.
    if (p_state.m_Num == 0) {
      post_diagnostic(
          message_type::error, p_input.source(),
          {p_input.line(), p_input.column()}, {p_input.column() + 5, 1},
          p_source,
          "Index 0 is not allowed here, since argv[0] is the program name");

      throw ::std::runtime_error("");
    }

    p_state.m_UseArgV = true;

    post_diagnostic(
        message_type::error, p_input.source(),
        {p_input.line(), p_input.column()},
        {p_input.column(), p_state.m_BracketPos - p_input.column() + 1},
        p_source, "\"argv[N]\" is deprecated, use \"$N\" instead");

    throw ::std::runtime_error("");

    p_state.m_BracketPos = 0;
  }
};

template <> struct parser_action<argv_dcl> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    // We know that an integral has matched.
    // It is not allowed to be zero though.
    if (p_state.m_Num == 0) {
      post_diagnostic(
          message_type::error, p_input.source(),
          {p_input.line(), p_input.column()}, {p_input.column() + 1, 1},
          p_source,
          "Index 0 is not allowed here, since argv[0] is the program name");

      throw ::std::runtime_error("");
    }

    p_state.m_UseArgV = true;
  }
};

template <> struct parser_action<ascii::identifier> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    p_state.m_Name = p_input.string();
  }
};

template <> struct parser_action<kw_var_type> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    p_state.m_Type = p_input.string();
  }
};

template <bool Inline> struct parser_action<ocl_input_dcl<Inline>> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    p_state.m_Job.m_OclData.m_Inputs.push_back(p_state.string());
    p_state.clear_string();
    if (!internal::change_mode_safe(p_state, mode::ocl)) {
      post_diagnostic(message_type::error, p_input.source(),
                      {p_input.line(), p_input.column()}, {p_input.column(), 1},
                      p_source, "can't use both OpenCL and CUDA directives");
      throw ::std::runtime_error("");
    }
  }
};

template <bool Inline> struct parser_action<cuda_input_dcl<Inline>> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    p_state.m_Job.m_CudaData.m_Inputs.push_back(p_state.string());
    p_state.clear_string();
    if (!internal::change_mode_safe(p_state, mode::cuda)) {
      post_diagnostic(message_type::error, p_input.source(),
                      {p_input.line(), p_input.column()}, {p_input.column(), 1},
                      p_source, "can't use both OpenCL and CUDA directives");
      throw ::std::runtime_error("");
    }
  }
};

template <bool Inline> struct parser_action<var_dcl<Inline>> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    var t_var{p_state.m_Name, p_state.m_Type, p_state.string(),
              p_state.m_UseArgV, p_state.m_Num};

    p_state.m_Job.m_Variables.push_back(t_var);

    ::std::stringstream t_str;
    t_str << "Found variable: Name " << t_var.m_Name << ", Type "
          << t_var.m_Type << ", Value: " << t_var.m_Value;

    const auto t_x = t_str.str();

    post_diagnostic(message_type::debug, p_input.source(),
                    {p_input.line(), p_input.column()}, {p_input.column(), 1},
                    p_source, {t_x});

    p_state.clear_string();
    p_state.clear_var_info();
  }
};

template <> struct parser_action<dcl_string> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    const auto t_str = p_input.string();

    if (t_str.empty()) {
      post_diagnostic(message_type::error, p_input.source(),
                      {p_input.line(), p_input.column()}, {p_input.column(), 1},
                      p_source, "Empty string is not allowed here");

      throw ::std::runtime_error("");
    } else
      p_state.set_string(t_str);
  }
};

template <> struct parser_action<dcl_tp_string> {
  static void apply(const pegtl::input &p_input, string_view p_source,
                    parser_state &p_state) {
    const auto t_str = p_input.string();

    if (t_str.empty()) {
      post_diagnostic(message_type::error, p_input.source(),
                      {p_input.line(), p_input.column()}, {p_input.column(), 1},
                      p_source, "Empty string is not allowed here");

      throw ::std::runtime_error("");
    } else
      p_state.m_Strings.push_back(t_str);
  }
};

template <bool B> struct parser_action<source_dcl<B>> {
  static void apply(const pegtl::input &p_in, string_view p_src,
                    parser_state &p_state) {
    p_state.m_Job.m_SourceFiles.push_back(p_state.string());
    p_state.clear_string();
  }
};

template <bool B>
struct parser_action<search_technique_dcl<B>>
    : internal::job_action<&job::m_SearchStrategy> {};

template <bool B>
struct parser_action<abort_condition_dcl<B>>
    : internal::job_action<&job::m_AbortCondition> {};

template <bool B>
struct parser_action<cost_file_dcl<B>>
    : internal::job_action<&job::m_CostFile> {};

template <bool B>
struct parser_action<run_script_dcl<B>>
    : internal::job_action<&job::m_RunScript> {};

template <bool B>
struct parser_action<compile_script_dcl<B>>
    : internal::job_action<&job::m_CompileScript> {};

template <bool B>
struct parser_action<ocl_kernel_name_dcl<B>>
    : internal::ocl_action<&ocl_data::m_KernelName> {};

template <bool B>
struct parser_action<ocl_gs_dcl<B>> : internal::ocl_action<&ocl_data::m_Gs> {};

template <bool B>
struct parser_action<ocl_ls_dcl<B>> : internal::ocl_action<&ocl_data::m_Ls> {};

template <bool B>
struct parser_action<cuda_gd_dcl<B>> : internal::cuda_action<&cuda_data::m_Gd> {
};

template <bool B>
struct parser_action<cuda_bd_dcl<B>> : internal::cuda_action<&cuda_data::m_Bd> {
};

template <bool B>
struct parser_action<cuda_kernel_name_dcl<B>>
    : internal::cuda_action<&cuda_data::m_KernelName> {};

template <bool B> struct parser_action<tp_dcl<B>> {
  static void apply(const pegtl::input &p_in, string_view p_src,
                    parser_state &p_state) {
    auto &t_strs = p_state.m_Strings;

    tp t_tp{t_strs.at(0), t_strs.at(1), t_strs.at(2),
            (t_strs.size() < 4 ? ::std::string{} : t_strs.at(3))};

    p_state.m_Job.m_TuningParams.push_back(t_tp);

    post_diagnostic(message_type::debug, p_in.source(),
                    {p_in.line(), p_in.column()}, {p_in.column(), 1}, p_src,
                    "Found tuning parameter");

    t_strs.clear();
  }
};
} // namespace atfc
