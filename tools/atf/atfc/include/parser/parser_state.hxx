// This file is part of the Auto Tuning Framework (ATF).
// To obtain permission for use, email Ari Rasch at: a.rasch@uni-muenster.de

#pragma once

#include "../job.hxx"
#include "../string_view.hxx"
#include <string>
#include <vector>

namespace atfc {
struct parser_state {
  auto has_string() const -> bool { return !m_CurrentString.empty(); }

  auto string() const -> const ::std::string & { return m_CurrentString; }

  auto clear_var_info() -> void {
    m_Num = 0;
    m_UseArgV = false;
    m_Type = "";
    m_Name = "";
  }

  auto clear_string() -> void { m_CurrentString.clear(); }

  auto set_string(string_view p_view) -> void {
    m_CurrentString = p_view.to_string();
  }

  ::std::string m_CurrentString;
  ::std::vector<::std::string> m_Strings;
  ::std::size_t m_BracketPos{
      0}; //< Column of last closing bracket, used for diagnostics
  job m_Job;

  // Value stuff
  ::std::size_t m_Num{0};
  bool m_UseArgV{false};
  ::std::string m_Type;
  ::std::string m_Name;

  // OCL stuff
  ::std::size_t m_OclPId; //< Platform id
  ::std::size_t m_OclDId; //< Device id
  ::std::size_t m_OclId;
  ::std::string m_OclVendor;
  ::std::string m_OclType;

  // CUDA stuff
  ::std::size_t m_CudaId;
};
} // namespace atfc
